package com.oec.wis.groupcall;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.github.siyamed.shapeimageview.ShapeImageView;
import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.tools.Tools;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import io.github.francoiscampbell.circlelayout.CircleLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GroupCallPreviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupCallPreviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupCallPreviewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout videoFrame;


    ImageView publisherView,sub0,sub1,sub2,sub3,sub4,sub5,sub6,sub7,sub8,sub9,sub10;
    TextView publisherLabel,subLabel0,subLabel1,subLabel2,subLabel3,subLabel4,subLabel5,subLabel6,subLabel7,subLabel8,subLabel9,subLabel10;    // TODO: Rename and change types of parameters
    private String publisherName;
    private String sub0Name,sub1Name,sub2Name,sub3Name;
    Typeface font;
    String senderPhoto,senderName,senderId;

    LinearLayout  sub0BorderView,sub1BorderView,sub2BorderView,sub3BorderView,sub4BorderView,sub5BorderView,sub6BorderView,sub7BorderView,sub8BorderView,sub9BorderView,sub10BorderView;
//    TextView headerTitleTV;


    String  memberLength;
    private OnFragmentInteractionListener mListener;

    public JSONArray memberArray;

    public GroupCallPreviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GroupCallPreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupCallPreviewFragment newInstance(String param1, String param2) {
        GroupCallPreviewFragment fragment = new GroupCallPreviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);



        }
    }

//   initV

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



//        videoFrame = (LinearLayout)rootView.findViewById(R.id.videoFrameView);
//
//        videoFrame.setVisibility(View.VISIBLE);


        View rootView = inflater.inflate(R.layout.fragment_group_call_preview, container, false);

//        initValues(rootView);





// fill in any details dynamically here


// insert into main view


        publisherView = (ImageView)rootView.findViewById(R.id.publisherView);
        sub0 =(ImageView)rootView.findViewById(R.id.sub0);
        sub1 =(ImageView)rootView.findViewById(R.id.sub1);
        sub2 =(ImageView)rootView.findViewById(R.id.sub2);
        sub3 = (ImageView)rootView.findViewById(R.id.sub3);
        sub4 = (ImageView)rootView.findViewById(R.id.sub4);
        sub5 =(ImageView) rootView.findViewById(R.id.sub5);
        sub6 =(ImageView) rootView.findViewById(R.id.sub6);


        sub7 = (ImageView)rootView.findViewById(R.id.sub7);
        sub8 = (ImageView)rootView.findViewById(R.id.sub8);
        sub9 =(ImageView) rootView.findViewById(R.id.sub9);
        sub10 =(ImageView) rootView.findViewById(R.id.sub10);

        publisherLabel = (TextView)rootView.findViewById(R.id.publisherLabel);

        subLabel0 = (TextView)rootView.findViewById(R.id.sub0label);

        subLabel1 = (TextView)rootView.findViewById(R.id.sub1label);

        subLabel2 = (TextView)rootView.findViewById(R.id.sub2label);

        subLabel3 = (TextView)rootView.findViewById(R.id.sub4label);

        subLabel4 = (TextView)rootView.findViewById(R.id.sub4label);
        subLabel5 = (TextView)rootView.findViewById(R.id.sub5label);

        subLabel6=(TextView)rootView.findViewById(R.id.sub6label);
        subLabel7 =(TextView)rootView.findViewById(R.id.sub7label);
        subLabel8 =(TextView)rootView.findViewById(R.id.sub8label);
        subLabel9=(TextView)rootView.findViewById(R.id.sub9label);
        subLabel10=(TextView)rootView.findViewById(R.id.sub10label);
//        headerTitleTV =(TextView)rootView.findViewById(R.id.headerTitleTV);


        sub0BorderView =(LinearLayout)rootView.findViewById(R.id.sub0BorderView);
        sub1BorderView =(LinearLayout)rootView.findViewById(R.id.sub1BorderView);
        sub2BorderView =(LinearLayout)rootView.findViewById(R.id.sub2BorderView);
        sub3BorderView =(LinearLayout)rootView.findViewById(R.id.sub3BorderView);
        sub4BorderView =(LinearLayout)rootView.findViewById(R.id.sub4BorderView);
        sub5BorderView =(LinearLayout)rootView.findViewById(R.id.sub5BorderView);
        sub6BorderView =(LinearLayout)rootView.findViewById(R.id.sub6BorderView);
        sub7BorderView =(LinearLayout)rootView.findViewById(R.id.sub7BorderView);
        sub8BorderView =(LinearLayout)rootView.findViewById(R.id.sub8BorderView);
        sub9BorderView =(LinearLayout)rootView.findViewById(R.id.sub9BorderView);
        sub10BorderView=(LinearLayout)rootView.findViewById(R.id.sub10BorderView);

        font= Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");

        subLabel0.setTypeface(font);
        subLabel1.setTypeface(font);
        subLabel2.setTypeface(font);
        subLabel3.setTypeface(font);
        subLabel4.setTypeface(font);
        subLabel5.setTypeface(font);
        subLabel6.setTypeface(font);
        subLabel7.setTypeface(font);
        subLabel8.setTypeface(font);
        subLabel9.setTypeface(font);
        subLabel10.setTypeface(font);
        publisherLabel.setTypeface(font);
//        headerTitleTV.setTypeface(font);

///        publisherLabel.setText("spart ans");
//
//        subLabe0.setText("chat USER M");
//
//        subLabel1.setText("Chat");
//
//        subLabel2.setText("Timmer");
//
//
//        updateImages();


        senderId = Tools.getData(getActivity(), "idprofile");
         senderName = Tools.getData(getActivity(), "name");
        senderPhoto = Tools.getData(getActivity(), "photo");

        publisherLabel.setText(senderName);
        downloadSenderImage();

        Log.e("sendername",senderName );
        Log.e("senderphoto",senderPhoto);
        Log.e("senderId",senderId);
        initValues(rootView);




        return rootView;
    }

    public void initValues(View rootView){

        Log.d("Group", String.valueOf(getArguments().getStringArray("members_details")));

//        Log.d("Members", String.valueOf(this.jsonArray));

//        publisherLabel.setText("spart ans");
//        updateImages("",publisherView);

        if(getArguments().getString("senderName")!=null){

            String groupName = getArguments().getString("senderName");

            publisherLabel.setText(groupName);
        }




        if(getArguments().getStringArray("members_details")!=null){



            String [] stringArray = getArguments().getStringArray("members_details");



            ArrayList<String>tempArray = new ArrayList<>(Arrays.asList(stringArray));

            try {
                JSONArray jsonArray  =new JSONArray(tempArray.get(0));

//                                    [[{"id":"226","name":"chat","photo":"img_post1469190255.jpg","country":"Afghanistan"},{"id":"227","name":"chat","photo":"","country":"Afghanistan"},{"id":"212","name":"Paule","photo":"current_loc_map_1478016127.png","country":"Afghanistan"},{"id":"225","name":"chat USER M","photo":"img_pub1482331044.png","country":"Antarctica"}]]

                Log.d("json memer Array", String.valueOf(jsonArray));

                Log.d("json member length", String.valueOf(jsonArray.length()));


                memberLength =String.valueOf(jsonArray.length());

                for(int i=0;i<jsonArray.length();i++){

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String name = jsonObject.getString("name");

                    String photo = jsonObject.getString("photo");

                    updateValues(i,name,photo);
//                    updateMemberValues(i,name,photo);
//                    updateMemberLength(i,name,photo);


//                    json memer Array: [{"id":"101","name":"Abfa","photo":"img_user1450170726_566fd966017c1.jpg","country":"France"},{"id":"226","name":"chat","photo":"img_post1469190255.jpg","country":"Afghanistan"},{"id":"125","name":"Gilles LALANNE ","photo":"img_user1457004642.jpg","country":"Guadeloupe"},{"id":"227","name":"chat","photo":"","country":"Afghanistan"},{"id":"225","name":"Chat User","photo":"img_user1491813249.jpg","country":"Angola"}]
//                    04-11 19:50:09.155 19096-19096/com.oec.wis D/json memer length: 5
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
//

    }

    private void updateMemberLength(int i,String name, String photo) {
           Log.e("memberLength" ,memberLength);


         if(memberLength.equals("1")){
             subLabel0.setText(name);
             updateImages(photo,sub0);
             sub0BorderView.setVisibility(View.VISIBLE);
             sub1BorderView.setVisibility(View.GONE);
             sub2BorderView.setVisibility(View.GONE);
             sub3BorderView.setVisibility(View.GONE);
             sub4BorderView.setVisibility(View.GONE);
             sub5BorderView.setVisibility(View.GONE);
             sub6BorderView.setVisibility(View.GONE);
             sub7BorderView.setVisibility(View.GONE);
             sub8BorderView.setVisibility(View.GONE);
             sub9BorderView.setVisibility(View.GONE);
             sub10BorderView.setVisibility(View.GONE);




        }else if(memberLength.equals("2")){

             subLabel1.setText(name);
             updateImages(photo,sub1);

             sub0BorderView.setVisibility(View.GONE);
             sub1BorderView.setVisibility(View.VISIBLE);
             sub2BorderView.setVisibility(View.GONE);
             sub3BorderView.setVisibility(View.GONE);
             sub4BorderView.setVisibility(View.GONE);
             sub5BorderView.setVisibility(View.GONE);
             sub6BorderView.setVisibility(View.GONE);
             sub7BorderView.setVisibility(View.GONE);
             sub8BorderView.setVisibility(View.GONE);
             sub9BorderView.setVisibility(View.GONE);
             sub10BorderView.setVisibility(View.GONE);

         }else if(memberLength.equals("3")){
             subLabel2.setText(name);
             updateImages(photo,sub2);

             sub0BorderView.setVisibility(View.GONE);
             sub1BorderView.setVisibility(View.GONE);
             sub2BorderView.setVisibility(View.VISIBLE);
             sub3BorderView.setVisibility(View.GONE);
             sub4BorderView.setVisibility(View.GONE);
             sub5BorderView.setVisibility(View.GONE);
             sub6BorderView.setVisibility(View.GONE);
             sub7BorderView.setVisibility(View.GONE);
             sub8BorderView.setVisibility(View.GONE);
             sub9BorderView.setVisibility(View.GONE);
             sub10BorderView.setVisibility(View.GONE);
         }else if(memberLength.equals("4")){


             subLabel3.setText(name);
             updateImages(photo,sub3);

             sub0BorderView.setVisibility(View.GONE);
             sub1BorderView.setVisibility(View.GONE);
             sub2BorderView.setVisibility(View.GONE);
             sub3BorderView.setVisibility(View.VISIBLE);
             sub4BorderView.setVisibility(View.GONE);
             sub5BorderView.setVisibility(View.GONE);
             sub6BorderView.setVisibility(View.GONE);
             sub7BorderView.setVisibility(View.GONE);
             sub8BorderView.setVisibility(View.GONE);
             sub9BorderView.setVisibility(View.GONE);
             sub10BorderView.setVisibility(View.GONE);

    }else if(memberLength.equals("5")){


        subLabel4.setText(name);
        updateImages(photo,sub4);

        sub0BorderView.setVisibility(View.GONE);
        sub1BorderView.setVisibility(View.GONE);
        sub2BorderView.setVisibility(View.GONE);
        sub3BorderView.setVisibility(View.GONE);
        sub4BorderView.setVisibility(View.VISIBLE);
        sub5BorderView.setVisibility(View.GONE);
        sub6BorderView.setVisibility(View.GONE);
        sub7BorderView.setVisibility(View.GONE);
        sub8BorderView.setVisibility(View.GONE);
        sub9BorderView.setVisibility(View.GONE);
        sub10BorderView.setVisibility(View.GONE);
      }else if(memberLength .equals("6")){


             subLabel5.setText(name);
             updateImages(photo,sub5);

             sub0BorderView.setVisibility(View.GONE);
             sub1BorderView.setVisibility(View.GONE);
             sub2BorderView.setVisibility(View.GONE);
             sub3BorderView.setVisibility(View.GONE);
             sub4BorderView.setVisibility(View.VISIBLE);
             sub5BorderView.setVisibility(View.GONE);
             sub6BorderView.setVisibility(View.GONE);
             sub7BorderView.setVisibility(View.GONE);
             sub8BorderView.setVisibility(View.GONE);
             sub9BorderView.setVisibility(View.GONE);
             sub10BorderView.setVisibility(View.GONE);

         }




    }

    public void updateMemberValues(int i,String name,String photo){

//
//        subLabe0.setText("chat USER M");
//
//        subLabel1.setText("Chat");
//
//        subLabel2.setText("Timmer");

        Log.e("update member details",name);
//

        switch (i){
            case 0:
                subLabel0.setText(name);
                updateImages(photo,sub0);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.VISIBLE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);
                break;

            case 1:
                subLabel1.setText(name);
                updateImages(photo,sub1);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.VISIBLE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);
                break;

            case 2:
                subLabel2.setText(name);
                updateImages(photo,sub2);
                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.VISIBLE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);

                break;

            case 3:
                subLabel3.setText(name);
                updateImages(photo,sub3);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.VISIBLE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);
                break;

            case 4:
                subLabel4.setText(name);
                updateImages(photo,sub4);



                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.VISIBLE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);

                break;

            case 5:
                subLabel5.setText(name);
                updateImages(photo,sub5);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);

            case 6:
                subLabel6.setText(name);
                updateImages(photo,sub6);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.VISIBLE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);

            case 7:
                subLabel7.setText(name);
                updateImages(photo,sub7);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.VISIBLE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);



            case 8:
                subLabel8.setText(name);
                updateImages(photo,sub8);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.VISIBLE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.GONE);

            case 9:
                subLabel9.setText(name);
                updateImages(photo,sub9);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.VISIBLE);
                sub10BorderView.setVisibility(View.GONE);
            case 10:


                subLabel10.setText(name);
                updateImages(photo,sub10);

                sub0BorderView.setVisibility(View.GONE);
                sub1BorderView.setVisibility(View.GONE);
                sub2BorderView.setVisibility(View.GONE);
                sub3BorderView.setVisibility(View.GONE);
                sub4BorderView.setVisibility(View.GONE);
                sub5BorderView.setVisibility(View.GONE);
                sub6BorderView.setVisibility(View.GONE);
                sub7BorderView.setVisibility(View.GONE);
                sub8BorderView.setVisibility(View.GONE);
                sub9BorderView.setVisibility(View.GONE);
                sub10BorderView.setVisibility(View.VISIBLE);

            case 11:
                break;
        }


    }

    public void updateValues(int i,String name,String photo){

//
//        subLabe0.setText("chat USER M");
//
//        subLabel1.setText("Chat");
//
//        subLabel2.setText("Timmer");

        Log.e("update member details",name);
        if(name.equals(senderName)){
            publisherLabel.setText(senderName);
        }

        else
        {
            Log.e("else","else");
        }
//

        switch (i){
            case 0:
                subLabel0.setText(name);
                updateImages(photo,sub0);
                break;

            case 1:
                subLabel1.setText(name);
                updateImages(photo,sub1);
                break;

            case 2:
                subLabel2.setText(name);
                updateImages(photo,sub2);
                break;

            case 3:
                subLabel3.setText(name);
                updateImages(photo,sub3);
                break;

            case 4:
                subLabel4.setText(name);
                updateImages(photo,sub4);

                break;

            case 5:
                break;
        }
    }



//    public void updateValues(int i,String name,String photo){
//
////
////        subLabe0.setText("chat USER M");
////
////        subLabel1.setText("Chat");
////
////        subLabel2.setText("Timmer");
//
//        Log.e("update member details",name);
////
//
//        switch (i){
//            case 0:
//                publisherLabel.setText(name);
//                updateImages(photo,publisherView);
//                break;
//
//            case 1:
//
//                subLabel0.setText(name);
//                updateImages(photo,sub0);
//                break;
//
//
//            case 2:
//                subLabel1.setText(name);
//                updateImages(photo,sub1);
//                break;
//
//
//            case 3:
//                subLabel2.setText(name);
//                updateImages(photo,sub2);
//                break;
//
//
//            case 4:
//                subLabel3.setText(name);
//                updateImages(photo,sub3);
//                break;
//
//
//            case 5:
//                subLabel4.setText(name);
//                updateImages(photo,sub4);
//
//                break;
//            case 6:
//                break;
//        }
//    }



    public void downloadSenderImage() {

//        Picasso.with(getActivity())
//                .load(this.getString(R.string.server_url2) + Tools.getData(getActivity(), "photo"))
//                .placeholder(R.drawable.empty)
//                .error(R.drawable.empty)
//                .into(publisherView);


        ApplicationController.getInstance().getImageLoader().get(getActivity().getString(R.string.server_url3) + senderPhoto, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                publisherView.setImageResource(R.drawable.profile);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    publisherView.setImageBitmap(response.getBitmap());
                } else {
                    publisherView.setImageResource(R.drawable.profile);
                }
            }
        });


    }




    public void updateImages(String photo, final ImageView imageView){

        ApplicationController.getInstance().getImageLoader().get(getActivity().getString(R.string.server_url3) +photo, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                imageView.setImageResource(R.drawable.profile);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    imageView.setImageBitmap(response.getBitmap());
                } else {
                    imageView.setImageResource(R.drawable.profile);
                }
            }
        });


//        ApplicationController.getInstance().getImageLoader().get(getActivity().getString(R.string.server_url3) +"img_post1469190255.jpg", new ImageLoader.ImageListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                sub1.setImageResource(R.drawable.profile);
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    sub1.setImageBitmap(response.getBitmap());
//                } else {
//                    sub1.setImageResource(R.drawable.profile);
//                }
//            }
//        });
//
//
//        ApplicationController.getInstance().getImageLoader().get(getActivity().getString(R.string.server_url3) +"img_user1484057109.jpg", new ImageLoader.ImageListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                sub2.setImageResource(R.drawable.profile);
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    sub2.setImageBitmap(response.getBitmap());
//                } else {
//                    sub2.setImageResource(R.drawable.profile);
//                }
//            }
//        });
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
