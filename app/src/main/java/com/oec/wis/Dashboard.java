package com.oec.wis;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.adapters.SampleImageListAdapter;
import com.oec.wis.database.DatabaseHandler;
import com.oec.wis.dialogs.MapsActivity;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.fragments.FragActu;
import com.oec.wis.fragments.FragCalendar;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragConditions;
import com.oec.wis.fragments.FragFriends;
import com.oec.wis.fragments.FragMyPub;
import com.oec.wis.fragments.FragNotif;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.fragments.FragProfile;
import com.oec.wis.fragments.FragVideo;
import com.oec.wis.fragments.FragWeather;
import com.oec.wis.tools.LocationReceiver;
import com.oec.wis.tools.Tools;
import com.oec.wis.wismusic.FragMusic;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dashboard extends AppCompatActivity {
    public DrawerLayout mDrawerLayout;
    public int cFrag = 0;
    ListView lvNav;
    Fragment fragment = null;
    TextView tvFName,tvEmail;
    List<WISMenu> menuList;
    ImageView ivProfile;
    SampleImageListAdapter adapter;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_dashboard);

        db = new DatabaseHandler(this);
        loadControls();
        setNavList();
        displayFrag(0);
        setListner();
        loadProfile();
        setLocationListener();


//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.cu_actionbar);





        try {
            if (getIntent().getExtras().getString("invit").equals("invit"))
                displayFrag(3);
        } catch (Exception e) {
        }




    }

    private void loadProfile() {
        try {
            String versionName = Dashboard.this.getPackageManager()
                    .getPackageInfo(Dashboard.this.getPackageName(), 0).versionName;

            Log.e("version name",versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        String appendText ="2.1.3";
        if (Tools.getData(this, "name").replace("null", "").equals(""))
            tvFName.setText(Tools.getData(this, "firstname_prt") + " " + Tools.getData(this, "lastname_prt"));
        else
            tvFName.setText(Tools.getData(this, "name"));

//        tvFName.setText(appendText.concat("\n").concat(tvFName.getText().toString()));
//        String name =tvFName.getText().toString();

        String newstring = tvFName.getText().toString() + " - " + appendText;


        tvFName.setText(newstring);
        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + Tools.getData(this, "photo"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

            @Override


            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivProfile.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    private void loadControls() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        lvNav = (ListView) findViewById(R.id.lvNav);
        tvFName = (TextView) findViewById(R.id.tvFName);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
//       String email= Tools.saveData(this, "email_pr");
        String email=Tools.getData(this, "email_pr");

        tvEmail.setText(email);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/Harmattan-R.ttf");
        tvFName.setTypeface(font);
        tvEmail.setTypeface(font);

        System.out.println("Email" +email);


//        logout=(TextView)findViewById(R.id.logout);
//        Typeface font = Typeface.createFromAsset(getApplication().getAssets(), "fonts/OPENSANS-SEMIBOLD.ttf");
//        logout.setTypeface(font);
    }

    private void setNavList() {

        String[] web = {
                "WISNews",
                "WISProfile",
                "WISFriends",
                "WISChat",
                "WISLocation",
                "WISCalender",
                "WISPub",
                "WISVideo",
                "WISPhoto",
                "WISMusic",
                "WISNotification",
                "WISCondition",
                "WISWeather",
                "Logout"

        } ;

//        Integer[] imageId = {
//                R.drawable.slide_news,
//                R.drawable.slide_profile,
//                R.drawable.slide_friends,
//                R.drawable.slide_chat,
//                R.drawable.slide_location,
//                R.drawable.slide_cal,
//                R.drawable.slide_pub,
//                R.drawable.slide_video,
//                R.drawable.slide_photo,
//                R.drawable.slide_music,
//                R.drawable.slide_notoification,
//                R.drawable.slide_condition,
//                R.drawable.slide_weather,
//                R.drawable.slide_logout
//
//        };


        Integer[] imageId = {
                R.drawable.grey_news,
                R.drawable.grey_profile_icon,
                R.drawable.grey_group_icon,
                R.drawable.grey_chat_icon,
                R.drawable.grey_location_icon,
                R.drawable.gre_cal,
                R.drawable.grey_pub_icon,
                R.drawable.new_video,
                R.drawable.slide_wis_photo,
                R.drawable.grey_music_icon,
                R.drawable.new_notification,
                R.drawable.new_condition,
                R.drawable.grey_weather_icon,
                R.drawable.new_logout

        };
        SampleImageListAdapter adapter = new
                SampleImageListAdapter(this, web, imageId);

        lvNav.setAdapter(adapter);

//        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//
////                Toast.makeText(getApplicationContext(),"listview clickable",Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.d("Clickable call" ,"lvn");
//                cFrag=position;
//                if(cFrag == 0){
//                    lvNav.setBackgroundColor(R.color.red_color);
//                }else if(position==1){
//                    lvNav.setBackgroundColor(R.color.blue_color);
//                }
//            }
//        });

//
//        ArrayAdapter<String> dataAdapter =
//                new ArrayAdapter<>(this, R.layout.row_nav_menu,android.R.id.text1, getResources().getStringArray(R.array.nav_list));
//        lvNav.setAdapter(dataAdapter);
//        ArrayAdapter<String> imageAdapter =
//                new ArrayAdapter<>(this, R.layout.row_nav_menu,R.id.slideImageView, getResources().getStringArray(R.array.nav_drawer_icons));
//        lvNav.setAdapter(imageAdapter);





//        menuList = new ArrayList<>();
//        menuList.add(new WISMenu(getString(R.string.actuality), R.drawable.menu_feed));
//        menuList.add(new WISMenu(getString(R.string.profile), R.drawable.menu_profile));
//        menuList.add(new WISMenu(getString(R.string.friends), R.drawable.menu_friends));
//        menuList.add(new WISMenu(getString(R.string.chat), R.drawable.menu_chat));
//        menuList.add(new WISMenu(getString(R.string.calender), R.drawable.menu_calender));
//        menuList.add(new WISMenu(getString(R.string.local), R.drawable.menu_local));
//        menuList.add(new WISMenu(getString(R.string.pub), R.drawable.menu_pub));
//        menuList.add(new WISMenu(getString(R.string.video), R.drawable.menu_video));
//        menuList.add(new WISMenu(getString(R.string.photo), R.drawable.menu_pics));
//        menuList.add(new WISMenu(getString(R.string.music), R.drawable.menu_music));
//        menuList.add(new WISMenu(getString(R.string.notif), R.drawable.menu_notif));
//        menuList.add(new WISMenu(getString(R.string.condition), R.drawable.info));
////        menuList.add(new WISMenu(getString(R.string.weather), R.drawable.weather_logo));
//
//        lvNav.setAdapter(new NavDrawarAdapter(this, menuList));
    }

    private void displayFrag(int position) {
        cFrag = position;


        Log.e("postion", String.valueOf(position));

        switch (position) {
            case 0:
                fragment = new FragActu();
                Log.e("newS", String.valueOf(position));
                break;
            case 1:
                fragment = new FragProfile();
                break;
            case 2:
                fragment = new FragFriends();
                break;
            case 3:
                fragment = new FragChat();
                UserNotification userNotification = new UserNotification();
                userNotification.setChatSelected(Boolean.FALSE);
                break;

            case 4:
                Intent it=new Intent(this, MapsActivity.class);
                startActivity(it);
                break;
            case 5:
                fragment = new FragCalendar();
                break;
            case 6:
                fragment = new FragMyPub();
                break;
            case 7:
                fragment = new FragVideo();
                break;
            case 8:
                fragment = new FragPhoto();
                break;
            case 9:
                fragment =new FragMusic();
//                Toast.makeText(this, getString(R.string.msg_soon), Toast.LENGTH_LONG).show();
                break;
            case 10:
                fragment = new FragNotif();
                break;
            case 11:
                fragment = new FragConditions();
                break;
            case 12:
//                if (dialogShown) {
                fragment = new FragWeather();
//                } else {
//                    showWeatherAlert();
//                }

                break;
            case 13:
                db.deleteAllRecords();
                requestLogout();
                clearSession();
                finish();
                startActivity(new Intent(getApplicationContext(), Login.class));

            default:
                break;
        }

        if (fragment != null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("Drawer","close drawer");


                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                }
            });



        } else {

        }
    }


    private void setListner() {

        lvNav.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(Dashboard.this, "selected ", Toast.LENGTH_SHORT).show();
//                lvNav.setBackgroundColor(getResources().getColor(R.color.light_blue_color));
                displayFrag(position);

                for (int i = 0; i < lvNav.getChildCount(); i++) {
                    if (position == i) {
                        lvNav.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.light_gray));

                        Log.d("position" , String.valueOf(lvNav.getChildAt(position)));
                        Log.d("position 1" , String.valueOf(position));
                    } else {
                        lvNav.getChildAt(i).setBackgroundColor(Color.WHITE);
                    }

                }
            }
        });


//        changed   =======

//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                requestLogout();
//                clearSession();
//                finish();
//                startActivity(new Intent(getApplicationContext(), Login.class));
//            }
//        });
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cFrag = 5;
                fragment = new FragProfile();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        });
    }

    private void clearSession() {
        Tools.saveData(getApplicationContext(), "firstname_prt", "");
        Tools.saveData(getApplicationContext(), "lastname_prt", "");
        Tools.saveData(getApplicationContext(), "token", "");
        Tools.saveData(getApplicationContext(), "photo", "");
        Tools.saveData(getApplicationContext(), "last_connected", "");
        Tools.saveData(getApplicationContext(), "idprofile", "");
        Tools.saveData(getApplicationContext(), "sexe_prt", "");
        Tools.saveData(getApplicationContext(), "lang_pr", "");
        Tools.saveData(getApplicationContext(), "tel_pr", "");
        Tools.saveData(getApplicationContext(), "profiletype", "");
        Tools.saveData(getApplicationContext(), "code_ape_etr", "");
        Tools.saveData(getApplicationContext(), "email_pr", "");
        Tools.saveData(getApplicationContext(), "special_other", "");
        Tools.saveData(getApplicationContext(), "place_addre", "");
        Tools.saveData(getApplicationContext(), "activite", "");
        Tools.saveData(getApplicationContext(), "registered", "");
        Tools.saveData(getApplicationContext(), "date_birth", "");
        Tools.saveData(getApplicationContext(), "name_representant_etr", "");
    }

    @Override
    public void onBackPressed(){
//        moveTaskToBack(true);

        Log.e("Back Clicked","EXIT");

    }

    private void requestLogout() {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(Dashboard.this, "idprofile"));
            jsonBody.put("id_gcm", Tools.getData(Dashboard.this, "regid"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.deconnexion_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                            }

                        } catch (JSONException e) {

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Dashboard.this, "token"));
                headers.put("lang", Tools.getData(Dashboard.this, "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (cFrag == 0) {
                finish();
            } else {
                displayFrag(0);
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void setLocationListener() {
        Intent intent = new Intent(Dashboard.this, LocationReceiver.class);
        LocationManager manager = (LocationManager) Dashboard.this.getSystemService(Context.LOCATION_SERVICE);
        float minDistance = 500;
        String provider = LocationManager.GPS_PROVIDER;

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);


        PendingIntent launchIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        manager.requestLocationUpdates(provider, 0, minDistance, launchIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_name) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
