package com.oec.wis.tools;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.adapters.UserLocationListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by asareri08 on 05/10/16.
 */
public  class UserLocation extends AsyncTask<String, Void, String> {


    Activity context;

    UserLocationListener listener;

    public UserLocation(Activity context,UserLocationListener listener) {

        this.context = context;

        this.listener = listener;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
   public String doInBackground(String... params) {
        String response;
        try {
            Double lat = Double.valueOf(params[0]);

            Double lang = Double.valueOf(params[1]);
            response = getCurrentLocationViaJSON(lat, lang);

            return response;
        } catch (Exception e) {
            return "error";
        }
    }

    @Override
    public void onPostExecute(String result) {

        Log.d("location response", String.valueOf(result));

             try{
                 listener.updateUserLocationInfo(result.toString());
             }catch (NullPointerException ex){
                 Log.e("NullpointerExce" ,ex.getMessage());
             }




    }


    public static JSONObject getLocationInfo(double lat, double lng) {

//        https://maps.googluieapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY
//        http://maps.google.com/maps/api/geocode/json?address="+address+"&sensor=false
        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=false");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }


    public static String getCurrentLocationViaJSON(double lat, double lng) {

        JSONObject jsonObj = getLocationInfo(lat, lng);
        Log.i("JSON string =>", jsonObj.toString());

        String currentLocation = "testing";
        String street_address = null;
        String postal_code = null;

        try {
            String status = jsonObj.getString("status").toString();
            Log.i("status", status);

            if (status.equalsIgnoreCase("OK")) {
                JSONArray results = jsonObj.getJSONArray("results");
                int i = 0;
                Log.i("i", i + "," + results.length()); //TODO delete this
                do {

                    JSONObject r = results.getJSONObject(i);
                    JSONArray typesArray = r.getJSONArray("types");
                    String types = typesArray.getString(0);

                    if (types.equalsIgnoreCase("street_address")) {
                        street_address = r.getString("formatted_address").split(",")[0];
                        Log.i("street_address", street_address);
                    } else if (types.equalsIgnoreCase("postal_code")) {
                        postal_code = r.getString("formatted_address");
                        Log.i("postal_code", postal_code);
                    }

                    if (street_address != null && postal_code != null) {
                        currentLocation = street_address + "," + postal_code;
                        Log.i("Current Location =>", currentLocation); //Delete this
                        i = results.length();
                    }

                    i++;
                } while (i < results.length());

                Log.i("JSON Geo Locatoin =>", currentLocation);
                return currentLocation;
            }

        } catch (JSONException e) {
            Log.e("testing", "Failed to load JSON");
            e.printStackTrace();
        }
        return null;
    }
}
