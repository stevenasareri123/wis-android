package com.oec.wis.tools;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.oec.wis.R;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.NotificationBadge;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import me.leolin.shortcutbadger.ShortcutBadger;

public class GcmMessageHandler extends IntentService {

    String mes;
    private Handler handler;
    int test  = 0;

    public GcmMessageHandler() {
        super(GcmMessageHandler.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final Bundle extras = intent.getExtras();
        GcmBroadcastReceiver.completeWakefulIntent(intent);
        try {
            if (!Tools.getData(this, "idprofile").equals("")) {
                Tools.setLocale(this, Tools.getData(this, "lang_pr"));
                if (extras.getString("type_send").equals("chat"))
                {
                    System.out.println("-------------------------------");
                    Log.d("handle notify","message receive");

                    Log.d("handle notify","message receive");

                    Log.e("Notifcaion Message", String.valueOf(extras));





                    int i = Chat.fId;
                    String msg = extras.getString("msg");
                    if (extras.getString("type_message").equals("photo"))
                        msg = getString(R.string.type_photo);
                    if (extras.getString("type_message").equals("video"))
                        msg = getString(R.string.type_video);


                    System.out.println(msg);


//
                    Log.d("is chant view", String.valueOf(ChatApi.getInstance().getIsChatView()));
                    if(ChatApi.getInstance().getIsChatView()==Boolean.FALSE){

                        try {
                            byte[] arrByte = msg.getBytes("ISO-8859-1");
                            msg = new String(arrByte, "UTF-8");
                        } catch(Exception e) {
                            Log.i("unicode exception",e.getMessage().toString());
                        }

                        Tools.showNotif2(this, Integer.parseInt(extras.getString("idprofile")), extras.getString("name"), msg);

                    }


                    final String currentUser = Tools.getData(Chat.context,"idprofile");
                    String msgCreator = extras.getString("idprofile");
                    System.out.println(currentUser);
                    System.out.println(msgCreator);
                    if(currentUser.equals(msgCreator)){
                        System.out.println("dont add msg");
                        if (!extras.getString("idprofile").equals(Tools.getData(this, "idprofile")))
                            try {
                                byte[] arrByte = msg.getBytes("ISO-8859-1");
                                msg = new String(arrByte, "UTF-8");
                            } catch(Exception e) {
                                Log.i("unicode exception",e.getMessage().toString());
                            }

//                        NotificationManager notificationManager = (NotificationManager)
//                                getSystemService(NOTIFICATION_SERVICE);
//
//                        Notification n  = new Notification.Builder(this)
//                                .setContentTitle(extras.getString("name"))
//                                .setContentText(msg)
//                                .setSmallIcon(R.drawable.logo_wis)
//
//                                .setAutoCancel(true)
//                                .build();
//
//                        notificationManager.notify(0, n);


//
//                        Tools.showNotif2(this, Integer.parseInt(extras.getString("idprofile")), extras.getString("name"), msg);
                    }
// else{
//                        System.out.println("add msg");
//                        System.out.println(extras.getString("group_id"));
//                        System.out.println(Chat.fId);
//
//
//                            addMsgToListView(extras);
//

//                    }


//                    if ((Chat.data == null || !extras.getString("idprofile").equals(String.valueOf(Chat.fId)))) {
//
//                    } else {
////
//
//                    }

//                    NotificationBadge badge=new NotificationBadge();
//                    badge.showBadge();
                } else if (extras.getString("type_send").equals("invit")) {
                    Tools.showNotif3(this, getString(R.string.new_invitation), extras.getString("name"));
                } else if (extras.getString("type_send").equals("pub")) {
                    Tools.showNotif(this, extras.getString("id_pub"), extras.getString("title"), extras.getString("msg"));
                }
            }
        } catch (Exception e) {

        }
    }


    public void addMsgToListView(final Bundle extras){
//
        Chat.context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String thumb = "";
                try {
                    thumb = extras.getString("photo_video");
                } catch (Exception e) {
                }
                boolean valid = true;
                Log.i("notification receive","receive thred");

                try {


                    if (Chat.data.size() > 0) {
                        String lastMsg = Chat.data.get(Chat.data.size() - 1).getMsg();
                        boolean isMe = Chat.data.get(Chat.data.size() - 1).getIsMe();
                        Log.i("isMe", String.valueOf(isMe));

                        System.out.println(lastMsg);
                        System.out.println(extras.getString("msg"));


                        if (lastMsg.equals(extras.getString("msg")) && !isMe)
                            valid = false;
                    }
                }
                catch (NullPointerException ex){
                    System.out.println(ex.getLocalizedMessage());
                }

                if (valid) {
                    String msg = extras.getString("msg");
                    try {
                        byte[] arrByte = msg.getBytes("ISO-8859-1");
                        msg = new String(arrByte, "UTF-8");
                    } catch(Exception e) {
                        Log.i("unicode exception",e.getMessage().toString());
                    }

//                                    override date and time
                    Date date=new Date();
                    SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    sourceFormat.format(date);


                    TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(),"timezone"));
                    SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    destFormat.setTimeZone(tz);

                    String result = destFormat.format(date);




                    if(msg.equals("Message photo")){
                        msg = extras.getString("photo_url");
                    }
                    int userid= Integer.parseInt(String.valueOf(extras.getString("idprofile")));
                    Chat.data.add(new WISChat(0, new WISUser(userid, extras.getString("name"), extras.getString("photo")), msg  ,result, false, extras.getString("type_message"), thumb,""));

                    Chat.adapter.notifyDataSetChanged();
                    Chat.lvChat.invalidateViews();
                    Chat.lvChat.setSelection(Chat.data.size() - 1);
                    Log.i("call",String.valueOf(test));
                    test++;




                }
            }
        });

    }
}
