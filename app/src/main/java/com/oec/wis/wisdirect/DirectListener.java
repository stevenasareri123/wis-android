package com.oec.wis.wisdirect;

import org.json.JSONObject;

/**
 * Created by asareri08 on 20/02/17.
 */

public interface DirectListener {

    public void directCreated(String apikey,String session, String token, JSONObject response);

}
