package com.oec.wis.wisdirect;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.GPSTracker;
import com.oec.wis.R;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.adapters.WisAmisAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragActu;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asareri08 on 20/02/17.
 */

public class WisDirectPreviewScreen extends Fragment implements DirectListener {


    public static final String PREFS_NAME = "DirectPermission";
    SharedPreferences settings;
    ListView groupFriendList;
    SelectFriendsAdapter selectFriendsAdapter;
    EditText searchFilter;
    EditText titleField;
    CircularImageView directIcon;
    Button nextBtn;
    TextView selectOne,selectAll;
    View view;
    TextView headerTitleTV;

    ImageView back;

    List<WISUser> friendList;
    String imagePath;

    private WisDirectPublisherView mPublisherFragment;

    double currentLatitude,currentLongitude;

    DialogPlus dialog;
    static final int REQUEST_CAMERA = 2;

    GPSTracker tracker;

    public String getAct_id() {
        return act_id;
    }

    public void setAct_id(String act_id) {
        this.act_id = act_id;
    }

    String act_id;


    private PreviewControlCallbacks mControlCallbacks = previewCallbacks;

    private WisDirect mActivity;

    ProgressDialog pd;
    CircularImageView userNotificationView;
    TextView notificationCount;


    Typeface font;
    public interface PreviewControlCallbacks {

        public void startBroadCast(String apikey,String sessionId, String token,String idAct);


    }

    private static PreviewControlCallbacks previewCallbacks = new PreviewControlCallbacks() {
        @Override
        public void startBroadCast(String apikey,String sessionId, String token, String idAct) {

            Log.e("startBroadCast","INITFRAGMENT");

            previewCallbacks.startBroadCast(apikey,sessionId,token,idAct);

        }
    };



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.direct_preview_screen, container, false);
            initView(view);
            boolean dialogShown = settings.getBoolean("directDialogShown", false);


            if (!dialogShown) {
                // AlertDialog code here
//                showDirectPermissionalert();
//                showDirectAlert();
                showDirectVotreAlert();

            }else{
                loadFriends();
                initGps();

            }
            userNotificationView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Tools.callDialog(getActivity());
                }
            });



        } catch (InflateException e) {
        }

        return view;
    }

    private void showDirectPermissionalert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.direct_permission_alert);

        final AlertDialog alert = builder.create();
        alert.show();
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();


            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDirectVotreAlert();
//                showDirectAlert();
//                loadFriends();
//                SharedPreferences.Editor editor = settings.edit();
//                editor.putBoolean("directDialogShown", true);
//                editor.commit();
                alert.dismiss();

//                initGps();
            }
        });


        try{
            Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            ((TextView) alert.findViewById(R.id.cancel_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.accept_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.alertMessage)).setTypeface(font);
        }catch (NullPointerException ex){

        }
    }




//    Permission Alert

    private void showDirectAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);

        final AlertDialog alert = builder.create();
        alert.show();
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFriends();
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("directDialogShown", true);
                editor.commit();
                alert.dismiss();

                initGps();
            }
        });


        try{
            Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            ((TextView) alert.findViewById(R.id.cancel_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.accept_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.alertMessage)).setTypeface(font);
        }catch (NullPointerException ex){

        }
    }


//    Permission Alert

    private void showDirectVotreAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.wis_direct_votere_alert);

        final AlertDialog alert = builder.create();
        alert.show();
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();

//                FragActu fragment = new FragActu();
//                android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
//                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frame_container, fragment).commit();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDirectAlert();
//                loadFriends();
//                SharedPreferences.Editor editor = settings.edit();
//                editor.putBoolean("directDialogShown", true);
//                editor.commit();
                alert.dismiss();
//
//                initGps();
            }
        });


        try{
            Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            ((TextView) alert.findViewById(R.id.cancel_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.accept_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.alertMessage)).setTypeface(font);
        }catch (NullPointerException ex){

        }
    }


//    checl gps stats


    public void initGps(){

        tracker = new GPSTracker(getActivity());

        if (tracker.canGetLocation()) {

            System.out.println("USER LOCATION CORDINATES");

            Log.e("LAT", String.valueOf(tracker.getLatitude()));

            Log.e("LONG", String.valueOf(tracker.getLongitude()));

            currentLatitude = tracker.getLatitude();
            currentLongitude = tracker.getLongitude();
        }

        else{

            tracker.showSettingsAlert();
        }
    }

    public void initView(View view){


        back = (ImageView) view.findViewById(R.id.toggle);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Back","Clicked");
                getActivity().finish();
            }
        });
        groupFriendList = (ListView)view.findViewById(R.id.groupfriendList);
        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        searchFilter = (EditText)view.findViewById(R.id.etFilter);
        nextBtn = (Button)view.findViewById(R.id.bNext);
        titleField = (EditText)view.findViewById(R.id.direct_name);
        directIcon = (CircularImageView)view.findViewById(R.id.direct_logo);
        headerTitleTV =(TextView)view.findViewById(R.id.headerTitleTV);


        selectOne = (TextView) view.findViewById(R.id.selectOne);
        selectAll = (TextView) view.findViewById(R.id.selectAll);



        notificationCount = (TextView) view.findViewById(R.id.target_view);
        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);

        downloadProfileImage();
        if(new UserNotification().getBatchcount()==0)
        {
            notificationCount.setVisibility(View.INVISIBLE);
        }
        else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));

                }
            });
        }


        try{
            font =Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            searchFilter.setTypeface(font);
            nextBtn.setTypeface(font);
            titleField.setTypeface(font);
            selectOne.setTypeface(font);
            selectAll.setTypeface(font);
            headerTitleTV.setTypeface(font);
        }catch (NullPointerException ex){
            Log.e("exce",ex.getMessage());
        }





        selectOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectOne.setBackgroundColor(getResources().getColor(R.color.blue_color));
                selectAll.setBackgroundColor(getResources().getColor(R.color.light_blue_color));

                if(selectFriendsAdapter!=null)
                {
                    selectFriendsAdapter.clearGroupMember();
                    selectFriendsAdapter.notifyDataSetChanged();
                }





            }
        });

        selectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAll.setBackgroundColor(getResources().getColor(R.color.blue_color));
                selectOne.setBackgroundColor(getResources().getColor(R.color.light_blue_color));
                if(selectFriendsAdapter!=null) {
                    selectFriendsAdapter.addAll();

                    selectFriendsAdapter.notifyDataSetChanged();
                }

            }
        });


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(titleField.getText().toString().equals("")){

                    Toast.makeText(getActivity(), getString(R.string.broadcast_title_alert), Toast.LENGTH_SHORT).show();

                }
                if(imagePath==null)
                {
                    Toast.makeText(getActivity(), getString(R.string.broadcast_logo), Toast.LENGTH_SHORT).show();
                }
                else if (tracker.canGetLocation()){

                    try {
                        selectFriendsAdapter.createWisDirect(titleField.getText().toString(),imagePath,tracker.getLatitude(),tracker.getLongitude());
                    }
                    catch (NullPointerException e)
                    {

                    }

                }
                else{

                    tracker.showSettingsAlert();
                }


            }
        });

        directIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                openMedia();

                openWISMedia();
            }
        });


        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);



    }

    private void downloadProfileImage() {

        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(userNotificationView);
    }


//    imgage picker

    public void openMedia() {

        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
//                boolean result=Utility.checkPermission(MainActivity.this);
                if (items[item].equals("Take Photo")) {
//                    userChoosenTask="Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
//                    userChoosenTask="Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();




    }




    public void openWISMedia(){

        final ArrayList popupList = new ArrayList();

        popupList.add(new Popupelements(R.drawable.camera_grey,"Take Photo"));
        popupList.add(new Popupelements(R.drawable.slide_wis_photo,"Choose Photo "));
        popupList.add(new Popupelements(R.drawable.close_grey,"Cancel"));

        Popupadapter simpleAdapter = new Popupadapter(getActivity(), false, popupList);


        dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
                            dialog.dismiss();
                            cameraIntent();

                        }
                        else if(position ==1){
                            dialog.dismiss();
                            galleryIntent();

                        }
                        else if(position == 2){
                            dialog.dismiss();
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }


    private void  galleryIntent() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }


    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Log.i("showImageLibrary", "showImageLibrary");

        Log.i("requestCode", String.valueOf(requestCode));

        Log.i("Activity.RESULT_OK", String.valueOf(Activity.RESULT_OK));


        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            Uri uri = imageReturnedIntent.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                directIcon.setImageBitmap(bitmap);

                System.out.println("Welcome"+bitmap);

                final String[] proj = {MediaStore.Images.Media.DATA};
                final Cursor cursor = getActivity().managedQuery(uri, proj, null, null,
                        null);
                final int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToLast();
                imagePath = cursor.getString(column_index);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(requestCode == 2 && resultCode == Activity.RESULT_OK){
            Bitmap thumbnail = (Bitmap)imageReturnedIntent.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
            directIcon.setImageBitmap(thumbnail);
            System.out.println("Welcome"+destination);
            //new DoUpload().execute();
            imagePath= String.valueOf(destination);
        }
    }



//    get friendslist

    private void loadFriendsList(ListView groupChatFriendList){

        selectFriendsAdapter = new SelectFriendsAdapter(getActivity(),friendList);
        groupChatFriendList.setAdapter(selectFriendsAdapter);
        selectFriendsAdapter.notifyDataSetChanged();
        loadFilterConfig(selectFriendsAdapter);

        selectFriendsAdapter.setDirectListener(WisDirectPreviewScreen.this);


    }

    private void loadFilterConfig(final SelectFriendsAdapter adapter){
        searchFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    private void loadFriends() {
        friendList = new ArrayList<>();



        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("data").contains("Acun amis"))
                                Toast.makeText(getActivity(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
                            else {
                                if (response.getString("result").equals("true")) {
                                    JSONArray data = response.getJSONArray("data");
                                    Log.i("contacts list", String.valueOf(response));


                                    for (int i = 0; i <data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
//                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
                                            friendList.add(new WISUser(Boolean.FALSE,obj.getString("blocked_by"),obj.getString("objectId"),obj.getString("activeNewsFeed"),obj.getInt("idprofile"), obj.getString("fullName"),obj.getString("photo"), obj.getInt("nbr_amis")));


                                        } catch (Exception e) {

                                        }
//

                                    }


                                    loadFriendsList(groupFriendList);


                                }
                            }


                        } catch (JSONException e) {


                            //if (getActivity() != null)
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);
    }
    private void setUPFilterConfig(final WisAmisAdapter adapter){
        searchFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    @Override
    public void directCreated(String apikey,String session, String token, JSONObject response) {

        Log.d("direct created",session +" || "+token);

        Log.d("direct members", String.valueOf(response));



        try {
            JSONObject direct_data = response.getJSONObject("wis_direct_data");

            JSONObject channels = response.getJSONObject("channel_ids");


            act_id = channels.getString("wis_direct_channel").replace("WisDirect_","");

            JSONArray memberArray = channels.getJSONArray("wis_direct_mebers");

            mControlCallbacks.startBroadCast(apikey,session,token,act_id);


            Log.e("Direct_data", String.valueOf(direct_data));

//            ChatApi.getInstance().setDirectResponse(direct_data);

            sendBroadCastAlertToFriends(apikey,session,token,act_id,memberArray,direct_data);

        } catch (JSONException e) {
            e.printStackTrace();
        }





//        sendBroadCastAlertToFriends(session,token,);
    }


    public void sendBroadCastAlertToFriends(String apikey,String session, String token, String act_id, JSONArray memberArray, JSONObject direct_data){


        Toast.makeText(getActivity(), "BroadCast Loading", Toast.LENGTH_LONG).show();

        Map<String, String> json = new HashMap<>();

        String fullname = "";

        if(Tools.getData(getActivity(), "name").equals("")||Tools.getData(getActivity(), "name")==null)
        {
            fullname = Tools.getData(getActivity(), "firstname_prt") + " " + Tools.getData(getActivity(), "lastname_prt");
        }
        else {
            fullname = Tools.getData(getActivity(), "name");
        }

        try {
            json.put("Name","WISDirectInvite");
            json.put("senderID", Tools.getData(getActivity(), "idprofile"));
            json.put("senderImage", Tools.getData(getActivity(), "photo"));
            json.put("senderName", fullname);
            json.put("title",direct_data.getString("title"));
            json.put("opentok_session_id",session);
            json.put("opentok_token",token);
            json.put("opentok_apikey",apikey);
            json.put("latitude",direct_data.getString("latitude"));
            json.put("langitude",direct_data.getString("langitude"));
            json.put("created_at",direct_data.getString("created_at"));
            json.put("logo",direct_data.getString("logo"));
            json.put("direct_id",act_id);
            json.put("id", String.valueOf(direct_data.getInt("id")));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i=0;i<memberArray.length();i++){

            try {
                JSONObject  jsonObject = new JSONObject(json);

                Log.e("MAP TO JSON", String.valueOf(jsonObject));

                ChatApi.getInstance().setDirectResponse(jsonObject);
                ChatApi.getInstance().sendMessage(json, String.valueOf(memberArray.get(i)));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);


    }


    @Override
    public void onAttach(Context context) {
        Log.i("LOGTAG", "OnAttach PreviewControlFragment");

        super.onAttach(context);

        //this.mActivity = (WisDirect) context;
        this.mControlCallbacks = (PreviewControlCallbacks) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            this.mActivity = (WisDirect) activity;
            this.mControlCallbacks = (PreviewControlCallbacks) activity;
        }
    }

    public void disableBackgroudView(){


    }

    @Override
    public void onDetach() {
        Log.i("LOGTAG", "onDetach PreviewControlFragment");

        super.onDetach();

        mControlCallbacks = previewCallbacks;
    }


}


