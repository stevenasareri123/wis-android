package com.oec.wis.wisdirect;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.GPSTracker;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.CmtAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.Comments;
import com.oec.wis.dialogs.CustomLocationActivity;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.tokbox.android.accpack.OneToOneCommunication;
import com.tokbox.android.logging.OTKAnalytics;
import com.tokbox.android.logging.OTKAnalyticsData;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class WisDirect extends AppCompatActivity implements Publisher.PublisherListener,Session.SessionListener,EasyPermissions.PermissionCallbacks,WisDirectPreviewScreen.PreviewControlCallbacks,PublisherListener,Session.SignalListener,Session.ArchiveListener,EmojiconsFragment.OnEmojiconBackspaceClickedListener,EmojiconGridFragment.OnEmojiconClickedListener {
    Session mSession;
    Subscriber mSubscriber;
    Publisher mPublisher;
    private OTKAnalyticsData mAnalyticsData;
    private OTKAnalytics mAnalytics;

    List<WISCmt> cmts;

    Boolean sessionStatus = false;

    Geocoder geocoder;
    List<Address> addresses;

    String archiveId;


    TextView viewsCount;
    ImageView stopTrigger;

    int subscriberCount=0;


    private OneToOneCommunication mComm;

    private RelativeLayout mPublisherViewContainer,mPreviewViewContainer;

    private WisDirectPreviewScreen mPreviewFragment;
    private FragmentTransaction mFragmentTransaction;

    private WisDirectPublisherView mPublisherFragment;

    private RelativeLayout.LayoutParams layoutParamsPreview;

    double latitude,langitude;

    String senderId;



    private static final int RC_SETTINGS_SCREEN_PERM = 123;
    private static final int RC_VIDEO_APP_PERM = 124;


    private  static Boolean hasPermission = Boolean.FALSE;
    private ArrayList<Subscriber> mSubscribers = new ArrayList<Subscriber>();
    private HashMap<Stream, Subscriber> mSubscriberStreams = new HashMap<Stream, Subscriber>();


    ProgressDialog pd;

    Bundle b;

    String idAct;

    String DIRECT_ID;


    GPSTracker tracker;
    double currentLatitude,currentLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wis_direct);


        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

        requestPermissions();
        initGps();



//        Button startDirect = (Button)findViewById(R.id.startBroadCast);


        mPublisherViewContainer = (RelativeLayout)findViewById(R.id.broadCasterView);



        b = getIntent().getExtras();

        cmts = new ArrayList<>();

        if(b!=null){


            if (savedInstanceState == null) {
                mFragmentTransaction = getSupportFragmentManager().beginTransaction();

                if(b.getString("is_publisher").equals("YES")){

                    initPreviewFragment();


//                    initPublisherFragment();
                }
                else{

                    showLoader();

                    Log.e("Subscriber view","Subsciper");

                    String session = b.getString("session_id");

                    String token = b.getString("token");

                    String apikey = b.getString("api_key");

                    DIRECT_ID = b.getString("id");

                    this.idAct = b.getString("idAct");

                    JSONObject directResponse = ChatApi.getInstance().getDirectResponse();

                    if(directResponse.has("senderId")){

                        try {
                            senderId = directResponse.getString("senderID");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                    if(session!=null&&token!=null){

                        initPublisherFragment();

                        initSesssion(apikey,session,token);



                    }


                }



                mFragmentTransaction.commitAllowingStateLoss();
            }
        }






//        startDirect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                startBroadCast();
//            }
//        });







//        initSesssion();


//        mSession.setSignalListener(this);


    }

    public void initGps(){

        tracker = new GPSTracker(this);

        if (tracker.canGetLocation()) {

            System.out.println("USER LOCATION CORDINATES");

            Log.e("LAT", String.valueOf(tracker.getLatitude()));

            Log.e("LONG", String.valueOf(tracker.getLongitude()));

            currentLatitude = tracker.getLatitude();
            currentLongitude = tracker.getLongitude();
        }

        else{

            tracker.showSettingsAlert();
        }
    }


    private void initPreviewFragment(){
        mPreviewFragment = new WisDirectPreviewScreen();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.direct_preview_fragment_container, mPreviewFragment).commit();


        mPreviewViewContainer = (RelativeLayout)findViewById(R.id.direct_preview_fragment_container);




//        mPreviewFragment.setEnabled(true);

        //init controls fragments

    }


    private void initPublisherFragment(){

        mPublisherFragment  = new WisDirectPublisherView();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.direct_publisher_fragment_container, mPublisherFragment).commit();

        mPublisherFragment.setPublisherListener(this);

        mPublisherFragment.setActivityId(Integer.parseInt(idAct));
//

        //mPublisherFragment.setControls(true);





//        mPublisherViewContainer = (RelativeLayout)findViewById(R.id.direct_publisher_fragment_container);






//        final ImageView swapCamera = (ImageView)findViewById(R.id.swapCamera);
//
//
//        swapCamera.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (mPublisher == null) {
//                    return;
//                }
//                mPublisher.swapCamera();
//            }
//        });


    }

//   check permsision


    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private void requestPermissions() {
        String[] perms = {
                Manifest.permission.INTERNET,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };
        if (EasyPermissions.hasPermissions(this, perms)) {

            hasPermission =Boolean.TRUE;

        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_video_app), RC_VIDEO_APP_PERM, perms);
        }
    }


//    Permission delegates

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d("TAG", "onPermissionsGranted:" + requestCode + ":" + perms.size());

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d("TAG", "onPermissionsDenied:" + requestCode + ":" + perms.size());

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {

            finish();
//            new AppSettingsDialog.Builder(this, getString(R.string.rationale_ask_again))
//                    .setTitle(getString(R.string.title_settings_dialog))
//                    .setPositiveButton(getString(R.string.setting))
//                    .setNegativeButton(getString(R.string.cancel), null)
//                    .setRequestCode(RC_SETTINGS_SCREEN_PERM)
//                    .build()
//                    .show();
        }
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Log.e("Premiss code", String.valueOf(requestCode));
//        Log.e("Premiss result", String.valueOf(resultCode));
//       if(hasPermission){
//
//       }else{
//
//       }
//    }



//    create Session


    public void initSesssion(String api_key,String session,String token){

        Log.e("ApiKey",api_key);
        Log.e("Session",session);
        Log.e("hasPermission", String.valueOf(hasPermission));


        if(hasPermission){



            mSession = new Session(this, api_key,session);
            mSession.setSessionListener(WisDirect.this);
            mSession.setSignalListener(WisDirect.this);
            mSession.setArchiveListener(WisDirect.this);
            mSession.connect(token);


            String idprofile = Tools.getData(WisDirect.this, "idprofile");

            int profileId = Integer.parseInt(idprofile);


//            JSONObject directResponse = ChatApi.getInstance().getDirectResponse();
//
//            if(directResponse.has("senderId")){
//
//                try {
//                    senderId = directResponse.getString("senderID");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }else{
//
//                try {
//                    senderId =directResponse.getString("created_by");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }




            Log.e("SenderId", String.valueOf(senderId));
            Log.e("ProfileId", String.valueOf(profileId));

            if(!idprofile.equals(senderId)){

                if(mSession!=null)
                {
                    updateNoOfViews(Integer.parseInt(idAct));
                }
            }





            mSession.setReconnectionListener(new Session.ReconnectionListener() {
                @Override
                public void onReconnecting(Session session) {

                    Log.e("session","reconect");
                }

                @Override
                public void onReconnected(Session session) {


                    Log.e("session","onReconnected");
                }
            });


        }else{

            Toast.makeText(this,"No Permisson",Toast.LENGTH_SHORT).show();

        }


    }

//    Start BroadCast


    public void startBroadCast(){

        Log.e("Publisher","initbroadcast");

        mPublisher = new Publisher(WisDirect.this,"WisDirectBroadCast");
        mPublisher.setPublisherListener(this);
        mPublisher.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);

        if(mPublisherViewContainer!=null){

            //setAddViewControls(overLayview);b

            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View overLayview = inflater.inflate(R.layout.display_over_view, null);

            overLayview.setClickable(true);


            mPublisherViewContainer.addView(mPublisher.getView());

            mPublisherViewContainer.addView(overLayview);

            setAddViewControlss(overLayview);


            mPublisher.setPublishAudio(true);

            if(mSession!=null){

                mSession.publish(mPublisher);

                //record video
                showLoader();

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                {
                    Log.i("Calling","executeOnExecutor()");
                    new RecordVideo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mSession.getSessionId());
                }
                else {
                    Log.i("Calling","execute()");

                    new RecordVideo().execute(mSession.getSessionId());
                }
            }
            else{

                Log.e("Sessoion","null");
            }



        }
        else{
            Toast.makeText(this,"Unable to get publisher frame",Toast.LENGTH_SHORT);
        }



    }

    public void setAddViewControlss(View addView)
    {
        try {
            final JSONObject directResponse = ChatApi.getInstance().getDirectResponse();

            Log.e("directRespone", String.valueOf(directResponse));


            TextView titleLive = (TextView) addView.findViewById(R.id.titleLive);

            TextView liveAddress = (TextView) addView.findViewById(R.id.locationLive);


            latitude=directResponse.getDouble("latitude");
            langitude=directResponse.getDouble("langitude");

            String adds = getCompleteAddressString(latitude,langitude);

//            Log.e("AddressText",adds);

            liveAddress.setText(adds);
            ImageView swapCamera = (ImageView)addView.findViewById(R.id.swapCamera);

            Log.e("overlys view", String.valueOf(addView));

            swapCamera.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Log.e("camera","clicked");
                    if (mPublisher == null) {
                        return;
                    }
                    mPublisher.swapCamera();

                }
            });





            final int directId = directResponse.getInt("id");





            stopTrigger = (ImageView) addView.findViewById(R.id.stopTrigger);
            stopTrigger.setVisibility(View.VISIBLE);
            stopTrigger.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    String idprofile = Tools.getData(WisDirect.this, "idprofile");

                    int profileId = Integer.parseInt(idprofile);

                    int senderId = 0;
                    try {
                        senderId = directResponse.getInt("senderID");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.e("SenderId", String.valueOf(senderId));
                    Log.e("ProfileId", String.valueOf(profileId));
                    if(profileId==senderId){

                        Log.e("INSIDe","PUBLIHEr");

                        updateDirectStatus(archiveId, String.valueOf(directId));

                        disConnectBroadCast();
                        mPublisherFragment.setArchiveId(archiveId);

                        mPublisherFragment.setDirectId(String.valueOf(directId));

                    }else{
                        showAlertBack();
                    }


//                    Toast.makeText(WisDirect.this,"The video has ended",Toast.LENGTH_SHORT).show();
                }
            });

            final String publisher = directResponse.getString("senderName");
            liveAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), CustomLocationActivity.class);
                    UserNotification.setIsFromPubView(Boolean.TRUE);
                    i.putExtra("lat", latitude);
                    i.putExtra("lng", langitude);
                    i.putExtra("currentlat",currentLatitude);
                    i.putExtra("currentlang",currentLongitude);
                    i.putExtra("advertiser_id",0);
                    i.putExtra("advertiser_name",publisher);
                    startActivity(i);
                }
            });

            titleLive.setText(directResponse.getString("title"));

            TextView senderName = (TextView) addView.findViewById(R.id.nameProducer);

            senderName.setText(directResponse.getString("senderName"));

            TextView createAt = (TextView) addView.findViewById(R.id.datelabel);


            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            String date = "";
            try {
                newDate = format.parse(directResponse.getString("created_at"));
                format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                date = format.format(newDate);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            System.out.println("Formatted date====>"+ date);

            String formatedDate = date.replace("-","/").replace(":","/").replace(" ",",");

            createAt.setText(formatedDate);

            String[] parts = formatedDate.split(",", 2);
            String dass = parts[0];
            String tass = parts[1];

            System.out.println("=======================>");

            Log.e("Date:",dass+"Time:"+tass);

            System.out.println("=======================>");

            createAt.setText(dass);

            TextView createTime = (TextView) addView.findViewById(R.id.timelabel);

            createTime.setText(tass);

            viewsCount = (TextView) addView.findViewById(R.id.viewsCount);
            viewsCount.setText("0");
            viewsCount.setTextColor(Color.RED);



            try{
                Typeface font = Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
                createTime.setTypeface(font);
                senderName.setTypeface(font);
                createAt.setTypeface(font);
                liveAddress.setTypeface(font);
                titleLive.setTypeface(font);

            }catch (NullPointerException ex){
                Log.e("exce",ex.getMessage());
            }

            final CircularImageView producerPhoto = (CircularImageView) addView.findViewById(R.id.producerPhoto);

            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + directResponse.getString("senderImage") , new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    producerPhoto.setImageResource(R.drawable.logo_wis);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        producerPhoto.setImageBitmap(response.getBitmap());
                    }
                }
            });


            String directLog = directResponse.getString("logo");

            Log.e("DirectLogo",directLog);

            final ImageView wisLogo = (ImageView) addView.findViewById(R.id.locationLogo);


            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + directResponse.getString("logo") , new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    wisLogo.setImageResource(R.drawable.logo_wis);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        wisLogo.setImageBitmap(response.getBitmap());
                    }
                }
            });



            final int senderId = directResponse.getInt("senderID");
            producerPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("clicked","clicked");
                    String idprofile = Tools.getData(WisDirect.this, "idprofile");

                    int profileId = Integer.parseInt(idprofile);

                    Log.e("SenderId", String.valueOf(senderId));
                    Log.e("ProfileId", String.valueOf(profileId));

                    if(senderId==profileId){
                        Log.e("ProfileId","ProfileViewTwo");
                        Intent i = new Intent(getApplicationContext(), ProfileViewTwo.class);
                        i.putExtra("id",senderId);
                        startActivity(i);

                    }else{
                        Log.e("SenderId","ProfileView");
                        Intent i = new Intent(getApplicationContext(), ProfileView.class);
                        i.putExtra("id",senderId);
                        startActivity(i);

                    }
                }
            });

        }



        catch (Exception e)
        {

        }
    }


    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {


        String strAdd = "";
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i("address",strAdd);

            } else {
                Log.i("address","no address found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Current loction", "Canont get Address!");
        }
        return strAdd;
    }


    public void disConnectBroadCast(){


        if (mSession == null) {
            return;
        }


        if (mSubscribers.size() > 0) {
            for (Subscriber subscriber : mSubscribers) {
                if (subscriber != null) {
                    mSession.unsubscribe(subscriber);
                    subscriber.destroy();
                }
            }
        }

        if (mPublisher != null) {
            mPublisherViewContainer.removeView(mPublisher.getView());
            mSession.unpublish(mPublisher);
            mPublisher.destroy();
            mPublisher = null;


        }

        mSession.sendSignal("connection_closed",mSession.getSessionId());
        mSession.disconnect();


        if(mPublisherFragment!=null)
        {
            mPublisherFragment.setshareBtnStatus(true);
            mPublisherFragment.setControls(false);
        }

        Log.e("SessionValue", String.valueOf(mSession));

        sessionStatus = true;

        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);



        //finish();
    }



//    broad cast delegates


    @Override
    public void onConnected(Session session) {

        Log.e("TAG", "onConnected: Connected to session " + session.getSessionId());

        if(b.getString("is_publisher").equals("YES")){

            initPublisherFragment();

            startBroadCast();
        }


    }

    @Override
    public void onDisconnected(Session session) {

        Toast.makeText(WisDirect.this,"Disconnected session", Toast.LENGTH_SHORT).show();

        Log.e("SessionDisConnect", String.valueOf(session));

        sessionStatus = true;

        try{
            if(stopTrigger!=null)
            {
                stopTrigger.setVisibility(View.INVISIBLE);
            }
        }
        catch (NullPointerException e)
        {

        }


        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);



    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {

        Log.d("TAG", "onStreamReceived: New stream " + stream.getStreamId() + " in session " + session.getSessionId());

        dismissLoader();

        Toast.makeText(WisDirect.this,"onStreamReceived", Toast.LENGTH_SHORT).show();

        mPublisherViewContainer = (RelativeLayout)findViewById(R.id.broadCasterView);

        final Subscriber subscriber = new Subscriber(WisDirect.this, stream);
        mSession.subscribe(subscriber);
        subscriber.setSubscribeToAudio(true);
        mSubscribers.add(subscriber);
        mSubscriberStreams.put(stream, subscriber);
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        mPublisherFragment.addPublisherView(subscriber.getView());


        subscriberCount = mSubscribers.size();

        Log.e("SubScriberCount",String.valueOf(mSubscribers.size()));

        if(mPublisherViewContainer!=null){
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View overLayview = inflater.inflate(R.layout.display_over_view, null);


//            mPublisherViewContainer.addView(subscriber.getView());

            mPublisherViewContainer.addView(overLayview);
            //setAddViewControls(overLayview);
            setAddViewControlss(overLayview);
        }
        else{
            Toast.makeText(this,"Unable to get publisher frame",Toast.LENGTH_SHORT).show();
        }

        if(mSession!=null){



            mSession.sendSignal("comment_referesh",mSession.getSessionId());


        }



//

    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {

        Subscriber subscriber = mSubscriberStreams.get(stream);
        if (subscriber == null) {
            return;
        }


        mSubscribers.remove(subscriber);
        mSubscriberStreams.remove(stream);

        subscriberCount = mSubscribers.size();



//        viewsCount.setText(subscriberCount);

        Log.e("Subscriber size", String.valueOf(mSubscribers.size()));

        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);


    }

    @Override
    public void onError(Session session, OpentokError opentokError) {

        String errMessage = "onError: Error (" + opentokError.getMessage() + ") in session " + session.getSessionId();

        Log.e("SESSIN ERRO","Unable to connect to the session: check the network connection");

//        Toast.makeText(this,"Unable to connect to the session: check the network connection.", Toast.LENGTH_LONG).show();
//
        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//
//        onBackPressed();
    }

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        Log.d("TAG", "onStreamCreated: Own stream " + stream.getStreamId() + " created");


    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
        Log.d("TAG", "onStreamDestroyed: Own stream " + stream.getStreamId() + " destroyed");
        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        Log.d("TAG", "onError: Error (" + opentokError.getMessage() + ") in publisher");

        Toast.makeText(this, "Session error. See the logcat please.", Toast.LENGTH_LONG).show();
    }



    private void addLogEvent(String action, String variation){
        if ( mAnalytics!= null ) {
            mAnalytics.logEvent(action, variation);
        }



    }







    @Override
    public void onBackPressed(){

        if(ApplicationController.getInstance().isNetworkConnected()){

            showAlert();

        }else{
            Toast.makeText(WisDirect.this,"Check your Internet Connection",Toast.LENGTH_SHORT);
        }

    }


    public void showAlert(){

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Wis")
                .setMessage("Are you sure you want to exit this session?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        Log.e("SessionValue", String.valueOf(mSession));

                        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

                        if(mSession!=null){

                            disConnectBroadCast();
                        }

                        finish();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


        try{
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);
            Typeface face=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            button1.setTypeface(face);
            Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            textView.setTypeface(face);
            button2.setTypeface(face);
            textView.setTextSize(20);//to change font size
            button1.setTextSize(20);
            button2.setTextSize(20);
        }catch (NullPointerException ex){
            Log.e("Exception",ex.getMessage());
        }
    }

    public void showAlertBack(){

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        new AlertDialog.Builder(this)
                .setTitle("Wis")
                .setMessage("Are you sure you want to exit this session?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        disConnectBroadCast();
                        mPublisherFragment.setArchiveId(archiveId);

                        mPublisherFragment.setDirectId("");

                       ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);


                        finish();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }



    @Override
    public void startBroadCast(String apikey,String sessionId, String token, String idAct) {


        this.idAct = idAct;

        initSesssion(apikey,sessionId,token);



        mPreviewViewContainer.setVisibility(View.INVISIBLE);
    }


    @Override
    public void postComments(String message) {

        Log.e("calling","Signal");

        Log.e("Message",message);
        System.out.println("================================");

        String unicode = StringEscapeUtils.escapeJava(message);

        Log.e("UnicodeValue",unicode);

        Log.e("DecodeValue",StringEscapeUtils.unescapeJava(unicode));

        System.out.println("================================");


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String cDate = sdf.format(new Date());

//                                override date and time
        Date date=new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);
        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(),"timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String usertimezone = destFormat.format(date);
        Log.i("resilt timezone ",usertimezone);

        String fullname = "";

      if(Tools.getData(this, "name").equals("")||Tools.getData(this, "name")==null)
       {
           fullname = Tools.getData(this, "firstname_prt") + " " + Tools.getData(this, "lastname_prt");
           }
       else {
           fullname = Tools.getData(this, "name");
            }

        HashMap<String,String> data = new HashMap<>();
        JSONObject object = null;

        try {
            object = new JSONObject();

            //object.put()

            object.put("senderName",fullname);
            object.put("message",unicode);
            object.put("idAct",this.idAct);
            object.put("sendericon",Tools.getData(this, "photo"));

            object.put("senderId",Tools.getData(this, "idprofile"));

            object.put("date",usertimezone);
        }
        catch (JSONException e)
        {

        }

        Log.e("message", String.valueOf(object));


//        convert to string
        mSession.sendSignal("comment_posted", String.valueOf(object));

        //        call api calls

        requetsSendCmt(Integer.parseInt(idAct),message);
    }

    @Override
    public void onControlsClicked() {



        mPublisherFragment.loadDirectStatus();

        if(mSession!=null){

            mSession.sendSignal("comment_referesh",mSession.getSessionId());
        }


        //loadDirectStatus();
    }

    @Override
    public void updateViewCount(String viewCount) {

        Log.e("ViewCount",viewCount);

        try {
            if(!viewCount.equals("0")||viewCount!=null)
            {
                viewsCount.setText(viewCount);
                viewsCount.setTextColor(Color.RED);
            }
            else {
                viewsCount.setText("0");
                viewsCount.setTextColor(Color.RED);
            }
        }catch (NullPointerException e)
        {

        }




    }

    @Override
    public void onSignalReceived(Session session, String s, String s1, Connection connection) {


//       check session
//               check tyope

        Log.e("onSignalReceived",s);

        Log.e("onSignalReceived",s1);




        if(mSession.getSessionId()==session.getSessionId())
        {
            if(s.equals("comment_posted"))
            {

                try {
                    JSONObject json = new JSONObject(s1);
                    //JSONTokener jsons = new JSONTokener(s1);
                    Log.e("JsonObject", String.valueOf(json));
                    mPublisherFragment.refereshComments(new WISCmt(0,new WISUser(json.getInt("senderId"), json.getString("senderName"),json.getString("sendericon")),json.getString("message"),json.getString("date"),s));
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            //updateNoOfViews(Integer.parseInt(idAct));
                            mPublisherFragment.loadDirectStatus();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }



            if(s.equals("comment_referesh")){

                mPublisherFragment.loadDirectStatus();
            }

            if(s.equals("connection_closed")){

                if(mPublisherFragment!=null)
                {
                    mPublisherFragment.setshareBtnStatus(true);
                    mPublisherFragment.setControls(false);
                }
//                String idprofile = Tools.getData(WisDirect.this, "idprofile");
//
//                int profileId = Integer.parseInt(idprofile);
//
//                Log.e("SenderId", String.valueOf(senderId));
//                Log.e("ProfileId", String.valueOf(profileId));
//
//                if(!idprofile.equals(senderId)){
//                    Toast.makeText(WisDirect.this,"The video has ended",Toast.LENGTH_SHORT).show();
//                }

                if(mPublisher==null){

                    disConnectBroadCast();
                }

            }
        }



//        here to refersh comment methods




    }


    public void requetsSendCmt(int idActu, final String cmt) {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", String.valueOf(Tools.getData(this, "idprofile")));
            jsonBody.put("id_act", String.valueOf(idActu));
            jsonBody.put("text", cmt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqCmt = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.send_cmt_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //{"Message":"","data":"7","result":true}
                        try {
                            if (response.getBoolean("result")) {
                                int id = response.getInt("data");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String cDate = sdf.format(new Date());

//                                override date and time
                                Date date=new Date();
                                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                sourceFormat.format(date);
                                Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(),"timezone"));
                                SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                destFormat.setTimeZone(tz);

                                String usertimezone = destFormat.format(date);
                                Log.i("resilt timezone ",usertimezone);

                                String uName=Tools.getData(getApplicationContext(),"name");


                                int user_id = Integer.parseInt(Tools.getData(getApplicationContext(), "idprofile"));

                                Log.i("comment user", String.valueOf(user_id));

//                                if(adapter!=null){
//                                    AdaptActu.actuList.get(p).getCmts().add(new WISCmt(id, new WISUser(user_id, uName, uPic), cmt, usertimezone,"true"));
//                                }else{
//
//                                    cmts.add(new WISCmt(id, new WISUser(user_id, uName, uPic), cmt,usertimezone,"true"));
//
//                                    AdaptActu.SELECTED.setCmts(cmts);
//                                    adapter = new CmtAdapter(Comments.this, AdaptActu.SELECTED.getCmts(),Comments.this);
//                                    lvCmt.setAdapter(adapter);
//
//                                }
//
//
//                                AdaptActu.SELECTED.setComment_count(AdaptActu.SELECTED.getCmts().size());
//
//                                adapter.notifyDataSetChanged();
//                                lvCmt.invalidateViews();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String res = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(WisDirect.this, "token"));
                headers.put("lang", Tools.getData(WisDirect.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmt);
    }

    @Override
    public void onArchiveStarted(Session session, String archiveId, String archiveName) {
//        mCurrentArchiveId = archiveId;
//        setStopArchiveEnabled(true);
//        mArchivingIndicatorView.setVisibility(View.VISIBLE);
        Log.e("Session", String.valueOf(session));

        if(mPublisherFragment!=null){

            mPublisherFragment.setshareBtnStatus(true);
            mPublisherFragment.setArchiveId(archiveId);
            mPublisherFragment.setDirectId(String.valueOf(DIRECT_ID));

        }

        Log.e("ArchiveId",archiveId);
        this.archiveId = archiveId;
        Log.e("ArchieveName",archiveName);
    }
    //    e4e93836-0c13-4eb4-a37e-641cf0ad4326
    @Override
    public void onArchiveStopped(Session session, String s) {

        Log.e("Session_stop", String.valueOf(session));
        Log.e("s",s);
        this.archiveId =s;
        if(mPublisherFragment!=null){

            mPublisherFragment.setshareBtnStatus(true);
            mPublisherFragment.setArchiveId(s);
        }

    }

    public void updateNoOfViews(int idActu)
    {

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("user_id", String.valueOf(Tools.getData(this, "idprofile")));
            jsonBody.put("idAct", String.valueOf(idActu));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqCmt = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.update_direct_views), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //{"Message":"","data":"7","result":true}
                        try {
                            Log.e("UpdateViews",String.valueOf(response));
                            if (response.getBoolean("result")) {


                                if(response.has("connection_status")){



                                    if (response.getString("connection_status").equals("closed")) {

                                        Toast.makeText(WisDirect.this,"The video has ended",Toast.LENGTH_SHORT).show();

                                    }else{

                                        if(mSession!=null)
                                        {
                                            mSession.sendSignal("comment_referesh",mSession.getSessionId());

                                        }
                                    }

                                }else {

//                                    Toast.makeText(WisDirect.this,"The video has ended",Toast.LENGTH_SHORT).show();
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String res = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(WisDirect.this, "token"));
                headers.put("lang", Tools.getData(WisDirect.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmt);
    }

    public void updateDirectStatus(String archiveId,String directId)
    {

        showLoader();

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("user_id", String.valueOf(Tools.getData(this, "idprofile")));
            jsonBody.put("archiveId", archiveId);
            jsonBody.put("direct_id",directId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JsonUpdate", String.valueOf(jsonBody));
        JsonObjectRequest reqCmt = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.update_direct_status), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        dismissLoader();
                        try {
                            Log.e("UpdateStatus", String.valueOf(response));
                            if(response.getString("result").equals("true"))
                            {
//                                mSession.sendSignal("connection_closed",mSession.getSessionId());



                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String res = "";

                dismissLoader();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(WisDirect.this, "token"));
                headers.put("lang", Tools.getData(WisDirect.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmt);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {

        mPublisherFragment.emojiBackspaceclicked(v);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {

        mPublisherFragment.emojiClcked(emojicon);
    }


    public class RecordVideo extends AsyncTask<String,Void,String>{

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            dismissLoader();
            try {
                if(result!=null) {
                    if (!result.equals("")) {
                        try {
                            JSONObject img = new JSONObject(result);

                            Log.e("RecordVideo", String.valueOf(img));

                        } catch (JSONException e) {

                        }

                    } else {

                    }
                }
                else {
                    Toast.makeText(WisDirect.this,"Connection TimeOut ",Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e)
            {

            }

        }


        @Override
        protected String doInBackground(String... params) {


            JSONObject response = null;
            try {
                response = Tools.recordVideo(getString(R.string.server_url) + getString(R.string.record_video),params[0],Tools.getData(WisDirect.this, "token"), Tools.getData(WisDirect.this, "idprofile"));
                if(response!=null){
                    return response.toString();
                }else{
                    return null;
                }

            }
            catch (Exception e)
            {
//                Log.e("Calling","Again");
//                try {
//                    response = Tools.recordVideo(getString(R.string.server_url) + getString(R.string.record_video),params[0],Tools.getData(WisDirect.this, "token"), Tools.getData(WisDirect.this, "idprofile"));
//                    if(response!=null){
//                        return response.toString();
//                    }else{
//                        return null;
//                    }
//                }
//                catch (Exception es)
//                {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(WisDirect.this,"Connection TimeOut ",Toast.LENGTH_LONG).show();
//                        }
//                    });
//
//                }
            }
            return null;

        }
    }

//    public class RecordVideo extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            pd = new ProgressDialog(WisDirect.this);
//            pd.setMessage("Please wait...");
//            pd.setCanceledOnTouchOutside(false);
//            pd.show();
//            Log.e("Callling","showLoader()");
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            if (!result.equals("")) {
//                try {
//                    JSONObject img = new JSONObject(result);
//
//                    Log.e("RecordVideo", String.valueOf(img));
//
//                    if(pd!=null){
//                        Log.e("Callling","dismissLoader()");
//                        pd.dismiss();
//                    }
//
//
//                } catch (JSONException e) {
//
//                }
//
//            } else {
//
//            }
//        }
//
//
//        @Override
//        protected String doInBackground(String... urls)
//        {
//            return recordVideo(getString(R.string.server_url) + getString(R.string.record_video),urls[0],Tools.getData(WisDirect.this, "token"), Tools.getData(WisDirect.this, "idprofile"));
//        }
//    }

    private String recordVideo(String sUrl,String sessionId,String token,String userId)
    {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        builder.addTextBody("user_id", userId);
        builder.addTextBody("sessionId", sessionId);
        HttpEntity entity = builder.build();
        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);

        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));

            http.getConnectionManager().shutdown();


        } catch (IOException e) {
            e.printStackTrace();
        }


        return res;

    }

    public void showLoader(){
        pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){
            pd.dismiss();
        }
    }


}

