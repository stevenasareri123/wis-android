package com.oec.wis.dialogs;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.ImagePickerListener;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragActu;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragMyLocation;
import com.oec.wis.fragments.FragMyPub;
import com.oec.wis.fragments.NewFragContact;
import com.oec.wis.groupcall.GroupCallMainActivity;
import com.oec.wis.tools.ItemClickSupport;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileView extends AppCompatActivity implements ImagePickerListener {
    ImageView ivProfile, ivProfile2;
    TextView tvFName, tvJob, tvCountry,userAddress;
    WISUser user;
    int id;
    LinearLayoutManager actuLM;
    List<WISActuality> actuList;
    RecyclerView.Adapter adaptActu;
    RecyclerView lvActu;
    String city_country;
    TextView contactTextTV,postTextTV,friendsTextTV,phototextTV,headerTitleTV;
    String city,country;
    Typeface font;
    String senderId;
    TextView tvDelete;
    RelativeLayout contactRL,postRL,newsRL,chatRL;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_profile_view);

        Tools.showLoader(this,getResources().getString(R.string.progress_loading));

        loadControls();
        setListener();
        setFontTypeFace();
        id = getIntent().getExtras().getInt("id");

        Log.e("id", String.valueOf(id));
        loadProfileData(id);

        getActivityLog(String.valueOf(id));
//        requestCountContact();
//        requestCountPub();
//        requestCountActu();


        senderId = Tools.getData(ProfileView.this, "idprofile");
        Log.e("senderId"  , senderId);


        loadActu();
        checkFriend();
    }

    private void setListener() {

//        if(senderId .equals(id)) {

            contactRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment = new NewFragContact();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();

                }
            });
            postRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Fragment fragment = new FragMyPub();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();
                }
            });
            newsRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Fragment fragment = new FragActu();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();

                }
            });
            chatRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    UserNotification.setDISCUSSIONSELECTED(Boolean.TRUE);
                    Fragment fragment = new FragChat();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();

                }
            });
//        }
//        else{
//
//            Log.e("else","else");
//        }
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

//                Intent inten = new Intent(ProfileView.this,FragActu.class);
//                startActivity(inten);

//                FragChat fragment = new FragChat();
//                Fragement fragmentManager =getSupportFragmentManager();
//                fragmentManager.beginTransaction()
//                        .replace(R.id.frame_container, fragment).commit();


            }
        });
        ItemClickSupport.addTo(lvActu).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                AdaptActu.SELECTED = actuList.get(position);
                startActivity(new Intent(ProfileView.this, ActuDetails.class));
            }
        });
        findViewById(R.id.tvDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog();
            }
        });

        findViewById(R.id.tvCountry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserNotification.setLocationSelected(Boolean.TRUE);
                UserNotification.setAddress(city.concat(",").concat(country));

                UserNotification.setViewType("ProfileView");
                Intent i = new Intent(ProfileView.this,CustomLocationActivity.class);
                startActivity(i);
//                Fragment fragment = new FragMyLocation();
//                FragmentManager fragmentManager = getFragmentManager();
//                fragmentManager.beginTransaction()
//                        .replace(R.id.frame_container, fragment).commit();
            }
        });
    }

    private void loadControls() {
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
//        ivProfile2 = (ImageView) findViewById(R.id.ivProfile2);
        tvFName = (TextView) findViewById(R.id.tvFName);
        tvJob = (TextView) findViewById(R.id.tvJob);
        tvCountry = (TextView) findViewById(R.id.tvCountry);
        userAddress = (TextView)findViewById(R.id.tvaddr);

        contactTextTV=(TextView)findViewById(R.id.contactTextTV);
        postTextTV=(TextView)findViewById(R.id.postTextTV);
        friendsTextTV=(TextView)findViewById(R.id.friendsTextTV);
        phototextTV=(TextView)findViewById(R.id.photoTextTV);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

        contactRL =(RelativeLayout)findViewById(R.id.contactRL);
        postRL=(RelativeLayout)findViewById(R.id.postRL);
        newsRL=(RelativeLayout)findViewById(R.id.newsRL);
        chatRL=(RelativeLayout)findViewById(R.id.chatRL);
        tvDelete=(TextView)findViewById(R.id.tvDelete);


        lvActu = (RecyclerView) findViewById(R.id.lvActu);
        lvActu.setHasFixedSize(true);
        actuLM = new LinearLayoutManager(this);
        lvActu.setLayoutManager(actuLM);

        actuList = new ArrayList<>();
        adaptActu = new AdaptActu(actuList,this);

        lvActu.setAdapter(adaptActu);


        String picture="";
        if(getIntent().getExtras().getString("image")!=null){

            picture = getIntent().getExtras().getString("image");

        }

        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + picture, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivProfile.setImageBitmap(response.getBitmap());
//                                                    ivProfile2.setImageBitmap(response.getBitmap());
                }
            }
        });



    }



    private void setFontTypeFace() {

        try{
            font= Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");

            tvFName.setTypeface(font);
            tvJob.setTypeface(font);
            tvCountry.setTypeface(font);
            tvCountry.setTypeface(font);
            userAddress.setTypeface(font);
//         userNameTextTV.setTypeface(font);
//         emailIdTextTV.setTypeface(font);
            tvFName.setTypeface(font);
            tvJob.setTypeface(font);
            postTextTV.setTypeface(font);
            contactTextTV.setTypeface(font);
            phototextTV.setTypeface(font);
            friendsTextTV.setTypeface(font);
            headerTitleTV.setTypeface(font);
            tvDelete.setTypeface(font);
        }catch (NullPointerException ex){

        }


    }

    private void loadProfileData(int id) {

//        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + id + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.userprofile_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("amis response  ======>", String.valueOf(response));
                            if (response.getString("result").equals("true")) {
                                JSONObject data = response.getJSONObject("data");


//                                String name=data.getString("name");
//                                System.out.println("name==>" +name);
//                                String  idProfile=data.getString("idprofile");
//                                String country=data.getString("country");
//                                System.out.println("idProfile==>" +idProfile);
//                                System.out.println("country==>" +country);
//
//                                String photo=data.getString("photo");
//                                System.out.println("photo" +photo);

//                                Bundle bundle = new Bundle();
//                                bundle.putString("name", name);
//                                bundle.putString("country",country);
//                                bundle.putString("photo",photo);
//                                Intent intent = new Intent(getApplicationContext(), Phone.class);
//                                intent.putExtras(bundle);
//                                startActivity(intent);
                                try {
                                    user = new WISUser(data.getInt("idprofile"), data.getString("firstname_prt"), data.getString("lastname_prt"), data.getString("photo"), data.getInt("nbramis"), data.getString("profiletype"), data.getString("email_pr"), data.getString("sexe_prt"));

                                    if (user != null) {
                                        tvFName.setText(user.getFirstName() + " " + user.getLastName() + "" + data.getString("name").replace("null", ""));
                                        if (data.getString("name").equals("") || data.getString("name").contains("null"))
                                            tvFName.setText(user.getFirstName() + " " + user.getLastName());
                                        else
                                            tvFName.setText(data.getString("name").replace("null", ""));
                                        tvCountry.setText(data.getString("country"));

                                        city_country = data.getString("state").concat(" ").concat(data.getString("country"));
                                        tvCountry.setText(city_country);
                                        city = data.getString("state");
                                        country = data.getString("country");
                                        String type = data.getString("typeaccount");
                                        Log.i("amis response 1", String.valueOf(response));
                                        Log.i("amis response 2", Tools.getData(getApplicationContext(),"tel_pr"));
                                        if(type.equals("profiletype")){
                                            userAddress.setVisibility(View.INVISIBLE);

                                        }else{
                                            userAddress.setVisibility(View.VISIBLE);
                                            userAddress.setText(Tools.getData(getApplicationContext(),"tel_pr"));
                                        }
                                        userAddress.setVisibility(View.VISIBLE);
                                        userAddress.setText("Tools.getData(getApplicationContext()");

                                        System.out.println("receiver name"+tvFName);
                                        System.out.println("receiver image"+ivProfile);



                                        findViewById(R.id.svProfile).setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {

                                }

                            }
//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
                            Tools.dismissLoader();

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        });

        ApplicationController.getInstance().addToRequestQueue(req);
    }

    private void getActivityLog(String id){
        JSONObject jsonBody = null;

        try {
            String json = "{\"user_id\": \"" + id + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.activityLog), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                Log.i("actrivity response", String.valueOf(response));

                                TextView tvContact = (TextView)findViewById(R.id.tvContacts);
                                TextView tvPub = (TextView)findViewById(R.id.tvPub);
                                TextView tvAct = (TextView)findViewById(R.id.tvActu);
                                TextView tvgroup = (TextView)findViewById(R.id.tvGroup);

                                JSONObject json = response.getJSONObject("data");
                                Log.i("actrivity response json", String.valueOf(json));
                                tvContact.setText(json.getString("contact"));
                                tvPub.setText(json.getString("pub"));
                                tvAct.setText(json.getString("actuality"));
                                tvgroup.setText(json.getString("group"));

                            }
                        } catch (Exception e) {

                        }
//                        findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//               findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);

    }

    private void requestCountContact() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + id + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_contact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvContact = (TextView) ProfileView.this.findViewById(R.id.tvContacts);
                                if (count > 500)
                                    tvContact.setText("500+");
                                else
                                    tvContact.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                        ProfileView.this.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                ProfileView.this.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void requestCountPub() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + id + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_pub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvPub = (TextView) ProfileView.this.findViewById(R.id.tvPub);
                                tvPub.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                        ProfileView.this.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                ProfileView.this.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void requestCountActu() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + id + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_actu_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvActu = (TextView) ProfileView.this.findViewById(R.id.tvActu);
                                tvActu.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                        ProfileView.this.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                ProfileView.this.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void loadActu() {
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.myact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("path_photo");
                                        } catch (Exception e) {
                                        }

                                        String video = "";
                                        try {
                                            video = obj.getString("path_video");
                                        } catch (Exception e) {
                                        }

                                        int nCmt = obj.getInt("nbr_comment");
                                        List<WISCmt> cmts = new ArrayList<>();
                                        if (nCmt > 0) {
                                            JSONArray aCmts = obj.getJSONArray("comment");
                                            for (int j = 0; j < aCmts.length(); j++) {
                                                try {
                                                    JSONObject ocmt = aCmts.getJSONObject(j);
                                                    JSONObject oUser = ocmt.getJSONObject("created_by");
                                                    cmts.add(new WISCmt(ocmt.getInt("id_comment"), new WISUser(oUser.getInt("idprofile"), oUser.getString("name"), oUser.getString("photo")), ocmt.getString("text"), ocmt.getString("last_edit_at"),"false"));
                                                } catch (Exception e2) {
                                                    String x = e2.getMessage();
                                                }
                                            }
                                        }
                                        String text = "";
                                        try {
                                            text = obj.getString("text");
                                        } catch (Exception e3) {
                                        }

                                        String cmtActu = "";
                                        try {
                                            cmtActu = obj.getString("commentair_act");
                                        } catch (Exception e3) {
                                        }

                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");
                                        } catch (Exception e3) {
                                        }
                                        actuList.add(new WISActuality(0,obj.getInt("id_act"), text, "", photo, video, thumb, obj.getInt("nbr_jaime"), obj.getInt("nbr_vue"), obj.getBoolean("like_current_profil"), obj.getString("type_act"), cmts, cmtActu, obj.getString("created_at"), new WISUser(obj.getJSONObject("created_by").getInt("idprofile"), obj.getJSONObject("created_by").getString("name"), obj.getJSONObject("created_by").getString("photo")),obj.getInt("dis_like"),obj.getBoolean("dislike_current_profil"),obj.getInt("share_count"),nCmt,null,null));

                                    } catch (Exception e) {

                                    }
                                }
                                adaptActu.notifyDataSetChanged();
                            } else {
                                Toast.makeText(ProfileView.this, getResources().getString(R.string.err_no_new_actu), Toast.LENGTH_SHORT).show();
                            }

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                        } catch (JSONException e) {
//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                            //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    private void deleteDialog() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.msg_confirm_unfriend))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteFriend();
                    }

                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }

    private void deleteFriend() {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_amis", String.valueOf(id));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.deletefriend_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                finish();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    private void checkFriend() {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_amis", String.valueOf(id));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.checkfriend_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                if (response.getString("data").equals("amis"))
                                    findViewById(R.id.tvDelete).setVisibility(View.VISIBLE);
                                else
                                    findViewById(R.id.tvDelete).setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(ProfileView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(ProfileView.this, "lang_pr"));
                headers.put("token", Tools.getData(ProfileView.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }
    @Override
    public void onResume() {
        super.onResume();
        adaptActu.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void showImageGallery() {

    }

    @Override
    public void showCustomPopup(int index,int act_id,String desc) {

    }

    @Override
    public void showViewMoreOptionView(int position, int act_id) {

    }

    @Override
    public void showCustomPopup() {
    }
    @Override
    protected void onPause() {
        super.onPause();

    }
}
