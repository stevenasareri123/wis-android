package com.oec.wis.dialogs;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.renderscript.Type;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.CmtAdapter;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.ReplyCommentsListener;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.tools.ListViewScrollable;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class ActuDetails extends AppCompatActivity implements ReplyCommentsListener {
    TextView tvPlus, tvTitle, tvDesc, tvNLike, tvNView, tvCmtActu,dislike_count,comment_count,share_count,created_at,created_time;
    ImageView ivThumb, ivVideo, ivLike, ivView, ivShare,dislike;
    RelativeLayout rlThumb;
    ListViewScrollable lvCmt;
    CmtAdapter adapter;
    String uName, uPic;
    EditText etCmt;
    Popupadapter simpleAdapter, live_status_adapter;
    ArrayList<Popupelements> popupList;
    UserNotification user_notify;
    private PopupWindow popWindow;
    private final int SELECT_PHOTO = 1;
    TextView headerTitleTV;
    ImageView userNotificationView, write_status, share_loc, share_status, profile_image, camera_btn, close_btn, post_image;
    Button post_status;
    String imgPath = "";
    EditText message;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actu_details);
        loadControls();
        if (AdaptActu.SELECTED != null)
            loadData();
        setListener();
        setupPopUPView();

    }

    private void loadControls() {
        tvPlus = (TextView) findViewById(R.id.tvPlus);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
        tvNLike = (TextView) findViewById(R.id.tvNLike);
        tvNView = (TextView) findViewById(R.id.tvNView);
        ivThumb = (ImageView) findViewById(R.id.ivThumb);
        ivLike = (ImageView) findViewById(R.id.ivLike);
        ivView = (ImageView) findViewById(R.id.ivView);
        ivShare = (ImageView) findViewById(R.id.ivShare);
        ivVideo = (ImageView) findViewById(R.id.ivVideo);
        rlThumb = (RelativeLayout) findViewById(R.id.rlThumb);
        lvCmt = (ListViewScrollable) findViewById(R.id.lvCmt);
        lvCmt.setExpanded(true);
        etCmt = (EditText) findViewById(R.id.etCmt);
        tvCmtActu = (TextView) findViewById(R.id.tvCmtActu);
        dislike = (ImageView)findViewById(R.id.dislike);
        dislike_count = (TextView) findViewById(R.id.dislike_count);
         share_count = (TextView) findViewById(R.id.sharecmnt);
         comment_count = (TextView) findViewById(R.id.comment_count);
        created_at = (TextView)findViewById(R.id.created_at);
        created_time = (TextView)findViewById(R.id.tvDate);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
        try{
            font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            headerTitleTV.setTypeface(font);
            tvTitle.setTypeface(font);
            tvDesc.setTypeface(font);
            created_time.setTypeface(font);
            created_at.setTypeface(font);
            etCmt.setTypeface(font);
            tvCmtActu.setTypeface(font);




        }catch (NullPointerException ex){
            Log.e("Exception",ex.getMessage());
        }

    }

    private void requetsDetails(int idActu) {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", String.valueOf(Tools.getData(this, "idprofile")));
            jsonBody.put("id_act", String.valueOf(idActu));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqCmt = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.unpost_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String res = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ActuDetails.this, "token"));
                headers.put("lang", Tools.getData(ActuDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmt);
    }

    private void setListener() {
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("back pressed","back");
                finish();
            }
        });
        findViewById(R.id.ivSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmt();
            }
        });
        etCmt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND) {
                    sendCmt();
                }
                return true;
            }
        });
        ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = getString(R.string.server_url3) + AdaptActu.SELECTED.getVideo();
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setDataAndType(uri, "video/mp4");
                startActivity(intent);
            }
        });
        ivThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("profile click","profile click");
            }
        });
    }

    private void loadData() {
        requetsDetails(AdaptActu.SELECTED.getId());
        uName = Tools.getData(this, "firstname_prt") + " " + Tools.getData(this, "lastname_prt");
        uPic = Tools.getData(this, "photo");



        if (AdaptActu.SELECTED.getpType().equals("")) {
            rlThumb.setVisibility(View.GONE);
        } else if (AdaptActu.SELECTED.getpType().equals("partage_photo") || !TextUtils.isEmpty(AdaptActu.SELECTED.getImg())) {
            rlThumb.setVisibility(View.VISIBLE);
            ivVideo.setVisibility(View.INVISIBLE);
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + AdaptActu.SELECTED.getImg(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else if (AdaptActu.SELECTED.getpType().equals("partage_video") || !TextUtils.isEmpty(AdaptActu.SELECTED.getVideo())) {
            ivVideo.setVisibility(View.VISIBLE);
            rlThumb.setVisibility(View.VISIBLE);
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + AdaptActu.SELECTED.getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else if (AdaptActu.SELECTED.getpType().equals("partage_post")) {
            if (TextUtils.isEmpty(AdaptActu.SELECTED.getVideo())) {
                ivVideo.setVisibility(View.INVISIBLE);
            }
            if (TextUtils.isEmpty(AdaptActu.SELECTED.getImg())) {
                rlThumb.setVisibility(View.GONE);
            }
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String strCurrentDate = "Wed, 18 Apr 2012 07:55:29 +0000";
        SimpleDateFormat format_two =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        format_two= new SimpleDateFormat("dd/MM/yy");

        Log.i("created_at",AdaptActu.SELECTED.getDate());

        Date d = null,d2=null;
        String created_date="";
        try {
            d = format.parse(AdaptActu.SELECTED.getDate());
            Date newDate = d;
            created_date = format_two.format(newDate);


        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(this, new DateTime(d), true).toString();
        created_time.setText(rDate);

        created_at.setText(created_date);


        tvTitle.setText(AdaptActu.SELECTED.getTitle());
        tvTitle.setTypeface(font);
        //tvDesc.setText(AdaptActu.SELECTED.getDesc());
        tvNLike.setText(String.valueOf(AdaptActu.SELECTED.getnLike()));
        tvNView.setText(String.valueOf(AdaptActu.SELECTED.getnView()));
        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AdaptActu.SELECTED.isiLike()) {
                    AdaptActu.SELECTED.setiLike(false);
                    AdaptActu.SELECTED.setnLike(AdaptActu.SELECTED.getnLike() - 1);
                    setLike();
                } else {
                    AdaptActu.SELECTED.setiLike(true);
                    AdaptActu.SELECTED.setnLike(AdaptActu.SELECTED.getnLike() + 1);
                    setLike();

                    if(AdaptActu.SELECTED.isdislike()){
                        AdaptActu.SELECTED.setIsdislike(false);
                        AdaptActu.SELECTED.setDislike_count(AdaptActu.SELECTED.getDislike_count() - 1);

                        dislike.setImageResource(R.drawable.dislike_unselect);
                        dislike_count.setText(String.valueOf(AdaptActu.SELECTED.getDislike_count()));

                    }

                }
                tvNLike.setText(String.valueOf(AdaptActu.SELECTED.getnLike()));
                if (AdaptActu.SELECTED.isiLike())
                    ivLike.setImageResource(R.drawable.ilike);
                else
                    ivLike.setImageResource(R.drawable.like);
            }
        });
        if (AdaptActu.SELECTED.isiLike())
            ivLike.setImageResource(R.drawable.ilike);
        else
            ivLike.setImageResource(R.drawable.like);

        dislike_count.setText(String.valueOf(AdaptActu.SELECTED.getDislike_count()));

        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AdaptActu.SELECTED.isdislike()) {
                    AdaptActu.SELECTED.setIsdislike(false);
                    AdaptActu.SELECTED.setDislike_count(AdaptActu.SELECTED.getDislike_count() - 1);
                    setDisLike();
                } else {
                    AdaptActu.SELECTED.setIsdislike(true);
                    AdaptActu.SELECTED.setDislike_count(AdaptActu.SELECTED.getDislike_count() + 1);
                   setDisLike();

                    if(AdaptActu.SELECTED.isiLike()){
                        AdaptActu.SELECTED.setiLike(false);
                        AdaptActu.SELECTED.setnLike(AdaptActu.SELECTED.getnLike() - 1);

                        ivLike.setImageResource(R.drawable.like);
                        tvNLike.setText(String.valueOf(AdaptActu.SELECTED.getnLike()));

                    }

                }

                dislike_count.setText(String.valueOf(AdaptActu.SELECTED.getDislike_count()));



                if (AdaptActu.SELECTED.isdislike())
                   dislike.setImageResource(R.drawable.dislike_select);
                else
                   dislike.setImageResource(R.drawable.dislike_unselect);

            }
        });
        if (AdaptActu.SELECTED.isdislike())
            dislike.setImageResource(R.drawable.dislike_select);
        else
            dislike.setImageResource(R.drawable.dislike_unselect);



        adapter = new CmtAdapter(ActuDetails.this, AdaptActu.SELECTED.getCmts(),this);
        lvCmt.setAdapter(adapter);
        if (AdaptActu.SELECTED.getCmts().size() > 0) {
            lvCmt.setVisibility(View.VISIBLE);
        } else {
            lvCmt.setVisibility(View.GONE);
        }
        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showPop(AdaptActu.SELECTED.getId(),AdaptActu.SELECTED.getCmtActu());



                Intent i = new Intent(ActuDetails.this, SharePost.class);
                startActivity(i);
            }
        });
        if (TextUtils.isEmpty(AdaptActu.SELECTED.getCmtActu())) {
            tvCmtActu.setVisibility(View.GONE);
        } else {
            tvCmtActu.setText(AdaptActu.SELECTED.getCmtActu());
            tvCmtActu.setVisibility(View.VISIBLE);
        }
        comment_count.setText(String.valueOf(AdaptActu.SELECTED.getComment_count()));
        share_count.setText(String.valueOf(AdaptActu.SELECTED.getShare_count()));

        getAllCmts();
    }

    public void setupPopUPView() {

        popupList = new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.ic_share, getString(R.string.actuality)));
        popupList.add(new Popupelements(R.drawable.edit, getString(R.string.post_status)));
        popupList.add(new Popupelements(R.drawable.grey_chat_icon, getString(R.string.chat)));
        popupList.add(new Popupelements(R.drawable.link, getString(R.string.copy_link)));


        simpleAdapter = new Popupadapter(this, false, popupList);

        ArrayList popupLisTwo = new ArrayList<>();

        popupLisTwo.add(new Popupelements(R.drawable.ic_share, getString(R.string.write_live)));
        popupLisTwo.add(new Popupelements(R.drawable.ic_takepic, getString(R.string.photo_video)));
        popupLisTwo.add(new Popupelements(R.drawable.grey_location_icon, getString(R.string.location)));


        live_status_adapter = new Popupadapter(this, false, popupLisTwo);


    }


    public void showPop(final int act_id,final String desc){

        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                        if(position == 0){
                            sendShare(act_id,desc);
                            dialog.dismiss();
                        }
                        if(position==1){

                            showWriteStatusPopup(view);
                            dialog.dismiss();

                        }

                        if(position == 2){

                            Intent i = new Intent(ActuDetails.this,Chat.class);
                           startActivity(i);

//                            user_notify = new UserNotification();
//                            user_notify.setChatSelected(Boolean.TRUE);
//                            user_notify.setAct_id(String.valueOf(act_id));
//                            Log.i("notoficatuoin", String.valueOf(user_notify.getChatSelected()));
//                            FragChat fragment = new FragChat();
//                            FragmentManager fragmentManager =getFragmentManager();
//                            fragmentManager.beginTransaction()
//                                    .replace(R.id.frame_container, fragment).commit();

                            dialog.dismiss();

                        }
                        if(position == 3){
                            showWriteStatusPopup(view);
                            dialog.dismiss();

                        }

                    }
                })
                .setExpanded(true).setCancelable(Boolean.TRUE)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }

    public void showContactView(){
//        DialogPlus dialog = DialogPlus.newDialog(getActivity())
//                .setAdapter(simpleAdapter)
//                .setOnItemClickListener(new OnItemClickListener() {
//                    @Override
//                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//
//                        if(position == 0){
//                            sendShare(act_id,desc);
//                            dialog.dismiss();
//                        }
//                        if(position==1){
//
//                            showWriteStatusPopup(view);
//                            dialog.dismiss();
//
//                        }
//
//                        if(position == 2){
//
//                            user_notify = new UserNotification();
//                            user_notify.setChatSelected(Boolean.TRUE);
//                            user_notify.setAct_id(String.valueOf(act_id));
//                            Log.i("notoficatuoin", String.valueOf(user_notify.getChatSelected()));
//                            FragChat fragment = new FragChat();
//                            FragmentManager fragmentManager =getActivity().getFragmentManager();
//                            fragmentManager.beginTransaction()
//                                    .replace(R.id.frame_container, fragment).commit();
//
//                            dialog.dismiss();
//
//                        }
//                        if(position == 3){
//                            showWriteStatusPopup(view);
//                            dialog.dismiss();
//
//                        }
//
//                    }
//                })
//                .setExpanded(true).setCancelable(Boolean.TRUE)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
//                .create();
//        dialog.show();
    }
    public void showWriteStatusPopup(View v) {
        LayoutInflater layoutInflater = (LayoutInflater) this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.write_status_popup, null, false);


        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

//       this.setAlpha(0.4f);
//
//        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.CENTER, 0, 100);

        camera_btn = (ImageView) inflatedView.findViewById(R.id.open_gallery);
        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);
        post_image = (ImageView) inflatedView.findViewById(R.id.post_image);
        message = (EditText) inflatedView.findViewById(R.id.message);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
//                views.setAlpha(1);
            }
        });


        camera_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageLibrary();
            }
        });

        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (message.getText().toString() != "") {
                    popWindow.dismiss();
                    if (Patterns.WEB_URL.matcher(message.getText().toString()).matches()) {
                        Log.i("is valid url", message.getText().toString());
                        sendUrlPosttoServer(message.getText().toString());
                    } else {

                        if (!imgPath.equals("")) {
                            new DoUpload().execute();
                        } else {
                            doUpdate(null);
                        }
                    }


                }


            }
        });


    }
    public void showImageLibrary() {

        Log.i("showImageLibrary", "showImageLibrary");
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("showImageLibrary", "showImageLibrary");


        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                post_image.setImageBitmap(bitmap);

                final String[] proj = {MediaStore.Images.Media.DATA};
                final Cursor cursor = this.managedQuery(uri, proj, null, null,
                        null);
                final int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToLast();
                imgPath = cursor.getString(column_index);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    private void sendShare(int idAct, String desc) {

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\",\"id_act\":\"" + idAct + "\",\"commentaire\":\"" + desc + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("response parms",jsonBody.toString());
        JsonObjectRequest reqActu = new JsonObjectRequest(this.getString(R.string.server_url) + this.getString(R.string.cloneActuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response",response.toString());
                        try {
                            if (response.getString("result").equals("true")) {
//                                Toast.makeText(getString(R.string.msg_share_success), Toast.LENGTH_SHORT).show();





                            }

                        } catch (JSONException e) {

                            //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    private void updateActuData(){

//        for like

        if(AdaptActu.SELECTED.isdislike()){
            dislike.setImageResource(R.drawable.dislike_unselect);
            dislike_count.setText(String.valueOf(AdaptActu.SELECTED.getDislike_count()-1));



        }

//        for dislike
        if(AdaptActu.SELECTED.isiLike()){
            ivLike.setImageResource(R.drawable.like);
            tvNLike.setText(String.valueOf(AdaptActu.SELECTED.getnLike()-1));
        }
    }



    private void setLike() {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\", \"id_act\":\"" + AdaptActu.SELECTED.getId() + "\", \"jaime\":\"" + AdaptActu.SELECTED.isiLike() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqLike = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.like_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                            }
                        } catch (JSONException e) {
                            //Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            AdaptActu.SELECTED.setiLike(false);
                            AdaptActu.SELECTED.setnLike(AdaptActu.SELECTED.getnLike() - 1);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                AdaptActu.SELECTED.setiLike(false);
                AdaptActu.SELECTED.setnLike(AdaptActu.SELECTED.getnLike() - 1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ActuDetails.this, "token"));
                headers.put("lang", Tools.getData(ActuDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }
    private void setDisLike() {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\", \"id_act\":\"" + AdaptActu.SELECTED.getId() + "\", \"jaime\":\"" + AdaptActu.SELECTED.isdislike() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqLike = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.dislike), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                            }
                        } catch (JSONException e) {
                            //Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            AdaptActu.SELECTED.setIsdislike(false);
                            AdaptActu.SELECTED.setDislike_count(AdaptActu.SELECTED.getDislike_count() - 1);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                AdaptActu.SELECTED.setIsdislike(false);
                AdaptActu.SELECTED.setDislike_count(AdaptActu.SELECTED.getDislike_count() - 1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ActuDetails.this, "token"));
                headers.put("lang", Tools.getData(ActuDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }
    private void sendCmt() {
        if (!TextUtils.isEmpty(etCmt.getText().toString())) {
            String msg = etCmt.getText().toString();
            requetsSendCmt(AdaptActu.SELECTED.getId(), msg);
            etCmt.setText(null);
            Tools.hideKeyboard(this);
        }
    }

    private void requetsSendCmt(int idActu, final String cmt) {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", String.valueOf(Tools.getData(this, "idprofile")));
            jsonBody.put("id_act", String.valueOf(idActu));
            jsonBody.put("text", cmt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqCmt = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.send_cmt_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                int id = response.getInt("data");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String cDate = sdf.format(new Date());

                                Date date=new Date();
                                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                sourceFormat.format(date);
                                Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(),"timezone"));
                                SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                destFormat.setTimeZone(tz);

                                String usertimezone = destFormat.format(date);
                                Log.i("resilt timezone ", response.toString());
                                uName=Tools.getData(getApplicationContext(),"name");

//                                int idds =
                                AdaptActu.SELECTED.getCmts().add(new WISCmt(id, new WISUser(0, uName, uPic), cmt,usertimezone,"false"));
                                AdaptActu.SELECTED.setComment_count(AdaptActu.SELECTED.getCmts().size());
                                comment_count.setText(String.valueOf(AdaptActu.SELECTED.getCmts().size()));
                                if (AdaptActu.SELECTED.getCmts().size() > 0) {
                                    lvCmt.setVisibility(View.VISIBLE);
                                } else {
                                    lvCmt.setVisibility(View.GONE);
                                }
                                adapter.notifyDataSetChanged();
                                lvCmt.invalidateViews();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String res = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ActuDetails.this, "token"));
                headers.put("lang", Tools.getData(ActuDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmt);
    }

    private void getAllCmts() {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_act", String.valueOf(AdaptActu.SELECTED.getId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqCmts = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.allcmt_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                List<WISCmt> cmts = new ArrayList<>();
                                JSONArray aCmts = response.getJSONArray("data");
                                for (int j = 0; j < aCmts.length(); j++) {
                                    JSONObject obj = aCmts.getJSONObject(j);
                                    try {
                                        cmts.add(new WISCmt(obj.getInt("id_comment"), new WISUser(obj.getJSONObject("info_created_by").getInt("id"), obj.getJSONObject("info_created_by").getString("name"), obj.getJSONObject("info_created_by").getString("photo")), obj.getString("text"), obj.getString("last_edit_at"),"false"));
                                        AdaptActu.SELECTED.setCmts(cmts);
                                        adapter = new CmtAdapter(ActuDetails.this, AdaptActu.SELECTED.getCmts(),ActuDetails.this);

                                        lvCmt.setAdapter(adapter);
                                        if (AdaptActu.SELECTED.getCmts().size() > 0) {
                                            lvCmt.setVisibility(View.VISIBLE);
                                        } else {
                                            lvCmt.setVisibility(View.GONE);
                                        }
                                    } catch (Exception e2) {
                                        String x = e2.getMessage();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            //Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ActuDetails.this, "token"));
                headers.put("lang", Tools.getData(ActuDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmts);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void replyListener() {

    }

    @Override
    public void addComments(int p, String message, ListView replyView, String comment_id) {

    }



    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image data set",img.getString("data"));
                    doUpdate(img.getString("data"));
                    //else
                    Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
//                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            Context contect = getApplicationContext();


            return Tools.doFileUpload(getString(R.string.server_url) + getString(R.string.upload_profile_meth), imgPath, Tools.getData(getApplicationContext(), "token"));
        }
    }

    public void sendUrlPosttoServer(String url_txt){
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(getApplicationContext(), "idprofile") + "\",\"url\": \"" + url_txt + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.url_post), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("respones write status",response.toString());
//                                view.setAlpha(1);
//                                customReloadData();
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

                Toast.makeText(getApplicationContext(),getString(R.string.msg_server_error),Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }


    private void doUpdate(final String photo) {
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(this, "idprofile") + "\",\"name_img\": \"" + photo + "\",\"message\": \"" +message.getText().toString() + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.write_status), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("respones write status",response.toString());
//                                view.setAlpha(1);
//                                customReloadData();
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }
}
