package com.oec.wis.dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.CountryAdapter;
import com.oec.wis.adapters.PopupadApterText;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.entities.PopupElementsText;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISCountry;
import com.oec.wis.entities.WISLanguage;
import com.oec.wis.tools.CustomPhoneNumberFormattingTextWatcher;
import com.oec.wis.tools.OnPhoneChangedListener;
import com.oec.wis.tools.PhoneUtils;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;

public class EditProfile extends AppCompatActivity implements OnDateSetListener,AdapterView.OnItemSelectedListener {
    protected static final TreeSet<String> CANADA_CODES = new TreeSet<>();

    static {
        CANADA_CODES.add("204");
        CANADA_CODES.add("236");
        CANADA_CODES.add("249");
        CANADA_CODES.add("250");
        CANADA_CODES.add("289");
        CANADA_CODES.add("306");
        CANADA_CODES.add("343");
        CANADA_CODES.add("365");
        CANADA_CODES.add("387");
        CANADA_CODES.add("403");
        CANADA_CODES.add("416");
        CANADA_CODES.add("418");
        CANADA_CODES.add("431");
        CANADA_CODES.add("437");
        CANADA_CODES.add("438");
        CANADA_CODES.add("450");
        CANADA_CODES.add("506");
        CANADA_CODES.add("514");
        CANADA_CODES.add("519");
        CANADA_CODES.add("548");
        CANADA_CODES.add("579");
        CANADA_CODES.add("581");
        CANADA_CODES.add("587");
        CANADA_CODES.add("604");
        CANADA_CODES.add("613");
        CANADA_CODES.add("639");
        CANADA_CODES.add("647");
        CANADA_CODES.add("672");
        CANADA_CODES.add("705");
        CANADA_CODES.add("709");
        CANADA_CODES.add("742");
        CANADA_CODES.add("778");
        CANADA_CODES.add("780");
        CANADA_CODES.add("782");
        CANADA_CODES.add("807");
        CANADA_CODES.add("819");
        CANADA_CODES.add("825");
        CANADA_CODES.add("867");
        CANADA_CODES.add("873");
        CANADA_CODES.add("902");
        CANADA_CODES.add("905");
    }

    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected SparseArray<ArrayList<WISCountry>> mCountriesMap = new SparseArray<>();
    protected String mLastEnteredPhone;
    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            WISCountry c = (WISCountry) spCountry.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(c.getCountryCodeStr())) {
                return;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    protected OnPhoneChangedListener mOnPhoneChangedListener = new OnPhoneChangedListener() {
        @Override
        public void onPhoneChanged(String phone) {
            try {
                mLastEnteredPhone = phone;
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(phone, null);
                ArrayList<WISCountry> list = mCountriesMap.get(p.getCountryCode());
                WISCountry country = null;
                if (list != null) {
                    if (p.getCountryCode() == 1) {
                        String num = String.valueOf(p.getNationalNumber());
                        if (num.length() >= 3) {
                            String code = num.substring(0, 3);
                            if (CANADA_CODES.contains(code)) {
                                for (WISCountry c : list) {
                                    if (c.getPriority() == 1) {
                                        country = c;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (country == null) {
                        for (WISCountry c : list) {
                            if (c.getPriority() == 0) {
                                country = c;
                                break;
                            }
                        }
                    }
                }
                if (country != null) {
                    final int position = country.getNum();
                    spCountry.post(new Runnable() {
                        @Override
                        public void run() {
                            spCountry.setSelection(position);
                        }
                    });
                }
            } catch (NumberParseException ignore) {
            }

        }
    };
    Spinner spCountry;
    EditText etFName, etLName, etTitle, etState, etPhone, etCName, etApe, etEmail, etPwd, etRName;
    CheckBox cbTourist;
    CountryAdapter cAdapter;
    Typeface  font;
    DialogPlus dialog;
    TextView companynameTV,codeAPETV,emailtextTV,passwordTV,genderTV,activityTV,datestartTV,namerepTV,countryTV,selecttimezoneTV,telephoneTV,dropLanguageTV,languageTV;
    RelativeLayout lanUseRelLay,countryRelLay;
    ImageView lanUseIV,activityIV,countryIV,genderIV,timezoneImagview;
    TextView activityTextView;
    RelativeLayout activityRl,timezoneRL;
    TextView headerTitleTV,countryNameTV;
    ArrayList<PopupElementsText> list;
    TextView timeZoneTextview;
    EditText etDate;
    EditText dpBirth;
    public static final String DATEPICKER_TAG = "datepicker";
    TextView  tvLName,tvFName;
//    TextView



    protected void initCodes(Context context) {
        new AsyncPhoneInitTask(context).execute();
    }

    protected String validate() {
        String region = null;
        String phone = null;
        if (mLastEnteredPhone != null) {
            try {
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
                StringBuilder sb = new StringBuilder(16);
                sb.append('+').append(p.getCountryCode()).append(p.getNationalNumber());
                phone = sb.toString();
                region = mPhoneNumberUtil.getRegionCodeForNumber(p);
            } catch (NumberParseException ignore) {
            }
        }
        if (region != null) {
            return phone;
        } else {
            return null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initCodes(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_edit_profile);


        Tools.showLoader(this,getResources().getString(R.string.progress_loading));
        loadControls();
        setListener();
        loadData();
        loadProfilInfo();
        setTypefaceFont();
    }


    private void loadData() {
        etFName.setText(Tools.getData(this, "firstname_prt"));
        etLName.setText(Tools.getData(this, "lastname_prt"));
        dropLanguageTV.setText(Tools.getData(this, "lang_pr"));
        cAdapter = new CountryAdapter(this);
        spCountry.setAdapter(cAdapter);

    }



    private ArrayList  getTimezone(){
        ArrayList timeZoneList=new ArrayList<>();
        String[] ids = TimeZone.getAvailableIDs();


        for (String id : ids) {
            timeZoneList.add(displayTimeZone(TimeZone.getTimeZone(id)));

        }
        return timeZoneList;
    }

    private static String displayTimeZone(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
//            :%02d
//            :%02d
            result = String.format("(GMT+%d) %s", hours,tz.getID());
        } else {
            result = String.format("(GMT%d) %s", hours,tz.getID());
        }

        return result;

    }

    private static String displayTimeZoneTwo(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
//            :%02d
//            :%02d
            result = String.format("%s",tz.getID());
        } else {
            result = String.format("%s",tz.getID());
        }

        return result;

    }



    private void setListener() {

        lanUseRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanDropDown();
            }
        });
        dropLanguageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanDropDown();

            }
        });
        lanUseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanDropDown();

            }
        });


        activityIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDropDown();
            }
        });
        activityTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDropDown();
            }
        });
        activityRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDropDown();

            }
        });
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("calling","EDATE");
                final Calendar calendar = Calendar.getInstance();

                final com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(EditProfile.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
            }
        });
        dpBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("calling","EDATE");
                final Calendar calendar = Calendar.getInstance();

                final com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(EditProfile.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
            }
        });



        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.bSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkData()) {
                    updateProfile();
                }
            }
        });
        spCountry.setOnItemSelectedListener(mOnItemSelectedListener);
        etPhone.addTextChangedListener(new CustomPhoneNumberFormattingTextWatcher(mOnPhoneChangedListener));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (dstart > 0 && !Character.isDigit(c)) {
                        return "";
                    }
                }
                return null;
            }
        };
        etPhone.setFilters(new InputFilter[]{filter});




//         country

        countryRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDown();
            }
        });
        countryNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDown();
            }
        });
        countryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDown();
            }
        });
        timezoneRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimeZoneDropdown();
            }


        });

        genderTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderDropDown();
            }
        });
        genderIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderDropDown();
            }
        });


    }

    private void openTimeZoneDropdown() {

        final ArrayList popupList = new ArrayList();

        ArrayList timeZoneList = getTimezone();
        for(int i=0;i<timeZoneList.size();i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(timeZoneList.get(i).toString()));
        }




        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            timeZoneTextview.setText(list.get(position).getTitle());
                            timeZoneTextview.setTextColor(getResources().getColor(R.color.black));

                            Log.e("timezobe",list.get(position).getTitle());
                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }
    private void genderDropDown() {

        final ArrayList popupList = new ArrayList();

        String[] menuArray;

        menuArray = getResources().getStringArray(R.array.sexe_list);
        for(int i=0;i<menuArray.length;i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(menuArray[i]));
        }




        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            genderTV.setText(list.get(position).getTitle());
                            genderTV.setTextColor(getResources().getColor(R.color.black));
                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }
    private void openCountryDropDown() {

        final ArrayList popupList = new ArrayList();

        String[] menuArray;

        menuArray = getResources().getStringArray(R.array.count_list);
        for(int i=0;i<menuArray.length;i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(menuArray[i]));
        }




        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            countryNameTV.setText(list.get(position).getTitle());
                            countryNameTV.setTextColor(getResources().getColor(R.color.black));
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }

    private void openActivityDropDown() {

        final ArrayList popupList = new ArrayList();

        String[] menuArray;

        menuArray = getResources().getStringArray(R.array.activities_list);
        for(int i=0;i<menuArray.length;i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(menuArray[i]));
        }




        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            activityTextView.setText(list.get(position).getTitle());
                            activityTextView.setTextColor(getResources().getColor(R.color.black));
                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }

    private void loadControls() {
        etFName = (EditText) findViewById(R.id.etFName);
        etLName = (EditText) findViewById(R.id.etLName);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etState = (EditText) findViewById(R.id.etState);
        cbTourist = (CheckBox) findViewById(R.id.cbTourist);
        spCountry = (Spinner) findViewById(R.id.cFlag);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etCName = (EditText) findViewById(R.id.etCName);
        etApe = (EditText) findViewById(R.id.etApe);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPwd = (EditText) findViewById(R.id.etPwd);
        etRName = (EditText) findViewById(R.id.etRepName);
        languageTV=(TextView)findViewById(R.id.languageTV);
        companynameTV=(TextView)findViewById(R.id.tvCName);
        codeAPETV=(TextView)findViewById(R.id.tvApe);
        emailtextTV=(TextView)findViewById(R.id.tvEmail);
        passwordTV=(TextView)findViewById(R.id.passwordTV);
        activityTV=(TextView)findViewById(R.id.tvActivity);
        datestartTV=(TextView)findViewById(R.id.dpStart);
        namerepTV=(TextView)findViewById(R.id.tvRep);
        countryTV=(TextView)findViewById(R.id.tvCountry);
        selecttimezoneTV=(TextView)findViewById(R.id.selectTimeZoneTV);
        telephoneTV=(TextView)findViewById(R.id.phoneNumTV);

        lanUseRelLay=(RelativeLayout)findViewById(R.id.lanUseRelLay);
        lanUseIV=(ImageView)findViewById(R.id.lanUseIV);
        dropLanguageTV=(TextView)findViewById(R.id.dropLanguageTV);

        activityRl=(RelativeLayout)findViewById(R.id.activityRl);
        activityTextView=(TextView)findViewById(R.id.activityTextView);
        activityIV=(ImageView) findViewById(R.id.activityIV);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

        etDate=(EditText)findViewById(R.id.etDate);
        dpBirth = (EditText)findViewById(R.id.dpBirth);

        countryRelLay=(RelativeLayout)findViewById(R.id.countryRelLay);
        countryNameTV=(TextView)findViewById(R.id.countryNameTV);
        countryIV=(ImageView)findViewById(R.id.countryIV);

        genderTV=(TextView)findViewById(R.id.genderTV);
        genderIV=(ImageView)findViewById(R.id.genderIV);
//         timezone
        timezoneRL=(RelativeLayout)findViewById(R.id.timezoneRL);
        timeZoneTextview=(TextView)findViewById(R.id.timeZoneTextview);
        timezoneImagview=(ImageView)findViewById(R.id.timezoneImagview);

        tvFName  =(TextView)findViewById(R.id.tvFName);
        tvLName =(TextView)findViewById(R.id.tvLName);
    }


    //
    private void setTypefaceFont() {
        try{
            font=Typeface.createFromAsset(EditProfile.this.getAssets(),"fonts/Harmattan-R.ttf");
            etRName.setTypeface(font);
            etPwd.setTypeface(font);
            etEmail.setTypeface(font);
            etApe.setTypeface(font);
            etCName.setTypeface(font);
            etPhone.setTypeface(font);
            etLName.setTypeface(font);
            etFName.setTypeface(font);
            etTitle.setTypeface(font);
            etState.setTypeface(font);
            companynameTV.setTypeface(font);
            languageTV.setTypeface(font);
            codeAPETV.setTypeface(font);
            emailtextTV.setTypeface(font);
            passwordTV.setTypeface(font);
            activityTV.setTypeface(font);
            datestartTV.setTypeface(font);
            namerepTV.setTypeface(font);
            countryTV.setTypeface(font);
            selecttimezoneTV.setTypeface(font);
            telephoneTV.setTypeface(font);
            etDate.setTypeface(font);
            dpBirth.setTypeface(font);
            dropLanguageTV.setTypeface(font);
            countryNameTV.setTypeface(font);

            activityTextView.setTypeface(font);
            headerTitleTV.setTypeface(font);

            genderTV.setTypeface(font);

            timeZoneTextview.setTypeface(font);
            tvLName.setTypeface(font);
            tvFName.setTypeface(font);
        }catch(NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }


    }

    private void openLanDropDown() {
        final ArrayList popupList = new ArrayList();

        String[] menuArray;

        menuArray = getResources().getStringArray(R.array.country_list);
        for(int i=0;i<menuArray.length;i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(menuArray[i]));
        }




        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            dropLanguageTV.setText(list.get(position).getTitle());
                            dropLanguageTV.setTextColor(getResources().getColor(R.color.black));
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }


    private boolean checkData() {
        boolean check = true;
        if (Tools.getData(EditProfile.this, "profiletype").equals("Particular")) {
            if (TextUtils.isEmpty(etFName.getText().toString())) {
                check = false;
                etFName.setError(getString(R.string.msg_empty_fname));
            }
            if (TextUtils.isEmpty(etLName.getText().toString())) {
                check = false;
                etLName.setError(getString(R.string.msg_empty_name));
            }
        }
        if (Tools.getData(EditProfile.this, "profiletype").equals("Business")) {

        }
        if (TextUtils.isEmpty(etState.getText().toString())) {
            check = false;
            etState.setError(getString(R.string.msg_empty_state));
        }
        return check;
    }

    private void saveUData() throws JSONException {
        Tools.saveData(getApplicationContext(), "firstname_prt", etFName.getText().toString());
        Tools.saveData(getApplicationContext(), "lastname_prt", etLName.getText().toString());
        Tools.saveData(getApplicationContext(), "sexe_prt", genderTV.getText().toString());
        Tools.saveData(getApplicationContext(), "lang_pr", dropLanguageTV.getText().toString());
        Tools.saveData(getApplicationContext(), "tel_pr", etPhone.getText().toString());
        Tools.saveData(getApplicationContext(), "code_ape_etr", etApe.getText().toString());
        Tools.saveData(getApplicationContext(), "email_pr", etEmail.getText().toString());
        Tools.saveData(getApplicationContext(), "activite", activityTextView.getText().toString());


        String dateField = "";
        if (Tools.getData(EditProfile.this, "profiletype").equals("Business")) {

            Tools.saveData(getApplicationContext(), "date_birth", etDate.getText().toString());


            dateField = etDate.getText().toString();
        }

        if (Tools.getData(EditProfile.this, "profiletype").equals("Particular")) {

            dateField = dpBirth.getText().toString();

        }
        if (Tools.getData(EditProfile.this, "profiletype").equals("Associations")) {

            Tools.saveData(getApplicationContext(), "date_birth", etDate.getText().toString());
            dateField = etDate.getText().toString();

        }

        Tools.saveData(getApplicationContext(), "date_birth", dateField);
        Tools.saveData(getApplicationContext(), "country", countryNameTV.getText().toString());
        Tools.saveData(getApplicationContext(), "state", etState.getText().toString());
        Tools.saveData(getApplicationContext(), "pwd", etPwd.getText().toString());
        int tourist = 0;
        if (cbTourist.isChecked())
            tourist = 1;
        Tools.saveData(getApplicationContext(), "touriste", String.valueOf(tourist));
        Tools.saveData(getApplicationContext(), "name", etCName.getText().toString());


        String CurrentString = timeZoneTextview.getText().toString();
        String[] timezone_values = CurrentString.split(" ");

        Tools.saveData(getApplicationContext(),"timezone",timezone_values[1]);
        Tools.saveData(getApplicationContext(),"timezone_format",timezone_values[0]);

    }

    private void updateProfile() {
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(EditProfile.this, "idprofile"));
            jsonBody.put("firstname_prt", etFName.getText().toString());
            jsonBody.put("lastname_prt", etLName.getText().toString());
            jsonBody.put("special_other", "");
            jsonBody.put("code_ape_etr", etApe.getText().toString());
            jsonBody.put("place_addre", "");
            jsonBody.put("country", countryNameTV.getText().toString());
            jsonBody.put("state", etState.getText().toString());
            jsonBody.put("name", etCName.getText().toString());
            jsonBody.put("name_representant_etr", etRName.getText().toString());
            jsonBody.put("email", etEmail.getText().toString());
            jsonBody.put("password", etPwd.getText().toString());
            jsonBody.put("photo", Tools.getData(this, "photo"));
            jsonBody.put("tel", etPhone.getText().toString());
            jsonBody.put("typeaccount", Tools.getData(this, "profiletype"));
            //timezone


            String CurrentString = timeZoneTextview.getText().toString();
            String[] timezone_values = CurrentString.split(" ");


            jsonBody.put("time_zone",timezone_values[1]);
            jsonBody.put("time_zone_format", timezone_values[0]);
            jsonBody.put("language",dropLanguageTV.getText().toString());
            jsonBody.put("date_birth", dpBirth.getText().toString());
            jsonBody.put("activite", activityTextView.getText().toString());
            if (datestartTV.getVisibility() == View.VISIBLE) {

                jsonBody.put("date_birth", etDate.getText().toString());
            }
            jsonBody.put("sexe_prt", genderTV.getText().toString());
            int tourist = 0;
            if (cbTourist.isChecked())
                tourist = 1;
            jsonBody.put("touriste", String.valueOf(tourist));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Text-Put", jsonBody.toString());
        Log.i("URL", getString(R.string.server_url) + getString(R.string.profile_update_meth));
        JsonObjectRequest reqUpdate = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.profile_update_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();
                        try {
                            if (response.getString("result").equals("true")) {
                                saveUData();
                                finish();
                                Toast.makeText(EditProfile.this, getString(R.string.msg_profile_updated), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Tools.dismissLoader();
//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(EditProfile.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(EditProfile.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(EditProfile.this, "token"));
                headers.put("lang", Tools.getData(EditProfile.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqUpdate);
    }

    private void loadProfilInfo() {
        if (Tools.getData(EditProfile.this, "lang_pr").equals("fr"))
            dropLanguageTV.setText(Tools.getData(EditProfile.this, "lang_pr"));
        if (Tools.getData(EditProfile.this, "lang_pr").equals("en"))
            dropLanguageTV.setText(Tools.getData(EditProfile.this, "lang_pr"));
        if (Tools.getData(EditProfile.this, "lang_pr").equals("es"))
            dropLanguageTV.setText(Tools.getData(EditProfile.this, "lang_pr"));

        if (Tools.getData(EditProfile.this, "profiletype").equals("Business")) {
            findViewById(R.id.tvFName).setVisibility(View.GONE);
            etFName.setVisibility(View.GONE);
            findViewById(R.id.tvLName).setVisibility(View.GONE);

            findViewById(R.id.tvSexe).setVisibility(View.GONE);
            findViewById(R.id.genderURL).setVisibility(View.GONE);
            findViewById(R.id.genderView).setVisibility(View.GONE);
            genderTV.setVisibility(View.GONE);

            dpBirth.setVisibility(View.GONE);
            etLName.setVisibility(View.GONE);
            cbTourist.setVisibility(View.GONE);
            findViewById(R.id.dpStart).setVisibility(View.VISIBLE);
            etDate.setVisibility(View.VISIBLE);
            findViewById(R.id.tvApe).setVisibility(View.GONE);
            etApe.setVisibility(View.GONE);

            String birthDate = Tools.getData(EditProfile.this, "date_birth");
            etDate.setText(birthDate);

        }

        if (Tools.getData(EditProfile.this, "profiletype").equals("Particular")) {
            findViewById(R.id.tvCName).setVisibility(View.GONE);
            etCName.setVisibility(View.GONE);
            findViewById(R.id.tvApe).setVisibility(View.GONE);
            etApe.setVisibility(View.GONE);
            findViewById(R.id.tvRep).setVisibility(View.GONE);
            etRName.setVisibility(View.GONE);

            findViewById(R.id.tvActivity).setVisibility(View.GONE);
            findViewById(R.id.activityRl).setVisibility(View.GONE);
            findViewById(R.id.activityView).setVisibility(View.GONE);
            activityTextView.setVisibility(View.GONE);

            findViewById(R.id.dpStart).setVisibility(View.GONE);
            etDate.setVisibility(View.GONE);



        }
        if (Tools.getData(EditProfile.this, "profiletype").equals("Associations")) {
            findViewById(R.id.tvFName).setVisibility(View.GONE);
            etFName.setVisibility(View.GONE);
            findViewById(R.id.tvLName).setVisibility(View.GONE);
            etLName.setVisibility(View.GONE);

            dpBirth.setVisibility(View.GONE);

            findViewById(R.id.dpStart).setVisibility(View.VISIBLE);
            etDate.setVisibility(View.VISIBLE);

            findViewById(R.id.tvSexe).setVisibility(View.GONE);
            findViewById(R.id.genderURL).setVisibility(View.GONE);
            findViewById(R.id.genderView).setVisibility(View.GONE);
            genderTV.setVisibility(View.GONE);

            cbTourist.setVisibility(View.GONE);

            findViewById(R.id.tvApe).setVisibility(View.GONE);
            etApe.setVisibility(View.GONE);

            findViewById(R.id.tvRep).setVisibility(View.GONE);
            etRName.setVisibility(View.GONE);

            String birthDate = Tools.getData(EditProfile.this, "date_birth");
            etDate.setText(birthDate);


        }

        System.out.println("=================lang========");
        Log.e("Langsas",Tools.getData(EditProfile.this, "lang_pr"));
        System.out.println("=================lang========");
        dropLanguageTV.setText(Tools.getData(EditProfile.this, "lang_pr"));
        etRName.setText(Tools.getData(EditProfile.this, "name_representant_etr"));
        etCName.setText(Tools.getData(EditProfile.this, "name"));
        etApe.setText(Tools.getData(EditProfile.this, "code_ape_etr"));
        etEmail.setText(Tools.getData(EditProfile.this, "email_pr"));
        etPwd.setText(Tools.getData(EditProfile.this, "pwd"));
        activityTextView.setText(Tools.getData(EditProfile.this, "activite"));



        String birthDate = Tools.getData(EditProfile.this, "date_birth");
        etDate.setText(birthDate);
        String startDate = Tools.getData(EditProfile.this, "start_act");
        dpBirth.setText(birthDate);

        Log.e("BirthDate",Tools.getData(EditProfile.this, "date_birth"));
        Log.e("StartDate",Tools.getData(EditProfile.this, "start_act"));



        countryNameTV.setText(Tools.getData(EditProfile.this, "country"));
        etState.setText(Tools.getData(EditProfile.this, "state"));
        String phone = Tools.getData(EditProfile.this, "tel_pr");
        etPhone.setText(phone);
        String timeZone = Tools.getData(getApplicationContext(),"timezone");
        String format =  Tools.getData(getApplicationContext(),"timezone_format");

        timeZoneTextview.setText(format+" "+timeZone);


        if (Tools.getData(EditProfile.this, "touriste").equals("1"))
            cbTourist.setChecked(true);
//        if (Tools.getData(EditProfile.this, "sexe_prt").equals("Femme"))
//            ((RadioButton) findViewById(R.id.rFemal)).setChecked(true);

        genderTV.setText(Tools.getData(EditProfile.this, "sexe_prt"));
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
    @Override
    public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
//        Toast.makeText(NewAds.this, "new date:" + year + "-" + month + "-" + day, Toast.LENGTH_LONG).show();
//         String date="Picked Date:" + day + "-"+ month + "-" + year;

        String date = year + "-" + (month+1) + "-" + day;
//        distance.replace("KM","")
        System.out.println("picked date is" +date.replace("Picked Date:",""));
        etDate.setText(date);
        dpBirth.setText(date);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        final ArrayList popupList = new ArrayList();

        popupList.add("FRENCH");
        popupList.add("ENGLISH");
        popupList.add("SPANISH");

        Popupadapter simpleAdapter = new Popupadapter(EditProfile.this, false, popupList);


        dialog = DialogPlus.newDialog(EditProfile.this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
//                            Intent i = new Intent(Intent.ACTION_PICK,
//                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            startActivityForResult(i, 1);
//                            dialog.dismiss();


                        }
                        else if(position ==1){

//                            openWisMedia(view,0,"photo");


                        }
                        else if(position == 2){
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent,REQUEST_CAMERA);
                            dialog.dismiss();

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<WISCountry>> {

        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<WISCountry> doInBackground(Void... params) {
            ArrayList<WISCountry> data = new ArrayList<>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    WISCountry c = new WISCountry(mContext, line, i);
                    data.add(c);
                    ArrayList<WISCountry> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
//            if (!TextUtils.isEmpty(etPhone.getText().toString())) {
//                return data;
//            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<WISCountry> list = mCountriesMap.get(code);
            if (list != null) {
                for (WISCountry c : list) {
                    if (c.getPriority() == 0) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<WISCountry> data) {
            cAdapter.addAll(data);
            if (mSpinnerPosition > 0) {
                spCountry.setSelection(mSpinnerPosition);
            }
        }
    }

}

