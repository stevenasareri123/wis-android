package com.oec.wis.dialogs;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.GalleryAdapter;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.tools.Tools;

public class PhotoGallery extends AppCompatActivity {
    GalleryAdapter adapter;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_photo_gallery);

        adapter = new GalleryAdapter(this, FragPhoto.photos);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.setCurrentItem(getIntent().getExtras().getInt("p"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
