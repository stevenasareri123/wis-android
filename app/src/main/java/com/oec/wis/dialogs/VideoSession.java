package com.oec.wis.dialogs;

import android.view.View;

/**
 * Created by asareri08 on 24/04/17.
 */

public class VideoSession {
    View videoFrame;
    int id;
    String name;

    public VideoSession(View videoFrame, int id, String name) {
        this.videoFrame = videoFrame;
        this.id = id;
        this.name = name;
    }

    public View getVideoFrame() {
        return videoFrame;
    }

    public void setVideoFrame(View videoFrame) {
        this.videoFrame = videoFrame;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}


