package com.oec.wis.dialogs;

import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.android.gms.drive.UserMetadata;

import com.google.android.gms.vision.barcode.Barcode;
import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.ApplicationController;
import com.oec.wis.Interfaces.VoiceListener;
import com.oec.wis.R;
import com.oec.wis.Subscribe;
import com.oec.wis.adapters.AudioAdapter3;
import com.oec.wis.adapters.ChatAdapter;
import com.oec.wis.adapters.CustomPhotoAdapter;
import com.oec.wis.adapters.GalleryListener;
import com.oec.wis.adapters.PhotoAdapter2;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.adapters.VideoAdapter2;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.chatApi.ChatListener;
import com.oec.wis.chatApi.ChatTextListener;
import com.oec.wis.chatApi.UserData;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISMusic1;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISUser;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.fragments.FragFriends;
import com.oec.wis.fragments.FragHome;
import com.oec.wis.groupcall.GroupCallMainActivity;
import com.oec.wis.singlecall.SingleCallMainActivity;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNLogVerbosity;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconTextView;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.rockerhieu.emojicon.emoji.Objects;

import junit.framework.Assert;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.leolin.shortcutbadger.ShortcutBadger;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Chat extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener,GalleryListener,ChatTextListener,VoiceListener {
    private static final String TAG = " ";
    public static ListView lvChat;
    public static List<WISChat> data;
    public static ChatAdapter adapter;
    public static int fId, groupId;
    public static Activity context;
    public static Dialog dPickMedia;
    static EmojiconEditText etMsg;
    public static ListView lvFriend;
    public static String receiverFirstName;
    Boolean is_group;
    public static Dialog dialoge;
    EditText searchFilter;
    ImageView phoneIv;

    TextView groupName;
    Button next;
    ImageButton close;
    ImageView groupIcon;
    ListView groupChatFriendList;
    List<WISUser> friendList;
    SelectFriendsAdapter selectFriendsAdapter;
    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

    public static List<WISPhoto> photos;
    public static List<WISVideo> video;
    public static DialogPlus dialog;
    Boolean WISPHOTOSELECTED;
    String WISPHOTOPATH;
    public static PopupWindow popWindow;
    ImageView close_btn;
    static final int REQUEST_CAMERA = 2;

    String imgPath = "";

    String currentChannel;



    String chatType;

    private static String channel;

    String strResponse;
    String contentType;

    PubNub pubnub;
    String message = null;

    Fragment fragment = null;

    String filepath;
    public static Context contexte=null;
    TextView headerTitleTV, dateTV;


    JSONObject obj;

    String AudioSavePathInDevice = null;
    public static String fileUrl;
    MediaRecorder mediaRecorder;
    Random random = null;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;
    String messageCallDisconect=null;


    List<WISChat> chatList, groupList;
    List<WISUser> userList;
    UserNotification userNotification;

    String current_groupName;
    public static String receiverImage;


    public static String  CallerName="ABC";

    public static String AudioId;

    public static String getAudioMessage() {
        return AudioMessage;
    }

    public static void setAudioMessage(String audioMessage) {
        AudioMessage = audioMessage;
    }

    public static String AudioMessage;

    public static String getAudioId() {
        return AudioId;
    }

    public static void setAudioId(String audioId) {
        AudioId = audioId;
    }

    String voicePath;
    String type_message;

    ArrayList popupList;




    static final int REQUEST_VIDEO_CAPTURE = 10;

    static final int REQUEST_VIDEO_PICKER = 1010;

    public static View rootView;

    private android.os.Handler myHandler = new android.os.Handler();



    //    MediaPlayer mediaplayer;
    public void hideKeyboard() {

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }






    public static void sendMsg(final String msg, int id, final String type, final String thumb, final String chatType) {

        JSONObject jsonBody = new JSONObject();
        context.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_profil_dest", String.valueOf(id));
            jsonBody.put("message", msg);
            jsonBody.put("type_message", type);
            jsonBody.put("chat_type", chatType);

            Log.i("json", jsonBody.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqSend = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.sendmsg_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                Calendar c = Calendar.getInstance();
                                Log.i("test image", response.toString());
                                String rDate = DateUtils.getRelativeTimeSpanString(c.getTimeInMillis(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS).toString();

//                                override date and time
                                Date date = new Date();
                                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                sourceFormat.format(date);
                                Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                TimeZone tz = TimeZone.getTimeZone(Tools.getData(context, "timezone"));
                                SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                destFormat.setTimeZone(tz);

                                String result = destFormat.format(date);
                                Log.i("resilt timezone ", result);

//                                int currentUser= Integer.parseInt(Tools.getData(context, "idprofile"));

                                String lastmessage = msg;

                                if(type.equals("audio")){

                                    lastmessage = getAudioMessage();
                                }
                                WISChat msg = new WISChat(response.getInt("data"), new WISUser(0, context.getString(R.string.me), "", Tools.getData(context, "photo"), 0, "", "", ""), lastmessage, result, true, type, thumb, "");
                                data.set(data.size() - 1, msg);
//                                adapter.notifyDataSetChanged();
//                              data.add(data.size(), msg);

//                                adapter.notifyDataSetChanged();
//                                lvChat.invalidateViews();
//                                lvChat.refreshDrawableState();
//                                etMsg.setText("");
//                                lvChat.setSelection(data.size() - 1);
                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                            context.findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            context.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                context.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }

    @Override
    protected void onPause() {
        super.onPause();

        ChatApi.getInstance().setIsChatView(Boolean.FALSE);

        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

    }

    @Override
    protected void onResume() {
        super.onResume();

        ChatApi.getInstance().setIsChatView(Boolean.TRUE);

        Log.e("Show voice Chat.java::", String.valueOf(ApplicationController.getInstance().isShowVoicePopup()));

        if(ApplicationController.getInstance().isShowVoicePopup()){

            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

            showpopup();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        Tools.showLoader(this,getResources().getString(R.string.progress_loading));
        setContentView(R.layout.activity_chat);
        loadControls();
        setChatControls();
        setUP();


        rootView = getWindow().getDecorView().getRootView();

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
        updateAppisUserSubscribe(false);
        random = new Random();

        if (getIntent().getExtras() != null) {
            fId = getIntent().getExtras().getInt("id");



            receiverFirstName = getIntent().getExtras().getString("name");

            System.out.println("name***" + receiverFirstName);

            receiverImage = getIntent().getExtras().getString("image");


            System.out.println("image**" + receiverImage);


            is_group = getIntent().getExtras().getBoolean("is_group");

            if(is_group){

                current_groupName = getIntent().getExtras().getString("groupName");
            }

            currentChannel = getIntent().getExtras().getString("currentChannel");
            channel = currentChannel;


            System.out.println("PUBLIC CHANEEL MEBERS");

//            Log.d("Channel Mebers", String.valueOf(getIntent().getStringArrayExtra("group_mebers")));

            if (is_group) {
                loadData(fId, "Groupe");
            } else {

                loadData(fId, "Amis");
            }

        }


        context = this;



//       ChatApi.getInstance().setListener(this);
        ChatApi.getInstance().subscribe();

        ChatApi.getInstance().setChatTextListener(this);


        phoneIv = (ImageView) findViewById(R.id.phoneIv);
        phoneIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                ChatApi.getInstance().setUserCurrentChannel(currentChannel);

//                    Intent it = new Intent(Chat.this, Phone.class);
//                    it.putExtra("id", fId);
//                    it.putExtra("receiver_name", receiverFirstName);
//                    it.putExtra("image", receiverImage);
//                    it.putExtra("actual_channel", currentChannel);
//                    it.putExtra("is_group", Boolean.FALSE);
//                    startActivity(it);
                Log.e("IconClick", "SingleCallMainActivity");

                if(is_group!=null && is_group){


                    Intent groupVideoCall = new Intent(Chat.this, GroupCallMainActivity.class);

                    groupVideoCall.putExtra("id", fId);
                    groupVideoCall.putExtra("receiver_name", receiverFirstName);
                    groupVideoCall.putExtra("image", receiverImage);
                    groupVideoCall.putExtra("actual_channel", currentChannel);
                    groupVideoCall.putExtra("is_group", Boolean.TRUE);
                    groupVideoCall.putExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(currentChannel));
                    groupVideoCall.putExtra("senderName",current_groupName);

                    String mememberArray = ChatApi.getInstance().getGroupMembersBychannel(currentChannel).get(0);


                    Log.d("Current group channel",currentChannel);

                    Log.d("Current group dict", String.valueOf(ChatApi.getInstance().getGroupMembersDetails()));

                    Log.d("Current members_details",ChatApi.getInstance().getGroupMembersBychannel(currentChannel).get(0));


                    String[] stringArray = new String[] {mememberArray};

                    groupVideoCall.putExtra("members_details",stringArray);



                    groupVideoCall.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(groupVideoCall);


                }else{

                    Log.e("receiverFirstName",receiverFirstName);

                    String ids = String.valueOf(fId);
                    Intent it = new Intent(Chat.this, SingleCallMainActivity.class);
                    it.putExtra("receiverId", ids);
                    it.putExtra("senderImage", Tools.getData(Chat.this, "photo"));
                    it.putExtra("activate_reject_btn","NO");
                    it.putExtra("receiver_name", receiverFirstName);
                    it.putExtra("ReceiverImage", receiverImage);
                    it.putExtra("actual_channel", currentChannel);
                    it.putExtra("is_group", Boolean.FALSE);
                    startActivity(it);
                }








            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        data = null;
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
        ApplicationController.getInstance().setShowVoicePopup(false);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setChatControls() {
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserNotification notification = new UserNotification();
                notification.setChatSelected(Boolean.FALSE);
                finish();
            }
        });
        findViewById(R.id.ivSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(etMsg.getText().toString())) {
                    Log.i("test smiley", etMsg.getText().toString());

                    String chatType;
                    String receiverID;

                    if (is_group) {
//                    sendMsg(etMsg.getText().toString(), fId, "text","","Groupe");

                        chatType = "Groupe";

                    } else {

                        chatType = "Amis";

                    }


                    Map<String, String> json = new HashMap<>();
                    json.put("message", etMsg.getText().toString());
                    json.put("chatType", chatType);
                    json.put("receiverID", String.valueOf(fId));
                    json.put("contentType", "text");
                    json.put("senderID", Tools.getData(Chat.this, "idprofile"));
                    json.put("senderName", Tools.getData(Chat.this, "name"));
                    String senderPhoto = Tools.getData(Chat.this, "photo");
                    json.put("isVideoCalling","no");
                    json.put("senderImage", senderPhoto);

                    json.put("actual_channel", currentChannel);

                    ChatApi.getInstance().sendMessage(json, currentChannel);

                    hideKeyboard();




                }
            }
        });

//        View myView = findViewById(R.id.llMode);
//
//        // get the center for the clipping circle
//        int cx = (myView.getLeft() + myView.getRight()) / 2;
//        int cy = (myView.getTop() + myView.getBottom()) / 2;
//
//        // get the final radius for the clipping circle
//        int dx = Math.max(cx, myView.getWidth() - cx);
//        int dy = Math.max(cy, myView.getHeight() - cy);
//        float finalRadius = (float) Math.hypot(dx, dy);
//
//        final Animator animator =
//                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
//        animator.setInterpolator(new AccelerateDecelerateInterpolator());
//        animator.setDuration(1500);


        findViewById(R.id.ivPickMedia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Android native animator

                openMedia();

          //                pickMedia();


            }
        });

        etMsg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etMsg.getRight() - etMsg.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
                            findViewById(R.id.emojicons).setVisibility(View.GONE);
                        else
                            findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });

    }


//    /    /        Chat api

//    @Override
//    public void didReceiveMessage(PNMessageResult messageResult) {
//
//
//        System.out.println("listner for message");
//
//        Log.i("Chat :: Message :::", messageResult.getMessage().toString());
//
//
//        Log.d("current Channel", currentChannel);
//
//        Log.d("subscribed channel", messageResult.getChannel());
//
//        String receivedChannel = messageResult.getChannel();
//        String senderId = null;
//        String senderName = null;
//        String receiverId = null;
//        String currentUserId=null;
//        if (receivedChannel.equals(currentChannel)) {
//
//            try {
//
//
//                JSONObject jsonData = new JSONObject(String.valueOf(messageResult.getMessage()));
//
//
//                Log.d("json res", String.valueOf(jsonData));
//
//
//                senderId = jsonData.getString("senderID");
//
//                receiverId = jsonData.getString("receiverID");
//
//                 currentUserId = Tools.getData(Chat.this, "idprofile");
//
//                  message = jsonData.getString("message");
//
////                messageCallDisconect=jsonData.getString("message");
//
//                String chatType = jsonData.getString("chatType");
//
//                senderName = jsonData.getString("senderName");
//
//                String senderIcon = jsonData.getString("senderImage");
//
//                String contentType = jsonData.getString("contentType");
//
//
//                System.out.println("message------->" +message);
//
//                System.out.println("senderId------->" +senderId);
//                System.out.println("receiverId------->" +receiverId);
//
//                System.out.println("ownerId------->" +currentUserId);
//
//
//                if (senderId.equals(currentUserId)) {
//
////                send message
//
//                    System.out.println("Send message");
//
//
//                    sendMessageToChatList(message, contentType, chatType);
//
//                } else {
//
////                receive meessage
//
//                    System.out.println("receive meessage");
//
//                    receiveMessage(message, chatType, Integer.parseInt(senderId), senderName, senderIcon, contentType);
//                }
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        } else {
//
//            Log.d("show", "notification");
//
//
//            showAlert(message, senderName);
//
//
//        }
//
//    }

    public void setUP(){

        popupList = new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.close_grey,"UnHide"));
        popupList.add(new Popupelements(R.drawable.close_grey, getString(R.string.Hide)));
        popupList.add(new Popupelements(R.drawable.write_status, getString(R.string.Edit)));
        popupList.add(new Popupelements(R.drawable.download_grey, getString(R.string.Save)));
        popupList.add(new Popupelements(R.drawable.close_grey, getString(R.string.Hide)));
        popupList.add(new Popupelements(R.drawable.download_grey, getString(R.string.Save)));
    }


    public void showMediaOption(){





        Popupadapter  simpleAdapter = new Popupadapter(Chat.this, false, popupList);

        DialogPlus dialog = DialogPlus.newDialog(Chat.this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//
//                                if (position == KeyEvent.KEYCODE_BACK) {
//                                    getActivity().finish();
//                                    dialog.dismiss();
//                                }

                        if (position == 0) {
                            dialog.dismiss();


                        } else if (position == 1) {
                            dialog.dismiss();


                        } else if (position == 2) {
                            dialog.dismiss();
                            Log.i("WIS chat", "chat click");


                        } else if (position == 3) {
                            dialog.dismiss();


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();

    }


    public void showAlert(String message, String senderName) {

        Log.i("notification", "alert");

        Tools.showNotif2(this, 0, senderName, message);

    }

//    @Override
//    public void didReceivePresenceEvent(PNPresenceEventResult eventResult) {
//
//
//        Log.d("presence event channel", eventResult.getChannel());
//
//        Log.d("presence event", eventResult.getState().toString());
//
//    }
//
//    @Override
//    public void onPublishSuccess(PNStatus status) {
//
//        if (!status.isError()) {
//
//            Log.d("message posted success", String.valueOf(status));
//
////            uplod message to server here
//
//        }
//
//
//    }


    public void sendMessageToChatList(final String sendMesage, final String contentType, final String type) {


//        override date and time
        Date date = new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);
        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

        TimeZone tz = TimeZone.getTimeZone(Tools.getData(context, "timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(date);

        if (ChatApi.getInstance().getIsChatView()) {


            WISChat msg = new WISChat(1, new WISUser(0, context.getString(R.string.me), "", Tools.getData(context, "photo"), 0, "", "", ""), sendMesage, result, true, contentType, sendMesage, "");
            data.add(data.size(), msg);


            lvChat.post(new Runnable() {
                public void run() {
//                    context.findViewById(R.id.loading).setVisibility(View.GONE);
                    etMsg.setText("");
                    adapter.notifyDataSetChanged();
//                    lvChat.invalidateViews();
//                    lvChat.refreshDrawableState();
                    lvChat.setSelection(data.size() - 1);


                    Chat.sendMsg(sendMesage, Chat.fId, contentType, "", type);
                }
            });




        }


    }








    public void receiveMessage(String receivedMessage, String chatType, int senderId, String senderName, String senderIcon, String contentType) {


        //                                    override date and time
        Date date = new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);


        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(), "timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(date);

        if (ChatApi.getInstance().getIsChatView()) {


            Chat.data.add(new WISChat(0, new WISUser(senderId, senderName, senderIcon), receivedMessage, result, false, contentType, receivedMessage, ""));


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //stuff that updates ui

//                    context.findViewById(R.id.loading).setVisibility(View.GONE);
                    Chat.adapter.notifyDataSetChanged();
//                    Chat.lvChat.invalidateViews();
                    Chat.lvChat.setSelection(Chat.data.size() - 1);


                }
            });

        }


    }


//    /    /        Chat api end

    private void loadControls() {

        lvChat = (ListView) findViewById(R.id.lvChat);
        registerForContextMenu(lvChat);
        data = new ArrayList<>();
        adapter = new ChatAdapter(this, data);

        headerTitleTV =(TextView)findViewById(R.id.headerTitleTV);
        dateTV =(TextView)findViewById(R.id.dateTV);
        adapter.setvoiceListener(this);

        lvChat.setAdapter(adapter);

        etMsg = (EmojiconEditText) findViewById(R.id.etMsg);

        etMsg.setUseSystemDefault(true);
        setEmojiconFragment(true);

        try{
            Typeface font = Typeface.createFromAsset(Chat.this.getAssets(),"fonts/Harmattan-R.ttf");
            dateTV.setTypeface(font);
            headerTitleTV.setTypeface(font);
            etMsg.setTypeface(font);
        }catch (NullPointerException ex){

        }


    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.lvChat) {
            try {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                if (data.get(info.position).isMe()) {

                    menu.add(Menu.NONE, 100, Menu.NONE, getString(R.string.Remove));
                    menu.add(Menu.NONE, 200, Menu.NONE, getString(R.string.transferer));
                    menu.add(Menu.NONE, 300, Menu.NONE, getString(R.string.copy));
                } else {
                    menu.add(Menu.NONE, 100, Menu.NONE, getString(R.string.Remove));
                    menu.add(Menu.NONE, 300, Menu.NONE, getString(R.string.copy));
                    menu.add(Menu.NONE, 200, Menu.NONE, getString(R.string.transferer));


                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 100:
                deleteMsg(data.get(info.position).getId());
                data.remove(info.position);
                adapter.notifyDataSetChanged();
                lvChat.invalidateViews();
                lvChat.refreshDrawableState();
                return true;

            case 200:
                showGroupChatView(String.valueOf(data.get(info.position).getId()), data.get(info.position).getMsg());
                return true;

            case 300:
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("text label", data.get(info.position).getMsg());
                clipboard.setPrimaryClip(clip);


            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteMsg(int id) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_message", String.valueOf(id));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest reqDelete = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.deletechat_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(Chat.this, "lang_pr"));
                headers.put("token", Tools.getData(Chat.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        Log.i("test", "pressed");
        EmojiconsFragment.input(etMsg, emojicon);
        findViewById(R.id.emojicons).setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        Log.i("test", "back pressed");
        EmojiconsFragment.backspace(etMsg);
    }

    private void loadGroupMessages(int groupId) {
        Log.i("goup message", "group message");
        Log.i("goup message", String.valueOf(groupId));

    }

    public void loadData(int id, final String chat_type) {
        Log.d("Chat history::","LOAD DATA");

//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\":\"" + Tools.getData(Chat.this, "idprofile") + "\",\"id_ami\":\"" + id + "\",\"chat_type\":\"" + chat_type + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("json parms", String.valueOf(jsonBody));

        String apiUrl = null;
        if (chat_type.equals("Groupe")) {
            apiUrl = getString(R.string.server_url) + getString(R.string.group_history);
        } else {
            apiUrl = getString(R.string.server_url) + getString(R.string.histo_meth);
        }
        JsonObjectRequest reqDisc = new JsonObjectRequest(apiUrl, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("CHAT HISTORY RES::", String.valueOf(response));

                        loadFriends(context);

                        try {
                            if (response.getString("result").equals("true")) {
                                JSONArray data = response.getJSONArray("data");
                                Log.i("JSON OBJ", String.valueOf(data));

//                                String idProfile=obj.getString("id_profil");
//                                String nom=obj.getString("nom");
//                                String photo=obj.getString("photo");
//                                System.out.println("idProfile" +idProfile);
//                                System.out.println("nom" +nom);
//                                System.out.println("photo" +photo);

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        obj = data.getJSONObject(i);
                                        boolean isMe = false;
                                        isMe = Tools.getData(Chat.this, "idprofile").equals(obj.getJSONObject("created_by").getString("id_profil"));
                                        String thumb = "";


//                                        try {
//                                            thumb = obj.getString("photo_video");
//                                        } catch (Exception e) {
//                                        }

                                        try {
                                           thumb = obj.getString("photo_video");
                                            } catch (Exception e) {
                                            }
                                        try {
                                            thumb = obj.getString("music_logo");
                                           } catch (Exception e) {
                                           }

                                        Log.e("TYPE",obj.getString("type_message"));

                                        Log.e("Mesage",obj.getString("message"));

                                        Chat.data.add(new WISChat(obj.getInt("id_message"), new WISUser(obj.getJSONObject("created_by").getInt("id_profil"), obj.getJSONObject("created_by").getString("nom"), obj.getJSONObject("created_by").getString("photo")), obj.getString("message"), obj.getString("created_at"), isMe, obj.getString("type_message"), thumb, ""));
                                        adapter.notifyDataSetChanged();
                                        lvChat.invalidateViews();
                                        lvChat.refreshDrawableState();
                                    } catch (Exception e) {

                                    }
                                    lvChat.setSelection(Chat.data.size() - 1);
                                }
                            }

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
                            Tools.dismissLoader();

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Chat.this, "token"));
                headers.put("lang", Tools.getData(Chat.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDisc);
    }


    private void pickMedia() {
        dPickMedia = new Dialog(this);
        dPickMedia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dPickMedia.setContentView(R.layout.dialog_pick_media);
        final GridView gvMedia = (GridView) dPickMedia.findViewById(R.id.gvMedia);
        dPickMedia.findViewById(R.id.ivImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.GONE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.VISIBLE);
                gvMedia.setVisibility(View.VISIBLE);
                dPickMedia.dismiss();

                openMedia();
//                loadPhotos(gvMedia);
            }
        });
        dPickMedia.findViewById(R.id.ivVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.GONE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.VISIBLE);
                gvMedia.setVisibility(View.VISIBLE);
                loadVideos(gvMedia);
//                new DoUpload().execute("");
            }
        });
        dPickMedia.findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.VISIBLE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.GONE);
                gvMedia.setVisibility(View.GONE);

            }
        });


        dPickMedia.findViewById(R.id.ivPhoneCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.VISIBLE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.GONE);

                pickMediaAudio();


            }
        });
        dPickMedia.show();
    }

    private void voiceDialog() {

        // custom dialog
        dialoge = new Dialog(context);
        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialoge.setContentView(R.layout.dialog_voice_record);
        // Custom Android Allert Dialog Title
        dialoge.setTitle("Custom Dialog Example");

        final Button buttonok = (Button) dialoge.findViewById(R.id.buttonok);
        Button buttoncancel = (Button) dialoge.findViewById(R.id.buttoncancel);

        buttonok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });
        buttoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialoge.cancel();

            }
        });

        dialoge.show();

    }


    private void pickMediaAudio() {


        // custom dialog
        dialoge = new Dialog(context);
        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialoge.setContentView(R.layout.dialog_pick);
        // Custom Android Allert Dialog Title
        dialoge.setTitle("Custom Dialog Example");

        final Button buttonstop = (Button) dialoge.findViewById(R.id.buttonstop);
        Button buttonplay = (Button) dialoge.findViewById(R.id.buttonplay);
        final Button buttonsendfile = (Button) dialoge.findViewById(R.id.buttonsendfile);
        final Button startButton = (Button) dialoge.findViewById(R.id.startButton);

        final ImageView microphoneIV = (ImageView) dialoge.findViewById(R.id.microphoneIV);
        final ImageView no_microphoneIV = (ImageView) dialoge.findViewById(R.id.no_microphoneIV);


        // Click cancel to dismiss android custom dialog box
        microphoneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                microphoneIV.setVisibility(View.GONE);
                no_microphoneIV.setVisibility(View.VISIBLE);
//                 mediaRecorder.stop();
//                microphoneIV.setEnabled(false);
//                startButton.setEnabled(true);

                Toast.makeText(Chat.this, "Recording stopped",
                        Toast.LENGTH_LONG).show();
            }
        });
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {

                    AudioSavePathInDevice =
                            Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                    CreateRandomAudioFileName(5) + "AudioRecording.mp3";


                    File AudioFile = new File(AudioSavePathInDevice);
                    System.out.println("Audio File" + AudioFile);


//                    MediaRecorderReady();


                    mediaRecorder = new MediaRecorder();
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                    mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    mediaRecorder.setOutputFile(AudioSavePathInDevice);

                    System.out.println("Audio path" + AudioSavePathInDevice);

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    startButton.setEnabled(false);
                    buttonstop.setEnabled(true);
                    microphoneIV.setEnabled(true);

                    Toast.makeText(Chat.this, "Recording started",
                            Toast.LENGTH_LONG).show();
                } else {
                    requestPermission();
                }
            }
        });
        buttonstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startButton.setEnabled(true);


                if(mediaRecorder!=null){

                    mediaRecorder.stop();

                    buttonstop.setEnabled(false);

                    Toast.makeText(Chat.this, "Recording Completed",
                            Toast.LENGTH_LONG).show();
                }



//                buttonPlayLastRecordAudio.setEnabled(true);

//                buttonStopPlayingRecording.setEnabled(false);


//                dialog.dismiss();
//                Toast.makeText(getApplicationContext(),"button stop",Toast.LENGTH_LONG).show();
            }
        });

        // Your android custom dialog ok action
        // Action for custom dialog ok button click
        buttonplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonstop.setEnabled(false);
                startButton.setEnabled(false);
//                buttonStopPlayingRecording.setEnabled(true);

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(AudioSavePathInDevice);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaPlayer.start();
                Toast.makeText(Chat.this, "Recording Playing",
                        Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(),"button play",Toast.LENGTH_LONG).show();

//                dialog.dismiss();
            }
        });


        // Action for custom dialog ok button click
        buttonsendfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "button send file", Toast.LENGTH_LONG).show();
                dialoge.dismiss();

//                    rejectSingleCall(receiver,currentChannel,type);

//                    new DoAudioUpload().execute(AudioSavePathInDevice);

                if(AudioSavePathInDevice==null){

                    Toast.makeText(getApplicationContext(), "No Recording Found", Toast.LENGTH_LONG).show();

                    ((Activity)ChatApi.getInstance().getPublisherContext()).finish();

                }else {

                    Log.e("SENDE","Voice fiules");
                    Chat.rootView.setAlpha(1f);
                    Chat.disMissPopMenus();


                    new DoUploadVoice().execute(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.uploadAudio), AudioSavePathInDevice, Tools.getData(Chat.this, "token"), Tools.getData(Chat.this, "idprofile"),"","", String.valueOf(Chat.fId));


//                    AsyncTask.execute(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            JSONObject response =Tools.uploadVoiceToServer(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.uploadAudio), AudioSavePathInDevice, Tools.getData(Chat.this, "token"),Tools.getData(Chat.this, "idprofile"),"","", String.valueOf(Chat.fId));
//
//
//                            Log.e("JSON VOICE RESPOSE::", String.valueOf(response));
//
//
//
//                            if(response!=null){
//
//
//                            }
//
//                        }
//                    });

//                    new DoUploadVoice().execute(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.uploadVoiceMessage), AudioSavePathInDevice, Tools.getData(Chat.this, "token"), Tools.getData(Chat.this, "idprofile"),"","", String.valueOf(Chat.fId));



                }






            }
        });

        if(!((Activity) context).isFinishing())
        {
            //show dialog
            dialoge.show();
        }




    }


//
//    {
//        "actual_channel": "Private_952",
//            "chatType": "Amis",
//            "senderID": "225",
//            "message_type": "Audio",


//            "file": "file:///storage/sdcard0/LPHKBAudioRecording.mp4",


//            "contentType": "Audio",
//            "message": "file:///storage/sdcard0/LPHKBAudioRecording.mp4",
//            "senderImage": "img_user1477998338.jpg",
//            "senderName": "chat USER M",
//            "receiverID": "101"
//    }


    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);

        mediaRecorder.setOutputFile(AudioSavePathInDevice);

        System.out.println("Audio path" + AudioSavePathInDevice);
    }




    private void showpopup() {


        pickMediaAudio();


//        Handler h = new Handler(Looper.getMainLooper());
//        h.post(new Runnable() {
//            public void run() {
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Chat.this);
//
//                // Setting Dialog Title
//                alertDialog.setTitle("Audio Record...");
//
//                // Setting Dialog Message
//                alertDialog.setMessage("Are you sure you want  record your voice");
//
//                // Setting Icon to Dialog
////        alertDialog.setIcon(R.drawable.delete);
//
//                // Setting Positive "Yes" Button
//                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//
//
////                audioRecordMethod();
//                        // Write your code here to invoke YES event
//                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//                // Setting Negative "NO" Button
//                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // Write your code here to invoke NO event
//                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
//                        dialog.cancel();
//
//                        ((Activity)context).finish();
//                    }
//                });
//
//                // Showing Alert Message
//                if(!((Activity) context).isFinishing())
//                {
//                    //show dialog
//                    alertDialog.show();
//                }
//
//
//            }
//        });

    }


    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        while (i < string) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++;
        }
        return stringBuilder.toString();
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(Chat.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(Chat.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Chat.this, "Permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }


    private void loadVideos(final GridView grid) {
        final ArrayList<WISVideo> video = new ArrayList<>();
        final VideoAdapter2 adapter = new VideoAdapter2(Chat.this, video, 1);
        grid.setAdapter(adapter);
        final ProgressDialog progress = ProgressDialog.show(Chat.this, "", getString(R.string.loading_progress), true, true);
        progress.show();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(Chat.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqVideos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String chat_type = "Amis";
                                        if (is_group) {
                                            chat_type = "Groupe";
                                        }
                                        video.add(new WISVideo(obj.getInt("id"), "", obj.getString("url_video"), obj.getString("created_at"), obj.getString("image_video"), chat_type));
                                    } catch (Exception e) {
                                    }
                                }
                                if (video.size() == 0)
                                    Toast.makeText(Chat.this, getString(R.string.no_video_element), Toast.LENGTH_SHORT).show();
                                adapter.notifyDataSetChanged();
                            }
                            progress.dismiss();

                        } catch (JSONException e) {
                            progress.dismiss();
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Chat.this, "token"));
                headers.put("lang", Tools.getData(Chat.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqVideos);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (video.get(i).isSelected()) {
                    video.get(i).setSelected(false);
                } else {
                    for (WISVideo item : video) {
                        item.setSelected(false);
                    }
                    video.get(i).setSelected(true);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void loadPhotos(final GridView grid) {
        final ArrayList<WISPhoto> photos = new ArrayList<>();
        final PhotoAdapter2 adapter = new PhotoAdapter2(Chat.this, photos, 1);
        grid.setAdapter(adapter);
        final ProgressDialog progress = ProgressDialog.show(Chat.this, "", getString(R.string.loading_progress), true, true);
        progress.show();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(Chat.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").contains("any image")) {
                                Toast.makeText(Chat.this, getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
                            } else {
                                if (response.getString("result").equals("true")) {

                                    JSONArray data = response.getJSONArray("data");
                                    Log.i("photo response", String.valueOf(data));
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            String chat_type = "Amis";
                                            if (is_group) {
                                                chat_type = "Groupe";
                                            }

//                                            Map<String,String> json = new HashMap<>();
//                                            json.put("message",obj.getString("pathoriginal"));
//                                            json.put("chatType",chat_type);
//                                            json.put("receiverID", String.valueOf(fId));
//                                            json.put("contentType","photo");
//                                            json.put("senderID",Tools.getData(Chat.this,"idprofile"));
//                                            json.put("senderName",Tools.getData(Chat.this,"name"));
//                                            String senderPhoto = Tools.getData(Chat.this,"photo");
//
//                                            json.put("senderImage",senderPhoto);
//
//                                            json.put("actual_channel",currentChannel);
////
//                                            ChatApi.getInstance().sendMessage(json,currentChannel);

                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at"), chat_type));
                                        } catch (Exception e) {
                                        }
                                    }

                                    adapter.notifyDataSetChanged();

                                }
                            }
                            progress.dismiss();

                        } catch (JSONException e) {
                            progress.dismiss();
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Chat.this, "token"));
                headers.put("lang", Tools.getData(Chat.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (photos.get(i).isSelected()) {
                    photos.get(i).setSelected(false);
                } else {
                    for (WISPhoto item : photos) {
                        item.setSelected(false);
                    }
                    photos.get(i).setSelected(true);

                }
                adapter.notifyDataSetChanged();

            }
        });
    }

    private void showGroupChatView(final String chatId, final String text) {


        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.contact_list, null);


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();


        groupName = (TextView) dialogView.findViewById(R.id.groupName);
        next = (Button) alert.findViewById(R.id.bNext);
        close = (ImageButton) alert.findViewById(R.id.closeGroupchat);
        groupChatFriendList = (ListView) alert.findViewById(R.id.contactList);
        searchFilter = (EditText) alert.findViewById(R.id.searchFilter);
        loadFriendsList(groupChatFriendList);

        groupName.setText("Select Contact");

        dialogView.findViewById(R.id.groupName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                alert.dismiss();
//                openMedia();


            }

        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String message;
                if (is_group) {
                    message = "Groupe";
                } else {
                    message = "Amis";
                }

                Boolean isSuccess = selectFriendsAdapter.checkMemberExists();
                Log.i("status", String.valueOf(isSuccess));
                if (isSuccess == true) {
                    alert.dismiss();
                    selectFriendsAdapter.transferTextToContacts(message, String.valueOf(chatId), text);
                    Toast.makeText(context, "Successfully shared", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, "choose contact", Toast.LENGTH_SHORT).show();
                }


            }


        });

    }

    private void loadFriendsList(ListView groupChatFriendList) {

        selectFriendsAdapter = new SelectFriendsAdapter(context, friendList);
        groupChatFriendList.setAdapter(selectFriendsAdapter);
        selectFriendsAdapter.notifyDataSetChanged();
        loadFilterConfig(selectFriendsAdapter);


    }

    private void loadFilterConfig(final SelectFriendsAdapter adapter) {
        searchFilter.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    public void loadFriends(final Context context) {
        friendList = new ArrayList<>();

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Url", context.getString(R.string.server_url) + context.getString(R.string.friends_meth));
        JsonObjectRequest req = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.friends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", String.valueOf(response));


                        try {
                            if (response.getString("result").equals("true")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);


//                                        Log.i("response 1", String.valueOf(response));
//                                        Log.i("response 2", String.valueOf(obj));

                                        //  friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName")," ", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));


                                        friendList.add(new WISUser(Boolean.FALSE, obj.getString("blocked_by"), obj.getString("objectId"), obj.getString("activeNewsFeed"), obj.getInt("idprofile"), obj.getString("fullName"), obj.getString("photo"), obj.getInt("nbr_amis")));


                                    } catch (Exception e) {

                                    }
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);


    }


//    -----------Photo-------------


    public void openMedia() {

        final ArrayList popupList = new ArrayList();

        popupList.add(new Popupelements(R.drawable.camera_grey, "Pick Photos"));
        popupList.add(new Popupelements(R.drawable.slide_wis_photo, "WisPhotos"));
        popupList.add(new Popupelements(R.drawable.ic_takepic, "Take Photos"));
        popupList.add(new Popupelements(R.drawable.ic_pick_video, "Pick Video"));
        popupList.add(new Popupelements(R.drawable.menu_grey_video, "WisVideo"));
        popupList.add(new Popupelements(R.drawable.ic_takevideo, "Take Video"));
        popupList.add(new Popupelements(R.drawable.menu_music_grey, "WisMusic"));
        popupList.add(new Popupelements(R.drawable.record_voice, "Record Voice"));





        Popupadapter simpleAdapter = new Popupadapter(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if (position == 0) {
                            Intent i = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 1);
                            dialog.dismiss();

                        } else if (position == 1) {
                            dialog.dismiss();
                            openWisMedia(null, 0, "photo");


                        } else if (position == 2) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                            dialog.dismiss();

                        }else if(position ==3){
//                            Intent.createChooser(
                            System.out.print("Pick Video");

                            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i,REQUEST_VIDEO_PICKER);
                            dialog.dismiss();

                        }
                        else if (position == 4){

                            System.out.print("WisVideo");
                            dialog.dismiss();

                            openWisMedia(null,0, "video");
                        }
                        else if (position == 5){

                            System.out.print("Take Video");
                            dialog.dismiss();


                            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                                dialog.dismiss();
                            }

                        }
                        else if (position == 6){
                            dialog.dismiss();
                            System.out.print("WisMusic");

                            showMusicMenus();
//
//                            final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);
//
//                            loadMusic(gvMedia);

                        }else if (position == 7){

                            dialog.dismiss();
                            System.out.print("Record Voice");

                            pickMediaAudio();





                        }




                    }
                })
                .setExpanded(true)
                // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }

    public void openWisMedia(final View v,final int position,String type){

        LayoutInflater layoutInflater = (LayoutInflater) this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.media_gallery, null, false);


        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

        rootView.setAlpha(0.4f);

//        final View views = this;


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);



        TextView header = (TextView)inflatedView.findViewById(R.id.headerTitleForWindow);

        try{
            Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            header.setTypeface(font);

        }catch(NullPointerException ex){

        }




        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rootView.setAlpha(1);
                popWindow.dismiss();
                dialog.dismiss();

            }
        });

        // show the popup at bottom of the screen and set some margin at bottom ie,


        final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);

        if(type.equals("photo")){
            header.setText(getString(R.string.photo));
            loadPhotos(gvMedia);
        }
        else {
            header.setText(getString(R.string.video));
            loadVideos(gvMedia);
        }

        popWindow.showAtLocation(rootView, Gravity.CENTER, 0, 0);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Log.e("requestCode", String.valueOf(requestCode));

        if (requestCode == REQUEST_VIDEO_PICKER && resultCode == RESULT_OK) {


            Log.e("VIDEO PICKER","DELEGATE CALLED");


            Uri selectedVideo = imageReturnedIntent.getData();

            final String[] proj = {MediaStore.Images.Media.DATA};
            final Cursor cursor = this.managedQuery(selectedVideo, proj, null, null,
                    null);
            final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToLast();
            String videoPath = cursor.getString(column_index);

            new DoUploadVideo().execute(videoPath,getString(R.string.upload_video_meth));


        }

        else  if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode ==RESULT_OK) {


            Log.e("VIDEO CAPTURE","DELEGATE CALLED");

            Uri videoUri = imageReturnedIntent.getData();

            final String[] proj = {MediaStore.Images.Media.DATA};
            final Cursor cursor = this.managedQuery(videoUri, proj, null, null,
                    null);
            final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToLast();
            String videoPath = cursor.getString(column_index);

            new DoUploadsNew().execute(videoPath,getString(R.string.upload_video_meth));


        }

        else {
            switch (requestCode) {



                case 1:
                    if (resultCode == this.RESULT_OK) {
                        Uri selectedImage = imageReturnedIntent.getData();
                        InputStream imageStream = null;
                        try {
                            imageStream = this.getContentResolver().openInputStream(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        final String[] proj = {MediaStore.Images.Media.DATA};
                        final Cursor cursor = this.managedQuery(selectedImage, proj, null, null,
                                null);
                        final int column_index = cursor
                                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        cursor.moveToLast();
                        imgPath = cursor.getString(column_index);

                        Log.i("galery path",imgPath);

                    }
                    break;
                case 2:
                    if(resultCode ==this.RESULT_OK) {

                        Bitmap thumbnail = (Bitmap) imageReturnedIntent.getExtras().get("data");
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                        FileOutputStream fo;
                        try {
                            destination.createNewFile();
                            fo = new FileOutputStream(destination);
                            fo.write(bytes.toByteArray());
                            fo.close();
                        }catch (FileNotFoundException e){
                            e.printStackTrace();
                        }catch (IOException e){
                            e.printStackTrace();
                        }

                        System.out.println("Welcome"+destination);

                        //new DoUpload().execute();
                        imgPath= String.valueOf(destination);

                        Log.i("camera path",imgPath);
                    }
                    break;

                default:
                    break;
            }

            new DoUpload().execute();


        }



    }

    @Override
    public void onPlayButtonClicked(String duration) {


        new DownloadVoice().execute(getString(R.string.server_url3)+duration);


    }

    @Override
    public void onStreamingPlayed(MediaPlayer mp) {

    }

    @Override
    public void onTouchTracking() {

    }

    @Override
    public void onstopTracking() {

    }

    @Override
    public void onStartPostDelayed() {

    }


//    private class DoUploads extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            findViewById(R.id.loading).setVisibility(View.GONE);
//
//            Tools.dismissLoader();
//
//            Log.e("upload results", result);
//
////             if(result != null){
////
////             }
//            if (!result.equals("")) {
//                try {
//                    JSONObject img = new JSONObject(result);
//
//
//                    if (img.getBoolean("result")) {
//                        Log.d("upload results", img.getString("data"));
//                        Log.d("upload image name", img.getString("data"));
//
//                        String type;
//                        if (is_group) {
//                            type = "Groupe";
//                        } else {
//                            type = "Amis";
//                        }
//                        JSONObject videoInfo = new JSONObject(img.getString("data"));
//
//
////                        JSONObject videoInfo = new JSONObject(img.getString("data"));
//
//                       Chat.sendMsg(videoInfo.getString("video"), Chat.fId, "video", videoInfo.getString("thumbnail"),type);
////                                             Map<String, String> json = new HashMap<>();
////                        json.put("message", videoInfo.getString("video"));
////                        json.put("chatType", type);
////                        json.put("receiverID", String.valueOf(fId));
////                        json.put("contentType", "video");
////                        json.put("senderID", Tools.getData(Chat.this, "idprofile"));
////                        json.put("senderName", Tools.getData(Chat.this, "name"));
////                        String senderPhoto = Tools.getData(Chat.this, "photo");
////                        json.put("senderImage", senderPhoto);
////                        json.put("actual_channel", currentChannel);
////
////                        System.out.println("chattype===>" + type);
////                        System.out.println("receiverId" + String.valueOf(fId));
////                        System.out.println("senderId" + Tools.getData(Chat.this, "idprofile"));
////                        ChatApi.getInstance().sendMessage(json, currentChannel);
//
//
//
//
//
////                        JSONObject videoInfo = img.getJSONObject("data");
////                        Chat.sendMsg(videoInfo.getString("video"), Chat.fId, "video", videoInfo.getString("thumbnail"),"Amis");
//                    }
//                } catch (JSONException e) {
//                    Tools.dismissLoader();
////                    findViewById(R.id.loading).setVisibility(View.GONE);
//                    Log.e("exception", e.getLocalizedMessage());
//                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                }
//
//            } else {
//                Tools.dismissLoader();
////                findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }
//
//
//        @Override
//        protected String doInBackground(String... urls) {
//
//            try {
//                Log.e("VIDEO DO ","UPLOADS CALLED");
//
//                return Tools.doFileUpload(getString(R.string.server_url) + urls[1], urls[0], Tools.getData(Chat.this, "token"));
//            }
//            catch (Exception e)
//            {
//                return "";
//            }
//
//
//        }
//    }


    private class DoUploadVideo extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            findViewById(R.id.loading).setVisibility(View.GONE);

            Log.d("upload results", result);

            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);


                    if (img.getBoolean("result")) {
                        Log.d("upload results", img.getString("data"));
                        Log.d("upload image name", img.getString("data"));

                        JSONObject videoInfo = img.getJSONObject("data");

                        Chat.sendMsg(videoInfo.getString("video"), Chat.fId, "video", videoInfo.getString("thumbnail"),"Amis");

                    }
                } catch (JSONException e) {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Log.e("exception", e.getLocalizedMessage());
                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {

            return Tools.doFileUpload(getString(R.string.server_url) + urls[1], urls[0], Tools.getData(Chat.this, "token"));
        }
    }
    private class DoUploads extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.e("results",result);
            findViewById(R.id.loading).setVisibility(View.GONE);

            Log.e("upload results", result);

            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);


                    if (img.getBoolean("result")) {
                        Log.d("upload results", img.getString("data"));
                        Log.d("upload image name", img.getString("data"));

                        String type;
                        if (is_group) {
                            type = "Groupe";
                        } else {
                            type = "Amis";
                        }
                        JSONObject videoInfo = new JSONObject(img.getString("data"));

                        Map<String, String> json = new HashMap<>();
                        json.put("message", videoInfo.getString("video"));
                        json.put("chatType", type);
                        json.put("receiverID", String.valueOf(fId));
                        json.put("contentType", "video");
                        json.put("senderID", Tools.getData(Chat.this, "idprofile"));
                        json.put("senderName", Tools.getData(Chat.this, "name"));
                        String senderPhoto = Tools.getData(Chat.this, "photo");
                        json.put("senderImage", senderPhoto);
                        json.put("actual_channel", currentChannel);

                        System.out.println("chattype===>" + type);
                        System.out.println("receiverId" + String.valueOf(fId));
                        System.out.println("senderId" + Tools.getData(Chat.this, "idprofile"));
                        ChatApi.getInstance().sendMessage(json, currentChannel);

                    }
                } catch (JSONException e) {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Log.e("exception", e.getLocalizedMessage());
                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {

            try {
                Log.e("VIDEO DO ","UPLOADS CALLED");

                return Tools.doFileUpload(getString(R.string.server_url) + urls[1], urls[0], Tools.getData(Chat.this, "token"));
            }
            catch (Exception e)
            {
                return "";
            }


        }
    }

    private class DoUploadsNew extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result on uploadnew ",result);
            findViewById(R.id.loading).setVisibility(View.GONE);
            Tools.dismissLoader();

            Log.e("upload results", result);

            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);


                    if (img.getBoolean("result")) {
                        Log.e("upload results", img.getString("data"));
                        Log.e("upload image name", img.getString("data"));

                        String type;
                        if (is_group) {
                            type = "Groupe";
                        } else {
                            type = "Amis";
                        }
                        JSONObject videoInfo = new JSONObject(img.getString("data"));


//                        JSONObject videoInfo = new JSONObject(img.getString("data"));

                        Chat.sendMsg(videoInfo.getString("video"), Chat.fId, "video", videoInfo.getString("thumbnail"),type);
//                                             Map<String, String> json = new HashMap<>();
//                        json.put("message", videoInfo.getString("video"));
//                        json.put("chatType", type);
//                        json.put("receiverID", String.valueOf(fId));
//                        json.put("contentType", "video");
//                        json.put("senderID", Tools.getData(Chat.this, "idprofile"));
//                        json.put("senderName", Tools.getData(Chat.this, "name"));
//                        String senderPhoto = Tools.getData(Chat.this, "photo");
//                        json.put("senderImage", senderPhoto);
//                        json.put("actual_channel", currentChannel);
//
//                        System.out.println("chattype===>" + type);
//                        System.out.println("receiverId" + String.valueOf(fId));
//                        System.out.println("senderId" + Tools.getData(Chat.this, "idprofile"));
//                        ChatApi.getInstance().sendMessage(json, currentChannel);





//                        JSONObject videoInfo = img.getJSONObject("data");
//                        Chat.sendMsg(videoInfo.getString("video"), Chat.fId, "video", videoInfo.getString("thumbnail"),"Amis");
                    }
                } catch (JSONException e) {
//                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Tools.dismissLoader();
                    Log.e("exception", e.getLocalizedMessage());
                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {

            try {
                Log.e("VIDEO DO ","UPLOADS CALLED");

                return Tools.doFileUpload(getString(R.string.server_url) + urls[1], urls[0], Tools.getData(Chat.this, "token"));
            }
            catch (Exception e)
            {
                return "";
            }


        }
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
//        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
//
//        switch (requestCode) {
//            case 1:
//                if (resultCode == this.RESULT_OK) {
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    InputStream imageStream = null;
//                    try {
//                        imageStream = this.getContentResolver().openInputStream(selectedImage);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//
//                    final String[] proj = {MediaStore.Images.Media.DATA};
//                    final Cursor cursor = this.managedQuery(selectedImage, proj, null, null,
//                            null);
//                    final int column_index = cursor
//                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                    cursor.moveToLast();
//                    imgPath = cursor.getString(column_index);
//
//                    Log.i("galery path", imgPath);
//
//                }
//                break;
//            case 2:
//                if (resultCode == this.RESULT_OK) {
//
//                    Bitmap thumbnail = (Bitmap) imageReturnedIntent.getExtras().get("data");
//                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
//                    FileOutputStream fo;
//                    try {
//                        destination.createNewFile();
//                        fo = new FileOutputStream(destination);
//                        fo.write(bytes.toByteArray());
//                        fo.close();
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    System.out.println("Welcome" + destination);
//
//                    //new DoUpload().execute();
//                    imgPath = String.valueOf(destination);
//
//                    Log.i("camera path", imgPath);
//                }
//                break;
//
//            default:
//                break;
//        }
//
//        new DoUpload().execute();
//
////       new DoAudioUpload().execute();
//
//
//    }


    @Override
    public void pickedItems(String objectId, Bitmap imageDrawable, String type, String name) {
        Log.i("wis photo", name);

        popWindow.dismiss();
        dialog.dismiss();

//        WISChat msg = new WISChat(response.getInt("data"), new WISUser(0, context.getString(R.string.me), "", Tools.getData(context, "photo"), 0, "", "", ""),name, result, true, type, thumb,"");
//        data.add(data.size(), msg);
        adapter.notifyDataSetChanged();

        WISPHOTOSELECTED = Boolean.TRUE;
        WISPHOTOPATH = name;
    }



    @Override
    public void didReceiveTextMessage(PNMessageResult messageResult) {

        System.out.println("&&&&&&&&********@@@@@@@");

        Log.d("CHAT CLASS::", String.valueOf(messageResult));


        String receivedChannel = messageResult.getChannel();
        String senderId = null;
        String senderName = null;
        String receiverId = null;
        String currentUserId=null;
        if (receivedChannel.equals(currentChannel)) {

            try {


                JSONObject jsonData = new JSONObject(String.valueOf(messageResult.getMessage()));


                Log.d("json res", String.valueOf(jsonData));


                senderId = jsonData.getString("senderID");

                receiverId = jsonData.getString("receiverID");

                currentUserId = Tools.getData(Chat.this, "idprofile");

                message = jsonData.getString("message");

//                messageCallDisconect=jsonData.getString("message");

                String chatType = jsonData.getString("chatType");

                senderName = jsonData.getString("senderName");

                String senderIcon = jsonData.getString("senderImage");

                String contentType = jsonData.getString("contentType");


                System.out.println("message------->" +message);

                System.out.println("senderId------->" +senderId);
                System.out.println("receiverId------->" +receiverId);

                System.out.println("ownerId------->" +currentUserId);


                if (senderId.equals(currentUserId)) {

//                send message

                    System.out.println("Send message");


                    if(contentType.equals("audio")||contentType.equals("video")){

                        message = getAudioId();
                    }


                    sendMessageToChatList(message, contentType, chatType);

                } else {

//                receive meessage

                    System.out.println("receive meessage");

                    receiveMessage(message, chatType, Integer.parseInt(senderId), senderName, senderIcon, contentType);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            Log.d("show", "notification");


            showAlert(message, senderName);


        }
    }

    @Override
    public void onTextPublishSuccess(PNStatus status) {


        System.out.println("&&&&&&&&********@@@@@@@");

        Log.d("CHAT CLASS status::", String.valueOf(status));
    }

    @Override
    public void didTextReceivePresenceEvent(PNPresenceEventResult eventResult) {


        System.out.println("&&&&&&&&********@@@@@@@");

        Log.d("eventResult::", String.valueOf(eventResult));
    }


    public class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            System.out.println("result=======>" + result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image results%@", String.valueOf(img));
                    String type;
                    if (is_group) {
                        type = "Groupe";
                    } else {
                        type = "Amis";
                    }

                    Map<String, String> json = new HashMap<>();
                    json.put("message", img.getString("data"));
                    json.put("chatType", type);
                    json.put("receiverID", String.valueOf(fId));
                    json.put("contentType", "photo");
                    json.put("senderID", Tools.getData(Chat.this, "idprofile"));
                    json.put("senderName", Tools.getData(Chat.this, "name"));
                    String senderPhoto = Tools.getData(Chat.this, "photo");
                    json.put("senderImage", senderPhoto);
                    json.put("actual_channel", currentChannel);

                    System.out.println("chattype===>" + type);
                    System.out.println("receiverId" + String.valueOf(fId));
                    System.out.println("senderId" + Tools.getData(Chat.this, "idprofile"));
                    ChatApi.getInstance().sendMessage(json, currentChannel);

                    hideKeyboard();

//                    Chat.sendMsg(img.getString("data"), Chat.fId, "photo", "",type);


//                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    findViewById(R.id.loading).setVisibility(View.GONE);
//                    Tools.dismissLoader();
                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                findViewById(R.id.loading).setVisibility(View.GONE);
//                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {
            ArrayList<String> images = new ArrayList<>();
            images.add(imgPath);

//            ArrayList<String> audiourls = new ArrayList<>();
//            images.add(fileUrl);
            try {

            }catch (NullPointerException ex){

            }
            return Tools.doFileUploadForChat(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.savePhotoAction), imgPath, Tools.getData(Chat.this, "token"), Tools.getData(Chat.this, "idprofile"), "", "");


        }


    }


    public static void sendPhotoTochat(String message, String chatId, String contentType, String chatType) {

        Log.e("send photo", "wis chat page");

        Map<String, String> json = new HashMap<>();
        json.put("message", message);
        json.put("chatType", chatType);
        json.put("receiverID", String.valueOf(chatId));
        json.put("contentType", "photo");
        json.put("senderID", Tools.getData(context, "idprofile"));
        json.put("senderName", Tools.getData(context, "name"));
        String senderPhoto = Tools.getData(context, "photo");

        json.put("senderImage", senderPhoto);

        json.put("actual_channel", channel);

        ChatApi.getInstance().sendMessage(json, channel);

        System.out.println("meaasge" + message);

    }


    public static void sendAudioFileToChat(String message, String chatId, String contentType, String chatType) {

        Log.e("send Audio File", "audio");

        Map<String, String> json = new HashMap<>();
        json.put("message", message);
        json.put("chatType", chatType);
        json.put("receiverID", String.valueOf(chatId));
        json.put("contentType", "photo");
        json.put("message_type", "Audio");
        json.put("file", fileUrl);
        json.put("senderID", Tools.getData(context, "idprofile"));
        json.put("senderName", Tools.getData(context, "name"));
        String senderPhoto = Tools.getData(context, "photo");

        json.put("senderImage", senderPhoto);

        json.put("actual_channel", channel);

        ChatApi.getInstance().sendMessage(json, channel);


        System.out.println("message===========>" + message);
    }


//    public class postAudioDetailsFile extends AsyncTask<String, String, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpPost httpPost = new HttpPost("http://146.185.161.92/public/activity/chat_audio1475740353.caf");
//
//            String message;
//            if (is_group) {
//                message = "Groupe";
//            } else {
//                message = "Amis";
//            }
//
//
//            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
//
//            nameValuePair.add(new BasicNameValuePair("user_id", Tools.getData(context, "idprofile")));
//            nameValuePair.add(new BasicNameValuePair("chatType", message));
//            nameValuePair.add(new BasicNameValuePair("message_type", "Audio"));
//
//            nameValuePair.add(new BasicNameValuePair("receiverID", String.valueOf(fId)));
//            nameValuePair.add(new BasicNameValuePair("file", fileUrl));
//
//
//            System.out.println("chattype===>" + message);
//            System.out.println("receiverId" + String.valueOf(fId));
//            System.out.println("senderId" + Tools.getData(Chat.this, "idprofile"));
////            nameValuePair.add(new BasicNameValuePair("order_ref", getorder_id));
////            nameValuePair.add(new BasicNameValuePair("currency", "123"));
//
//            try {
//                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
//            } catch (UnsupportedEncodingException e) {
//                System.out.println("===== pointer Exception==");
//                e.printStackTrace();
//            }
//            // Making HTTP Request
//            try {
//                HttpResponse response = httpClient.execute(httpPost);
//                InputStream inputStream = response.getEntity().getContent();
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                BufferedReader bufferReader = new BufferedReader(inputStreamReader);
//                StringBuilder stringBuilder = new StringBuilder();
//                String strBuffer = null;
//                while ((strBuffer = bufferReader.readLine()) != null) {
//                    stringBuilder.append(strBuffer);
//                }
//                strResponse = stringBuilder.toString();
//                Log.e("getdata", strResponse);
//            } catch (ClientProtocolException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return strResponse;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//
//            if (strResponse != null) {
//                try {
//
//                    System.out.println("response=========" + s);
////                    org.json.JSONObject jobject = new org.json.JSONObject(strResponse);
//                    System.out.println("response=========" + strResponse);
////                org.json.JSONObject jobject = jobj.getJSONObject("");
//
////                    System.out.println("paymentstatus" +payment_status);
//
//                } catch (NullPointerException exce) {
//                    System.out.println("Exception====>" + exce.getMessage());
//
//                }
//
//            }
//        }
//    }

    public class DoAudioUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            System.out.println("result on Audio =======>" + result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image results%@", String.valueOf(img));
                    String type;
                    if (is_group) {
                        type = "Groupe";
                    } else {
                        type = "Amis";
                    }


                    System.out.println("fileurl=====>" + AudioSavePathInDevice);
                    Map<String, String> json = new HashMap<>();
                    json.put("message", img.getString("data"));
                    json.put("chatType", type);
                    json.put("receiverID", String.valueOf(fId));
                    json.put("contentType", "Audio");
                    json.put("senderID", Tools.getData(Chat.this, "idprofile"));
                    json.put("senderName", Tools.getData(Chat.this, "name"));
                    String senderPhoto = Tools.getData(Chat.this, "photo");

                    json.put("senderImage", senderPhoto);

                    json.put("actual_channel", currentChannel);


                    System.out.println("chattype===>" + type);
                    System.out.println("receiverId" + String.valueOf(fId));
                    System.out.println("senderId" + Tools.getData(Chat.this, "idprofile"));


                    ChatApi.getInstance().sendMessage(json, currentChannel);

                    hideKeyboard();

//                    Chat.sendMsg(img.getString("data"), Chat.fId, "photo", "",type);


//                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {
            ArrayList<String> images = new ArrayList<>();
            images.add(imgPath);

//            ArrayList<String> audiourls = new ArrayList<>();
//            images.add(fileUrl);
            return Tools.doFileUploadForChat(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.savePhotoAction), imgPath = AudioSavePathInDevice, Tools.getData(Chat.this, "token"), Tools.getData(Chat.this, "idprofile"), "", "");


//            /storage/sdcard0/JCIENAudioRecording.3gp
        }

    }


    private void updateAppisUserSubscribe(boolean isSubscribed) {
        SharedPreferences.Editor editor = getSharedPreferences("shared_pref_name",
                Context.MODE_PRIVATE).edit();

        Log.e(TAG, "save User already Subscribed In: " + (isSubscribed ? "Subscribed IN" : "NOT Subscribe IN"));
        editor.putBoolean("is_clickable_call", isSubscribed);
        editor.commit();
        editor.apply();


    }


    private class DoUploadVoice extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {

                Log.e("Voice upload RESULTS:",result);

                try {
                    JSONObject img = new JSONObject(result);

                    if (img.getBoolean("result"))

                        Log.i("image results%@", String.valueOf(img));
                    String type;
                    if(is_group){
                        type="Groupe";
                    }else{
                        type ="Amis";
                    }

                    JSONObject data = img.getJSONObject("message_data");
                    Log.i("Response", String.valueOf(data));

                    voicePath = data.getString("message");
                    new DownloadVoice().execute(getString(R.string.server_url3)+voicePath);

                    type_message = data.getString("type_message");

                    Log.d("MESSAGE1",voicePath);
                    Log.d("MESSAGE2",type_message);
                    Log.d("MESSAGE3",data.getString("type_disc"));


                    Map<String,String> json = new HashMap<>();
                    json.put("message",voicePath);
                    json.put("chatType",type);
                    json.put("receiverID", String.valueOf(fId));
                    json.put("contentType",type_message);
                    json.put("senderID",Tools.getData(Chat.this,"idprofile"));
                    json.put("senderName",Tools.getData(Chat.this,"name"));
                    String senderPhoto = Tools.getData(Chat.this,"photo");

                    json.put("senderImage",senderPhoto);

                    json.put("actual_channel",currentChannel);
//
                    ChatApi.getInstance().sendMessage(json,currentChannel);

                    hideKeyboard();

                    Chat.rootView.setAlpha(1);
                    Chat.disMissPopMenus();




                } catch (JSONException e) {
//                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Tools.dismissLoader();
                    Toast.makeText(Chat.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {
            try {

                Log.e("UPLOADING DoINBa","background");

                return Tools.doVoiceUploadForChat(urls[0],urls[1],urls[2],urls[3],urls[4],urls[5],urls[6]);
            }
            catch (Exception e)
            {
                return "";
            }

        }
    }

    private class DownloadVoice extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {

            int count;
            String imageURL = url[0];
            try {
                URL urls = new URL(imageURL);

                URLConnection conexion = urls.openConnection();

                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(urls.openStream());
                imageURL = imageURL.replace(getString(R.string.server_url3),"");
                String fname = imageURL;
                String PATH = Environment.getExternalStorageDirectory()
                        + "/VoiceAudio/";
                File file = new File(PATH);
                if (!file.exists()) {
                    file.mkdirs();
                }
                File outputFile = new File(file, fname);
                if (!outputFile.exists ())
                {
                    try {
                        OutputStream output = new FileOutputStream(outputFile);
                        byte data[] = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {

                            total += count;
                            publishProgress((int) (total * 100 / lenghtOfFile));

                            output.write(data, 0, count);

                        }
                        output.flush();

                        output.close();

                        input.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //outputFile.delete ();


            } catch (Exception e) {
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void loadMusic(final GridView grid, final ProgressDialog progress) {
        final ArrayList<WISMusic1> audio = new ArrayList<>();
        final AudioAdapter3 adapter = new AudioAdapter3(Chat.this, audio, 1);
        grid.setAdapter(adapter);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(Chat.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqVideos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getmusic_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//
//
//
//                            }
//
//
//                        } catch (JSONException e) {
//
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {




                                try {
                                    if (response.getBoolean("result")) {
                                        Log.i("wismusic", String.valueOf(response));
                                        JSONArray data1 = response.getJSONArray("album_files");

                                        for (int i = 0; i < data1.length(); i++) {
                                            try {
                                                JSONObject obj = data1.getJSONObject(i);
                                                JSONArray songsArry = obj.getJSONArray("songs");
                                                Log.i("AlbumArray",String.valueOf(songsArry));
                                                for(int c =0;c<songsArry.length();c++)
                                                {
                                                    JSONObject obj1 = songsArry.getJSONObject(c);
                                                    String chat_type = "Amis";
                                                    if(is_group){
                                                        chat_type = "Groupe";
                                                    }
                                                    audio.add(new WISMusic1(obj1.getInt("id"),obj1.getString("name"),obj1.getString("audio_path"),obj1.getString("artists"),obj1.getString("created_at"),obj1.getString("audio_thumbnail"),chat_type));
                                                }
                                            } catch (Exception e) {
                                            }
                                        }

                                        JSONArray data = response.getJSONArray("songs_files");
                                        Log.i("songArray",String.valueOf(data));
                                        for (int i = 0; i < data.length(); i++) {
                                            try {
                                                JSONObject obj = data.getJSONObject(i);
                                                String chat_type = "Amis";
                                                if(is_group){
                                                    chat_type = "Groupe";
                                                }
                                                audio.add(new WISMusic1(obj.getInt("id"),obj.getString("name"), obj.getString("audio_path"),obj.getString("artists"),obj.getString("created_at"), obj.getString("audio_thumbnail"),chat_type));
                                            } catch (Exception e) {
                                            }
                                        }
                                        if (audio.size() == 0){

                                            Toast.makeText(Chat.this, getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
                                        }
                                        adapter.notifyDataSetChanged();
                                    }
                                    progress.dismiss();

                                } catch (JSONException e) {
                                    progress.dismiss();
                                    //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Chat.this, "token"));
                headers.put("lang", Tools.getData(Chat.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqVideos);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    if (audio.get(i).isSelected()) {
                        audio.get(i).setSelected(false);
                    } else {
                        for (WISMusic1 item : audio) {
                            item.setSelected(false);
                        }
                        audio.get(i).setSelected(true);
                    }
                    adapter.notifyDataSetChanged();
                }
                catch (ArrayIndexOutOfBoundsException e)
                {

                }

            }
        });
    }


    public void showMusicMenus() {




        // StartAction



        LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.music_items, null, false);


        // get device size
        Display display =getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);


        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);


//        new SlideInAnimation(popWindow.getContentView()).setDirection(com.easyandroidanimations.library.Animation.DIRECTION_UP)
//                .animate();
        rootView.setAlpha(0.4f);


//        getActivity().getV


//        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,





        popWindow.showAtLocation(rootView, Gravity.CENTER, 0, 0);

//        setAlpha(0.5f);


//      runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//
//
//            }
//        });


//

//
//        new AlertDialog.Builder(getActivity())
//                .setView(R.layout.write_status_popup)
//                .show();
//
        final GridView gridView = (GridView)inflatedView.findViewById(R.id.gvMusicMedia);
        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);

        TextView header = (TextView)inflatedView.findViewById(R.id.headerTitleForWindow);

        try{
            Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            header.setTypeface(font);

        }catch(NullPointerException ex){

        }

//        post_status = (Button) inflatedView.findViewById(R.id.post_status);
//        post_image = (ImageView) inflatedView.findViewById(R.id.post_image);
//        message = (EditText) inflatedView.findViewById(R.id.message);
//        wispopTV = (TextView) inflatedView.findViewById(R.id.wispopTV);
//
//
//        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
//        message.setTypeface(font);
//        message.setTextColor(getResources().getColor(R.color.light_black));
//        post_status.setTypeface(font);
//        wispopTV.setTypeface(font);


//        clear previous data


        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popWindow.dismiss();
                rootView.setAlpha(1f);

            }
        });


        final ProgressDialog progress = ProgressDialog.show(Chat.this, "", getString(R.string.loading_progress), true, true);
        progress.show();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {


                loadMusic(gridView,progress);
            }
        });




    }




    public static void disMissPopMenus(){

        if(popWindow!=null){
            popWindow.dismiss();




        }

    }

    public static void makePubCalls(String videoPath,String chatType,String senderID,String senderName,String senderPhoto,String contentType){


        setAudioMessage(videoPath);

        Map<String, String> json = new HashMap<>();
        json.put("message", videoPath);
        json.put("chatType", chatType);
        json.put("receiverID", String.valueOf(fId));
        json.put("contentType",contentType);
        json.put("senderID", senderID);
        json.put("senderName", senderName);
        json.put("senderImage", senderPhoto);
        json.put("actual_channel", channel);
        ChatApi.getInstance().sendMessage(json,channel);
    }



    class Player extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog progress;

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            Boolean prepared;
            try {

                mediaPlayer.setDataSource(params[0]);


                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
//                        intialStage = true;
//                        playPause=false;
//                        btn.setBackgroundResource(R.drawable.button_play);


                        mp.stop();
                        mp.reset();
                        adapter.resetPlayer();
                    }
                });
                mediaPlayer.prepareAsync();
//                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                    @Override
//                    public void onPrepared(MediaPlayer mp) {
//
//                        final long totalDuration =  mp.getDuration();
//                        final long currentDuration =mp.getCurrentPosition();
//
//                        Log.e("Audio duration",milliSecondsToTimer(totalDuration));
//
//                        Log.e("Audio Progress", String.valueOf(getProgressPercentage(currentDuration,totalDuration)));
//
//                        mediaPlayer = mp;
//
//                        updateProgressBar();
//
////
////                        runOnUiThread(new Runnable() {
////                            @Override
////                            public void run() {
////
////
////
////                                adapter.updateSeekBars(milliSecondsToTimer(totalDuration),(int)getProgressPercentage(currentDuration,totalDuration));
////                            }
////                        });
////
//
//
////                                holder.endTime.setText(milliSecondsToTimer(totalDuration));
//
//
//                    }
//                });
                prepared = true;
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                Log.d("IllegarArgument", e.getMessage());
                prepared = false;
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (progress.isShowing()) {
                progress.cancel();
            }
            Log.d("Prepared", "//" + result);


            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {

                    mp.start();

                    final long totalDuration =  mp.getDuration();
                    final long currentDuration =mp.getCurrentPosition();

                    Log.e("Audio duration",milliSecondsToTimer(totalDuration));

                    Log.e("Audio Progress", String.valueOf(getProgressPercentage(currentDuration,totalDuration)));

                    mediaPlayer = mp;

                    updateProgressBar();

//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//
//
//                                adapter.updateSeekBars(milliSecondsToTimer(totalDuration),(int)getProgressPercentage(currentDuration,totalDuration));
//                            }
//                        });
//


//                                holder.endTime.setText(milliSecondsToTimer(totalDuration));


                }
            });




//            intialStage = false;
        }

        public Player() {

            progress = new ProgressDialog(Chat.this);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            this.progress.setMessage("Buffering...");
            this.progress.show();

        }
    }


    public void updateProgressBar() {

        myHandler.postDelayed(mUpdateTimeTask, 100);
    }

    public void removeHandler() {

        myHandler.removeCallbacks(mUpdateTimeTask);
    }



    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                final long totalDuration = mediaPlayer.getDuration();
                final long currentDuration = mediaPlayer.getCurrentPosition();
                int duration = (int) currentDuration;

                // Displaying Total Duration time

                String stat = milliSecondsToTimer(totalDuration);
                //endTime.setText();
                // Displaying time completed playing
                final String endt = milliSecondsToTimer(currentDuration);
                //startTime.setText();


                final int progress = (int)(getProgressPercentage(currentDuration, totalDuration));
                Log.d("Progress", ""+progress+stat+endt);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {



                        adapter.updateSeekBars(String.valueOf(endt),progress);
                    }
                });






                // Running this thread after 100 milliseconds
                myHandler.postDelayed(this, 100);
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
            }

        }
    };


    public int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }


    public String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int)( milliseconds / (1000*60*60));
        int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
        int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
        // Add hours if there
        if(hours > 0){
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if(seconds < 10){
            secondsString = "0" + seconds;
        }else{
            secondsString = "" + seconds;}

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }






}


//    private void loadPhotos(final GridView gridPhoto) {
//        photos = new ArrayList<>();
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("Message").contains("any image")) {
//                                Toast.makeText(this, getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
//                            } else {
//                                if (response.getString("result").equals("true")) {
//
//                                    JSONArray data = response.getJSONArray("data");
//                                    for (int i = 0; i < data.length(); i++) {
//                                        try {
//                                            JSONObject obj = data.getJSONObject(i);
//                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at")));
//                                        } catch (Exception e) {
//                                        }
//                                    }
////                                    gridPhoto.setAdapter(new PhotoAdapter((FragmentActivity) getActivity(), photos));
//                                    gridPhoto.setAdapter(new CustomPhotoAdapter(photos,ChatApi.this));
//                                }
//                            }
//                           findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//
//                           findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
//                //if (getActivity() != null)
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(ChatApi.this, "token"));
//                headers.put("lang", Tools.getData(ChatApi.this, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqPhotos);
//    }

