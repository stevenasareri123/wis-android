package com.oec.wis.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.CountryAdapter;
import com.oec.wis.adapters.SelectCountry;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.chatApi.ChatListener;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISCountry;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragHome;
//import com.oec.wis.singlecall.SingleCallMainActivity;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;


import static android.Manifest.permission.MANAGE_DOCUMENTS;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Phone extends AppCompatActivity {
    private static final String TAG = "";
    ImageView closeIV, phoneRedThumbIv, videoCallIv, highVolumeIv, phoneGreenThumbIv, ivReceivercountryImage, ivSendercountryImage;
    CircularImageView userNotificationView, ivRegisterProfile, ivOtherUserProfile;
    TextView target_view, registerUserTV, registerUserCountryTv, otherUserNameTv, otherUserCountryTV, dateTV;
    public static DialogPlus dialog;
    public static PopupWindow popup;
    public static int fId;
    Boolean is_group;
    public String currentChannel;

    public String receiverImage;
    int id;
    private static String channel;
    String city_country;
    String city, country;
    Bundle getBundle = null;
    WISUser user;
    UserNotification userNotification;
    ArrayList<String> countryFlag = new ArrayList<>();
    List<WISChat> data;
    int position;
    private Timer mTimer;
    private TimerTask mTimerTask;
    public static String receiverFirstName;
    public static Activity context;
    String chatType;
    CountryAdapter cAdapter;
    Spinner spCountry;

    public static Dialog dialoge;
    WISCountry wisCountry = null;
    String imgPath = "";
    ArrayList<String> senderList = new ArrayList<>();

    Boolean isRingToneClick;

    public String OwnerId;

    String AudioSavePathInDevice = null;
    public static String fileUrl;
    MediaRecorder mediaRecorder;
    Random random = null;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;
    String senderId;
    String receiverAmisId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);
        initViews();
        setupProfileInfos();
        OwnerId=Tools.getData(Phone.this, "idprofile");
        System.out.println("ownerId==>" +OwnerId);

      if (getIntent().getExtras() != null) {
            fId = getIntent().getExtras().getInt("id");
            receiverFirstName = getIntent().getExtras().getString("receiver_name");
            otherUserNameTv.setText(receiverFirstName);
            receiverImage = getIntent().getExtras().getString("image");
            currentChannel = getIntent().getExtras().getString("actual_channel");
            is_group = getIntent().getExtras().getBoolean("is_group");

            System.out.println("Sender name"+getIntent().getExtras().getString("senderName"));


            System.out.println("receiverName===>" + receiverFirstName);
            System.out.println("receiverimage===>" + receiverImage);
            System.out.println("group" + is_group);
            System.out.println("receiverId=====>" + fId);
            System.out.println("current channel=====>" + currentChannel);

        }


    }

//    current channel=====>Private_951


    private void initViews() {

        closeIV = (ImageView) findViewById(R.id.closeIV);
        phoneRedThumbIv = (ImageView) findViewById(R.id.phoneRedThumbIv);
        videoCallIv = (ImageView) findViewById(R.id.videoCallIv);
        highVolumeIv = (ImageView) findViewById(R.id.videoCallIv);
        phoneGreenThumbIv = (ImageView) findViewById(R.id.phoneGreenThumbIv);
//        userNotificationView = (CircularImageView) findViewById(R.id.userNotificationView);
        ivRegisterProfile = (CircularImageView) findViewById(R.id.ivRegisterProfile);
        ivOtherUserProfile = (CircularImageView) findViewById(R.id.ivOtherUserProfile);
//        target_view = (TextView) findViewById(R.id.target_view);
        registerUserTV = (TextView) findViewById(R.id.registerUserTV);
        registerUserCountryTv = (TextView) findViewById(R.id.registerUserCountryTv);
        otherUserCountryTV = (TextView) findViewById(R.id.otherUserCountryTV);
        otherUserNameTv = (TextView) findViewById(R.id.otherUserNameTv);
        ivReceivercountryImage = (ImageView) findViewById(R.id.ivReceivercountryImage);
        ivSendercountryImage = (ImageView) findViewById(R.id.ivSendercountryImage);
        dateTV = (TextView) findViewById(R.id.dateTV);


        Calendar c = Calendar.getInstance();

        int seconds = c.get(Calendar.SECOND);
        int minutes = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR);
        String time = hour + ":" + minutes + ":" + seconds;

        Log.i("time", time);

        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
//         months 0 to 11
        int year = c.get(Calendar.YEAR);
        String date = day + "/" + month + "/" + year;
        Log.i("date", date);


        dateTV.setText(date + " " + time);
        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });
//        ivRegisterProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FragProfile fragment = new FragProfile();
//            FragmentManager fragmentManager = getFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.frame_container, fragment).commit();
//        }
//        });
    }

    public void setupProfileInfos() {

        registerUserTV.setText(Tools.getData(this, "name"));

        registerUserCountryTv.setText(Tools.getData(this, "country"));

        if(getIntent().getExtras().getString("senderID",null)!=null){

            senderId=getIntent().getExtras().getString("senderID");
            System.out.println("senderId  if "  + senderId);

        }else{

            senderId = Tools.getData(this, "idprofile");
            System.out.println("senderId  else "  + senderId);

        }



        System.out.println("senderId" + senderId);
//        downloadProfileImage();
        downloadProfileImageTwo();
        downloadProfileImageReceiver();

        phoneGreenThumbIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 if(senderId.equals(OwnerId)){
                    Toast.makeText(getApplicationContext(), "Calling", Toast.LENGTH_LONG).show();
                    Map<String, String> json = new HashMap<>();
                    json.put("message", "videoCalling");
                    json.put("receiverID", String.valueOf(fId));
                    json.put("contentType", "text");
                    json.put("senderID", Tools.getData(Phone.this, "idprofile"));
                    json.put("senderName", Tools.getData(Phone.this, "name"));
                    json.put("ReceiverName", receiverFirstName);
                    json.put("chatType", "amis");
                    json.put("isVideoCalling","yes");
                    json.put("Name","OnetoOnePopupViewShow");
                     json.put("type","Call Connecting");
                    json.put("actual_channel", currentChannel);
                    String senderPhoto = Tools.getData(Phone.this, "photo");
                    json.put("senderImage", senderPhoto);
                    json.put("ReceiverImage", senderPhoto);
                    json.put("senderCountry", Tools.getData(Phone.this, "country"));
                    json.put("ReceiverCountry", "India");
                    System.out.println("REceiver Id==>"+fId);
                    System.out.println("json ==========>" +json);
                    ChatApi.getInstance().sendMessage(json, currentChannel);
                    hideKeyboard();

                }
                    else
                {


                          Log.e("else calling", "phone page");

                        Toast.makeText(getApplicationContext(), "Connecting.....", Toast.LENGTH_LONG).show();

//                        ChatApi.getInstance().stopRingTone();
//
//                       Intent it= new Intent(Phone.this, SingleCallMainActivity.class);
//                       startActivity(it);

//                    /System.out: json params==========>{actual_channel=null, ReceiverName=null, chatType=null, senderID=225, contentType=text, senderImage=img_pub1482331044.png, ReceiverCountry=India, senderName=chat USER M, senderCountry=Antarctica, isAcceptCall=yes, message=Call Connected, ReceiverImage=img_pub1482331044.png, type=Call Accepted, Name=connecting, receiverID=0}
//
//                    D/sende message called: {actual_channel=null, ReceiverName=null, chatType=null, senderID=225, contentType=text, senderImage=img_pub1482331044.png, ReceiverCountry=India, senderName=chat USER M, senderCountry=Antarctica, isAcceptCall=yes, message=Call Connected, ReceiverImage=img_pub1482331044.png, type=Call Accepted, Name=connecting, receiverID=0}

                        Map<String, String> json = new HashMap<>();
                        json.put("message", "Call Connected");
                        json.put("receiverID",receiverAmisId);
                        json.put("contentType", "text");
                        json.put("senderID", Tools.getData(Phone.this, "idprofile"));
                        json.put("senderName", Tools.getData(Phone.this, "name"));
                        json.put("ReceiverName", receiverFirstName);
                        json.put("chatType", "amis");
                        json.put("actual_channel",currentChannel);
                        json.put("type","Call Accepted");
                        json.put("isAcceptCall","yes");
                        String senderPhoto = Tools.getData(Phone.this, "photo");
                        json.put("Name","connecting");
                        json.put("senderImage", senderPhoto);
                        json.put("ReceiverImage", senderPhoto);
                        json.put("senderCountry", Tools.getData(Phone.this, "country"));
                        json.put("ReceiverCountry", "India");

                        System.out.println("json params==========>" +json);

                    System.out.println("REceiver Id else ==>"+fId);
                    System.out.println("REceiver Amis Id ==>"+receiverAmisId);
                    System.out.println("current channel ==>"+currentChannel);
                    ChatApi.getInstance().sendMessage(json,currentChannel);

                    }
//                current channel=====>Private_951


            }
        });


        phoneRedThumbIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "Call Rejected", Toast.LENGTH_LONG).show();


//                Log.d("current channel",currentChannel);


                ChatApi.getInstance().stopRingTone();

                Map<String, String> json = new HashMap<>();
                json.put("message", "Call RejectOnetoONE");
                json.put("receiverID", String.valueOf(fId));
                json.put("contentType", "text");
                json.put("senderID", Tools.getData(Phone.this, "idprofile"));
                json.put("senderName", Tools.getData(Phone.this, "name"));
                json.put("ReceiverName", receiverFirstName);
                json.put("chatType", chatType);
                json.put("actual_channel", currentChannel);
                String senderPhoto = Tools.getData(Phone.this, "photo");
                json.put("senderImage", senderPhoto);
                json.put("ReceiverImage", senderPhoto);
                json.put("senderCountry", Tools.getData(Phone.this, "country"));
                json.put("ReceiverCountry", "India");
                json.put("Name","RejectOnetoONE");
                ChatApi.getInstance().sendMessage(json, currentChannel);
                hideKeyboard();


//                if(senderId.equals(OwnerId)){
//
//                    Toast.makeText(getApplicationContext(), "Call Rejected", Toast.LENGTH_LONG).show();
//
//                    ChatApi.getInstance().stopRingTone();
//
//                    Map<String, String> json = new HashMap<>();
//                    json.put("message", "Call RejectOnetoONE");
//                    json.put("receiverID", String.valueOf(fId));
//                    json.put("contentType", "text");
//                    json.put("senderID", Tools.getData(Phone.this, "idprofile"));
//                    json.put("senderName", Tools.getData(Phone.this, "name"));
//                    json.put("ReceiverName", receiverFirstName);
//                    json.put("chatType", chatType);
//                    json.put("actual_channel", currentChannel);
//                    String senderPhoto = Tools.getData(Phone.this, "photo");
//                    json.put("senderImage", senderPhoto);
//                    json.put("ReceiverImage", senderPhoto);
//                    json.put("senderCountry", Tools.getData(Phone.this, "country"));
//                    json.put("ReceiverCountry", "India");
//                    json.put("Name","RejectOnetoONE");
//                    ChatApi.getInstance().sendMessage(json, "Private_951");
//                    hideKeyboard();
//
//                }else
//                {
//                    Toast.makeText(getApplicationContext(), "Call Disconnected", Toast.LENGTH_LONG).show();
//                    ChatApi.getInstance().stopRingTone();
//
//                    Map<String, String> json = new HashMap<>();
//                    json.put("message", "Call Rejected");
//                    json.put("receiverID",Tools.getData(Phone.this, "amis_id"));
//                    json.put("contentType", "text");
//                    json.put("senderID", Tools.getData(Phone.this, "idprofile"));
//                    json.put("senderName", Tools.getData(Phone.this, "name"));
//                    json.put("ReceiverName", receiverFirstName);
//                    json.put("chatType", chatType);
//                    json.put("actual_channel", currentChannel);
//                    String senderPhoto = Tools.getData(Phone.this, "photo");
//                    json.put("senderImage", senderPhoto);
//                    json.put("ReceiverImage", senderPhoto);
//                    json.put("senderCountry", Tools.getData(Phone.this, "country"));
//                    json.put("ReceiverCountry", "India");
//                    json.put("Name","Reject");
//                    ChatApi.getInstance().sendMessage(json, "Private_951");
//
//                }
//



            }
        });


    }



    private void setTimer() {


        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);

                Toast.makeText(getApplicationContext(), "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_LONG).show();


                showpopup();
            }

            public void onFinish() {
//                mTextField.setText("done!");

//                showpopup();
            }
        }.start();


    }

    private void showpopup() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Phone.this);

        // Setting Dialog Title
        alertDialog.setTitle("Audio Record...");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want  record your voice");

        // Setting Icon to Dialog
//        alertDialog.setIcon(R.drawable.delete);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                pickMediaAudio();
//                audioRecordMethod();
                // Write your code here to invoke YES event
                Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }


    private void pickMediaAudio() {

        // custom dialog
        dialoge = new Dialog(Phone.this);
        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialoge.setContentView(R.layout.dialog_share_actu);
        // Custom Android Allert Dialog Title
        dialoge.setTitle("Custom Dialog Example");

        final Button buttonstop = (Button) dialoge.findViewById(R.id.buttonstop);
        Button buttonplay = (Button) dialoge.findViewById(R.id.buttonplay);
        final Button buttonsendfile = (Button) dialoge.findViewById(R.id.buttonsendfile);
        final Button startButton = (Button) dialoge.findViewById(R.id.startButton);

        final ImageView microphoneIV = (ImageView) dialoge.findViewById(R.id.microphoneIV);
        final ImageView no_microphoneIV = (ImageView) dialoge.findViewById(R.id.no_microphoneIV);


        // Click cancel to dismiss android custom dialog box
        microphoneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                microphoneIV.setVisibility(View.GONE);
                no_microphoneIV.setVisibility(View.VISIBLE);
//                 mediaRecorder.stop();
//                microphoneIV.setEnabled(false);
//                startButton.setEnabled(true);

                Toast.makeText(Phone.this, "Recording stopped",
                        Toast.LENGTH_LONG).show();
            }
        });
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {

                    AudioSavePathInDevice =
                            Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                    CreateRandomAudioFileName(5) + "AudioRecording.mp3";


                    File AudioFile = new File(AudioSavePathInDevice);
                    System.out.println("Audio File" + AudioFile);


//                    MediaRecorderReady();


                    mediaRecorder = new MediaRecorder();
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                    mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    mediaRecorder.setOutputFile(AudioSavePathInDevice);

                    System.out.println("Audio path" + AudioSavePathInDevice);

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    startButton.setEnabled(false);
                    buttonstop.setEnabled(true);
                    microphoneIV.setEnabled(true);

                    Toast.makeText(Phone.this, "Recording started",
                            Toast.LENGTH_LONG).show();
                } else {
                    requestPermission();
                }
            }
        });
        buttonstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mediaRecorder.stop();
                buttonstop.setEnabled(false);
//                buttonPlayLastRecordAudio.setEnabled(true);
                startButton.setEnabled(true);
//                buttonStopPlayingRecording.setEnabled(false);

                Toast.makeText(Phone.this, "Recording Completed",
                        Toast.LENGTH_LONG).show();
//                dialog.dismiss();
//                Toast.makeText(getApplicationContext(),"button stop",Toast.LENGTH_LONG).show();
            }
        });

        // Your android custom dialog ok action
        // Action for custom dialog ok button click
        buttonplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonstop.setEnabled(false);
                startButton.setEnabled(false);
//                buttonStopPlayingRecording.setEnabled(true);

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(AudioSavePathInDevice);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaPlayer.start();
                Toast.makeText(Phone.this, "Recording Playing",
                        Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(),"button play",Toast.LENGTH_LONG).show();

//                dialog.dismiss();
            }
        });


        // Action for custom dialog ok button click
        buttonsendfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "button send file", Toast.LENGTH_LONG).show();
                dialoge.dismiss();

                new DoAudioUpload().execute();

//               new  DoUpload().execute();
//                Map<String, String> json = new HashMap<>();
//                json.put("message", String.valueOf(data));
//                json.put("chatType", chatType);
//                json.put("receiverID", String.valueOf(fId));
//                json.put("contentType", "Audio");
//                json.put("senderID", Tools.getData(context, "idprofile"));
//                json.put("senderName", Tools.getData(context, "name"));
//                String senderPhoto = Tools.getData(context, "photo");
//
//                json.put("senderImage", senderPhoto);
//
//                json.put("actual_channel", channel);
//
//                ChatApi.getInstance().sendMessage(json, channel);
//
//
//                System.out.println("data===>" +data);
////                System.out.println("meaasge" + message);


            }
        });

        dialoge.show();


    }



    private void setRingToneToCall() {
        Log.e("calling", "called");


//        AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
//        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
//        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
//        audioManager.setStreamVolume(AudioManager.STREAM_RING, maxVolume, AudioManager.FLAG_SHOW_UI + AudioManager.FLAG_PLAY_SOUND);


        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mediaPlayer = MediaPlayer.create(getApplicationContext(), RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));

        try {
            mediaPlayer.setVolume((float) (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) / 7.0), (float) (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) / 7.0));
        } catch (Exception e) {
            e.printStackTrace();
        }

        mediaPlayer.start();
    }


    public void sendAudioCallToChatList() {


//        override date and time
        Date date = new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);
        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

        TimeZone tz = TimeZone.getTimeZone(Tools.getData(context, "timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(date);

        if (ChatApi.getInstance().getIsChatView()) {

            Intent it = new Intent(this, Phone.class);
            startActivity(it);
        }
    }



    private void downloadProfileImageReceiver() {


        Picasso.with(this)
                .load(this.getString(R.string.server_url2) + receiverImage)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(ivOtherUserProfile);


        //        img_user1450170726_566fd966017c1.jpg

//        img_user1477998338.jpg

//


    }


//    public void downloadProfileImage() {
//
//        Picasso.with(this)
//                .load(this.getString(R.string.server_url2) + Tools.getData(this, "photo"))
//                .placeholder(R.drawable.empty)
//                .error(R.drawable.empty)
//                .into(userNotificationView);
//
//
//    }

    public void downloadProfileImageTwo() {

        Picasso.with(this)
                .load(this.getString(R.string.server_url2) + Tools.getData(this, "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(ivRegisterProfile);


    }


    public void hideKeyboard() {

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ECLAIR
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            // Take care of calling this method on earlier versions of
            // the platform where it doesn't exist.
            onBackPressed();
        }

        return super.onKeyDown(keyCode, event);
    }


    public class DoAudioUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            System.out.println("result on Audio =======>" + result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image results%@", String.valueOf(img));
                    String type;
                    if (is_group) {
                        type = "Groupe";
                    } else {
                        type = "Amis";
                    }


                    System.out.println("fileurl=====>" + AudioSavePathInDevice);
                    Map<String, String> json = new HashMap<>();
                    json.put("message", img.getString("data"));
                    json.put("chatType", type);
                    json.put("receiverID", String.valueOf(fId));
                    json.put("contentType", "Audio");
                    json.put("senderID", Tools.getData(Phone.this, "idprofile"));
                    json.put("senderName", Tools.getData(Phone.this, "name"));
                    String senderPhoto = Tools.getData(Phone.this, "photo");

                    json.put("senderImage", senderPhoto);

                    json.put("actual_channel", currentChannel);


                    System.out.println("chattype===>" + type);
                    System.out.println("receiverId" + String.valueOf(fId));
                    System.out.println("senderId" + Tools.getData(Phone.this, "idprofile"));


                    ChatApi.getInstance().sendMessage(json, currentChannel);

                    hideKeyboard();

//                    Chat.sendMsg(img.getString("data"), Chat.fId, "photo", "",type);


//                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(Phone.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {
            ArrayList<String> images = new ArrayList<>();
            images.add(imgPath);

//            ArrayList<String> audiourls = new ArrayList<>();
//            images.add(fileUrl);
            return Tools.doFileUploadForChat(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.savePhotoAction), imgPath = AudioSavePathInDevice, Tools.getData(Phone.this, "token"), Tools.getData(Phone.this, "idprofile"), "", "");


//            /storage/sdcard0/JCIENAudioRecording.3gp
        }

    }









    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == this.RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = this.getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = this.managedQuery(selectedImage, proj, null, null,
                            null);
                    final int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    imgPath = cursor.getString(column_index);

                    Log.i("galery path", imgPath);

                }
                break;
            case 2:
                if (resultCode == this.RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Welcome" + destination);

                    //new DoUpload().execute();
                    imgPath = String.valueOf(destination);

                    Log.i("camera path", imgPath);
                }
                break;

            default:
                break;
        }

    }


    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        try {

            while (i < string) {

                stringBuilder.append(RandomAudioFileName.
                        charAt(random.nextInt(RandomAudioFileName.length())));

                i++;
            }


        } catch (NullPointerException exce) {
            System.out.println("exce ==>" + exce.getMessage());
        }
        return stringBuilder.toString();
    }




    private void requestPermission() {
        ActivityCompat.requestPermissions(Phone.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(Phone.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Phone.this, "Permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }




}


