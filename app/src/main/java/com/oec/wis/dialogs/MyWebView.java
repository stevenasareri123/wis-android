package com.oec.wis.dialogs;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.oec.wis.R;

public class MyWebView extends AppCompatActivity {
    WebView webView;
    Typeface font;
    TextView headerTitleTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_web_view);
        String price = getIntent().getExtras().getString("price");
        String idPub = getIntent().getExtras().getString("idPub");




        Resources res = getResources();
        String url  = String.format(res.getString(R.string.payment_url),price, idPub);



//        String url = String.format("http://vps222729.ovh.net/wis/auth/paypub?prix=%1$s&idpub=\'%2$s\'", price, idPub);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView v, String url) {
                findViewById(R.id.progress).setVisibility(View.GONE);
            }
        });

        findViewById(R.id.progress).setVisibility(View.VISIBLE);
        webView.loadUrl(url);
         headerTitleTV =(TextView)findViewById(R.id.headerTitleTV);



        try{
            Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            headerTitleTV.setTypeface(font);


        }catch (NullPointerException ex){

        }
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
