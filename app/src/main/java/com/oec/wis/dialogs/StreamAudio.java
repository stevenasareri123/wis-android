package com.oec.wis.dialogs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.oec.wis.Interfaces.VoiceListener;
import com.oec.wis.R;

/**
 * Created by asareri08 on 29/03/17.
 */

public class StreamAudio extends Activity implements View.OnClickListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {

    MediaPlayer mp;
    ProgressDialog pd;

    Context context;
    String audioPath;
    VoiceListener voiceListener;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main);
//        Button bt = (Button)findViewById(R.id.play);
//        bt.setOnClickListener(this);
    }


    public StreamAudio (Context context,String audioPath,VoiceListener listener){

        this.context = context;
        this.audioPath = audioPath;

        this.voiceListener = listener;


        playStraming();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.i("StreamAudioDemo", "prepare finished");
        pd.setMessage("Playing.....");
        mp.start();

        voiceListener.onStreamingPlayed(mp);

        getCurrentDuration(mp);


    }


    public void playStraming(){

        try
        {
            pd = new ProgressDialog(context);
            pd.setMessage("Buffering.....");
            pd.show();
            mp = new MediaPlayer();
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.setOnPreparedListener(this);
            mp.setOnErrorListener(this);
            mp.setDataSource(audioPath);
            mp.prepareAsync();
            mp.setOnCompletionListener(this);
        }
        catch(Exception e)
        {
            Log.e("StreamAudioDemo", e.getMessage());
        }
    }

    public String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int)( milliseconds / (1000*60*60));
        int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
        int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
        // Add hours if there
        if(hours > 0){
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if(seconds < 10){
            secondsString = "0" + seconds;
        }else{
            secondsString = "" + seconds;}

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    public int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    public String   getCurrentDuration(MediaPlayer mp){

        long totalDuration = mp.getDuration();
        long currentDuration = mp.getCurrentPosition();
        int duration = (int) currentDuration;

        String endt = milliSecondsToTimer(currentDuration);

        // Displaying Total Duration time

        Log.e("TOTAL duraion", String.valueOf(totalDuration));

        voiceListener.onPlayButtonClicked(endt);

        return endt;

    }




    public int getProgress(){

        int progress = (int)(getProgressPercentage(mp.getDuration(),mp.getCurrentPosition()));
        Log.d("Progress", String.valueOf(progress));

        return progress;


    }

    @Override
    public void onClick(View v) {
        try
        {
            pd = new ProgressDialog(context);
            pd.setMessage("Buffering.....");
            pd.show();
            mp = new MediaPlayer();
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.setOnPreparedListener(this);
            mp.setOnErrorListener(this);
            mp.setDataSource("http://www.robtowns.com/music/blind_willie.mp3");
            mp.prepareAsync();
            mp.setOnCompletionListener(this);
        }
        catch(Exception e)
        {
            Log.e("StreamAudioDemo", e.getMessage());
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        pd.dismiss();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        pd.dismiss();
        mp.stop();
        mp.release();
        Toast.makeText(context, "Completed", Toast.LENGTH_LONG).show();
    }
}

