package com.oec.wis.dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SharePhoto extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    Button bShare;
    private FrameLayout emojicons;
    private EmojiconEditText etCmt;
    private ImageView ivPhoto;
    TextView headerTitleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_photo);
        Tools.showLoader(this,getResources().getString(R.string.progress_loading));

        emojicons = (FrameLayout) findViewById(R.id.emojicons);
        etCmt = (EmojiconEditText) findViewById(R.id.etDesc);
        etCmt.setUseSystemDefault(true);
        ivPhoto = (ImageView) findViewById(R.id.ivVideo);
        bShare = (Button) findViewById(R.id.bShare);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
        setEmojiconFragment(false);

        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);

        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + getIntent().getExtras().getString("path"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ivPhoto.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });


        bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharePhoto(getIntent().getExtras().getInt("id"), etCmt.getText().toString());
            }
        });

        etCmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etCmt.getRight() - etCmt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
                            findViewById(R.id.emojicons).setVisibility(View.GONE);
                        else
                            findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(etCmt, emojicon);
        emojicons.setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(etCmt);
    }

    private void sharePhoto(int idPhoto, String cmt) {
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(SharePhoto.this, "idprofile"));
            jsonBody.put("id_photo", String.valueOf(idPhoto));
            jsonBody.put("commentaire", cmt);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqShare = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.sharephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                finish();
                                Toast.makeText(SharePhoto.this, getString(R.string.msg_photo_shared), Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(SharePhoto.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                            Tools.dismissLoader();
//                            findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Tools.dismissLoader();
                            //Toast.makeText(SharePhoto.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                            findViewById(R.id.loading).setVisibility(View.VISIBLE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (SharePhoto.this != null)
                //Toast.makeText(SharePhoto.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                findViewById(R.id.loading).setVisibility(View.VISIBLE);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(SharePhoto.this, "lang_pr"));
                headers.put("token", Tools.getData(SharePhoto.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqShare);
    }
}
