package com.oec.wis.dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.CmtAdapter;
import com.oec.wis.adapters.NotifAdapter;
import com.oec.wis.adapters.ReplyCommentAdapter;
import com.oec.wis.adapters.ReplyCommentsListener;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class Comments extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener,ReplyCommentsListener {
    ListView lvCmt;
    int p;
    EmojiconEditText etCmt;
    String uName, uPic;
    CmtAdapter adapter;
    List<WISCmt> cmts;
    List<WISCmt> replycmts;
    JSONObject jsonBody;
    ReplyCommentAdapter replyAdapter;
    TextView headerTitleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_comments);
        Tools.showLoader(this,getResources().getString(R.string.progress_loading));

        p = getIntent().getExtras().getInt("p");
        uName = Tools.getData(this, "firstname_prt") + " " + Tools.getData(this, "lastname_prt");
        uPic = Tools.getData(this, "photo");
        loadControls();
        loadCmts();
        setListener();
    }

    private void setListener() {
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.ivSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCmt();
            }
        });
        etCmt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND) {
                    sendCmt();
                }
                return true;
            }
        });
        etCmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etCmt.getRight() - etCmt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
                            findViewById(R.id.emojicons).setVisibility(View.GONE);
                        else
                            findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void loadCmts() {
//       findViewById(R.id.loading).setVisibility(View.VISIBLE);
        getAllCmts();
//        adapter = new CmtAdapter(this, AdaptActu.actuList.get(p).getCmts());
//        lvCmt.setAdapter(adapter);
    }

    private void loadControls() {
        lvCmt = (ListView) findViewById(R.id.lvCmt);
        etCmt = (EmojiconEditText) findViewById(R.id.etCmt);
        etCmt.setUseSystemDefault(true);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
        setEmojiconFragment(true);


        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    private void sendCmt() {
        if (!TextUtils.isEmpty(etCmt.getText().toString())) {
            String msg = etCmt.getText().toString();
           jsonBody = new JSONObject();
            try {
                jsonBody.put("id_profil", String.valueOf(Tools.getData(this, "idprofile")));
                jsonBody.put("id_act",AdaptActu.actuList.get(p).getId());
                jsonBody.put("text", msg);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            requetsSendCmt(AdaptActu.actuList.get(p).getId(), msg);
            etCmt.setText(null);
            Tools.hideKeyboard(this);
        }
    }

    private void requetsSendCmt(int idActu, final String cmt) {
//        JSONObject jsonBody = new JSONObject();
//        try {
//            jsonBody.put("id_profil", String.valueOf(Tools.getData(this, "idprofile")));
//            jsonBody.put("id_act", String.valueOf(idActu));
//            jsonBody.put("text", cmt);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        JsonObjectRequest reqCmt = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.send_cmt_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //{"Message":"","data":"7","result":true}
                        try {
                            if (response.getBoolean("result")) {
                                int id = response.getInt("data");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String cDate = sdf.format(new Date());

//                                override date and time
                                Date date=new Date();
                                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                sourceFormat.format(date);
                                Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(),"timezone"));
                                SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                destFormat.setTimeZone(tz);

                                String usertimezone = destFormat.format(date);
                                Log.i("resilt timezone ",usertimezone);

                                String fullname = "";

                                if(Tools.getData(getApplicationContext(), "name").equals("")||Tools.getData(getApplicationContext(), "name")==null)
                                {
                                    uName = Tools.getData(getApplicationContext(), "firstname_prt") + " " + Tools.getData(getApplicationContext(), "lastname_prt");
                                }
                                else {
                                    uName= Tools.getData(getApplicationContext(), "name");
                                }




                                int user_id = Integer.parseInt(Tools.getData(getApplicationContext(), "idprofile"));

                                Log.i("comment user", String.valueOf(user_id));

                                if(adapter!=null){
                                    AdaptActu.actuList.get(p).getCmts().add(new WISCmt(id, new WISUser(user_id, uName, uPic), cmt, usertimezone,"true"));
                                }else{

                                    cmts.add(new WISCmt(id, new WISUser(user_id, uName, uPic), cmt,usertimezone,"true"));

                                    AdaptActu.SELECTED.setCmts(cmts);
                                    adapter = new CmtAdapter(Comments.this, AdaptActu.SELECTED.getCmts(),Comments.this);
                                    lvCmt.setAdapter(adapter);

                                }


                                AdaptActu.SELECTED.setComment_count(AdaptActu.SELECTED.getCmts().size());

                                adapter.notifyDataSetChanged();
                                lvCmt.invalidateViews();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String res = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Comments.this, "token"));
                headers.put("lang", Tools.getData(Comments.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmt);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(etCmt, emojicon);
        findViewById(R.id.emojicons).setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(etCmt);
    }


    private void getAllCmts() {
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_act", String.valueOf(AdaptActu.SELECTED.getId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqCmts = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.allcmt_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
//                                findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                                Tools.dismissLoader();
                                cmts = new ArrayList<>();
                                JSONArray aCmts = response.getJSONArray("data");

                                for (int j = 0; j < aCmts.length(); j++) {
                                    JSONObject obj = aCmts.getJSONObject(j);









                                    try {
//                                        adapter = new CmtAdapter(this, AdaptActu.actuList.get(p).getCmts());
//                                        lvCmt.setAdapter(adapter);


//                                        Log.i("comment result",response.toString());
                                        int idd = Integer.parseInt(obj.getJSONObject("info_created_by").getString("id"));
                                        cmts.add(new WISCmt(obj.getInt("id_comment"), new WISUser(idd, obj.getJSONObject("info_created_by").getString("name"), obj.getJSONObject("info_created_by").getString("photo")), obj.getString("text"), obj.getString("last_edit_at"),"true"));
                                        AdaptActu.SELECTED.setCmts(cmts);

                                        adapter = new CmtAdapter(Comments.this, AdaptActu.SELECTED.getCmts(),Comments.this);
                                        lvCmt.setAdapter(adapter);


//                                        Reply adapter

//                                        for (int r=0;r<reply_comments.length();r++){
//
//
//
////                                            replycmts.add(new WISCmt(.getInt("id_comment"), new WISUser(idd, obj.getJSONObject("info_created_by").getString("name"), obj.getJSONObject("info_created_by").getString("photo")), obj.getString("text"), obj.getString("last_edit_at"),"true"));
//
//
//                                        }

//



                                        if (AdaptActu.SELECTED.getCmts().size() > 0) {
                                            lvCmt.setVisibility(View.VISIBLE);
                                        } else {
                                            lvCmt.setVisibility(View.GONE);
                                        }
                                    } catch (Exception e2) {
                                        String x = e2.getMessage();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            //Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(ActuDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(Comments.this, "token"));
                headers.put("lang", Tools.getData(Comments.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqCmts);
    }


    @Override
    public void replyListener() {
        getAllCmts();
    }

    @Override
    public void addComments(int p, String message,ListView replyView,String comment_id) {
//        requetsSendCmt(AdaptActu.actuList.get(p).getId(),message);


Log.i("actu id", String.valueOf(AdaptActu.SELECTED.getId()));
        jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", String.valueOf(Tools.getData(this, "idprofile")));
            jsonBody.put("id_act",String.valueOf(AdaptActu.SELECTED.getId()));
            jsonBody.put("text", message);
            jsonBody.put("is_reply","1");
            jsonBody.put("reply_id",comment_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        requetsSendCmt(AdaptActu.SELECTED.getId(),message);

//        replyAdapter  =  new ReplyCommentAdapter(Comments.this,replycmts);
//        replyView.setAdapter(adapter);
//        replyView.deferNotifyDataSetChanged();

        adapter.notifyDataSetChanged();


    }

    public void sendReplyComments(int idActu, final String cmt){

    }


}
