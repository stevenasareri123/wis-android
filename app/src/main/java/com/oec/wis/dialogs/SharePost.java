package com.oec.wis.dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SharePost extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    private FrameLayout emojicons;
    private EmojiconEditText etDesc;
    TextView headerTitleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post);
        Tools.showLoader(this,getResources().getString(R.string.progress_loading));


        etDesc = (EmojiconEditText) findViewById(R.id.etDesc);
        emojicons = (FrameLayout) findViewById(R.id.emojicons);
        setEmojiconFragment(false);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        ImageView ivVideo = (ImageView) findViewById(R.id.ivVideo);
        RelativeLayout rlThumb = (RelativeLayout) findViewById(R.id.rlThumb);
        final ImageView ivThumb = (ImageView) findViewById(R.id.ivThumb);
        Button dialogButton = (Button) findViewById(R.id.bShare);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

         try{

             Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
             headerTitleTV.setTypeface(font);
             dialogButton.setTypeface(font);
             etDesc.setTypeface(font);

         }catch (NullPointerException ex){

         }



        tvTitle.setText(AdaptActu.SELECTED.getCmtActu());
        if (AdaptActu.SELECTED.getpType().equals("")) {
            rlThumb.setVisibility(View.GONE);
        } else if (AdaptActu.SELECTED.getpType().equals("partage_photo") || !TextUtils.isEmpty(AdaptActu.SELECTED.getImg())) {
            rlThumb.setVisibility(View.VISIBLE);
            ivVideo.setVisibility(View.INVISIBLE);
            ApplicationController.getInstance().getImageLoader().get(SharePost.this.getString(R.string.server_url3) + AdaptActu.SELECTED.getImg(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else if (AdaptActu.SELECTED.getpType().equals("video") || !TextUtils.isEmpty(AdaptActu.SELECTED.getVideo())) {
            ivVideo.setVisibility(View.VISIBLE);
            rlThumb.setVisibility(View.VISIBLE);
            ApplicationController.getInstance().getImageLoader().get(SharePost.this.getString(R.string.server_url4) + AdaptActu.SELECTED.getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else if (AdaptActu.SELECTED.getpType().equals("partage_post")) {
            if (TextUtils.isEmpty(AdaptActu.SELECTED.getVideo()))
                ivVideo.setVisibility(View.INVISIBLE);
            if (TextUtils.isEmpty(AdaptActu.SELECTED.getImg()))
                rlThumb.setVisibility(View.GONE);
        }


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendShare(AdaptActu.SELECTED.getId(), etDesc.getText().toString());
            }
        });

        etDesc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etDesc.getRight() - etDesc.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
                            findViewById(R.id.emojicons).setVisibility(View.GONE);
                        else
                            findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void sendShare(int idAct, String desc) {
        findViewById(R.id.bShare).setEnabled(false);
        findViewById(R.id.etDesc).setEnabled(false);
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(SharePost.this, "idprofile") + "\",\"id_act\":\"" + idAct + "\",\"commentaire\":\"" + desc + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqActu = new JsonObjectRequest(SharePost.this.getString(R.string.server_url) + SharePost.this.getString(R.string.share_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_share_success), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            findViewById(R.id.bShare).setEnabled(true);
                            findViewById(R.id.etDesc).setEnabled(true);
//                            findViewById(R.id.loading).setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            findViewById(R.id.bShare).setEnabled(true);
                            findViewById(R.id.etDesc).setEnabled(true);
//                            findViewById(R.id.loading).setVisibility(View.VISIBLE);
                            //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.bShare).setEnabled(true);
                findViewById(R.id.etDesc).setEnabled(true);
//                findViewById(R.id.loading).setVisibility(View.VISIBLE);
                //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(SharePost.this, "token"));
                headers.put("lang", Tools.getData(SharePost.this, "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(etDesc, emojicon);
        emojicons.setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(etDesc);
    }
}
