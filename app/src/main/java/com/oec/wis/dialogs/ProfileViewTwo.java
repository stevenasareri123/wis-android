package com.oec.wis.dialogs;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.ImagePickerListener;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragActu;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragContact;
import com.oec.wis.fragments.FragMyLocation;
import com.oec.wis.fragments.FragMyPub;
import com.oec.wis.fragments.NewFragContact;
import com.oec.wis.tools.ItemClickSupport;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileViewTwo extends ActionBarActivity{


    ImageView ivProfile, ivProfile2;
    TextView tvFName, tvJob, tvCountry,userAddress;
    Bitmap oldPic;
    String imgPath = "";
    LinearLayoutManager actuLM;
    List<WISActuality> actuList;
    RecyclerView.Adapter adaptActu;
    RecyclerView lvActu;
    TextView contactTextTV,postTextTV,friendsTextTV,phototextTV,headerTitleTV;
    Typeface font;
    RelativeLayout contactRL,postRL,newsRL,chatRL,headermainRL;
    String senderId;
    int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.profile_type_two);

        id = getIntent().getExtras().getInt("id");

        Log.e("id", String.valueOf(id));
        Tools.showLoader(this,getResources().getString(R.string.progress_loading));

        senderId = Tools.getData(ProfileViewTwo.this, "idprofile");
        Log.e("senderId"  , senderId);
        loadControls();
        setListener();
        loadProfileData();
        getActivityLog();
        setFontTypeFace();
//        requestCountContact();
//        requestCountPub();
//        requestCountActu();
        loadActu();
    }

    private void setFontTypeFace() {

        try {
            font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            tvFName.setTypeface(font);
            tvJob.setTypeface(font);
            tvCountry.setTypeface(font);
            postTextTV.setTypeface(font);
            contactTextTV.setTypeface(font);
            phototextTV.setTypeface(font);
            friendsTextTV.setTypeface(font);
            tvFName.setTypeface(font);
            tvJob.setTypeface(font);
            userAddress.setTypeface(font);
            tvCountry.setTypeface(font);
            headerTitleTV.setTypeface(font);
        }catch(NullPointerException ex){
            Log.d("Exception",ex.getMessage());
        }
    }




    private void setListener() {

//          if(senderId.equals(id)) {

              contactRL.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      headermainRL.setVisibility(View.GONE);
                      Fragment fragment = new NewFragContact();
                      FragmentManager fragmentManager = getFragmentManager();
                      fragmentManager.beginTransaction()
                              .replace(R.id.frame_container, fragment).commit();

                  }
              });
              postRL.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      headermainRL.setVisibility(View.GONE);

                      Fragment fragment = new FragMyPub();
                      FragmentManager fragmentManager = getFragmentManager();
                      fragmentManager.beginTransaction()
                              .replace(R.id.frame_container, fragment).commit();
                  }
              });
              newsRL.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      headermainRL.setVisibility(View.GONE);

                      Fragment fragment = new FragActu();
                      FragmentManager fragmentManager = getFragmentManager();
                      fragmentManager.beginTransaction()
                              .replace(R.id.frame_container, fragment).commit();

                  }
              });
              chatRL.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      headermainRL.setVisibility(View.GONE);

                      UserNotification.setDISCUSSIONSELECTED(Boolean.TRUE);
                      Fragment fragment = new FragChat();
                      FragmentManager fragmentManager = getFragmentManager();
                      fragmentManager.beginTransaction()
                              .replace(R.id.frame_container, fragment).commit();

                  }
              });
//          }
//        else{
//
//              Log.e("else","else");
//          }

        findViewById(R.id.bEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), EditProfile.class));
            }
        });
       findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                ((DrawerLayout)findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
//                ((DrawerLayout).findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
        View.OnClickListener pickPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        };
        ivProfile.setOnClickListener(pickPhoto);
//        ivProfile2.setOnClickListener(pickPhoto);
        findViewById(R.id.bSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.llSave).setVisibility(View.GONE);
                if (!imgPath.equals(""))
                    new DoUpload().execute();
            }
        });
       findViewById(R.id.bCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.llSave).setVisibility(View.GONE);
                ivProfile.setImageBitmap(oldPic);
//                ivProfile2.setImageBitmap(oldPic);
            }
        });

       findViewById(R.id.tvContacts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headermainRL.setVisibility(View.GONE);

                Fragment fragment = new NewFragContact();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });


        findViewById(R.id.tvPub).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headermainRL.setVisibility(View.GONE);

                Fragment fragment = new FragMyPub();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });

        findViewById(R.id.tvActu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headermainRL.setVisibility(View.GONE);
                Fragment fragment = new FragActu();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });

        findViewById(R.id.tvGroup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headermainRL.setVisibility(View.GONE);
                Fragment fragment = new FragChat();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });
        ItemClickSupport.addTo(lvActu).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                AdaptActu.SELECTED = actuList.get(position);
                startActivity(new Intent(getApplicationContext(), ActuDetails.class));
            }
        });

        findViewById(R.id.tvCountry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserNotification.setLocationSelected(Boolean.TRUE);
                String addr = Tools.getData(ProfileViewTwo.this,"state")+","+Tools.getData(ProfileViewTwo.this,"country");
                UserNotification.setAddress(addr);
                UserNotification.setViewType("ProfileViewTwo");
                Intent i = new Intent(ProfileViewTwo.this,CustomLocationActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == this.RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getApplicationContext().getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    oldPic = ((BitmapDrawable) ivProfile.getDrawable()).getBitmap();
                    Bitmap sImage = BitmapFactory.decodeStream(imageStream);
                    ivProfile.setImageBitmap(sImage);
//                    ivProfile2.setImageBitmap(sImage);
                    findViewById(R.id.llSave).setVisibility(View.VISIBLE);
                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = this.managedQuery(selectedImage, proj, null, null,
                            null);
                    final int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    imgPath = cursor.getString(column_index);
                }
                break;
            default:
                break;
        }
    }

    private void loadControls() {
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
//        ivProfile2 = (ImageView) findViewById(R.id.ivProfile2);
        tvFName = (TextView) findViewById(R.id.tvFName);
        tvJob = (TextView) findViewById(R.id.tvJob);
        tvCountry = (TextView) findViewById(R.id.tvCountry);
        userAddress = (TextView)findViewById(R.id.tvaddr);


        tvFName = (TextView) findViewById(R.id.tvFName);
        tvJob = (TextView) findViewById(R.id.tvJob);
        tvCountry = (TextView) findViewById(R.id.tvCountry);
        userAddress = (TextView)findViewById(R.id.tvaddr);
        contactTextTV=(TextView)findViewById(R.id.contactTextTV);
        postTextTV=(TextView)findViewById(R.id.postTextTV);
        friendsTextTV=(TextView)findViewById(R.id.friendsTextTV);
        phototextTV=(TextView)findViewById(R.id.photoTextTV);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

        contactRL =(RelativeLayout)findViewById(R.id.contactRL);
        postRL=(RelativeLayout)findViewById(R.id.postRL);
        newsRL=(RelativeLayout)findViewById(R.id.newsRL);
        chatRL=(RelativeLayout)findViewById(R.id.chatRL);


        headermainRL=(RelativeLayout)findViewById(R.id.headermainRL);
        lvActu = (RecyclerView)findViewById(R.id.lvActu);
        lvActu.setHasFixedSize(true);
        actuLM = new LinearLayoutManager(getApplicationContext());
        lvActu.setLayoutManager(actuLM);
        actuList = new ArrayList<>();
    }

    private void loadProfileData() {
        if (Tools.getData(getApplicationContext(), "name").replace("null", "").equals(""))
            tvFName.setText(Tools.getData(getApplicationContext(), "firstname_prt") + " " + Tools.getData(getApplicationContext(), "lastname_prt"));
        else
            tvFName.setText(Tools.getData(getApplicationContext(), "name").replace("null", ""));

//        tvCountry.setText(Tools.getData(getApplicationContext(), "country"));
        String city_country = Tools.getData(ProfileViewTwo.this, "state").concat(" ").concat(Tools.getData(ProfileViewTwo.this, "country"));
        tvCountry.setText(city_country);
        String type = Tools.getData(getApplicationContext(),"profiletype");

        if(type.equals("profiletype")){
            userAddress.setVisibility(View.INVISIBLE);

        }else{
//            userAddress.setVisibility(View.VISIBLE);
//            userAddress.setText(Tools.getData(getApplicationContext(),"tel_pr"));
        }



        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + Tools.getData(getApplicationContext(), "photo"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivProfile.setImageBitmap(response.getBitmap());
//                    ivProfile2.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adaptActu!=null) {
            adaptActu.notifyDataSetChanged();
        }
    }

    private void doUpdate(final String photo) {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getApplicationContext(), "idprofile") + "\",\"name_img\": \"" + photo + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.update_img_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Tools.saveData(getApplicationContext(), "photo", photo);
                            }
                        } catch (Exception e) {

                        }
//                        findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//               findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void requestCountContact() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getApplicationContext(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_contact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvContact = (TextView)findViewById(R.id.tvContacts);
                                if (count > 500)
                                    tvContact.setText("500+");
                                else
                                    tvContact.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                       findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//               findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void requestCountPub() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getApplicationContext(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_pub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvPub = (TextView)findViewById(R.id.tvPub);
                                tvPub.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
                        Tools.dismissLoader();
//                        findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }
    private void getActivityLog(){
        JSONObject jsonBody = null;

        try {
            String json = "{\"user_id\": \"" + Tools.getData(ProfileViewTwo.this, "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.activityLog), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                Log.i("actrivity response", String.valueOf(response));

                                TextView tvContact = (TextView)findViewById(R.id.tvContacts);
                                TextView tvPub = (TextView)findViewById(R.id.tvPub);
                                TextView tvAct = (TextView)findViewById(R.id.tvActu);
                                TextView tvgroup = (TextView)findViewById(R.id.tvGroup);

                                JSONObject json = response.getJSONObject("data");
                                Log.i("actrivity response json", String.valueOf(json));
                                tvContact.setText(json.getString("contact"));
                                tvPub.setText(json.getString("pub"));
                                tvAct.setText(json.getString("actuality"));
                                tvgroup.setText(json.getString("group"));

                            }
                        } catch (Exception e) {

                        }
//                       findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ProfileViewTwo.this, "token"));
                headers.put("lang", Tools.getData(ProfileViewTwo.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);

    }

    private void requestCountActu() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getApplicationContext(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_actu_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvActu = (TextView)findViewById(R.id.tvActu);
                                tvActu.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                        findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void loadActu() {
//       findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getApplicationContext(), "idprofile"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.myact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("path_photo");
                                        } catch (Exception e) {
                                        }

                                        String video = "";
                                        try {
                                            video = obj.getString("path_video");
                                        } catch (Exception e) {
                                        }

                                        int nCmt = obj.getInt("nbr_comment");
                                        List<WISCmt> cmts = new ArrayList<>();
                                        if (nCmt > 0) {
                                            JSONArray aCmts = obj.getJSONArray("comment");
                                            for (int j = 0; j < aCmts.length(); j++) {
                                                try {
                                                    JSONObject ocmt = aCmts.getJSONObject(j);
                                                    JSONObject oUser = ocmt.getJSONObject("created_by");
                                                    cmts.add(new WISCmt(ocmt.getInt("id_comment"), new WISUser(oUser.getInt("idprofile"), oUser.getString("name"), oUser.getString("photo")), ocmt.getString("text"), ocmt.getString("last_edit_at"),"false"));
                                                } catch (Exception e2) {
                                                    String x = e2.getMessage();
                                                }
                                            }
                                        }
                                        String text = "";
                                        try {
                                            text = obj.getString("text");
                                        } catch (Exception e3) {
                                        }

                                        String cmtActu = "";
                                        try {
                                            cmtActu = obj.getString("commentair_act");
                                        } catch (Exception e3) {
                                        }

                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");
                                        } catch (Exception e3) {
                                        }
                                        actuList.add(new WISActuality(0,obj.getInt("id_act"), text, "", photo, video, thumb, obj.getInt("nbr_jaime"), obj.getInt("nbr_vue"), obj.getBoolean("like_current_profil"), obj.getString("type_act"), cmts, cmtActu, obj.getString("created_at"), new WISUser(obj.getJSONObject("created_by").getInt("idprofile"), obj.getJSONObject("created_by").getString("name"), obj.getJSONObject("created_by").getString("photo")),obj.getInt("dis_like"),obj.getBoolean("dislike_current_profil"),obj.getInt("share_count"),nCmt,null,null));

                                    } catch (Exception e) {

                                    }
                                }

                                adaptActu = new AdaptActu(actuList,null);

                                lvActu.setAdapter(adaptActu);
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.err_no_new_actu), Toast.LENGTH_SHORT).show();
                            }

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }



    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//           findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Tools.dismissLoader();
//                   findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                Tools.dismissLoader();
//               findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Tools.doFileUpload(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.upload_profile_meth), imgPath, Tools.getData(getApplicationContext(), "token"));
        }
    }

}
