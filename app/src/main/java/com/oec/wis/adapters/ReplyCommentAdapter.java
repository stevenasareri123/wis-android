package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.Comments;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconTextView;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by asareri08 on 20/06/16.
 */
public class ReplyCommentAdapter extends BaseAdapter {

    Context context;
    List<WISCmt> data;
    TextView comment_userName;

    public ReplyCommentAdapter(Context context, List<WISCmt> data){
        this.data = data;
        this.context = context;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;

        convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.replycomment_row, null);



        holder = new ViewHolder(convertView);
        holder.ivUPic.setImageResource(0);
        holder.tvUName.setText(data.get(position).getUser().getFirstName());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SimpleDateFormat create_date_format = new SimpleDateFormat("dd-MM-yy");
        String created_at="";
        Date d = null;
        try {
            d = format.parse(data.get(position).getDate());
            created_at = create_date_format.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvDate.setText(rDate);

        holder.tvCmt.setText(data.get(position).getCmt());

        holder.created_at.setText(created_at);

        Log.i("comments type",data.get(position).getCommentview_type());





        holder.ivUPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                data.get(position).getUser().getId()
                Log.i("user", String.valueOf(data.get(position).getUser().getId()));
                String user_id = Tools.getData(context, "idprofile");
                String amis_id = String.valueOf(data.get(position).getUser().getId());
                if(user_id.contains(amis_id))
                {
                    Log.i(" amis", String.valueOf(Integer.parseInt(amis_id)));
                    Comments cmnt = new Comments();

                    Intent i = new Intent(context, ProfileViewTwo.class);
                    i.putExtra("id",Integer.parseInt(user_id));
                    context.startActivity(i);





                }else{
                    Log.i("amis", String.valueOf(Integer.parseInt(amis_id)));
                    Intent i = new Intent(context, ProfileView.class);
                    i.putExtra("id",Integer.parseInt(amis_id));
                    context.startActivity(i);
                }
            }
        });

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.get(position).getUser().getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivUPic.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivUPic.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivUPic.setImageResource(R.drawable.empty);
                }
            }
        });
        return convertView;

    }

    private static class ViewHolder {
        TextView tvUName, tvDate,created_at;
        EmojiconTextView tvCmt;
        ImageView ivUPic;

        public ViewHolder(View view) {
            tvUName = (TextView) view.findViewById(R.id.tvUName);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvCmt = (EmojiconTextView) view.findViewById(R.id.tvCmt);
            ivUPic = (ImageView) view.findViewById(R.id.ivUPic);
            created_at = (TextView)view.findViewById(R.id.created_date);
            view.setTag(this);
        }
    }
}
