package com.oec.wis.adapters;

import android.widget.ListView;

/**
 * Created by asareri08 on 20/06/16.
 */
public interface ReplyCommentsListener {

    void replyListener();

    void addComments(final int p, String message, ListView replyView,String comment_id);
}
