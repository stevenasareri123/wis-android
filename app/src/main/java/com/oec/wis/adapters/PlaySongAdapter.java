package com.oec.wis.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.R;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISMusic;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by asareri12 on 30/01/17.
 */

public class PlaySongAdapter extends BaseAdapter {
    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<WISMusic> worldpopulationlist = null;
    private ArrayList<WISMusic> arraylist;
    Typeface font;

    public PlaySongAdapter(Context context, List<WISMusic> worldpopulationlist) {
        mContext = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<WISMusic>();
        this.arraylist.addAll(worldpopulationlist);
    }



    @Override
    public int getCount() {
        return worldpopulationlist.size();
    }

    @Override
    public WISMusic getItem(int position) {
        return worldpopulationlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        TextView songName ;
        TextView itemCount;
        TextView songDuration;
        CircularImageView playicon;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.song, null);
            holder.itemCount = (TextView) view.findViewById(R.id.itemCount);
            holder.songName = (TextView) view.findViewById(R.id.song_title);
            holder.songDuration = (TextView) view.findViewById(R.id.song_duration);
            holder.playicon = (CircularImageView) view.findViewById(R.id.btPly);

            Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/Harmattan-R.ttf");

            holder.itemCount.setTypeface(font);
            holder.songName.setTypeface(font);
            holder.songDuration.setTypeface(font);

            // Locate the ImageView in listview_item.xml
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        //        set Type face

        font= Typeface.createFromAsset(mContext.getAssets(),"fonts/Harmattan-R.ttf");
        holder.itemCount.setTypeface(font);
        holder.songDuration.setTypeface(font);
        holder.songName.setTypeface(font);



        holder.songName.setText(worldpopulationlist.get(position).getName());
        holder.songDuration.setText(worldpopulationlist.get(position).getDuration());
        holder.playicon.setBorderColor(Color.WHITE);
        //        holder.playicon.setImageResource(android.R.drawable.ic_media_play);
        int selected = new UserNotification().getSelected();
        Log.i("selected", String.valueOf(selected));
        Log.i("selected", String.valueOf(position));

        if(position==new UserNotification().getSelectedPosition()){

            Log.e("selected adap pos::=>", String.valueOf(new UserNotification().getSelectedPosition()));

            Log.e("selected adap pos::", String.valueOf(new UserNotification().getClicked()));

            if(new UserNotification().getClicked()){

                holder.playicon.setImageResource(R.drawable.pause_blue_icon);

            }else{


                holder.playicon.setImageResource(R.drawable.play_blue_icon);
            }



        }else{

            holder.playicon.setImageResource(0);

        }

//        if(position!=selected)
//        {
////            holder.playicon.setImageResource(R.drawable.black_image);
////            holder.playicon.setBorderColor(Color.BLACK);
//        }
//        else {
//            if((new UserNotification().getClicked()== Boolean.TRUE))
//            {
//                holder.playicon.setImageResource(R.drawable.play_blue_icon);
//                holder.playicon.setBorderColor(Color.WHITE);
//            }
//            else {
//                holder.playicon.setImageResource(R.drawable.pause_blue_icon);
//                holder.playicon.setBorderColor(Color.WHITE);
//            }
//
//
//        }
        holder.itemCount.setText(String.valueOf(position+1));


//        final int ds = position;
//
//        view.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                new UserNotification().setSelected(position);
//                stopRadio();
//                playSongs(ds);
//
//            }
//        });




        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        worldpopulationlist.clear();
        if (charText.length() == 0) {
            worldpopulationlist.addAll(arraylist);
        } else {
            for (WISMusic wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)||wp.getArtists().toLowerCase(Locale.getDefault()).contains(charText)||wp.getDateTime().toLowerCase(Locale.getDefault()).contains(charText)) {
                    worldpopulationlist.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
