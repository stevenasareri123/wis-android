package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WisEvents;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asareri08 on 29/09/16.
 */
public class EventsAdapter extends BaseAdapter {

    List<WisEvents>data;
    Context context;

    DeleteEventListener listener;

    public EventsAdapter(Context context,List<WisEvents>data,DeleteEventListener listener){

        this.context = context;

        this.data = data;

        this.listener = listener;


    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.event_row_item, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String eventDate =formatDate(data.get(position).getEventDate());



        String eventTime =formatTime(data.get(position).getEventTime().concat("h"));

        Log.d("time" ,eventTime);

        String events = data.get(position).getComments();

//        String eventsDetails = "Le".concat(" ").concat(eventDate).concat(" à").concat(" ").concat(eventTime).concat("h : ").concat(events);
        //        holder.eventsDes.setText(eventsDetails);

        holder.eventsDes.setText(events);


        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-R.ttf");
        holder.eventsDes.setTypeface(font);
         holder.timeTV.setTypeface(font);
        holder.timeLabelTV.setTypeface(font);


        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                deleteEvent(String.valueOf(data.get(position).getId()),position);

            }
        });


        return convertView;
    }



    public String formatDate(String eventdate){

        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = fmt.parse(eventdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yyyy");
        return fmtOut.format(date);
    }

    public String formatTime(String eventTime){

        SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = fmt.parse(eventTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat("HH");
        return fmtOut.format(date);
    }

    public void deleteEvent(String eventId, final int pos){


        listener.deleteEventSucess(pos,eventId);

    }


    public static class ViewHolder{

        TextView eventsDes,timeTV,timeLabelTV;

        ImageView deleteBtn;

        public ViewHolder(View view){

            eventsDes = (TextView)view.findViewById(R.id.eventDetails);

            deleteBtn = (ImageView) view.findViewById(R.id.removeEvent);
            timeTV=(TextView)view.findViewById(R.id.timeTV);
            timeLabelTV=(TextView)view.findViewById(R.id.timeLabelTV);
            view.setTag(this);


        }



    }
}
