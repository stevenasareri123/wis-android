package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.R;
import com.oec.wis.entities.WISContact;

import java.util.List;

public class ContactAdapter extends BaseAdapter {
    Context context;
    List<WISContact> data;

    public ContactAdapter(Context context, List<WISContact> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_contact, null);

        holder = new ViewHolder(convertView);

        if (data.get(position).getPhoto() != null)
            holder.ivPhoto.setImageBitmap(data.get(position).getPhoto());

        holder.tvName.setText(data.get(position).getName());
        holder.tvPhone.setText(data.get(position).getNumber());

        holder.bInvit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                final String appPackageName = context.getPackageName();
                Uri applinkMarket=null,applink=null;// getPackageName() from Context or Activity object
                applinkMarket =  Uri.parse("market://details?id=" + appPackageName);
                applink = Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName);


                Uri uri = Uri.parse("smsto:" + data.get(position).getNumber());
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                String AppConnectionLink;
//                if(applinkMarket!=null){
//                    AppConnectionLink  = String.valueOf(applinkMarket);
//                }else if(applink!=null){
//                    AppConnectionLink = String.valueOf(applink);
//                }else{
//                    AppConnectionLink = context.getString(R.string.sms_invit);
//                }
                AppConnectionLink = context.getString(R.string.app_invit)+"https://play.google.com/store/apps/details?id=" + appPackageName;
                it.putExtra("sms_body",AppConnectionLink);
                context.startActivity(it);
            }
        });

        if (TextUtils.isEmpty(data.get(position).getEmail())) {
            holder.tvEmail.setVisibility(View.GONE);
        } else {
            holder.tvEmail.setText(data.get(position).getEmail());
            holder.tvEmail.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView tvName, tvPhone, tvEmail;
        CircularImageView ivPhoto;
        Button bInvit;

        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvPhone = (TextView) view.findViewById(R.id.tvPhone);
            tvEmail = (TextView) view.findViewById(R.id.tvEmail);
            ivPhoto = (CircularImageView) view.findViewById(R.id.ivPhoto);
            bInvit = (Button) view.findViewById(R.id.bInvit);
            view.setTag(this);
        }
    }
}
