package com.oec.wis.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.renderscript.Type;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oec.wis.R;
import com.oec.wis.entities.PopupElementsText;
import com.oec.wis.entities.Popupelements;

import java.util.ArrayList;

/**
 * Created by asareri08 on 02/06/16.
 */
public class PopupadApterText extends BaseAdapter{

    private LayoutInflater layoutInflater;
    private boolean isGrid;
    ArrayList<PopupElementsText> list;

    public PopupadApterText(Context context, boolean isGrid, ArrayList<PopupElementsText> list) {
        layoutInflater = LayoutInflater.from(context);
        this.isGrid = isGrid;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View view = convertView;
        try{
            if (view == null) {
                view = layoutInflater.inflate(R.layout.simple_list_item_text, parent, false);
//            if (isGrid) {
//                view = layoutInflater.inflate(R.layout.simple_grid_item, parent, false);
//            } else {
//
//            }

                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) view.findViewById(R.id.element1);
//            viewHolder.imageView = (ImageView) view.findViewById(R.id.icon_1);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            Context context = parent.getContext();
            viewHolder.textView.setText(list.get(position).getTitle().toString());

            System.out.println("position" +list.get(position).getTitle().toString());

            Typeface font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            viewHolder.textView.setTypeface(font);
        }catch (ClassCastException ex){
            System.out.println("ex-->" +ex.getMessage());
        }


//        viewHolder.textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(viewHolder.textView.isClickable() == true){
//                    viewHolder.textView.setBackgroundColor(R.color.gray1);
//                }else
//                {
//                    viewHolder.textView.setBackgroundColor(R.color.red1);
//                }
//            }
//        });
//        viewHolder.imageView.setImageResource(list.get(position).getId());

//
//        switch (position) {
//            case 0:
//                viewHolder.textView.setText(context.getString(R.string.actuality));
//                viewHolder.imageView.setImageResource(R.drawable.logo_wis);
//                break;
//            case 1:
//                viewHolder.textView.setText(context.getString(R.string.actuality));
//                viewHolder.imageView.setImageResource(R.drawable.logo_wis);
//                break;
//            default_ringtone:
//                viewHolder.textView.setText(context.getString(R.string.actuality));
//                viewHolder.imageView.setImageResource(R.drawable.logo_wis);
//                break;
//        }

        return view;
    }

    static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }
}
