package com.oec.wis.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISSongs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asareri12 on 30/01/17.
 */

public class ShareAlbumAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<WISSongs> worldpopulationlist = null;
    private ArrayList<WISSongs> arraylist;
    Typeface  font;



    //public  ChatIndivisualListener listener;
    public  ShareAlbumListener listener;

    public ShareAlbumAdapter(Context context, List<WISSongs> worldpopulationlist, ShareAlbumListener listener) {
        mContext = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<WISSongs>();
        this.arraylist.addAll(worldpopulationlist);
        this.listener=listener;
    }



    @Override
    public int getCount() {
        return worldpopulationlist.size();
    }

    @Override
    public WISSongs getItem(int position) {
        return worldpopulationlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        ImageView songIcon;
        TextView songName ;
        //TextView songDuration;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.sharesong, null);
            holder.songIcon = (ImageView) view.findViewById(R.id.songIcon);
            holder.songName = (TextView) view.findViewById(R.id.song_title);
            //holder.songDuration = (TextView) view.findViewById(R.id.song_duration);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.songName.setText(worldpopulationlist.get(position).getName());
        //holder.songDuration.setText(worldpopulationlist.get(position).getDuration());

        try{

            Typeface font= Typeface.createFromAsset(mContext.getAssets(),"fonts/Harmattan-R.ttf");
            holder.songName.setTypeface(font);
        }catch (NullPointerException ex){

        }
        ApplicationController.getInstance().getImageLoader().get(mContext.getString(R.string.server_url3) + worldpopulationlist.get(position).getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.songIcon.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                    holder.songIcon.setImageBitmap(scaled);
                } else {
                    holder.songIcon.setImageResource(R.drawable.empty);
                }
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "Name :".concat(worldpopulationlist.get(position).getName()).concat("\n").concat("Artists :").concat(worldpopulationlist.get(position).getA()).concat("\n");

                UserNotification.setMusicChatMessage(msg.concat(worldpopulationlist.get(position).getPath()));

                UserNotification.setSongThumb(worldpopulationlist.get(position).getThumb());
                listener.showlist(worldpopulationlist.get(position).getId(), position,"album",worldpopulationlist.get(position).getPath());


            }
        });
        return view;
    }
}
