package com.oec.wis.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oec.wis.R;
import com.oec.wis.entities.WISMY;

import java.util.List;

public class YMAdapter extends BaseAdapter {
    private List<WISMY> data;
    private Activity context;

    public YMAdapter(Activity context, List<WISMY> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = context.getLayoutInflater().inflate(R.layout.row_ym, null);
        }
        TextView text = (TextView) convertView.findViewById(R.id.text);
        text.setText(data.get(position).getName().replace("_", ""));
        if (data.get(position).isSelected())
            convertView.findViewById(R.id.llYM).setBackgroundColor(context.getResources().getColor(R.color.trans_white));
        else
            convertView.findViewById(R.id.llYM).setBackgroundColor(context.getResources().getColor(R.color.blue4));
        return convertView;
    }
}
