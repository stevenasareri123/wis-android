package com.oec.wis.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.Interfaces.MusicButtonListner;
import com.oec.wis.R;
import com.oec.wis.database.DatabaseHandler;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAlbum;
import com.oec.wis.entities.WISSongs;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AlbumAdapter extends BaseAdapter
{
    Context mContext;
    LayoutInflater inflater;
    private List<WISAlbum> data = null;
    private ArrayList<WISAlbum> arraylist;

    Animation scaleUp;

    RefreshListener refreshListener;

    DatabaseHandler db;

    List<WISSongs> songarray;

    ArrayList<Integer> songslist = new ArrayList<Integer>();
    //   public  ShareAlbumListener listener;
    public  ChatIndivisualListener listener;
    Typeface font;

    public AlbumAdapter(Context context,
                        List<WISAlbum> data, ChatIndivisualListener listener, RefreshListener refereshlisener) {
        mContext = context;
        this.data = data;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<WISAlbum>();
        this.arraylist.addAll(data);
        this.listener=listener;
        this.refreshListener =refereshlisener;
        scaleUp = AnimationUtils.loadAnimation(context, R.anim.down_from_top);
    }

   @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public WISAlbum getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        ImageView ivMusic;
        TextView tvDate,name,artists,duration;
        Button bDelete, bShare;
        LinearLayout shareLayout;
        CheckBox checkBox;




    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        db = new DatabaseHandler(mContext);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item, null);
            holder.ivMusic = (ImageView) view.findViewById(R.id.ivMusic);
            holder.bShare = (Button) view.findViewById(R.id.bShare);
            holder.shareLayout=(LinearLayout) view.findViewById(R.id.shareLayout);
            holder.tvDate = (TextView) view.findViewById(R.id.tvDate);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.artists = (TextView) view.findViewById(R.id.artists);
            holder.duration =(TextView) view.findViewById(R.id.duration);
            holder.bDelete = (Button) view.findViewById(R.id.bDelete);
            holder.checkBox = (CheckBox) view.findViewById(R.id.selected);
            // Locate the ImageView in listview_item.xml
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.ivMusic.setImageResource(0);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = null;
        String date = "";
        try {
            newDate = format.parse(data.get(position).getDateTime());
            format = new SimpleDateFormat("dd.MMM.yyyy");
            date = format.format(newDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Formatted date====>"+ date);
        holder.tvDate.setText(date);

        songarray = db.getAlbumSongs(data.get(position).getId());
        holder.duration.setText(songarray.get(0).getDuration());
        System.out.println("duration" +songarray.get(0).getDuration());
        //holder.duration.setText(data.get(position).getListdata().get(0).getDuration());
        holder.name.setText("Albums : "+data.get(position).getName());
        holder.artists.setText("Artiste :"+data.get(position).getArtists());
        if(new UserNotification().getCheckboxs()== Boolean.TRUE)
        {
            holder.checkBox.setVisibility(View.VISIBLE);


        }
        else {
            holder.checkBox.setVisibility(View.INVISIBLE);
        }





//        set typeface
        try{

            font= Typeface.createFromAsset(mContext.getAssets(),"fonts/Harmattan-R.ttf");
            holder.tvDate.setTypeface(font);
            holder.name.setTypeface(font);
            holder.artists.setTypeface(font);
            holder.duration.setTypeface(font);

        }catch (NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }
        holder.checkBox.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v ;
                int id = data.get(position).getId();
                data.get(position).setBox(cb.isChecked());

                if(cb.isChecked())
                {
                    songslist.add(id);



                }
                else{
                    // remove from arraylist
                    songslist.remove(songslist.indexOf(id));
                }

                new UserNotification().setAlbumids(songslist);

                //Toast.makeText(context, "LIST" + String.valueOf(songslist)+"sent", Toast.LENGTH_LONG).show();
            }
        });
        holder.checkBox.setTag(data.get(position));
        holder.checkBox.setChecked(data.get(position).isBox());
//        data.get(position).getListdata().get(0).getThumb()
        ApplicationController.getInstance().getImageLoader().get(mContext.getString(R.string.server_url3) + songarray.get(0).getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivMusic.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                    holder.ivMusic.setImageBitmap(scaled);
                } else {
                    holder.ivMusic.setImageResource(R.drawable.empty);
                }
            }
        });
        holder.bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog(position);
            }
        });
        holder.bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(data.get(position).getArtists()).concat("\n");
//                UserNotification.setMusicChatMessage(msg.concat(data.get(position).getListdata().get(0).getPath()));
//                UserNotification.setSongThumb(data.get(position).getListdata().get(0).getThumb());
//                listener.showPopUP(data.get(position).getListdata().get(0).getId(), position,"album",data.get(position).getListdata().get(0).getPath());
//
//                String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(data.get(position).getArtists()).concat("\n");
//                UserNotification.setMusicChatMessage(msg.concat(songarray.get(0).getPath()));
//                UserNotification.setSongThumb(songarray.get(0).getThumb());
//                listener.showlist(data.get(position).getId(), position,"album",null);
                listener.showPopUP(data.get(position).getId(), position,"album",null);


            }
        });




//        view.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//
//                List<WISSongs> array = worldpopulationlist.get(position).getListdata();
//                Log.i("songsData",String.valueOf(array));
//                Uri uri = Uri.parse(worldpopulationlist.get(position).getListdata().get(0).getPath());
//                new UserNotification().setSongs(array);
//
//                new UserNotification().setSelected(position);
//                Intent intent = new Intent(getActivity(),Player.class);
//                intent .putExtra("uri_Str", uri);
//                intent.putExtra("songIcon",worldpopulationlist.get(position).getListdata().get(0).getThumb());
//                intent.putExtra("songIndex",worldpopulationlist.get(position).getListdata().get(0).toString());
//                intent.putExtra("type","album");
//                intent.putExtra("songname",worldpopulationlist.get(position).getName());
//                intent.putExtra("artist",worldpopulationlist.get(position).getArtists());
//                intent.putExtra("duration",worldpopulationlist.get(position).getListdata().get(0).getDuration());
//                startActivity(intent);
//
//
//            }
//        });

//        view.setAnimation(scaleUp);
//        view.startAnimation(scaleUp);

        return view;
    }

    private void deleteDialog(final int position) {
       AlertDialog dialog = new AlertDialog.Builder(mContext)
                .setMessage(mContext.getString(R.string.msg_confirm_delete_music))
                .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteMusic(data.get(position).getId(), position,"album");

                    }

                })
                .setNegativeButton(mContext.getString(R.string.no), null)
                .show();



        try{
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);
            Typeface face=Typeface.createFromAsset(mContext.getAssets(),"fonts/Harmattan-R.ttf");
            Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            button1.setTypeface(face);
            Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            textView.setTypeface(face);
            button2.setTypeface(face);
            textView.setTextSize(20);//to change font size
            button1.setTextSize(20);
            button2.setTextSize(20);
        }catch (NullPointerException ex){
            Log.e("Exception",ex.getMessage());
        }


    }



    private void deleteMusic(int album_id, final int position,String type) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
            jsonBody.put("object_id", String.valueOf(album_id));
            jsonBody.put("type",type);
            Log.i("user_id", Tools.getData(mContext, "idprofile"));
            Log.i("object_id", String.valueOf(album_id));
            Log.i("type",type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.delmusic_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("deleteResponse", String.valueOf(response));

                                try {
                                    db.deleteAlbumId(data.get(position).getId());

//                                data.remove(position);
                                    notifyDataSetChanged();

                                    refreshListener.refreshData();
                                }
                                catch (IndexOutOfBoundsException e)
                                {

                                }

                                //refreshListener.refreshWithType("album",data.get(position),null);
//                                UserNotification.setIsAlbumUploaded(Boolean.TRUE);
                            }

                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(mContext, "lang_pr"));
                headers.put("token", Tools.getData(mContext, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    // Filter Class
    public void filter(String charText) {
        if (charText.equals("")) {
            data.addAll(arraylist);
        }
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(arraylist);
        } else {
            for (WISAlbum wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)||wp.getArtists().toLowerCase(Locale.getDefault()).contains(charText)||wp.getDateTime().toLowerCase(Locale.getDefault()).contains(charText)) {
                    data.add(wp);
                }
                else {
                    data.clear();
                }
            }
        }
        notifyDataSetChanged();
    }




}

