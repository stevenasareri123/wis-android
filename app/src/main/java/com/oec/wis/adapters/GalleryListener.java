package com.oec.wis.adapters;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by asareri08 on 22/06/16.
 */
public interface GalleryListener {

    void pickedItems(String objectId,Bitmap imageDrawable,String type,String name);

}
