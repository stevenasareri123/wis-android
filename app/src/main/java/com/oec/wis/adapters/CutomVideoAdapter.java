package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISVideo;

import java.util.List;

/**
 * Created by asareri08 on 22/06/16.
 */
public class CutomVideoAdapter extends BaseAdapter {

    static List<WISVideo> data;
    Context context;

    GalleryListener galleryListener;

    public CutomVideoAdapter(List<WISVideo> data,GalleryListener listener) {

        this.data = data;

        this.galleryListener  =  listener;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        this.context = parent.getContext();
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_video_items, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivVideo.setImageResource(0);
//        holder.tvTitle.setText(data.get(position).getTitle());

//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d = null;
//        try {
//            d = format.parse(data.get(position).getDateTime());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
//        holder.tvDate.setText(rDate);

//        holder.tvDate.setText(rDate);

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivVideo.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivVideo.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivVideo.setImageResource(R.drawable.empty);
                }
            }
        });

        holder.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                Bitmap image=((BitmapDrawable)holder.ivVideo.getDrawable()).getBitmap();

                Log.i("bitmamp", String.valueOf(image));

                galleryListener.pickedItems(String.valueOf(data.get(position).getId()),image,"partage_video","video");

            }
        });

//        holder.bDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                deleteDialog(position);
//            }
//        });

//        holder.bShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(context, SharePhoto.class);
//                i.putExtra("path", data.get(position).getPath());
//                i.putExtra("id", data.get(position).getId());
//                context.startActivity(i);
//            }
//        });
        return convertView;
    }

    private static class ViewHolder {
        ImageView  ivVideo;
        TextView tvTitle, tvDate;
        Button bDelete, bShare;

        public ViewHolder(View view) {
            ivVideo = (ImageView) view.findViewById(R.id.ivVideo);
//            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
//            tvDate = (TextView) view.findViewById(R.id.tvDate);
//            bDelete = (Button) view.findViewById(R.id.bDelete);
//            bShare = (Button) view.findViewById(R.id.bShare);
            view.setTag(this);
        }
    }
}
