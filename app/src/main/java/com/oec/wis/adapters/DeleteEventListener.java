package com.oec.wis.adapters;

/**
 * Created by asareri08 on 29/09/16.
 */
public interface DeleteEventListener {

    void deleteEventSucess(int position,String eventId);
}
