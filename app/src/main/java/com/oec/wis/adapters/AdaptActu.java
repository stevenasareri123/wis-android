//package com.oec.wis.adapters;
//
//import android.app.Activity;
//
//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.Point;
//import android.graphics.Typeface;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.provider.MediaStore;
//import android.support.v4.view.GravityCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.RecyclerView;
//import android.text.TextUtils;
//import android.text.util.Linkify;
//import android.util.Log;
//import android.util.Patterns;
//import android.view.Display;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Filter;
//import android.widget.Filterable;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.PopupWindow;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.CustomImageLibrary;
//import com.oec.wis.Dashboard;
//import com.oec.wis.R;
//import com.oec.wis.dialogs.ActuDetails;
//import com.oec.wis.dialogs.Comments;
//import com.oec.wis.dialogs.PhotoGallery;
//import com.oec.wis.dialogs.ProfileView;
//import com.oec.wis.dialogs.ProfileViewTwo;
//import com.oec.wis.dialogs.SharePost;
//import com.oec.wis.entities.Popupelements;
//import com.oec.wis.entities.UserNotification;
//import com.oec.wis.entities.WISActuality;
//import com.oec.wis.entities.WISCmt;
//import com.oec.wis.entities.WISPhoto;
//import com.oec.wis.entities.WISUser;
//import com.oec.wis.fragments.FragActu;
//import com.oec.wis.fragments.FragChat;
//import com.oec.wis.fragments.FragMyLocation;
//import com.oec.wis.fragments.FragPhoto;
//import com.oec.wis.fragments.FragProfile;
//import com.oec.wis.tools.ListViewScrollable;
//import com.oec.wis.tools.Tools;
//import com.orhanobut.dialogplus.DialogPlus;
//import com.orhanobut.dialogplus.OnItemClickListener;
//import com.squareup.picasso.Picasso;
//
//import org.joda.time.DateTime;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//public class AdaptActu extends RecyclerView.Adapter<AdaptActu.ViewHolder> implements Filterable,ReplyCommentsListener{
//
//    UserNotification userNotification;
//    private ImagePickerListener listener;
//    public static List<WISActuality> actuList;
//    public static WISActuality SELECTED;
//    public Activity activity;
//    Context context;
//    ArrayList<Popupelements> popupList;
//    Popupadapter simpleAdapter,live_status_adapter,actuality_share_adapter;
//    private PopupWindow popWindow;
//    ImageView camera_btn,close_btn,post_image;
//    Button post_status;
//    EditText message;
//    String imgPath = "";
//    Typeface font;
//    Animation scaleUp;
//
//
//    private final int SELECT_PHOTO = 1;
//    View view;
//    public AdaptActu(List<WISActuality> actuList,ImagePickerListener listenr) {
//        super();
//        AdaptActu.actuList = actuList;
//
//        this.listener = listenr;
//
//    }
//
//    @Override
//    public AdaptActu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        this.context = parent.getContext();
//
//        View v;
//
//        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_actuality, parent, false);
//
//        this.view = v;
//
//
//
//
//        NormalViewHolder vh = new NormalViewHolder(v);
//
//        return vh;
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
//        if (viewHolder instanceof NormalViewHolder) {
//            final NormalViewHolder holder = (NormalViewHolder) viewHolder;
//            setupWriteStatus();
//            holder.ivThumb.setImageDrawable(null);
//            holder.ivUser.setImageDrawable(null);
//
//
//            holder.ivUser.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    int current_user = Integer.parseInt(Tools.getData(context, "idprofile"));
//                    if(actuList.get(position).getCreator().getId() ==current_user){
////                        Intent intent = new Intent(context, ProfileViewTwo.class);
//
////                        FragProfile fragment = new FragProfile();
////                        FragmentManager fragmentManager = activity.getFragmentManager();
////                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////                        fragmentTransaction.replace(R.id.frame_container, fragment).commit();
//
//                        Fragment fragment = new FragProfile();
//                        FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.frame_container, fragment).commit();
////                        intent.putExtra("id",actuList.get(position).getCreator().getId());
////                        context.startActivity(intent);
//
//                    }else{
//                        Log.i(" ProfileView.class", "ProfileView.class");
//                        Intent intent = new Intent(context, ProfileView.class);
//                        intent.putExtra("id",actuList.get(position).getCreator().getId());
//                        context.startActivity(intent);
//
//                    }
//
//
//                }
//            });
//            if (actuList.get(position).getpType().equals("")) {
//                holder.rlThumb.setVisibility(View.GONE);
//            } else if (actuList.get(position).getpType().equals("partage_photo") ||actuList.get(position).getpType().equals("Map")|| !TextUtils.isEmpty(actuList.get(position).getImg())) {
//                holder.rlThumb.setVisibility(View.VISIBLE);
//                holder.ivVideo.setVisibility(View.INVISIBLE);
//                holder.ivAudio.setVisibility(View.INVISIBLE);
//                Picasso.with(context)
//                        .load(context.getString(R.string.server_url3) + actuList.get(position).getImg())
//                        .placeholder(R.drawable.empty)
//                        .error(R.drawable.empty)
//                        .into(holder.ivThumb);
//            } else if (actuList.get(position).getpType().equals("partage_video") || !TextUtils.isEmpty(actuList.get(position).getVideo())) {
//                holder.ivVideo.setVisibility(View.VISIBLE);
//                holder.rlThumb.setVisibility(View.VISIBLE);
//                holder.ivAudio.setVisibility(View.INVISIBLE);
//                holder.ivThumb.setImageDrawable(null);
//                Picasso.with(context)
//                        .load(context.getString(R.string.server_url4) + actuList.get(position).getThumb())
//                        .placeholder(R.drawable.empty)
//                        .error(R.drawable.empty)
//                        .into(holder.ivThumb);
//            }
//            else if (actuList.get(position).getpType().equals("partage_audio") || !TextUtils.isEmpty(actuList.get(position).getAudio())) {
//
//                holder.ivAudio.setVisibility(View.VISIBLE);
//                holder.rlThumb.setVisibility(View.VISIBLE);
//                holder.ivThumb.setImageDrawable(null);
//                Picasso.with(context)
//                        .load(context.getString(R.string.server_url3) + actuList.get(position).getThumbnail())
//                        .placeholder(R.drawable.empty)
//                        .error(R.drawable.empty)
//                        .into(holder.ivThumb);
//            }
//
//
//
//            else if (actuList.get(position).getpType().equals("partage_post")) {
//                holder.ivAudio.setVisibility(View.INVISIBLE);
//                if (TextUtils.isEmpty(actuList.get(position).getVideo()))
//                    holder.ivVideo.setVisibility(View.INVISIBLE);
//                if (TextUtils.isEmpty(actuList.get(position).getImg()))
//                    holder.rlThumb.setVisibility(View.GONE);
//            }
//
////            holder.tvTitle.setText(actuList.get(position).getTitle());
//            //holder.tvDesc.setText(actuList.get(position).getDesc());
//            holder.tvNLike.setText(String.valueOf(actuList.get(position).getnLike()));
//            holder.tvNLike.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    showUserList(position,actuList.get(position).getLikedUser(),"Liked Users",v);
//                }
//            });
//            holder.tvNView.setText(String.valueOf(actuList.get(position).getnView()));
//            holder.tvNView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    showUserList(position,actuList.get(position).getDislikedUser(),"Viewed Users",v);
//                }
//            });
//            holder.ivLike.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (actuList.get(position).isiLike()) {
//                        actuList.get(position).setiLike(false);
//                        actuList.get(position).setnLike(actuList.get(position).getnLike() - 1);
//                        setLike(position);
//                    } else {
//                        actuList.get(position).setiLike(true);
//                        actuList.get(position).setnLike(actuList.get(position).getnLike() + 1);
//                        setLike(position);
//                        if(actuList.get(position).isdislike()){
//                            actuList.get(position).setIsdislike(false);
//                            actuList.get(position).setDislike_count(actuList.get(position).getDislike_count() - 1);
//
//                        }
//                    }
//                    notifyDataSetChanged();
//                }
//            });
//
//            holder.ivAudio.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String url = context.getString(R.string.server_url3) + actuList.get(position).getAudio();
//                    Uri uri = Uri.parse(url);
////                    Toast.makeText(context,url, Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    intent.setDataAndType(uri, "audio/mp3");
//                    context.startActivity(intent);
//                }
//            });
//
//
//            if (actuList.get(position).isiLike())
//                holder.ivLike.setImageResource(R.drawable.news_liked);
//            else
//
//
////                holder.ivLike.getLayoutParams().height = 23;
////                holder.ivLike.getLayoutParams().width = 23;
//                holder.ivLike.setImageResource(R.drawable.news_like);
//
//
//
//            holder.dislike_count.setText(String.valueOf(actuList.get(position).getDislike_count()));
//            holder.dislike_count.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    showUserList(position,actuList.get(position).getDislikedUser(),"DisLiked Users",v);
//                }
//            });
//            holder.dislike.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (actuList.get(position).isdislike()) {
//                        actuList.get(position).setIsdislike(false);
//                        actuList.get(position).setDislike_count(actuList.get(position).getDislike_count() - 1);
//                        setDisLike(position);
//                    } else {
//                        actuList.get(position).setIsdislike(true);
//                        actuList.get(position).setDislike_count(actuList.get(position).getDislike_count() +1);
//                        setDisLike(position);
//                        if(actuList.get(position).isiLike()){
//                            actuList.get(position).setiLike(false);
//                            actuList.get(position).setnLike(actuList.get(position).getnLike() - 1);
//
//                        }
//
//                    }
//                    notifyDataSetChanged();
//
//                }
//            });
//
//            if (actuList.get(position).isdislike())
//                holder.dislike.setImageResource(R.drawable.news_disliked);
//            else
//                holder.dislike.setImageResource(R.drawable.news_dislike);
//
//
//
//            if (actuList.get(position).getCmts().size() > 0) {
//                List<WISCmt> cmts = new ArrayList<>(actuList.get(position).getCmts());
//                if (cmts.size() > 2)
//                    cmts = new ArrayList<>(actuList.get(position).getCmts().subList(cmts.size()-2,cmts.size()));
//                CmtAdapter adapter = new CmtAdapter(context, cmts,this);
//                holder.lvCmt.setAdapter(adapter);
//                holder.lvCmt.setVisibility(View.VISIBLE);
//            } else {
//                holder.lvCmt.setVisibility(View.GONE);
//            }
//
//
//
//
//            holder.comment_count.setText(String.valueOf(actuList.get(position).getComment_count()));
//
//
//            setupPopUPView();
//            holder.ivShare.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (listener == null) {
////                        Intent i = new Intent(context,SharePost.class);
////                        context.startActivity(i);
//
//                        AdaptActu.SELECTED = actuList.get(position);
//
//                        Intent i = new Intent(context, SharePost.class);
//                        context.startActivity(i);
//
//                    } else {
//                        Log.i("desc", actuList.get(position).getDesc());
//
//                        listener.showCustomPopup(position, actuList.get(position).getId(), actuList.get(position).getCmtActu());
//
//                    }
//                }
//            });
//
//            holder.share_count.setText(String.valueOf(actuList.get(position).getShare_count()));
//
//            if (TextUtils.isEmpty(actuList.get(position).getCmtActu())) {
//                //holder.tvCmtActu.setVisibility(View.GONE);
//            } else {
//                holder.tvCmtActu.setText(actuList.get(position).getCmtActu());
////                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OPENSANS-SEMIBOLD.ttf");
////                holder.tvCmtActu.setTypeface(font);
//                //holder.tvCmtActu.setVisibility(View.VISIBLE);
//            }
//            holder.ivCmt.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    AdaptActu.SELECTED = actuList.get(position);
//                    showCmts(position);
//                }
//            });
//            holder.comment_count.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    System.out.println("Coment user list--------------");
//                    showUserList(position,actuList.get(position).getCommentsUser() ,"Commented users",view);
//                }
//            });
//
//            holder.ivVideo.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String url = context.getString(R.string.server_url3) + actuList.get(position).getVideo();
//                    Uri uri = Uri.parse(url);
//                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    intent.setDataAndType(uri, "video/mp4");
//                    context.startActivity(intent);
//                }
//            });
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//
//            SimpleDateFormat format_two =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//
//            format_two= new SimpleDateFormat("dd/MMM/yyyy");
//
//            SimpleDateFormat format_hour =new SimpleDateFormat("HH");
//            SimpleDateFormat format_min =new SimpleDateFormat("mm");
//            SimpleDateFormat format_sec =new SimpleDateFormat("ss");
//
//            Log.i("created_at",actuList.get(position).getDate());
//
//            Date d = null,d2=null;
//            String created_date="",h = null,m = null,s = null;
//            try {
//                d = format.parse(actuList.get(position).getDate());
//                Date newDate = d;
//                created_date = format_two.format(newDate);
//                h = format_hour.format(newDate);
//                m =format_min.format(newDate);
//                s = format_sec.format(newDate);
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
////             changed 45
//
//            String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
//            if (net.danlew.android.joda.DateUtils.isToday(new DateTime(d))) {
//                holder.tvDate.setText(rDate);
//            }
//            else{
//                holder.tvDate.setText(" ");
//            }
//
//            holder.tvDate.setText(h.concat("h").concat(m).concat("m").concat(s).concat("'").concat("ago"));
//
//
////            end 45
////            holder.created_at.setText( created_date);
//
//
//            try{
//                font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
//                holder.tvPlus.setTypeface(font);
////            holder.tvTitle.setTypeface(font);
////            holder.tvDesc.setTypeface(font);
//                holder.tvNLike.setTypeface(font);
//                holder.tvNView.setTypeface(font);
//              holder.tvCmtActu.setTypeface(font);
////            holder.tvCmt.setTypeface(font);
//                holder.tvLMore.setTypeface(font);
//                holder.tvDate.setTypeface(font);
//                holder.tvUName.setTypeface(font);
//                holder.comment_count.setTypeface(font);
//                holder.dislike_count.setTypeface(font);
//                holder.share_count.setTypeface(font);
////            holder.created_at.setTypeface(font);
//
//
//            }catch (NullPointerException Ex){
//                System.out.println("Null Exception" +Ex.getMessage());
//            }
//
//
//
//
//
//            if (String.valueOf(actuList.get(position).getCreator().getId()).equals(Tools.getData(context, "idprofile")))
//                holder.bDelete.setVisibility(View.VISIBLE);
//            else
//                holder.bDelete.setVisibility(View.GONE);
//
//            holder.bDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    deleteDialog(position);
//                }
//            });
//
//
//
//            holder.viewMoreOption.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(listener!=null){
//                        listener.showViewMoreOptionView(position, actuList.get(position).getId());
//                    }
//
//                }
//            });
//
//            holder.ivVideo.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Uri uri = Uri.parse(context.getString(R.string.server_url3) + actuList.get(position).getVideo());
//                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    intent.setDataAndType(uri, "video/mp4");
//                    context.startActivity(intent);
//                }
//            });
//            holder.ivThumb.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    if(actuList.get(position).getpType().equals("Map")){
//
//
//                        if(actuList.get(position).getCmtActu().contains("http://")){
//
//
//                            List<String> extractedUrls = extractUrls(actuList.get(position).getCmtActu());
//
//                            for (String url : extractedUrls)
//                            {
//                                System.out.println(url);
//
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                                context.startActivity(browserIntent);
//                            }
//                        }
//                        else{
//
//
//                            UserNotification.setViewType("DashBoard");
//                            String addr =actuList.get(position).getCmtActu();
//                            System.out.println("-------");
//                            System.out.println(actuList.get(position).getCmtActu());
//                            System.out.println("-------");
//                            UserNotification.setAddress(addr);
//                            UserNotification.setLocationSelected(Boolean.TRUE);
//                            Fragment fragment = new FragMyLocation();
//                            FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
//                            fragmentManager.beginTransaction()
//                                    .replace(R.id.frame_container, fragment).commit();
//
//                        }
//
//                    }else {
//
//
//                        if (holder.ivVideo.getVisibility() == View.INVISIBLE) {
//                            FragPhoto.photos = new ArrayList<>();
//                            FragPhoto.photos.add(new WISPhoto(0, "", actuList.get(position).getImg(), actuList.get(position).getDate()));
//                            Intent i = new Intent(context, PhotoGallery.class);
//                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            i.putExtra("p", 0);
//                            context.startActivity(i);
//                        }
//                    }
//                }
//            });
//            holder.llPlus.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    AdaptActu.SELECTED = actuList.get(position);
//                    Intent i = new Intent(context, ActuDetails.class);
//                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(new Intent(context, ActuDetails.class));
//                }
//            });
//
//            Picasso.with(context)
//                    .load(context.getString(R.string.server_url2) + actuList.get(position).getCreator().getPic())
//                    .placeholder(R.drawable.empty)
//                    .error(R.drawable.empty)
//                    .into(holder.ivUser);
//
//            holder.tvUName.setText(actuList.get(position).getCreator().getFirstName());
//        }
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return actuList.size();
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
//    }
//
//    public static List<String> extractUrls(String text)
//    {
//        List<String> containedUrls = new ArrayList<String>();
//        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
//        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
//        Matcher urlMatcher = pattern.matcher(text);
//
//        while (urlMatcher.find())
//        {
//            containedUrls.add(text.substring(urlMatcher.start(0),
//                    urlMatcher.end(0)));
//        }
//
//        return containedUrls;
//    }
//
//    private void setDisLike(final int p) {
//
//        JSONObject jsonBody = null;
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\", \"id_act\":\"" + actuList.get(p).getId() + "\", \"jaime\":\"" + actuList.get(p).isdislike() + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.i("dislike statu", jsonBody.toString());
//        JsonObjectRequest reqLike = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.dislike), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("result").equals("true")) {
//
//                            }
//                        } catch (JSONException e) {
//                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                            try {
//                                actuList.get(p).setIsdislike(false);
//                                actuList.get(p).setDislike_count(actuList.get(p).getDislike_count() - 1);
//                            } catch (Exception e2) {
//
//                            }
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                actuList.get(p).setIsdislike(false);
//                actuList.get(p).setDislike_count(actuList.get(p).getDislike_count() - 1);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqLike);
//    }
//
//    private void setLike(final int p) {
//        JSONObject jsonBody = null;
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\", \"id_act\":\"" + actuList.get(p).getId() + "\", \"jaime\":\"" + actuList.get(p).isiLike() + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqLike = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.like_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("result").equals("true")) {
//
//                            }
//                        } catch (JSONException e) {
//                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                            try {
//                                actuList.get(p).setiLike(false);
//                                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
//                            } catch (Exception e2) {
//
//                            }
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                actuList.get(p).setiLike(false);
//                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqLike);
//    }
//
//    private void showCmts(int position) {
//        Intent i = new Intent(context, Comments.class);
//        i.putExtra("p", position);
//        context.startActivity(i);
//    }
//
//    public void sendMsgToChat(){
//
//        Intent i = new Intent(context,FragChat.class);
//        context.startActivity(i);
//
//
//    }
//
//
//
//
//
//
//    private void deleteDialog(final int position) {
//      AlertDialog dialog = new AlertDialog.Builder(context)
//                .setMessage(context.getString(R.string.msg_confirm_delete_post))
//                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        deleteActu(actuList.get(position).getId(), position);
//                    }
//
//                })
//                .setNegativeButton(context.getString(R.string.no), null)
//                .show();
//
//        try{
//            TextView textView = (TextView) dialog.findViewById(android.R.id.message);
//            Typeface face=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
//            Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
//            button1.setAllCaps(false);
//            button1.setTypeface(face);
//            Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
//            button2.setAllCaps(false);
//            textView.setTypeface(face);
//            button2.setTypeface(face);
//            textView.setTextSize(20);//to change font size
//            button1.setTextSize(20);
//            button2.setTextSize(20);
//        }catch (NullPointerException ex){
//            Log.e("Exception",ex.getMessage());
//        }
//
//    }
//
//    private void deleteActu(int idActu, final int position) {
//        JSONObject jsonBody = new JSONObject();
//
//        try {
//            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
//            jsonBody.put("id_act", String.valueOf(idActu));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.deleteact_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
////                                UPDATE BAGE
//                                UserNotification notifiyData=new UserNotification();
//                                if(notifiyData.getBadge_count()>1)
//                                {
//                                    notifiyData.setBadge_count(notifiyData.getBadge_count() - 1);
//                                }
//
//                                actuList.remove(position);
//                                notifyDataSetChanged();
//                            }
//
//                        } catch (JSONException e) {
//                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //if (context != null)
//                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                headers.put("token", Tools.getData(context, "token"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqDelete);
//    }
//
//    public void setupWriteStatus(){
//
//
//    }
//
//    public void showWriteStatusPopup(View v){
//        LayoutInflater layoutInflater = (LayoutInflater)context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // inflate the custom popup layout
//        final View inflatedView = layoutInflater.inflate(R.layout.write_status_popup, null,false);
//
//
//        // get device size
//        Display display =  ((Activity)context).getWindowManager().getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
////        mDeviceHeight = size.y;
//
//
//
//
//
//        // set height depends on the device size
//        popWindow = new PopupWindow(inflatedView, size.x - 35,size.y - 300, true);
//
//        popWindow.setTouchable(true);
//
//        // make it focusable to show the keyboard to enter in `EditText`
////        popWindow.setFocusable(true);
//        // make it outside touchable to dismiss the popup window
////        popWindow.setBackgroundDrawable(new BitmapDrawable());
//        popWindow.setOutsideTouchable(false);
//
//        popWindow.setAnimationStyle(R.anim.bounce);
//
//
//
////
////        final View views = this.view;
//
//        // show the popup at bottom of the screen and set some margin at bottom ie,
//
//
//        camera_btn = (ImageView)inflatedView.findViewById(R.id.open_gallery);
//        close_btn = (ImageView)inflatedView.findViewById(R.id.close_btn);
//        post_status = (Button)inflatedView.findViewById(R.id.post_status);
//        post_image = (ImageView)inflatedView.findViewById(R.id.post_image);
//        message = (EditText)inflatedView.findViewById(R.id.message);
//
//        close_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popWindow.dismiss();
////                views.setAlpha(1);
//            }
//        });
//
//
//        camera_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                listener.showImageGallery();
//
//
////                CustomImageLibrary library = new CustomImageLibrary();
////                library.showGallery();
//
//
//
//
//            }
//        });
//
//
//        post_status.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(message.getText().toString()!=""){
//                    popWindow.dismiss();
//                    if(Patterns.WEB_URL.matcher(message.getText().toString()).matches()){
//                        Log.i("is valid url",message.getText().toString());
//                        sendUrlPosttoServer(message.getText().toString());
//                    }else {
//
//                        if (!imgPath.equals("")) {
//                            new DoUpload().execute();
//                        } else {
//                            doUpdate(null);
//                        }
//                    }
//
//
//
//                }
//
//
//
//            }
//        });
//
//
//        popWindow.showAtLocation(v, Gravity.CENTER, 0,100);
//
//
//    }
//
//    public void showImageLibrary(){
//
//
//
//
//        CustomImageLibrary imageLibrary = new CustomImageLibrary();
//        imageLibrary.showGallery();
//
//
//    }
//
//    @Override
//    public void replyListener() {
//
//    }
//
//    @Override
//    public void addComments(int p, String message, ListView replyView, String comment_id) {
//
//    }
//
//    @Override
//    public Filter getFilter() {
//        return null;
//    }
//
//
////public class CustomImageLibrary extends AppCompatActivity{
////    @Override
////    public void startActivityForResult(Intent intent, int requestCode) {
////        super.startActivityForResult(intent, requestCode);
////        Log.i("image call","image calling");
////    }
////
////    public void showGallery(){
//////
////   Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
////
////
////        photoPickerIntent.setType("image/*");
////        ((Activity)context).startActivityForResult(photoPickerIntent, SELECT_PHOTO);
////
////
////    }
////
////    @Override
////    public void onActivityResult(int requestCode, int resultCode, Intent data) {
////        super.onActivityResult(requestCode, resultCode, data);
////
////        Log.i("image call","image calling");
////
////        // Log.d(TAG, String.valueOf(bitmap));
////
////
////
//////        if (requestCode ==1&& resultCode == Activity.RESULT_OK ) {
////
////        Uri uri = data.getData();
////
////        try {
////            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
////            // Log.d(TAG, String.valueOf(bitmap));
////
////            post_image.setImageBitmap(bitmap);
////
////            final String[] proj = {MediaStore.Images.Media.DATA};
////            final Cursor cursor = this.managedQuery(uri, proj, null, null,
////                    null);
////            final int column_index = cursor
////                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
////            cursor.moveToLast();
////            imgPath = cursor.getString(column_index);
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//////        }
////    }
////
////
////}
//
//
//
//
//    class ViewHolder extends RecyclerView.ViewHolder {
//        public ViewHolder(View itemView) {
//            super(itemView);
//
//        }
//    }
//
//    public void  setupPopUPView(){
//
//        popupList=new ArrayList<>();
//        popupList.add(new Popupelements(R.drawable.ic_share,context.getResources().getString(R.string.actuality)));
//        popupList.add(new Popupelements(R.drawable.edit,context.getResources().getString(R.string.post_status)));
//        popupList.add(new Popupelements(R.drawable.grey_chat_icon,context.getResources().getString(R.string.wis_chat)));
//        popupList.add(new Popupelements(R.drawable.link,context.getResources().getString(R.string.copy_link)));
//        simpleAdapter = new Popupadapter(context,false,popupList);
//
//
//    }
//
//    private void sendShare(int idAct, String desc) {
//
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\",\"id_act\":\"" + idAct + "\",\"commentaire\":\"" + desc + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest reqActu = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.cloneActuality), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("result").equals("true")) {
//                                Toast.makeText(context, context.getString(R.string.msg_share_success), Toast.LENGTH_SHORT).show();
//
//
//
//                            }
//
//                        } catch (JSONException e) {
//
//                            //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                return headers;
//            }
//        };
//        ApplicationController.getInstance().addToRequestQueue(reqActu);
//    }
//
//    private class DoUpload extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            if (!result.equals("")) {
//                try {
//                    JSONObject img = new JSONObject(result);
//                    if (img.getBoolean("result"))
//                        Log.i("image data set",img.getString("data"));
//                    doUpdate(img.getString("data"));
//                    //else
//                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
//                    view.findViewById(R.id.loading).setVisibility(View.GONE);
//                    Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                }
//
//            } else {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }
//
//        @Override
//        protected String doInBackground(String... urls) {
//            return Tools.doFileUpload(context.getString(R.string.server_url) + context.getString(R.string.upload_profile_meth), imgPath, Tools.getData(context, "token"));
//        }
//    }
//
//    public interface OnClickImageListener{
//        void onClickImage();
//    }
//
//    private void doUpdate(final String photo) {
//        JSONObject jsonBody = null;
//
//        try {
//
//            String json = "{\"user_id\": \"" + Tools.getData(context, "idprofile") + "\",\"name_img\": \"" + photo + "\",\"message\": \"" +message.getText().toString() + "\"}";
//            jsonBody = new JSONObject(json);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqAddPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.write_status), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//                                Log.i("respones write status",response.toString());
//                                view.setAlpha(1);
////                                customReloadData();
//                            }
//                        } catch (Exception e) {
//
//                        }
////                        view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("lang", "en_US");
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
//    }
//
//
//    public void sendUrlPosttoServer(String url_txt){
//        JSONObject jsonBody = null;
//
//        try {
//
//            String json = "{\"user_id\": \"" + Tools.getData(context, "idprofile") + "\",\"url\": \"" + url_txt + "\"}";
//            jsonBody = new JSONObject(json);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqAddPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.url_post), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//                                Log.i("respones write status",response.toString());
//                                view.setAlpha(1);
////                                customReloadData();
//                            }
//                        } catch (Exception e) {
//
//                        }
////                        view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("lang", "en_US");
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
//    }
//
//
//
//
//    public void showUserList(final int index, List<WISUser>user,String title,final View currentView){
//
//
//
//        LayoutInflater layoutInflater = (LayoutInflater) ((Activity)context).getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // inflate the custom popup layout
//        final View inflatedView = layoutInflater.inflate(R.layout.user_listview, null, false);
//
//
//        // get device size
//        Display display =((Activity)context).getWindowManager().getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
////        mDeviceHeight = size.y;
//
////         set text font name
//
//
//
//
//
//        // set height depends on the device size
//        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);
//
//        popWindow.setTouchable(true);
//
//        // make it focusable to show the keyboard to enter in `EditText`
////        popWindow.setFocusable(true);
//        // make it outside touchable to dismiss the popup window
////        popWindow.setBackgroundDrawable(new BitmapDrawable());
//        popWindow.setOutsideTouchable(false);
//
//        popWindow.setAnimationStyle(R.style.animationName);
//
//        this.view.setAlpha(0.4f);
//
//        final View views = this.view;
//
//
//
//
//        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
//
//        ListView userListview = (ListView)inflatedView.findViewById(R.id.user_list);
//
//        TextView header = (TextView)inflatedView.findViewById(R.id.header_title);
//
//        TextView emptylabel = (TextView)inflatedView.findViewById(R.id.emptylabel);
//
//
//
//        LinearLayout layout = (LinearLayout)inflatedView.findViewById(R.id.pop_layout);
//
//
//        header.setText(title);
//        System.out.println("titile" +title);
//
//
//
//
//
//
//
//        close_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popWindow.dismiss();
//                views.setAlpha(1);
//            }
//        });
//
//
//
//        UserAdapter useradapter = new UserAdapter(context,user);
//        userListview.setAdapter(useradapter);
//        useradapter.notifyDataSetChanged();
//
//
//
//
//
//        if(user.size()==0){
//            userListview.setVisibility(View.GONE);
//            emptylabel.setVisibility(View.VISIBLE);
//            emptylabel.setTypeface(font);
//
////
//        }
//
//        try {
//            font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
//            header.setTypeface(font);
//            post_status.setTypeface(font);
//            emptylabel.setTypeface(font);
//        }catch (NullPointerException exe){
//            System.out.println("Exception" + exe.getMessage());
//        }
//        popWindow.showAtLocation(currentView, Gravity.CENTER, 0, 100);
//
//    }
//
//
//
//    public class NormalViewHolder extends ViewHolder {
//        public TextView tvPlus, tvTitle, tvDesc, tvNLike, tvNView, tvCmt, tvLMore, tvCmtActu, tvDate, tvUName,comment_count,dislike_count,share_count,created_at;
//        public ImageView ivVideo, ivLike, ivView, ivCmt, ivShare, ivUser, ivThumb,dislike,ivAudio;
//        public RelativeLayout rlThumb;
//        public LinearLayout llPlus;
//        public ListViewScrollable lvCmt;
//        public Button bDelete;
//        public Button viewMoreOption;
//
//        public NormalViewHolder(View view) {
//            super(view);
//
//            tvPlus = (TextView) view.findViewById(R.id.tvPlus);
////            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
//            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
//            tvNLike = (TextView) view.findViewById(R.id.tvNLike);
//            tvNView = (TextView) view.findViewById(R.id.tvNView);
//            tvCmt = (TextView) view.findViewById(R.id.tvCmt);
//            tvLMore = (TextView) view.findViewById(R.id.tvLMore);
//            ivThumb = (ImageView) view.findViewById(R.id.ivThumb);
//            ivLike = (ImageView) view.findViewById(R.id.ivLike);
//            ivView = (ImageView) view.findViewById(R.id.ivView);
//            ivCmt = (ImageView) view.findViewById(R.id.ivCmt);
//            ivShare = (ImageView) view.findViewById(R.id.ivShare);
//            ivVideo = (ImageView) view.findViewById(R.id.ivVideo);
//            rlThumb = (RelativeLayout) view.findViewById(R.id.rlThumb);
//            lvCmt = (ListViewScrollable) view.findViewById(R.id.lvCmt);
//            lvCmt.setExpanded(true);
//            tvCmtActu = (TextView) view.findViewById(R.id.tvCmtActu);
//            tvDate = (TextView) view.findViewById(R.id.tvDate);
//            bDelete = (Button) view.findViewById(R.id.bDelete);
//            llPlus = (LinearLayout) view.findViewById(R.id.llPlus);
//            tvUName = (TextView) view.findViewById(R.id.tvUName);
//            ivUser = (ImageView) view.findViewById(R.id.ivUser);
//            comment_count = (TextView)view.findViewById(R.id.comment_count);
//            dislike_count = (TextView)view.findViewById(R.id.dislike_count);
//            share_count = (TextView)view.findViewById(R.id.sharecmnt);
//            dislike = (ImageView)view.findViewById(R.id.dislike);
//            created_at = (TextView)view.findViewById(R.id.created_at);
//            viewMoreOption = (Button)view.findViewById(R.id.viewMoreOption);
//            ivAudio = (ImageView) view.findViewById(R.id.ivAudio);
//        }
//
//
//
//
//    }
//
//
//
//
//}
//
package com.oec.wis.adapters;

import android.app.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.ApplicationController;
import com.oec.wis.CustomImageLibrary;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.controls.GetDateTime;
import com.oec.wis.dialogs.ActuDetails;
import com.oec.wis.dialogs.Comments;
import com.oec.wis.dialogs.PhotoGallery;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.dialogs.SharePost;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragActu;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragMyLocation;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.fragments.FragProfile;
import com.oec.wis.tools.ListViewScrollable;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdaptActu extends RecyclerView.Adapter<AdaptActu.ViewHolder> implements Filterable,ReplyCommentsListener{
    UserNotification userNotification;
    private ImagePickerListener listener;
    public static List<WISActuality> actuList;
    public static WISActuality SELECTED;
    public Activity activity;
    Context context;
    ArrayList<Popupelements> popupList;
    Popupadapter simpleAdapter,live_status_adapter,actuality_share_adapter;
    private PopupWindow popWindow;
    ImageView camera_btn,close_btn,post_image;
    Button post_status;
    EditText message;
    String imgPath = "";
    Typeface font;
    Animation scaleUp;


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private final int SELECT_PHOTO = 1;
    View view;
    public AdaptActu(List<WISActuality> actuList,ImagePickerListener listenr) {
        super();
        AdaptActu.actuList = actuList;

        this.listener = listenr;

    }

    @Override
    public AdaptActu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();

        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_actuality, parent, false);

        this.view = v;

        NormalViewHolder vh = new NormalViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof NormalViewHolder) {
            final NormalViewHolder holder = (NormalViewHolder) viewHolder;
            setupWriteStatus();
//            holder.ivThumb.setImageDrawable(null);
            holder.ivUser.setImageDrawable(null);


            holder.ivUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int current_user = Integer.parseInt(Tools.getData(context, "idprofile"));
                    if(actuList.get(position).getCreator().getId() ==current_user){
//                        Intent intent = new Intent(context, ProfileViewTwo.class);
//                        FragProfile fragment = new FragProfile();
//                        FragmentManager fragmentManager = activity.getFragmentManager();
//                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                        Fragment fragment = new FragProfile();
                        FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.frame_container, fragment).commit();
//                        intent.putExtra("id",actuList.get(position).getCreator().getId());
//                        context.startActivity(intent);

                    }else{
                        Log.i(" ProfileView.class", "ProfileView.class");
                        Intent intent = new Intent(context, ProfileView.class);
                        intent.putExtra("id",actuList.get(position).getCreator().getId());
                        context.startActivity(intent);

                    }


                }
            });
            if (actuList.get(position).getpType().equals("")) {
                holder.rlThumb.setVisibility(View.GONE);
            } else if (actuList.get(position).getpType().equals("partage_photo") ||actuList.get(position).getpType().equals("Map")|| !TextUtils.isEmpty(actuList.get(position).getImg())) {
                holder.rlThumb.setVisibility(View.VISIBLE);
                holder.ivVideo.setVisibility(View.INVISIBLE);
                holder.ivAudio.setVisibility(View.INVISIBLE);

                holder.ivThumb.setVisibility(View.VISIBLE);
                Log.e("ImagePath:",R.string.server_url3 + actuList.get(position).getImg());

                Log.e("get image path from adapter",actuList.get(position).getImg());
                String url ="http://wis-dev.ideliver.top/public/activity/img_user1493115994.jpg";


//
                Picasso.with(context)
                        .load(context.getString(R.string.server_url3) + actuList.get(position).getImg())
                        .placeholder(R.drawable.empty)
                        .error(R.drawable.empty)
                        .fit()
                        .centerCrop()
                        .into(holder.ivThumb);

//                Picasso.with(context)
//                        .load(context.getString(R.string.server_url3) + actuList.get(position).getImg())
//                        .placeholder(R.drawable.empty)
//                        .error(R.drawable.empty)
//                        .resize(600,800)
//                        .centerInside()
//                        .into(holder.ivThumb);

//                Picasso.with(context)
//                        .load(context.getString(R.string.server_url3) + actuList.get(position).getImg())
//                        .placeholder(R.drawable.empty)
//                        .error(R.drawable.empty)
//                        .into(holder.testimage);

//                holder.ivThumb.setImageResource(R.drawable.empty);



//                ApplicationController.getInstance().getImageLoader().get("http://wis-dev.ideliver.top/public/activity/img_user1493115994.jpg", new ImageLoader.ImageListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.e("error", String.valueOf(error));
//                        holder.ivThumb.setImageResource(R.drawable.empty);
//                    }
//
//                    @Override
//                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                        if (response.getBitmap() != null) {
//                            holder.ivThumb.setImageBitmap(response.getBitmap());
//
//                            Log.e("Bitmap",String.valueOf(response.getBitmap()));
//                        } else {
//                            holder.ivThumb.setImageResource(R.drawable.empty);
//                        }
//                    }
//                });

            } else if (actuList.get(position).getpType().equals("partage_video") || !TextUtils.isEmpty(actuList.get(position).getVideo())) {
                holder.ivVideo.setVisibility(View.VISIBLE);
                holder.rlThumb.setVisibility(View.VISIBLE);
                holder.ivAudio.setVisibility(View.INVISIBLE);
                holder.ivThumb.setImageDrawable(null);
                Picasso.with(context)
                        .load(context.getString(R.string.server_url4) + actuList.get(position).getThumb())
                        .placeholder(R.drawable.empty)
                        .error(R.drawable.empty)
                        .into(holder.ivThumb);
            }
            else if (actuList.get(position).getpType().equals("partage_audio") || !TextUtils.isEmpty(actuList.get(position).getAudio())) {

                holder.ivAudio.setVisibility(View.VISIBLE);
                holder.rlThumb.setVisibility(View.VISIBLE);
                holder.ivThumb.setImageDrawable(null);
                Picasso.with(context)
                        .load(context.getString(R.string.server_url3) + actuList.get(position).getThumbnail())
                        .placeholder(R.drawable.empty)
                        .error(R.drawable.empty)
                        .into(holder.ivThumb);
            }



            else if (actuList.get(position).getpType().equals("partage_post")) {
                holder.ivAudio.setVisibility(View.INVISIBLE);
                if (TextUtils.isEmpty(actuList.get(position).getVideo()))
                    holder.ivVideo.setVisibility(View.INVISIBLE);
                if (TextUtils.isEmpty(actuList.get(position).getImg()))
                    holder.rlThumb.setVisibility(View.GONE);
            }

//            holder.tvTitle.setText(actuList.get(position).getTitle());
            //holder.tvDesc.setText(actuList.get(position).getDesc());
            holder.tvNLike.setText(String.valueOf(actuList.get(position).getnLike()));
            holder.tvNLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showUserList(position,actuList.get(position).getLikedUser(),"Liked Users",v);
                }
            });
            holder.tvNView.setText(String.valueOf(actuList.get(position).getnView()));
            holder.tvNView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showUserList(position,actuList.get(position).getDislikedUser(),"Viewed Users",v);
                }
            });
            holder.ivLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (actuList.get(position).isiLike()) {
                        actuList.get(position).setiLike(false);
                        actuList.get(position).setnLike(actuList.get(position).getnLike() - 1);
                        setLike(position);
                    } else {
                        actuList.get(position).setiLike(true);
                        actuList.get(position).setnLike(actuList.get(position).getnLike() + 1);
                        setLike(position);
                        if(actuList.get(position).isdislike()){
                            actuList.get(position).setIsdislike(false);
                            actuList.get(position).setDislike_count(actuList.get(position).getDislike_count() - 1);

                        }
                    }
                    notifyDataSetChanged();
                }
            });

            holder.ivAudio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = context.getString(R.string.server_url3) + actuList.get(position).getAudio();
                    Uri uri = Uri.parse(url);
//                    Toast.makeText(context,url, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "audio/mp3");
                    context.startActivity(intent);
                }
            });


            if (actuList.get(position).isiLike())
                holder.ivLike.setImageResource(R.drawable.news_liked);
            else


//                holder.ivLike.getLayoutParams().height = 23;
//                holder.ivLike.getLayoutParams().width = 23;
                holder.ivLike.setImageResource(R.drawable.news_like);



            holder.dislike_count.setText(String.valueOf(actuList.get(position).getDislike_count()));
            holder.dislike_count.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showUserList(position,actuList.get(position).getDislikedUser(),"DisLiked Users",v);
                }
            });
            holder.dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (actuList.get(position).isdislike()) {
                        actuList.get(position).setIsdislike(false);
                        actuList.get(position).setDislike_count(actuList.get(position).getDislike_count() - 1);
                        setDisLike(position);
                    } else {
                        actuList.get(position).setIsdislike(true);
                        actuList.get(position).setDislike_count(actuList.get(position).getDislike_count() +1);
                        setDisLike(position);
                        if(actuList.get(position).isiLike()){
                            actuList.get(position).setiLike(false);
                            actuList.get(position).setnLike(actuList.get(position).getnLike() - 1);

                        }

                    }
                    notifyDataSetChanged();

                }
            });

            if (actuList.get(position).isdislike())
                holder.dislike.setImageResource(R.drawable.news_disliked);
            else
                holder.dislike.setImageResource(R.drawable.news_dislike);



            if (actuList.get(position).getCmts().size() > 0) {
                List<WISCmt> cmts = new ArrayList<>(actuList.get(position).getCmts());
                if (cmts.size() > 2)
                    cmts = new ArrayList<>(actuList.get(position).getCmts().subList(cmts.size()-2,cmts.size()));
                CmtAdapter adapter = new CmtAdapter(context, cmts,this);
                holder.lvCmt.setAdapter(adapter);
                holder.lvCmt.setVisibility(View.VISIBLE);
            } else {
                holder.lvCmt.setVisibility(View.GONE);
            }





            holder.comment_count.setText(String.valueOf(actuList.get(position).getComment_count()));


            setupPopUPView();
            holder.ivShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener == null) {
//                        Intent i = new Intent(context,SharePost.class);
//                        context.startActivity(i);

                        AdaptActu.SELECTED = actuList.get(position);

                        Intent i = new Intent(context, SharePost.class);
                        context.startActivity(i);

                    } else {
                        Log.i("desc", actuList.get(position).getDesc());

                        listener.showCustomPopup(position, actuList.get(position).getId(), actuList.get(position).getCmtActu());

                    }
                }
            });

            holder.share_count.setText(String.valueOf(actuList.get(position).getShare_count()));

            if (TextUtils.isEmpty(actuList.get(position).getCmtActu())) {
                //holder.tvCmtActu.setVisibility(View.GONE);
            } else {
                holder.tvCmtActu.setText(actuList.get(position).getCmtActu());
//                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OPENSANS-SEMIBOLD.ttf");
//                holder.tvCmtActu.setTypeface(font);
                //holder.tvCmtActu.setVisibility(View.VISIBLE);
            }
            holder.ivCmt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AdaptActu.SELECTED = actuList.get(position);
                    showCmts(position);
                }
            });
            holder.comment_count.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Coment user list--------------");
                    showUserList(position,actuList.get(position).getCommentsUser() ,"Commented users",view);
                }
            });

            holder.ivVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = context.getString(R.string.server_url3) + actuList.get(position).getVideo();
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "video/mp4");
                    context.startActivity(intent);
                }
            });

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            SimpleDateFormat format_hour =new SimpleDateFormat("HH");
            SimpleDateFormat format_min =new SimpleDateFormat("mm");
            SimpleDateFormat format_sec =new SimpleDateFormat("ss");



            SimpleDateFormat create_date_format = new SimpleDateFormat("dd/MMM/yyyy");
            String created_at="",h = null,m = null,s = null;
            Date d = null;
            try {
                d = format.parse(actuList.get(position).getDate());
                created_at = create_date_format.format(d);
                h = format_hour.format(d);
                m =format_min.format(d);
                s = format_sec.format(d);
//
            } catch (Exception e) {
                e.printStackTrace();
            }
            String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
//            holder.tvDate.setText(h.concat("h").concat(m).concat("'").concat(s).concat("\n").concat(created_at));
//            holder.created_at.setText(created_at);

            //String dayAgo = h.concat("h").concat(m).concat("'").concat(s).concat("\n").concat(created_at);


            Log.e("created_at1",actuList.get(position).getDate());

            //long ls = getDateInMillis(actuList.get(position).getDate());

            long ls = GetDateTime.getDateInMillis(actuList.get(position).getDate());

            String getTimeAgo = GetDateTime.getTimeAgo(ls,created_at);

            //String getTimeAgo = getTimeAgo(ls,created_at);

            //Log.e("getTineAgo",getTimeAgo);

            holder.tvDate.setText(getTimeAgo);


//            end 45
//            holder.created_at.setText( created_date);


            try{
                font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
                holder.tvPlus.setTypeface(font);
//            holder.tvTitle.setTypeface(font);
//            holder.tvDesc.setTypeface(font);
                holder.tvNLike.setTypeface(font);
                holder.tvNView.setTypeface(font);
                holder.tvCmtActu.setTypeface(font);
//            holder.tvCmt.setTypeface(font);
                holder.tvLMore.setTypeface(font);
                holder.tvDate.setTypeface(font);
                holder.tvUName.setTypeface(font);
                holder.comment_count.setTypeface(font);
                holder.dislike_count.setTypeface(font);
                holder.share_count.setTypeface(font);
//            holder.created_at.setTypeface(font);


            }catch (NullPointerException Ex){
                System.out.println("Null Exception" +Ex.getMessage());
            }





            if (String.valueOf(actuList.get(position).getCreator().getId()).equals(Tools.getData(context, "idprofile")))
                holder.bDelete.setVisibility(View.VISIBLE);
            else
                holder.bDelete.setVisibility(View.GONE);

            holder.bDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteDialog(position);
                }
            });



            holder.viewMoreOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        listener.showViewMoreOptionView(position, actuList.get(position).getId());
                    }

                }
            });

            holder.ivVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(context.getString(R.string.server_url3) + actuList.get(position).getVideo());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "video/mp4");
                    context.startActivity(intent);
                }
            });
            holder.ivThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(actuList.get(position).getpType().equals("Map")){


                        if(actuList.get(position).getCmtActu().contains("http://")){


                            List<String> extractedUrls = extractUrls(actuList.get(position).getCmtActu());

                            for (String url : extractedUrls)
                            {
                                System.out.println(url);

                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                context.startActivity(browserIntent);
                            }
                        }
                        else{


                            UserNotification.setViewType("DashBoard");
                            String addr =actuList.get(position).getCmtActu();
                            System.out.println("-------");
                            System.out.println(actuList.get(position).getCmtActu());
                            System.out.println("-------");
                            UserNotification.setAddress(addr);
                            UserNotification.setLocationSelected(Boolean.TRUE);
                            Fragment fragment = new FragMyLocation();
                            FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.frame_container, fragment).commit();

                        }

                    }else {


                        if (holder.ivVideo.getVisibility() == View.INVISIBLE) {

                            Log.e("selected image ::=>",actuList.get(position).getImg());

                            FragPhoto.photos = new ArrayList<>();
                            FragPhoto.photos.add(new WISPhoto(0, "", actuList.get(position).getImg(), actuList.get(position).getDate()));
                            Intent i = new Intent(context, PhotoGallery.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("p", 0);
                            context.startActivity(i);
                        }
                    }
                }
            });
            holder.llPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AdaptActu.SELECTED = actuList.get(position);
                    Intent i = new Intent(context, ActuDetails.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(new Intent(context, ActuDetails.class));
                }
            });

            Picasso.with(context)
                    .load(context.getString(R.string.server_url2) + actuList.get(position).getCreator().getPic())
                    .placeholder(R.drawable.empty)
                    .error(R.drawable.empty)
                    .into(holder.ivUser);

            holder.tvUName.setText(actuList.get(position).getCreator().getFirstName());
        }
    }



    private long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
//            Log.i("Muil", String.valueOf(dateInMillis));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }



    private String getTimeAgo(long time, String daoAgo) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            //return "yesterday";

            return daoAgo;

        } else {
            return daoAgo;
            //return diff / DAY_MILLIS + " days ago";
        }
    }


    @Override
    public int getItemCount() {
        return actuList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public static List<String> extractUrls(String text)
    {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find())
        {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }

    private void setDisLike(final int p) {

        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\", \"id_act\":\"" + actuList.get(p).getId() + "\", \"jaime\":\"" + actuList.get(p).isdislike() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("dislike statu", jsonBody.toString());
        JsonObjectRequest reqLike = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.dislike), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                            }
                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            try {
                                actuList.get(p).setIsdislike(false);
                                actuList.get(p).setDislike_count(actuList.get(p).getDislike_count() - 1);
                            } catch (Exception e2) {

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                actuList.get(p).setIsdislike(false);
                actuList.get(p).setDislike_count(actuList.get(p).getDislike_count() - 1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }


    private class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView){
            this.imageView = imageView;

            Log.e("IMAgeView", String.valueOf(imageView));
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String...urls){
            String urlOfImage = urls[0];
            Bitmap logo = null;
            try{
                try {
                    InputStream is = new URL(urlOfImage).openStream();

                    Log.e("InputStream",String.valueOf(is));
               /*
                   decodeStream(InputStream is)
                       Decode an input stream into a bitmap.
                */
                    logo = BitmapFactory.decodeStream(is);
                }catch (OutOfMemoryError err){

                }

            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        /*
            onPostExecute(Result result)
                Runs on the UI thread after doInBackground(Params...).
         */
        protected void onPostExecute(Bitmap result){
            try{
                imageView.setImageBitmap(result);
            }catch (OutOfMemoryError error){

            }



        }
    }
    private void setLike(final int p) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\", \"id_act\":\"" + actuList.get(p).getId() + "\", \"jaime\":\"" + actuList.get(p).isiLike() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqLike = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.like_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("response----->", String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {

                            }
                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            try {
                                actuList.get(p).setiLike(false);
                                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
                            } catch (Exception e2) {

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                actuList.get(p).setiLike(false);
                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }

    private void showCmts(int position) {
        Intent i = new Intent(context, Comments.class);
        i.putExtra("p", position);
        context.startActivity(i);
    }

    public void sendMsgToChat(){

        Intent i = new Intent(context,FragChat.class);
        context.startActivity(i);


    }






    private void deleteDialog(final int position) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.msg_confirm_delete_post))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteActu(actuList.get(position).getId(), position);
                    }

                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();

        try{
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);
            Typeface face=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            button1.setAllCaps(false);
            button1.setTypeface(face);
            Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            button2.setAllCaps(false);
            textView.setTypeface(face);
            button2.setTypeface(face);
            textView.setTextSize(20);//to change font size
            button1.setTextSize(20);
            button2.setTextSize(20);
        }catch (NullPointerException ex){
            Log.e("Exception",ex.getMessage());
        }

    }

    private void deleteActu(int idActu, final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_act", String.valueOf(idActu));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.deleteact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
//                                UPDATE BAGE
                                UserNotification notifiyData=new UserNotification();
                                if(notifiyData.getBadge_count()>1)
                                {
                                    notifiyData.setBadge_count(notifiyData.getBadge_count() - 1);
                                }

                                actuList.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    public void setupWriteStatus(){


    }

    public void showWriteStatusPopup(View v){
        LayoutInflater layoutInflater = (LayoutInflater)context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.write_status_popup, null,false);


        // get device size
        Display display =  ((Activity)context).getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;





        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35,size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);



//
//        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,


        camera_btn = (ImageView)inflatedView.findViewById(R.id.open_gallery);
        close_btn = (ImageView)inflatedView.findViewById(R.id.close_btn);
        post_status = (Button)inflatedView.findViewById(R.id.post_status);
        post_image = (ImageView)inflatedView.findViewById(R.id.post_image);
        message = (EditText)inflatedView.findViewById(R.id.message);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
//                views.setAlpha(1);
            }
        });


        camera_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.showImageGallery();


//                CustomImageLibrary library = new CustomImageLibrary();
//                library.showGallery();




            }
        });


        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(message.getText().toString()!=""){
                    popWindow.dismiss();
                    if(Patterns.WEB_URL.matcher(message.getText().toString()).matches()){
                        Log.i("is valid url",message.getText().toString());
                        sendUrlPosttoServer(message.getText().toString());
                    }else {

                        if (!imgPath.equals("")) {
                            new DoUpload().execute();
                        } else {
                            doUpdate(null);
                        }
                    }



                }



            }
        });


        popWindow.showAtLocation(v, Gravity.CENTER, 0,100);


    }

    public void showImageLibrary(){




        CustomImageLibrary imageLibrary = new CustomImageLibrary();
        imageLibrary.showGallery();


    }

    @Override
    public void replyListener() {

    }

    @Override
    public void addComments(int p, String message, ListView replyView, String comment_id) {

    }

    @Override
    public Filter getFilter() {
        return null;
    }


//public class CustomImageLibrary extends AppCompatActivity{
//    @Override
//    public void startActivityForResult(Intent intent, int requestCode) {
//        super.startActivityForResult(intent, requestCode);
//        Log.i("image call","image calling");
//    }
//
//    public void showGallery(){
////
//   Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//
//
//        photoPickerIntent.setType("image/*");
//        ((Activity)context).startActivityForResult(photoPickerIntent, SELECT_PHOTO);
//
//
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Log.i("image call","image calling");
//
//        // Log.d(TAG, String.valueOf(bitmap));
//
//
//
////        if (requestCode ==1&& resultCode == Activity.RESULT_OK ) {
//
//        Uri uri = data.getData();
//
//        try {
//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
//            // Log.d(TAG, String.valueOf(bitmap));
//
//            post_image.setImageBitmap(bitmap);
//
//            final String[] proj = {MediaStore.Images.Media.DATA};
//            final Cursor cursor = this.managedQuery(uri, proj, null, null,
//                    null);
//            final int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToLast();
//            imgPath = cursor.getString(column_index);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
////        }
//    }
//
//
//}




    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);

        }
    }

    public void  setupPopUPView(){

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.ic_share,context.getResources().getString(R.string.actuality)));
        popupList.add(new Popupelements(R.drawable.edit,context.getResources().getString(R.string.post_status)));
        popupList.add(new Popupelements(R.drawable.grey_chat_icon,context.getResources().getString(R.string.wis_chat)));
        popupList.add(new Popupelements(R.drawable.link,context.getResources().getString(R.string.copy_link)));
        simpleAdapter = new Popupadapter(context,false,popupList);


    }

    private void sendShare(int idAct, String desc) {

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\",\"id_act\":\"" + idAct + "\",\"commentaire\":\"" + desc + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqActu = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.cloneActuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                Toast.makeText(context, context.getString(R.string.msg_share_success), Toast.LENGTH_SHORT).show();



                            }

                        } catch (JSONException e) {

                            //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image data set",img.getString("data"));
                    doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Tools.doFileUpload(context.getString(R.string.server_url) + context.getString(R.string.upload_profile_meth), imgPath, Tools.getData(context, "token"));
        }
    }

    public interface OnClickImageListener{
        void onClickImage();
    }

    private void doUpdate(final String photo) {
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(context, "idprofile") + "\",\"name_img\": \"" + photo + "\",\"message\": \"" +message.getText().toString() + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.write_status), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("respones write status",response.toString());
                                view.setAlpha(1);
//                                customReloadData();
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }


    public void sendUrlPosttoServer(String url_txt){
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(context, "idprofile") + "\",\"url\": \"" + url_txt + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.url_post), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("respones write status",response.toString());
                                view.setAlpha(1);
//                                customReloadData();
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }




    public void showUserList(final int index, List<WISUser>user,String title,final View currentView){



        LayoutInflater layoutInflater = (LayoutInflater) ((Activity)context).getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.user_listview, null, false);


        // get device size
        Display display =((Activity)context).getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;

//         set text font name





        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);

        this.view.setAlpha(0.4f);

        final View views = this.view;




        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);

        ListView userListview = (ListView)inflatedView.findViewById(R.id.user_list);

        TextView header = (TextView)inflatedView.findViewById(R.id.header_title);

        TextView emptylabel = (TextView)inflatedView.findViewById(R.id.emptylabel);



        LinearLayout layout = (LinearLayout)inflatedView.findViewById(R.id.pop_layout);


        header.setText(title);
        System.out.println("titile" +title);







        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                views.setAlpha(1);
            }
        });



        UserAdapter useradapter = new UserAdapter(context,user);

        userListview.setAdapter(useradapter);
        useradapter.notifyDataSetChanged();





        if(user.size()==0){
            userListview.setVisibility(View.GONE);
            emptylabel.setVisibility(View.VISIBLE);
            emptylabel.setTypeface(font);

//
        }

        try {
            font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            header.setTypeface(font);
            post_status.setTypeface(font);
            emptylabel.setTypeface(font);
        }catch (NullPointerException exe){
            System.out.println("Exception" + exe.getMessage());
        }
        popWindow.showAtLocation(currentView, Gravity.CENTER, 0, 100);

    }



    public class NormalViewHolder extends ViewHolder {
        public TextView tvPlus, tvTitle, tvDesc, tvNLike, tvNView, tvCmt, tvLMore, tvCmtActu, tvDate, tvUName,comment_count,dislike_count,share_count,created_at;
        public ImageView ivVideo, ivLike, ivView, ivCmt, ivShare, ivUser, ivThumb,dislike,ivAudio;
        public RelativeLayout rlThumb;
        public LinearLayout llPlus;
        public ListViewScrollable lvCmt;
        public Button bDelete;
        public Button viewMoreOption;

        public NormalViewHolder(View view) {
            super(view);

            tvPlus = (TextView) view.findViewById(R.id.tvPlus);
//            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
            tvNLike = (TextView) view.findViewById(R.id.tvNLike);
            tvNView = (TextView) view.findViewById(R.id.tvNView);
            tvCmt = (TextView) view.findViewById(R.id.tvCmt);
            tvLMore = (TextView) view.findViewById(R.id.tvLMore);
            ivThumb = (ImageView) view.findViewById(R.id.ivThumb);
            ivLike = (ImageView) view.findViewById(R.id.ivLike);
            ivView = (ImageView) view.findViewById(R.id.ivView);
            ivCmt = (ImageView) view.findViewById(R.id.ivCmt);
            ivShare = (ImageView) view.findViewById(R.id.ivShare);
            ivVideo = (ImageView) view.findViewById(R.id.ivVideo);
            rlThumb = (RelativeLayout) view.findViewById(R.id.rlThumb);
            lvCmt = (ListViewScrollable) view.findViewById(R.id.lvCmt);
            lvCmt.setExpanded(true);
            tvCmtActu = (TextView) view.findViewById(R.id.tvCmtActu);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            bDelete = (Button) view.findViewById(R.id.bDelete);
            llPlus = (LinearLayout) view.findViewById(R.id.llPlus);
            tvUName = (TextView) view.findViewById(R.id.tvUName);
            ivUser = (ImageView) view.findViewById(R.id.ivUser);
            comment_count = (TextView)view.findViewById(R.id.comment_count);
            dislike_count = (TextView)view.findViewById(R.id.dislike_count);
            share_count = (TextView)view.findViewById(R.id.sharecmnt);
            dislike = (ImageView)view.findViewById(R.id.dislike);
            created_at = (TextView)view.findViewById(R.id.created_at);
            viewMoreOption = (Button)view.findViewById(R.id.viewMoreOption);
            ivAudio = (ImageView) view.findViewById(R.id.ivAudio);

        }




    }




}
