package com.oec.wis.adapters;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.oec.wis.ApplicationController;
import com.oec.wis.Interfaces.VoiceListener;
import com.oec.wis.R;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.dialogs.PhotoGallery;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.dialogs.StreamAudio;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconTextView;

import net.danlew.android.joda.DateUtils;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatAdapter extends BaseAdapter {
    private static final int ITEM_VIEW_TYPE_ME = 0;
    private static final int ITEM_VIEW_TYPE_HE = 1;
    Context context;
    List<WISChat> data;
    DisplayImageOptions defaultOptions;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    Chat chat;
    Typeface font;

    VoiceListener voiceListener;
    private android.os.Handler myHandler = new android.os.Handler();
    private MediaPlayer mMediaPlayer;
    String AudioPath = null;
    private boolean mIsMediaPlaying = false;
    boolean clicked = false;
    ViewHolder currentHolder;
    int progress;
    Bitmap bitmap;

    MediaPlayer  currentPlayer;
    Animation scaleUp;

    private int lastPosition = -1;
    public ChatAdapter(Context context, List<WISChat> data) {
        this.data = data;

        this.context = context;
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration configu = new ImageLoaderConfiguration.Builder(context).
                defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new UsingFreqLimitedMemoryCache(200000))
                .imageDownloader(new BaseImageDownloader(context, 15 * 1000, 40 * 1000))
                .build();

        imageLoader.getInstance().init(configu);

        scaleUp = AnimationUtils.loadAnimation(context, R.anim.scroll_up_fast);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Log.d("position" ,String.valueOf(data.get(position)));
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.d("item id position", String.valueOf(data.get(position).getId()));
        return data.get(position).getId();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final WISChat item = (WISChat) this.getItem(position);
        if (convertView == null) {
            Chat chat=new Chat();
            holder = new ViewHolder();
            LayoutInflater inflater = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE));
            convertView = inflater.inflate(item.getIsMe() ? R.layout.row_chat_me : R.layout.row_chat_he, null);

            holder.tvMsg = (EmojiconTextView) convertView.findViewById(R.id.tvMsg);

            holder.tvMsg.setUseSystemDefault(false);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            holder.ivPic = (ImageView) convertView.findViewById(R.id.ivPic);
            holder.ivThumb = (ImageView) convertView.findViewById(R.id.ivThumb);
            holder.ivVideo = (ImageView) convertView.findViewById(R.id.ivVideo);
//            holder.bDelete = (Button) convertView.findViewById(R.id.bDelete);
            holder.rlThumb = (RelativeLayout) convertView.findViewById(R.id.rlThumb);
//            holder.msgLayout =(LinearLayout) convertView.findViewById(R.id.msgLLayout);
//            holder.videoLLayout=(LinearLayout)convertView.findViewById(R.id.videoLLayout);
            holder.ivName = (TextView) convertView.findViewById(R.id.ivName);
//            holder.msgTimeTV=(TextView)convertView.findViewById(R.id.msgTimeTV);
//            holder.msgSeenTimeTV=(TextView)convertView.findViewById(R.id.msgSeenTimeTV);
//            holder.videotTimeTV=(TextView)convertView.findViewById(R.id.videoTimeTV);
//            holder.videoSeenTimeTV=(TextView)convertView.findViewById(R.id.videoSeentimeTV);
//            holder.vDelete = (Button) convertView.findViewById(R.id.vDelete);
            holder.rlThumb = (RelativeLayout) convertView.findViewById(R.id.rlThumb);
            holder.playIcon=(ImageView) convertView.findViewById(R.id.playIcon);
//            holder.startTime = (TextView) convertView.findViewById(R.id.startTime);
            holder.endTime = (TextView) convertView.findViewById(R.id.endTime);
            holder.ivAudio = (RelativeLayout) convertView.findViewById(R.id.ivAudio);
            holder.playSeekBar = (SeekBar) convertView.findViewById(R.id.playSeekBar);

            holder.acceoryView = (ImageButton) convertView.findViewById(R.id.acceoryView);


            holder.mediaWithText = (RelativeLayout) convertView.findViewById(R.id.mediaWithText);
            holder.medaiMessage = (TextView) convertView.findViewById(R.id.media_message);
            holder.mediaImage = (ImageView) convertView.findViewById(R.id.media_thumb);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivThumb.setImageResource(0);


        try{
            font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            holder.tvMsg.setTypeface(font);
            holder.tvTime.setTypeface(font);
            holder.medaiMessage.setTypeface(font);
            holder.ivName.setTypeface(font);
//              holder.msgTimeTV.setTypeface(font);
//              holder.msgSeenTimeTV.setTypeface(font);
//              holder.videotTimeTV.setTypeface(font);
//            holder.videoSeenTimeTV.setTypeface(font);

        }catch (NullPointerException ex){
            System.out.println("Null pouinter Exception" +ex.getMessage());
        }

        System.out.println("data test position==>" +data.get(position).getUser().getId());

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + Tools.getData(context, "photo"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                reDownloadImage();

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPic.setImageBitmap(response.getBitmap());
//                    ivProfile2.setImageBitmap(response.getBitmap());
                }
            }
        });



        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(position).isMe()){

                    Intent i = new Intent(context, ProfileViewTwo.class);
                    i.putExtra("id",Integer.parseInt(String.valueOf(data.get(position).getUser().getId())));
                    context.startActivity(i);

                }else{
                    Log.e("else calling" ,"Chat Adapter");
                    Intent i = new Intent(context, ProfileView.class);
                    int ids = data.get(position).getUser().getId();
                    System.out.println("data position==>" +data.get(position));
                    System.out.println("data test position==>" +data.get(position).getUser().getId());
                    i.putExtra("id",ids);
                    i.putExtra("image",data.get(position).getUser().getPic());
                    context.startActivity(i);

                }
            }
        });
        if (data.get(position).getTypeMsg().equals("photo")) {
            try{
                holder.ivThumb.setVisibility(View.VISIBLE);
//                if (data.get(position).isMe())
//                    holder.bDelete.setVisibility(View.VISIBLE);
                holder.rlThumb.setVisibility(View.VISIBLE);
                holder.ivVideo.setVisibility(View.GONE);
                holder.ivAudio.setVisibility(View.GONE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.acceoryView.setVisibility(View.VISIBLE);


                holder.mediaWithText.setVisibility(View.GONE);
                holder.medaiMessage.setVisibility(View.GONE);
                holder.mediaImage.setVisibility(View.GONE);
//            roopa
//                holder.videoLLayout.setVisibility(View.GONE);
//                holder.msgLayout.setVisibility(View.GONE);



            }catch(NullPointerException ex){
                System.out.println("Excep" +ex.getMessage());
            }



            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + item.getMsg(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("image cannot download",context.getString(R.string.server_url3) + item.getMsg());
                    holder.ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });

        } else if (data.get(position).getTypeMsg().equals("video")) {
            try {


                holder.ivThumb.setVisibility(View.VISIBLE);
                if (data.get(position).isMe())
//                    holder.bDelete.setVisibility(View.VISIBLE);
              holder.rlThumb.setVisibility(View.VISIBLE);
                holder.ivVideo.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.ivAudio.setVisibility(View.GONE);
                holder.acceoryView.setVisibility(View.VISIBLE);
                holder.mediaWithText.setVisibility(View.GONE);
                holder.medaiMessage.setVisibility(View.GONE);
                holder.mediaImage.setVisibility(View.GONE);

                //            roopa
//                holder.videoLLayout.setVisibility(View.GONE);
//                holder.msgLayout.setVisibility(View.GONE);


            }catch (NullPointerException ex){
                System.out.println("Exce" +ex.getMessage());
            }


            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        }


        else if (data.get(position).getTypeMsg().equals("photo_video")) {
            try {


                holder.ivThumb.setVisibility(View.VISIBLE);
                if (data.get(position).isMe())
//                    holder.bDelete.setVisibility(View.VISIBLE);
                    holder.rlThumb.setVisibility(View.VISIBLE);
                holder.ivVideo.setVisibility(View.VISIBLE);
                holder.tvMsg.setVisibility(View.GONE);
                holder.ivAudio.setVisibility(View.GONE);
                holder.acceoryView.setVisibility(View.VISIBLE);
                holder.mediaWithText.setVisibility(View.GONE);
                holder.medaiMessage.setVisibility(View.GONE);
                holder.mediaImage.setVisibility(View.GONE);

                //            roopa
//                holder.videoLLayout.setVisibility(View.GONE);
//                holder.msgLayout.setVisibility(View.GONE);


            }catch (NullPointerException ex){
                System.out.println("Exce" +ex.getMessage());
            }


            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        }

        else if(data.get(position).getTypeMsg().equals("qweraudio")) {

            Log.e("Audio" ,"Audio") ;
//            holder.ivThumb.setVisibility(View.GONE);
//            holder.ivVideo.setVisibility(View.GONE);
//            holder.bDelete.setVisibility(View.GONE);
//            holder.rlThumb.setVisibility(View.GONE);
//            holder.tvMsg.setVisibility(View.VISIBLE);
            try{
                holder.ivThumb.setVisibility(View.GONE);
                holder.ivVideo.setVisibility(View.GONE);

                holder.rlThumb.setVisibility(View.GONE);

                holder.ivAudio.setVisibility(View.GONE);
                holder.acceoryView.setVisibility(View.GONE);
                //            roopa
//                holder.videoLLayout.setVisibility(View.GONE);
//                holder.msgLayout.setVisibility(View.GONE);

                holder.tvMsg.setVisibility(View.GONE);

                holder.mediaWithText.setVisibility(View.VISIBLE);
                holder.mediaImage.setVisibility(View.VISIBLE);
                holder.medaiMessage.setVisibility(View.VISIBLE);


            }catch (NullPointerException ex){
                System.out.println("Null exe" +ex.getMessage());
            }


        }

        else if(data.get(position).getTypeMsg().equals("voice_recording"))
        {



            holder.ivThumb.setVisibility(View.GONE);
            holder.ivVideo.setVisibility(View.GONE);

            holder.rlThumb.setVisibility(View.GONE);
            holder.tvMsg.setVisibility(View.GONE);
            holder.ivAudio.setVisibility(View.VISIBLE);

            holder.mediaWithText.setVisibility(View.GONE);
            holder.medaiMessage.setVisibility(View.GONE);
            holder.mediaImage.setVisibility(View.GONE);

            voiceListener.onPlayButtonClicked(data.get(position).getMsg());

            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getMsg(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
                    } else {
                        holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
                    }
                }
            });




//
//            holder.ivVideo.setVisibility(View.GONE);
////            if (data.get(position).isMe())
////                holder.vDelete.setVisibility(View.VISIBLE);
//            holder.rlThumb.setVisibility(View.GONE);
//            holder.tvMsg.setVisibility(View.GONE);
//            holder.ivAudio.setVisibility(View.VISIBLE);
//            holder.acceoryView.setVisibility(View.VISIBLE);
//
//            holder.mediaWithText.setVisibility(View.GONE);
//            holder.medaiMessage.setVisibility(View.GONE);
//            holder.mediaImage.setVisibility(View.GONE);
//
////            if(item.getIsMe()){
//////                @drawable/new_chat_he_msg
////
////            }
////
////            else{
////
////                holder.ivAudio.setBackground(R.drawable.new_chat_he_msg);
////
////
////            }
//
//
//
//            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getMsg(), new ImageLoader.ImageListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
//                }
//
//                @Override
//                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                    if (response.getBitmap() != null) {
//                        holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
//                    } else {
//                        holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
//                    }
//                }
//            });
//

        }

        else {

            try{

                holder.tvMsg.setVisibility(View.VISIBLE);

                holder.ivThumb.setVisibility(View.GONE);
                holder.ivVideo.setVisibility(View.GONE);
//                holder.bDelete.setVisibility(View.GONE);
                holder.rlThumb.setVisibility(View.GONE);

                holder.ivAudio.setVisibility(View.GONE);
                holder.acceoryView.setVisibility(View.GONE);

                //            roopa
//                holder.videoLLayout.setVisibility(View.GONE);


                holder.mediaWithText.setVisibility(View.GONE);
                holder.medaiMessage.setVisibility(View.GONE);
                holder.mediaImage.setVisibility(View.GONE);

            }catch (NullPointerException ex){
                System.out.println("exception" +ex.getMessage());
            }

        }
        holder.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri uri = Uri.parse(context.getString(R.string.server_url3) + data.get(position).getMsg());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setDataAndType(uri, "video/mp4");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.ivThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.get(position).getTypeMsg().equals("photo")) {
                    FragPhoto.photos = new ArrayList<>();
                    FragPhoto.photos.add(new WISPhoto(0, "", data.get(position).getMsg(), data.get(position).getDateTime()));
                    Intent i = new Intent(context, PhotoGallery.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("p", 0);
                    context.startActivity(i);
                }
            }
        });



        holder.playIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("ExactLink", context.getString(R.string.server_url3) + data.get(position).getMsg());
                AudioPath = context.getString(R.string.server_url3) + data.get(position).getMsg();


                if(clicked==Boolean.FALSE)
                {
                    //voiceListener.onPlayButtonClicked(data.get(position).getMsg());

                    try {
                        mMediaPlayer = new MediaPlayer();
                        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        String PATH = Environment.getExternalStorageDirectory()
                                + "/VoiceAudio/";
                        //mp.setDataSource(PATH+filePath);
                        mMediaPlayer.setDataSource(PATH+data.get(position).getMsg());
                        mMediaPlayer.prepareAsync();
                        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                                holder.playSeekBar.setProgress(0);
                                holder.playSeekBar.setMax(100);
                                updateProgressBar(holder);
                                holder.playIcon.setImageResource(android.R.drawable.ic_media_pause);
                                clicked=true;
//

                            }
                        });

                        holder.playSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                                myHandler.removeCallbacks(mUpdateTimeTask);
                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                                myHandler.removeCallbacks(mUpdateTimeTask);
                                try {
                                    int totalDuration = mMediaPlayer.getDuration();
                                    int currentPosition = progressToTimer(seekBar.getProgress(), totalDuration);

                                    // forward or backward to certain seconds
                                    mMediaPlayer.seekTo(currentPosition);

                                    // update timer progress again
                                    updateProgressBar(holder);
                                }
                                catch (NullPointerException e)
                                {

                                }

                            }
                        }); // Important
                        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.seekTo(0);
                                mp.release();
                                mMediaPlayer = null;
                                holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                else {
                    try {
                        if(mMediaPlayer.isPlaying())
                        {
                            mMediaPlayer.pause();
                        }
                    }
                    catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }
                    holder.playIcon.setImageResource(android.R.drawable.ic_media_play);
                    clicked=false;
                }








            }
        });






//        System.out.println("item value" +item.getMsg().toString());
//
//        System.out.println("item position value" +data.get(position).getTypeMsg());
//
//        System.out.println("item id value" +data.get(position).getId());






//



        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + item.getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                        bitmap = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                    }
                }
            });





//        holder.tvMsg.setText(item.getMsg().toString());
        if(item.getTypeMsg().equals("audio"))
        {
          Log.e("images",data.get(position).getThumb());
           SpannableStringBuilder builder = new SpannableStringBuilder();
           builder.append(item.getMsg().toString());
            builder.setSpan(new ImageSpan(context, bitmap),builder.length() - 1, builder.length(), 0);
            holder.tvMsg.setText(builder);
           }
       else {

            try {
                holder.tvMsg.setText(item.getMsg().toString());

            }catch (NullPointerException ex){
                Log.e("Exception",ex.getMessage());

            }
           }
//
//        if(item.getMsg().startsWith("Name:"))
//        {
////            String pathName = Environment.getExternalStorageDirectory()
////                    + "/wismusic/"+item.getThumb();
////            Log.i("songicon",pathName);
////            bitmap = BitmapFactory.decodeFile(pathName);
////            Log.i("bitmap",String.valueOf(bitmap));
//
//            SpannableStringBuilder builder = new SpannableStringBuilder();
//            builder.append(item.getMsg().toString());
//            builder.setSpan(new ImageSpan(context, bitmap),
//                    builder.length() - 1, builder.length(), 0);
//            holder.tvMsg.setText(builder);
//        }


        System.out.println("-----------------START MESSAGE TYPE-------------------");


        Log.e("TYPE+++=>",data.get(position).getTypeMsg());


        System.out.println("-----------------END MESSAGE TYPE-------------------");


//
//        if(item.getMsg().startsWith("Name:")&&item.getMsg().contains("Artists:")&&item.getMsg().contains("http:")){
//
//
//
//
//        }else{
//
//
//
//            holder.tvMsg.setText(item.getMsg().toString());
//        }



        String url=item.getMsg();
        if (url.startsWith("https://") || url.startsWith("http://")){
            holder.tvMsg.setTextColor(Color.BLUE);
            SpannableString content = new SpannableString(item.getMsg().toString());
            content.setSpan(new UnderlineSpan(), 0, item.getMsg().length(), 0);
//            holder.tvMsg.setText(content);

            Log.d("http " ,"color check");
        }
        else if(item.getIsMe()){
            holder.tvMsg.setTextColor(Color.BLACK);

            Log.d("http " ,"black check");
        }else{
            holder.tvMsg.setTextColor(Color.WHITE);
            Log.d("http " ,"white check");
        }
        SimpleDateFormat format_hour =new SimpleDateFormat("HH");
        SimpleDateFormat format_min =new SimpleDateFormat("mm");
        SimpleDateFormat format_sec =new SimpleDateFormat("ss");


        Date d2=null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String h = null,m = null,s = null;
        Date d = null;
        try {
            d = format.parse(item.getDateTime());
            c.setTime(d);
            h = format_hour.format(d);
            m =format_min.format(d);
            s = format_sec.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }





        String rDate = DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvTime.setText(h.concat("h").concat(m).concat("'").concat(s));


        if (data.get(position).isMe())
        {

            if (Tools.getData(context, "name").replace("null", "").equals(""))
                holder.ivName.setText(Tools.getData(context, "firstname_prt") + " " + Tools.getData(context, "lastname_prt"));
            else
                holder.ivName.setText(Tools.getData(context, "name").replace("null", ""));
        }
        else {
//            holder.ivName.setText(data.get(position).getUser().getFirstName() + " " + data.get(position).getUser().getLastName().replace("null",""));
            if(data.get(position).getUser().getLastName()==null)
            {
                holder.ivName.setText(data.get(position).getUser().getFirstName());
            }
            else {
                holder.ivName.setText(data.get(position).getUser().getFirstName() + " " + data.get(position).getUser().getLastName());
            }
        }



//        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + item.getUser().getPic(), new ImageLoader.ImageListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                retryImageDownload(holder,position,item);
//                holder.ivPic.setImageResource(R.drawable.profile);
//
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    holder.ivPic.setImageBitmap(response.getBitmap());
//                } else {
//                    holder.ivPic.setImageResource(R.drawable.profile);
//                }
//            }
//        });
        String imageUrls = context.getString(R.string.server_url3) + item.getUser().getPic();
        retryImageDownload(holder,imageUrls);
        String retryimageUrls = context.getString(R.string.server_url3) + item.getUser().getPic();
//        boolean isOkay = retryImageDownload(holder,imageUrls);
//        if(isOkay){
//
//        }else{
//            String retryimageUrls = context.getString(R.string.server_url3) + item.getUser().getPic();
//            retryImageDownload(holder,retryimageUrls);
//        }



//        holder.bDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("delete","pressed");
//                deleteMsg(data.get(position).getId());
//                data.remove(position);
//                notifyDataSetChanged();
//            }
//        });


        holder.acceoryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("deleteDialog Position::", String.valueOf(position));
                AlertDialog dialog =new  AlertDialog.Builder(context)
                        .setMessage(context.getString(R.string.msg_confirm_delete_chat))
                        .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                deleteMsg(data.get(position).getId());
                                data.remove(position);
                                notifyDataSetChanged();

//                                deleteNotif(data.get(selectedPosotion).getId(), selectedPosotion);
                            }

                        })
                        .setNegativeButton(context.getString(R.string.no), null)
                        .show();


                try{
                    TextView textView = (TextView) dialog.findViewById(android.R.id.message);
                    Typeface face=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
                    Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    button1.setTypeface(face);
                    Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    textView.setTypeface(face);
                    button2.setTypeface(face);
                    textView.setTextSize(20);//to change font size
                    button1.setTextSize(20);
                    button2.setTextSize(20);
                }catch (NullPointerException ex){
                    Log.e("Exception",ex.getMessage());
                }



            }
        });










        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;



        return convertView;
    }



    boolean isSuccess;
    public boolean retryImageDownload(final ViewHolder holder ,String imageUrl){



        imageLoader.displayImage(imageUrl,holder.ivPic, defaultOptions, new ImageLoadingListener(){
            @Override
            public void onLoadingStarted(String imageUri, View view) {

//                pDialog.show();


            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                Log.i("strat", "fail");
//                pDialog.dismissWithAnimation();
                isSuccess  = false;
                holder.ivPic.setImageResource(R.drawable.empty);

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Log.i("strat", "complete");
//                pDialog.dismissWithAnimation();

                holder.ivPic.setImageBitmap(loadedImage);

                isSuccess  = true;


            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
//                pDialog.dismissWithAnimation();
                Log.i("strat", "cancel");

            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });

        return isSuccess;

    }


    private void deleteMsg(int id) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_message", String.valueOf(id));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.deletechat_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                            }

                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getIsMe() ? ITEM_VIEW_TYPE_ME : ITEM_VIEW_TYPE_HE;
    }



    private static class ViewHolder {
        //        startTime
        public TextView tvTime,ivName,endTime,medaiMessage;
        public EmojiconTextView tvMsg;
        public ImageView ivPic, ivThumb, ivVideo,playIcon,mediaImage;
        //        public Button bDelete,vDelete;
        public LinearLayout msgLayout,videoLLayout;
        TextView msgTimeTV,msgSeenTimeTV,videotTimeTV,videoSeenTimeTV;
        public RelativeLayout rlThumb,ivAudio,mediaWithText;

        public ImageButton acceoryView;

        public SeekBar playSeekBar;
    }


    public void updateSeekBars(String duration,int progress){

        if(currentHolder!=null){

            Log.e("UPDATED PROGRESS", String.valueOf(progress));


            currentHolder.endTime.setText(String.valueOf(duration));

            currentHolder.playSeekBar.setProgress(progress);
        }
    }

    public void resetPlayer(){

        Log.e("RESETPLAER","CALLEd");

        if(currentHolder!=null){

            currentHolder.playIcon.setImageResource(R.drawable.play);
            currentHolder.endTime.setText("00:00");
            currentHolder.playSeekBar.setProgress(0);
        }

    }


    public void setvoiceListener(VoiceListener voiceListener)
    {
        this.voiceListener=voiceListener;
    }


    public void updateProgressBar(ViewHolder holder) {
        this.currentHolder = holder;
        myHandler.postDelayed(mUpdateTimeTask, 100);
    }



    public void updateProgressBartest(ViewHolder holder,MediaPlayer mp) {

        this.currentPlayer = mp;
        this.currentHolder = holder;
        myHandler.postDelayed(mUpdateTimeTask, 100);
    }
    public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }
    public String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int)( milliseconds / (1000*60*60));
        int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
        int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
        // Add hours if there
        if(hours > 0){
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if(seconds < 10){
            secondsString = "0" + seconds;
        }else{
            secondsString = "" + seconds;}

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }
    public int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                long totalDuration = mMediaPlayer.getDuration();
                long currentDuration = mMediaPlayer.getCurrentPosition();
                int duration = (int) currentDuration;

                // Displaying Total Duration time

                String stat = milliSecondsToTimer(totalDuration);
                //endTime.setText();
                // Displaying time completed playing
                String endt = milliSecondsToTimer(currentDuration);
                //startTime.setText();


                progress = (int)(getProgressPercentage(currentDuration, totalDuration));
                Log.d("Progress", ""+progress+stat+endt);

                if(currentHolder!=null)
                {
                    currentHolder.endTime.setText(milliSecondsToTimer(totalDuration));
                    // Displaying time completed playing
//                    currentHolder.startTime.setText(milliSecondsToTimer(currentDuration));

                    currentHolder.playSeekBar.setProgress(progress);
                }
                else {
                    Log.e("currentHolder","NULL");
                }

                // Running this thread after 100 milliseconds
                myHandler.postDelayed(this, 100);
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
            }

        }
    };







}
