package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.R;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISMusic;

import java.util.List;

/**
 * Created by asareri12 on 07/11/16.
 */

public class SongsAdapter extends BaseAdapter {

    //song list and layout
    private List<WISMusic> songs;
    Context context;
    private String type;
    boolean itemClicked = true;

    private SparseBooleanArray checked = new SparseBooleanArray();

    private int selectedPosition = -1;

    //constructor
    public SongsAdapter(Context context, List<WISMusic> songList, String type){

        Log.e("songlist", String.valueOf(songList));
        this.songs=songList;
        this.context = context;
        this.type = type;
        //songInf=LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.song, null);
            holder = new ViewHolder(convertView);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.songView.setText(songs.get(position).getName());
        holder.songDuration.setText(songs.get(position).getDuration());
//        holder.playicon.setImageResource(android.R.drawable.ic_media_play);
        int selected = new UserNotification().getSelected();
        Log.i("selected", String.valueOf(selected));
        Log.i("selected", String.valueOf(position));
        if(position!=selected)
        {
            holder.playicon.setImageResource(R.drawable.black_image);
            holder.playicon.setBorderColor(Color.BLACK);
        }
        else {
            if(new UserNotification().getClicked()== Boolean.TRUE)
            {
                holder.playicon.setImageResource(android.R.drawable.ic_media_play);
                holder.playicon.setBorderColor(Color.WHITE);
            }
            else {
                holder.playicon.setImageResource(android.R.drawable.ic_media_pause);
                holder.playicon.setBorderColor(Color.WHITE);
            }


        }
        holder.itemCount.setText(String.valueOf(position+1));

        //convertView.setTag(position);

        return convertView;
    }
    @Override
    public int getViewTypeCount() {
        if (songs.size() > 1)
            return songs.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView songView ;
        TextView itemCount;
        TextView songDuration;
        CircularImageView playicon;
        //CircularImageView pauseicon;

        public ViewHolder(View view) {

            itemCount = (TextView) view.findViewById(R.id.itemCount);
            songView = (TextView) view.findViewById(R.id.song_title);
            songDuration = (TextView) view.findViewById(R.id.song_duration);
            playicon = (CircularImageView) view.findViewById(R.id.btPly);
            //pauseicon = (CircularImageView) view.findViewById(R.id.btPause);

            view.setTag(this);
        }
    }


}
