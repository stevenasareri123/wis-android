package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oec.wis.R;
import com.oec.wis.entities.WISMenu;

import java.util.List;

public class NavDrawarAdapter extends BaseAdapter {
    private Context context;
    private List<WISMenu> items;

    public NavDrawarAdapter(Context context, List<WISMenu> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.nav_list_item, null);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.title);
        ImageView ivLogo = (ImageView) convertView.findViewById(R.id.logo);
        tvTitle.setText(items.get(position).getTitle());
        ivLogo.setImageResource(items.get(position).getLogo());

        return convertView;
    }
}
