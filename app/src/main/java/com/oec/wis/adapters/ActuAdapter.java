package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.ActuDetails;
import com.oec.wis.dialogs.Comments;
import com.oec.wis.dialogs.PhotoGallery;
import com.oec.wis.dialogs.SharePost;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.tools.ListViewScrollable;
import com.oec.wis.tools.Tools;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActuAdapter extends BaseAdapter implements ReplyCommentsListener {
    List<WISActuality> actuList;
    WISActuality SELECTED;
    Context context;
    ImageLoader imageLoader = ApplicationController.getInstance().getImageLoader();
    public ActuAdapter(Context context, List<WISActuality> actuList) {
        this.context = context;
        this.actuList = actuList;
    }

    @Override
    public int getCount() {
        return actuList.size();
    }

    @Override
    public Object getItem(int position) {
        return actuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return actuList.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_actuality, null);
            holder = new ViewHolder(convertView);

        } else {
            holder = (ViewHolder) convertView.getTag();
            //holder.ivUser.setImageDrawable(null);
            //holder.ivThumb.setImageDrawable(null);
        }

        if (actuList.get(position).getpType().equals("")) {
            holder.rlThumb.setVisibility(View.GONE);
        } else if (actuList.get(position).getpType().equals("partage_photo") || !TextUtils.isEmpty(actuList.get(position).getImg())) {
            holder.rlThumb.setVisibility(View.VISIBLE);
            holder.ivVideo.setVisibility(View.INVISIBLE);
            //holder.ivThumb.setImageUrl(context.getString(R.string.server_url3) + actuList.get(position).getImg(), imageLoader);

        } else if (actuList.get(position).getpType().equals("partage_video") || !TextUtils.isEmpty(actuList.get(position).getVideo())) {
            holder.ivVideo.setVisibility(View.VISIBLE);
            holder.rlThumb.setVisibility(View.VISIBLE);
            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + actuList.get(position).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivThumb.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivThumb.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivThumb.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else if (actuList.get(position).getpType().equals("partage_post")) {
            if (TextUtils.isEmpty(actuList.get(position).getVideo()))
                holder.ivVideo.setVisibility(View.INVISIBLE);
            if (TextUtils.isEmpty(actuList.get(position).getImg()))
                holder.rlThumb.setVisibility(View.GONE);
        }

        holder.tvTitle.setText(actuList.get(position).getTitle());
        //holder.tvDesc.setText(actuList.get(position).getDesc());
        holder.tvNLike.setText(String.valueOf(actuList.get(position).getnLike()));
        holder.tvNView.setText(String.valueOf(actuList.get(position).getnView()));
        holder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actuList.get(position).isiLike()) {
                    actuList.get(position).setiLike(false);
                    actuList.get(position).setnLike(actuList.get(position).getnLike() - 1);
                    setLike(position);
                } else {
                    actuList.get(position).setiLike(true);
                    actuList.get(position).setnLike(actuList.get(position).getnLike() + 1);
                    setLike(position);
                }
                notifyDataSetInvalidated();
            }
        });
        if (actuList.get(position).isiLike())
            holder.ivLike.setImageResource(R.drawable.news_liked);
        else
            holder.ivLike.setImageResource(R.drawable.news_like);
        if (actuList.get(position).getCmts().size() > 0) {
            List<WISCmt> cmts = new ArrayList<>(actuList.get(position).getCmts());
            if (cmts.size() > 2)
                cmts = new ArrayList<>(actuList.get(position).getCmts().subList(0, 2));
            CmtAdapter adapter = new CmtAdapter(context, cmts,this);
            holder.lvCmt.setAdapter(adapter);
            holder.lvCmt.setVisibility(View.VISIBLE);
        } else {
            holder.lvCmt.setVisibility(View.GONE);
        }
        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SharePost.class);
                SELECTED = actuList.get(position);
                context.startActivity(i);
            }
        });
        if (TextUtils.isEmpty(actuList.get(position).getCmtActu())) {
            //holder.tvCmtActu.setVisibility(View.GONE);
        } else {
            holder.tvCmtActu.setText(actuList.get(position).getCmtActu());
            //holder.tvCmtActu.setVisibility(View.VISIBLE);
        }
        holder.ivCmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCmts(position);
            }
        });
        holder.tvCmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCmts(position);
            }
        });

        holder.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = context.getString(R.string.server_url3) + actuList.get(position).getVideo();
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setDataAndType(uri, "video/mp4");
                context.startActivity(intent);
            }
        });
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = format.parse(actuList.get(position).getDate());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvDate.setText(rDate);

        if (String.valueOf(actuList.get(position).getCreator().getId()).equals(Tools.getData(context, "idprofile")))
            holder.bDelete.setVisibility(View.VISIBLE);
        else
            holder.bDelete.setVisibility(View.GONE);

        holder.bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog(position);
            }
        });

        holder.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(context.getString(R.string.server_url3) + actuList.get(position).getVideo());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setDataAndType(uri, "video/mp4");
                context.startActivity(intent);
            }
        });
        holder.ivThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.ivVideo.getVisibility() == View.INVISIBLE) {
                    FragPhoto.photos = new ArrayList<>();
                    FragPhoto.photos.add(new WISPhoto(0, "", actuList.get(position).getImg(), actuList.get(position).getDate()));
                    Intent i = new Intent(context, PhotoGallery.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("p", 0);
                    context.startActivity(i);
                }
            }
        });
        holder.llPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SELECTED = actuList.get(position);
                Intent i = new Intent(context, ActuDetails.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(new Intent(context, ActuDetails.class));
            }
        });

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + actuList.get(position).getCreator().getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivUser.setImageBitmap(response.getBitmap());
                }
            }
        });

        holder.tvUName.setText(actuList.get(position).getCreator().getFirstName());
        return convertView;
    }

    private void setLike(final int p) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\", \"id_act\":\"" + actuList.get(p).getId() + "\", \"jaime\":\"" + actuList.get(p).isiLike() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqLike = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.like_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                            }
                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            try {
                                actuList.get(p).setiLike(false);
                                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
                            } catch (Exception e2) {

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                actuList.get(p).setiLike(false);
                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }

    private void showCmts(int position) {
        Intent i = new Intent(context, Comments.class);
        i.putExtra("p", position);
        context.startActivity(i);
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void deleteDialog(final int position) {
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.msg_confirm_delete_post))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteActu(actuList.get(position).getId(), position);
                    }

                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    private void deleteActu(int idActu, final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_act", String.valueOf(idActu));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.deleteact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                actuList.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    @Override
    public void replyListener() {

    }

    @Override
    public void addComments(int p, String message, ListView replyView,String comments_id) {

    }

    private static class ViewHolder {
        TextView tvPlus, tvTitle, tvDesc, tvNLike, tvNView, tvCmt, tvLMore, tvCmtActu, tvDate, tvUName;
        ImageView ivVideo, ivLike, ivView, ivCmt, ivShare, ivUser;
        ImageView ivThumb;
        RelativeLayout rlThumb;
        LinearLayout llPlus;
        ListViewScrollable lvCmt;
        Button bDelete;

        public ViewHolder(View view) {
            tvPlus = (TextView) view.findViewById(R.id.tvPlus);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
            tvNLike = (TextView) view.findViewById(R.id.tvNLike);
            tvNView = (TextView) view.findViewById(R.id.tvNView);
            tvCmt = (TextView) view.findViewById(R.id.tvCmt);
            tvLMore = (TextView) view.findViewById(R.id.tvLMore);
            ivThumb = (ImageView) view.findViewById(R.id.ivThumb);
            ivLike = (ImageView) view.findViewById(R.id.ivLike);
            ivView = (ImageView) view.findViewById(R.id.ivView);
            ivCmt = (ImageView) view.findViewById(R.id.ivCmt);
            ivShare = (ImageView) view.findViewById(R.id.ivShare);
            ivVideo = (ImageView) view.findViewById(R.id.ivVideo);
            rlThumb = (RelativeLayout) view.findViewById(R.id.rlThumb);
            lvCmt = (ListViewScrollable) view.findViewById(R.id.lvCmt);
            lvCmt.setExpanded(true);
            tvCmtActu = (TextView) view.findViewById(R.id.tvCmtActu);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            bDelete = (Button) view.findViewById(R.id.bDelete);
            llPlus = (LinearLayout) view.findViewById(R.id.llPlus);
            tvUName = (TextView) view.findViewById(R.id.tvUName);
            ivUser = (ImageView) view.findViewById(R.id.ivUser);
            view.setTag(this);
        }
    }
}
