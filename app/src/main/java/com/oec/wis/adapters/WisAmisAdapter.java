//package com.oec.wis.adapters;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.app.admin.SystemUpdatePolicy;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.CheckBox;
//import android.widget.Filter;
//import android.widget.Filterable;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.SectionIndexer;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.ImageLoader;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.R;
//import com.oec.wis.dialogs.Chat;
//import com.oec.wis.dialogs.ProfileView;
//import com.oec.wis.entities.EntryItem;
//import com.oec.wis.entities.Item;
//import com.oec.wis.entities.SectionItem;
//import com.oec.wis.entities.WISAds;
//import com.oec.wis.entities.WISUser;
//import com.oec.wis.tools.Tools;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//import java.util.Set;
//import java.util.TreeSet;
//
///**
// * Created by asareri08 on 21/07/16.
// */
//public class WisAmisAdapter extends BaseAdapter implements SectionIndexer,Filterable {
//
//
//
//    Map<String, List<WISUser>> mapIndex;
//    String[] sections;
//
//    Context context;
//
//    ArrayList <Item>orginalitems;
//    ArrayList <Item>items;
//    List<WISAds> ads;
//    Map<String, List<WISUser>> map;
//    private ItemFilter mFilter = new ItemFilter();
//
//
//    private LayoutInflater mInflater;
//
//    public WisAmisAdapter (Context context,ArrayList items,String []sectionIndex, Map<String, List<WISUser>> map) {
//
//        this.context = context;
//
//        mInflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        this.orginalitems = items;
//        this.map = map;
//        this.mapIndex = map;
//
//        this.items = orginalitems;
//
//
//
////        mapIndex = new LinkedHashMap<String, Integer>();
////
////        for (int x = 0; x < items.size(); x++) {
////            Item i = (Item) items.get(x);
////            EntryItem userData = (EntryItem)i;
////            String nameIndex = String.valueOf(userData.getUser().getFullName());
////            String ch = nameIndex.substring(0, 1);
////            ch = ch.toUpperCase(Locale.US);
////
////            // HashMap will prevent duplicates
////            mapIndex.put(ch, x);
////        }
////
////        Set<String> sectionLetters = mapIndex.keySet();
////
////        // create a list from the set to sort
////        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
////
////        Log.d("sectionList", sectionList.toString());
////        Collections.sort(sectionList);
////
////        sections = new String[sectionList.size()];
////
////        sectionList.toArray(sections);
//        this.sections = sectionIndex;
//
//        Log.d("section array", String.valueOf(sections));
//
//
//    }
//
//
//    @Override
//    public int getCount() {
//        return items.size();
//    }
//
//    @Override
//    public  Object getItem(int position) {
//        return items.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        int rowType = getItemViewType(position);
//
//        final ViewHolder holder;
//
//
//        final Item i = items.get(position);
//        int index = 0;
//
//        if (i != null) {
//            if(i.isSection()){
//                index = index++;
//                SectionItem si = (SectionItem)i;
//                convertView =mInflater.inflate(R.layout.snippet_item2, null);
//
//                convertView.setOnClickListener(null);
//                convertView.setOnLongClickListener(null);
//                convertView.setLongClickable(false);
//
////                TextView textView = (TextView) convertView.findViewById(R.id.textSeparator);
////                if (si.getTitle()!=null)
////                    textView.setText(si.getTitle());
//
//            }else{
//
//                EntryItem ei = (EntryItem)i;
//                convertView =mInflater.inflate(R.layout.row_friend, null);
//                holder = new ViewHolder(convertView);
////                convertView =((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_friend, null);
//                final TextView title = (TextView)convertView.findViewById(R.id.uName);
//                title.setText(ei.getUser().getFullName());
//
////                String country=ei.getC
//
////changed 45
//
////                if(position %2 != 0) {
////                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
////                    holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.white));
////                }
//
////                if(position %2 != 0) {
////                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));
////                    holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.gray2));
////                }
//
//
//
//                configureCell(holder,position,ei);
//            }
//        }
//
//
//        return convertView;
//    }
//
//    private void configureCell(final ViewHolder  holder,final int position,final EntryItem data){
//
//        holder.ivPic.setImageResource(0);
//        if(data.getUser().isPublishNewsFeed()!=null){
//            if(data.getUser().isPublishNewsFeed().equals("0")){
////                holder.showStatus.setImageResource(R.drawable.unchecked);
//            }else{
////                holder.showStatus.setImageResource(R.drawable.done);
//            }
//
//
//        }
//        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-Regular.ttf");
//        holder.tvNFriend.setTypeface(font);
//
//
//        holder.tvNFriend.setText("Brussels");
//
//
//
//        if(data.getUser().getFullName()!=null){
//            holder.tvName.setText(data.getUser().getFullName());
//            holder.tvName.setTypeface(font);
//        }else{
//            holder.tvName.setText(data.getUser().getFirstName() + " " + data.getUser().getLastName());
//            holder.tvName.setTypeface(font);
//
//        }
//
//         String country=Tools.getData(context, "country");
//        System.out.println("Country======>" +country);
//
//
////        holder.tvNFriend.setText(String.valueOf(data.getUser().getnFriend()) + " Amis");
////
////        holder.tvNFriend.setTypeface(font);
//
//
//        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.getUser().getPic(), new ImageLoader.ImageListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                holder.ivPic.setImageResource(R.drawable.empty);
//                retryImageDownload(holder,position,data);
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    holder.ivPic.setImageBitmap(response.getBitmap());
//                } else {
//                    holder.ivPic.setImageResource(R.drawable.empty);
//                }
//            }
//        });
//
//
//        if(data.getUser().getGroupChatalive()!=null){
//
//            if(data.getUser().getGroupChatalive()){
//
//                holder.selecteFriends.setVisibility(View.VISIBLE);
//                holder.selecteFriends.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//            }else{
//
//                holder.selecteFriends.setVisibility(View.GONE);
//            }
//        }
//
//
////        holder.bChat.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                Intent i = new Intent(context, Chat.class);
////                i.putExtra("id", data.getUser().getId());
////                context.startActivity(i);
////            }
////        });
////        holder.bNotif.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                dialogPubs(data.getUser().getId());
////            }
////        });
//        holder.ivPic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(context, ProfileView.class);
//                i.putExtra("id", data.getUser().getId());
//                context.startActivity(i);
//            }
//        });
//
//        final String currentUser = Tools.getData(context, "idprofile");
//        final String amisId= String.valueOf(data.getUser().getId());
//
//        final String objectIds= String.valueOf(data.getUser().getObjectId());
//
//        final  String unfollowerId = data.getUser().getUnfollowerId();
//
//
////         changed 45
//
////        holder.showStatus.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if(!currentUser.equals(unfollowerId)) {
////                    if (data.getUser().isPublishNewsFeed().equals("1")) {
////
////                        data.getUser().setPublishNewsFeed("0");
////                        updateAmisView(objectIds, currentUser, amisId, "0",position,data);
////                    } else {
////                        data.getUser().setPublishNewsFeed("1");
////                        updateAmisView(objectIds, currentUser, amisId, "1",position,data);
////                    }
////                }
////                else{
////                    Toast.makeText(context,"your friend unfollowed you", Toast.LENGTH_SHORT).show();
////                }
////
////            }
////        });
//
//
//
//    }
//
//    public void retryImageDownload(final ViewHolder holder,int position,EntryItem data){
//
//        try {
//
//            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.getUser().getPic(), new ImageLoader.ImageListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    holder.ivPic.setImageResource(R.drawable.empty);
//                }
//
//                @Override
//                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                    if (response.getBitmap() != null) {
//                        holder.ivPic.setImageBitmap(response.getBitmap());
//                    } else {
//                        holder.ivPic.setImageResource(R.drawable.empty);
//                    }
//                }
//            });
//
//        }
//        catch (IndexOutOfBoundsException ex){
//            ex.printStackTrace();
//        }
//
//
//    }
//    private void dialogPubs(int idFriend) {
//        final Dialog dialog = new Dialog(context);
//        dialog.setTitle(context.getString(R.string.select_pub_tonotif));
//        dialog.setContentView(R.layout.dialog_pickpub);
//        ListView lvPubs = (ListView) dialog.findViewById(R.id.lvPubs);
//        dialog.findViewById(R.id.bFinish).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Log.i("pub dialog","dismissed");
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//        loadMyAds(dialog, lvPubs, idFriend);
//    }
//
//    private void loadMyAds(final Dialog dialog, final ListView lvPubs, final int idFriend) {
//        dialog.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        ads = new ArrayList<>();
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.actpub_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("result").equals("true")) {
//                                Log.i("pub response", String.valueOf(response));
//                                JSONArray data = response.getJSONArray("data");
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj = data.getJSONObject(i);
//                                        String thumb = "";
//                                        try {
//                                            thumb = obj.getString("photo_video");
//
//                                        } catch (Exception e) {
//                                        }
//
//                                        ads.add(new WISAds(obj.getInt("idpub"), obj.getInt("idact"), obj.getString("titre"), obj.getString("description"), obj.getString("photo"), obj.getString("type_obj"), thumb,false));
//                                        Log.i("pub adapter count", String.valueOf(ads.size()));
//                                        lvPubs.setAdapter(new PickPubAdapter(context, ads, idFriend));
//
//                                    } catch (Exception e) {
//
//                                    }
//                                }
//                            }
//                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dialog.findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqPub);
//    }
//    public void updateAmisView(String objectIds, String currentUserId, final String amisId, final String  status, final int pos, final EntryItem data){
//
//        ads = new ArrayList<>();
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody =new JSONObject();
//            jsonBody.put("user_id",currentUserId);
//            jsonBody.put("object_id",objectIds);
//            jsonBody.put("status",status);
//            jsonBody.put("unfollower_id",amisId);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        Log.i("parms", String.valueOf(jsonBody));
//
//        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.updatedAmisView), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("result").equals("true")) {
////                                JSONArray data = response.getJSONArray("data");
//
//                                data.getUser().setPublishNewsFeed(status);
//                                data.getUser().setUnfollowerId(amisId);
//
//
//                                data.getUser().setPublishNewsFeed(status);
//                                data.getUser().setUnfollowerId(amisId);
//
//
//
//                                notifyDataSetChanged();
//
//
//                            }
//
//
//                        } catch (JSONException e) {
//
//                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(context, "token"));
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqPub);
//
//    }
//
//    @Override
//    public Object[] getSections() {
//        return sections;
//    }
//
//    @Override
//    public int getPositionForSection(int sectionIndex) {
//
//
//        Log.i("section index%@", String.valueOf(map.get(sections[sectionIndex])));
//        return sectionIndex ;
//    }
//
//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }
//
//    @Override
//    public Filter getFilter() {
//        Log.i("filter","filter");
//        return mFilter;
//    }
//
//
//    private static class ViewHolder {
//        TextView tvName, tvNFriend,time,createdDate;
//        ImageView ivPic, bChat, bNotif;
//        ImageButton showStatus;
//        LinearLayout llContent;
//        TextView textView;
//        CheckBox selecteFriends;
//
//
//        public ViewHolder(View view) {
//            tvName = (TextView) view.findViewById(R.id.uName);
//            tvNFriend = (TextView) view.findViewById(R.id.fCount);
//            ivPic = (ImageView) view.findViewById(R.id.ivPic);
////            bChat = (ImageView) view.findViewById(R.id.bChat);
////            bNotif = (ImageView) view.findViewById(R.id.bNotif);
//            llContent = (LinearLayout) view.findViewById(R.id.llContent);
////            showStatus = (ImageButton)view.findViewById(R.id.statusOption);
//            time = (TextView)view.findViewById(R.id.time);
//            createdDate = (TextView)view.findViewById(R.id.tvDate);
////            selecteFriends = (CheckBox)view.findViewById(R.id.selectFriends);
//
//
//            view.setTag(this);
//        }
//    }
//
//
//    // Filter Class
//    private class ItemFilter extends Filter {
//
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//
//            FilterResults filterResults = new FilterResults();
//            Log.i("fileter search keyword", String.valueOf(filterResults));
//
//            if (constraint!=null && constraint.length()>0) {
//                ArrayList<Item> tempList = new ArrayList<Item>();
//
//                // search content in friend list
//                for (Item list : items) {
//                    Log.i("search keyword", String.valueOf(list));
//                    if(list.isSection()){
////                        SectionItem user = (SectionItem) list;
////                        Log.i("search keyword user", String.valueOf(user));
////                        if (user.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
////                            tempList.add(list);
////                        }
//
//                    }else{
//                        EntryItem user = (EntryItem)list;
//                        Log.i("search keyword user", String.valueOf(user));
//                        if (user.getUser().getFullName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
//                            tempList.add(list);
//                        }
//                    }
//
//                }
//
//                filterResults.count = tempList.size();
//                filterResults.values = tempList;
//            } else {
//                items = orginalitems;
//                filterResults.count = items.size();
//                filterResults.values = items;
//            }
//
//            return filterResults;
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//
//            items = (ArrayList<Item>) results.values;
//
//            Log.i("publish","search text");
//            Log.i("publish constraint", String.valueOf(constraint));
//            Log.i("publish results", String.valueOf(results));
//            notifyDataSetChanged();
//        }
//
//    }
//
//}
package com.oec.wis.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.admin.SystemUpdatePolicy;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.entities.EntryItem;
import com.oec.wis.entities.Item;
import com.oec.wis.entities.SectionItem;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by asareri08 on 21/07/16.
 */
public class WisAmisAdapter extends BaseAdapter implements SectionIndexer,Filterable {



    Map<String, List<WISUser>> mapIndex;
    String[] sections;

    Context context;

    ArrayList <Item>orginalitems;
    ArrayList <Item>items;
    List<WISAds> ads;
    Map<String, List<WISUser>> map;
    private ItemFilter mFilter = new ItemFilter();


    private LayoutInflater mInflater;

    public WisAmisAdapter (Context context,ArrayList items,String []sectionIndex, Map<String, List<WISUser>> map) {

        this.context = context;

        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.orginalitems = items;
        this.map = map;
        this.mapIndex = map;

        this.items = orginalitems;
        this.sections = sectionIndex;

        Log.d("section array", String.valueOf(sections));


    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public  Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        int rowType = getItemViewType(position);

        final ViewHolder holder;


        final Item i = items.get(position);
        int index = 0;

        if (i != null) {
            if(i.isSection()){
                index = index++;
                SectionItem si = (SectionItem)i;
                convertView =mInflater.inflate(R.layout.snippet_item2, null);

                convertView.setOnClickListener(null);
                convertView.setOnLongClickListener(null);
                convertView.setLongClickable(false);
                TextView textView = (TextView) convertView.findViewById(R.id.textSeparator);
                if (si.getTitle()!=null)
                    textView.setText(si.getTitle());

            }else{

                EntryItem ei = (EntryItem)i;
                convertView =mInflater.inflate(R.layout.row_friend, null);
                holder = new ViewHolder(convertView);
//                convertView =((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_friend, null);
                final TextView title = (TextView)convertView.findViewById(R.id.uName);


                title.setText(ei.getUser().getFullName());


                if(position %2 != 0) {
                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
                    holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.white));
                }

                if(position %2 != 0) {
                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));
                    holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.gray2));
                }


                configureCell(holder,position,ei);
            }
//
//            else{
//
//                EntryItem ei = (EntryItem)i;
//                convertView =mInflater.inflate(R.layout.row_friend, null);
//                holder = new ViewHolder(convertView);
////                convertView =((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_friend, null);
//                final TextView title = (TextView)convertView.findViewById(R.id.uName);
//                title.setText(ei.getUser().getFullName());
//
////                String country=ei.getC
//
////changed 45
//
//                if(position %2 != 0) {
//                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
//                    holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.white));
//                }
//
//                if(position %2 != 0) {
//                    holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));
//                    holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.gray2));
//                }
//
//
//
//                configureCell(holder,position,ei);
//            }
        }


        return convertView;
    }

    private void configureCell(final ViewHolder  holder,final int position,final EntryItem data){

        holder.ivPic.setImageResource(0);
        if(data.getUser().isPublishNewsFeed()!=null){
            if(data.getUser().isPublishNewsFeed().equals("0")){
                holder.showStatus.setImageResource(R.drawable.done_two);
            }else{
                holder.showStatus.setImageResource(R.drawable.circle_blue_color);
            }


        }
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-Regular.ttf");
        holder.tvNFriend.setTypeface(font);


        holder.tvNFriend.setText("Brussels");



        if(data.getUser().getFullName()!=null){
            holder.tvName.setText(data.getUser().getFullName());
            holder.tvName.setTypeface(font);
        }else{
            holder.tvName.setText(data.getUser().getFirstName() + " " + data.getUser().getLastName());
            holder.tvName.setTypeface(font);

        }

        String country=Tools.getData(context, "country");
        System.out.println("Country======>" +country);





        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.getUser().getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPic.setImageResource(R.drawable.empty);
                retryImageDownload(holder,position,data);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPic.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPic.setImageResource(R.drawable.empty);
                }
            }
        });


        if(data.getUser().getGroupChatalive()!=null){

            if(data.getUser().getGroupChatalive()){

                holder.selecteFriends.setVisibility(View.VISIBLE);
                holder.selecteFriends.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }else{

                holder.selecteFriends.setVisibility(View.GONE);
            }
        }



        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ProfileView.class);
                i.putExtra("id", data.getUser().getId());
                context.startActivity(i);
            }
        });

        final String currentUser = Tools.getData(context, "idprofile");
        final String amisId= String.valueOf(data.getUser().getId());

        final String objectIds= String.valueOf(data.getUser().getObjectId());

        final  String unfollowerId = data.getUser().getUnfollowerId();


        holder.showStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!currentUser.equals(unfollowerId)) {
                    if (data.getUser().isPublishNewsFeed().equals("1")) {

                        data.getUser().setPublishNewsFeed("0");
                        updateAmisView(objectIds, currentUser, amisId, "0",position,data);
                    } else {
                        data.getUser().setPublishNewsFeed("1");
                        updateAmisView(objectIds, currentUser, amisId, "1",position,data);
                    }
                }
                else{
                    Toast.makeText(context,"your friend unfollowed you", Toast.LENGTH_SHORT).show();
                }

            }
        });






    }

//    private void configureCell(final ViewHolder  holder,final int position,final EntryItem data){
//
//        holder.ivPic.setImageResource(0);
//        if(data.getUser().isPublishNewsFeed()!=null){
//            if(data.getUser().isPublishNewsFeed().equals("0")){
//                holder.showStatus.setImageResource(R.drawable.unchecked);
//            }else{
//                holder.showStatus.setImageResource(R.drawable.done);
//            }
//
//
//        }
//
//
//        if(data.getUser().getFullName()!=null){
//            holder.tvName.setText(data.getUser().getFullName());
//        }else{
//            holder.tvName.setText(data.getUser().getFirstName() + " " + data.getUser().getLastName());
//        }
//
//
//
//        holder.tvNFriend.setText(String.valueOf(data.getUser().getnFriend()) + " Amis");
//        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.getUser().getPic(), new ImageLoader.ImageListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                holder.ivPic.setImageResource(R.drawable.empty);
//                retryImageDownload(holder,position,data);
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    holder.ivPic.setImageBitmap(response.getBitmap());
//                } else {
//                    holder.ivPic.setImageResource(R.drawable.empty);
//                }
//            }
//        });
//
//
//        if(data.getUser().getGroupChatalive()!=null){
//
//            if(data.getUser().getGroupChatalive()){
//
//                holder.selecteFriends.setVisibility(View.VISIBLE);
//                holder.selecteFriends.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//            }else{
//
//                holder.selecteFriends.setVisibility(View.GONE);
//            }
//        }
//
//
//        holder.bChat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(context, Chat.class);
//                i.putExtra("id", data.getUser().getId());
//                context.startActivity(i);
//            }
//        });
//        holder.bNotif.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialogPubs(data.getUser().getId());
//                //loadData(data.getUser().getId());
//            }
//        });
//        holder.ivPic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(context, ProfileView.class);
//                i.putExtra("id", data.getUser().getId());
//                context.startActivity(i);
//            }
//        });
//
//        final String currentUser = Tools.getData(context, "idprofile");
//        final String amisId= String.valueOf(data.getUser().getId());
//
//        final String objectIds= String.valueOf(data.getUser().getObjectId());
//
//        final  String unfollowerId = data.getUser().getUnfollowerId();
//
//
//        holder.showStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(!currentUser.equals(unfollowerId)) {
//                    if (data.getUser().isPublishNewsFeed().equals("1")) {
//
//                        data.getUser().setPublishNewsFeed("0");
//                        updateAmisView(objectIds, currentUser, amisId, "0",position,data);
//                    } else {
//                        data.getUser().setPublishNewsFeed("1");
//                        updateAmisView(objectIds, currentUser, amisId, "1",position,data);
//                    }
//                }
//                else{
//                    Toast.makeText(context,"your friend unfollowed you", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//
//
//
//    }

    public void retryImageDownload(final ViewHolder holder,int position,EntryItem data){

        try {

            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.getUser().getPic(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPic.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPic.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPic.setImageResource(R.drawable.empty);
                    }
                }
            });

        }
        catch (IndexOutOfBoundsException ex){
            ex.printStackTrace();
        }


    }
    private void dialogPubs(int idFriend) {
        final Dialog dialog = new Dialog(context);
        dialog.setTitle(context.getString(R.string.select_pub_tonotif));
        dialog.setContentView(R.layout.dialog_pickpub);
        ListView lvPubs = (ListView) dialog.findViewById(R.id.lvPubs);
        dialog.findViewById(R.id.bFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("pub dialog","dismissed");
                dialog.dismiss();
            }
        });
        dialog.show();
        loadMyAds(dialog, lvPubs, idFriend);
    }

    private void loadMyAds(final Dialog dialog, final ListView lvPubs, final int idFriend) {
        dialog.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        ads = new ArrayList<>();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.actpub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                Log.i("pub response", String.valueOf(response));
                                JSONArray data = response.getJSONArray("data");

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");

                                        } catch (Exception e) {
                                        }

                                        ads.add(new WISAds(obj.getInt("idpub"), obj.getInt("idact"), obj.getString("titre"), obj.getString("description"), obj.getString("photo"), obj.getString("type_obj"), thumb,false));
                                        Log.i("pub adapter count", String.valueOf(ads.size()));
                                        lvPubs.setAdapter(new PickPubAdapter(context, ads, idFriend));

                                    } catch (Exception e) {

                                    }
                                }
                            }
                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }
    public void updateAmisView(String objectIds, String currentUserId, final String amisId, final String  status, final int pos, final EntryItem data){

        ads = new ArrayList<>();
        JSONObject jsonBody = null;

        try {
            jsonBody =new JSONObject();
            jsonBody.put("user_id",currentUserId);
            jsonBody.put("object_id",objectIds);
            jsonBody.put("status",status);
            jsonBody.put("unfollower_id",amisId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("parms", String.valueOf(jsonBody));

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.updatedAmisView), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
//                                JSONArray data = response.getJSONArray("data");

                                data.getUser().setPublishNewsFeed(status);
                                data.getUser().setUnfollowerId(amisId);


                                data.getUser().setPublishNewsFeed(status);
                                data.getUser().setUnfollowerId(amisId);



                                notifyDataSetChanged();


                            }


                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);

    }

    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {


        Log.i("section index%@", String.valueOf(map.get(sections[sectionIndex])));
        return sectionIndex ;
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {
        Log.i("filter","filter");
        return mFilter;
    }


    private static class ViewHolder {
        TextView tvName, tvNFriend,time,createdDate;
        ImageView ivPic, bChat, bNotif;
        ImageButton showStatus;
        LinearLayout llContent;
        TextView textView;
        CheckBox selecteFriends;


        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.uName);
            tvNFriend = (TextView) view.findViewById(R.id.fCount);
            ivPic = (ImageView) view.findViewById(R.id.ivPic);
//            bChat = (ImageView) view.findViewById(R.id.bChat);
//            bNotif = (ImageView) view.findViewById(R.id.bNotif);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
            showStatus = (ImageButton)view.findViewById(R.id.statusOption);
            time = (TextView)view.findViewById(R.id.time);
            createdDate = (TextView)view.findViewById(R.id.tvDate);
//            selecteFriends = (CheckBox)view.findViewById(R.id.selectFriends);


            view.setTag(this);
        }
    }


    // Filter Class
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults filterResults = new FilterResults();
            Log.i("fileter search keyword", String.valueOf(filterResults));

            if (constraint!=null && constraint.length()>0) {
                ArrayList<Item> tempList = new ArrayList<Item>();

                // search content in friend list
                for (Item list : items) {
                    Log.i("search keyword", String.valueOf(list));
                    if(list.isSection()){
//                        SectionItem user = (SectionItem) list;
//                        Log.i("search keyword user", String.valueOf(user));
//                        if (user.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                            tempList.add(list);
//                        }

                    }else{
                        EntryItem user = (EntryItem)list;
                        Log.i("search keyword user", String.valueOf(user));
                        if (user.getUser().getFullName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            tempList.add(list);
                        }
                    }

                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                items = orginalitems;
                filterResults.count = items.size();
                filterResults.values = items;
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            items = (ArrayList<Item>) results.values;

            Log.i("publish","search text");
            Log.i("publish constraint", String.valueOf(constraint));
            Log.i("publish results", String.valueOf(results));
            notifyDataSetChanged();
        }

    }

}