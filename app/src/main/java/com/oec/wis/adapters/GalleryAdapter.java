package com.oec.wis.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISPhoto;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class GalleryAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<WISPhoto> data;

    public GalleryAdapter(Context context, List<WISPhoto> data) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.row_gallery, container, false);

        final ImageView ivPhoto = (ImageView) itemView.findViewById(R.id.ivPhoto);
        final TextView tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        ivPhoto.setImageResource(0);

        Log.e("Image url ",mContext.getString(R.string.server_url3) + data.get(position).getPath());
        ApplicationController.getInstance().getImageLoader().get(mContext.getString(R.string.server_url3) + data.get(position).getPath(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ivPhoto.setImageResource(R.drawable.empty);
                Log.e("Image error","Download");
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivPhoto.setImageBitmap(response.getBitmap());
                    Log.e("Image success","Download");
                } else {
                    ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = format.parse(data.get(position).getDateTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(mContext, new DateTime(d), true).toString();
        tvDate.setText(rDate);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
