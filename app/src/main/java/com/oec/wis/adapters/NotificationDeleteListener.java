package com.oec.wis.adapters;

/**
 * Created by asareri08 on 07/10/16.
 */
public interface NotificationDeleteListener {

    public void updateDeleteResponse(int position, int notifyid);

}
