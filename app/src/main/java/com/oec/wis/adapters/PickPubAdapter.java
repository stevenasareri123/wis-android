package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISAds;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PickPubAdapter extends BaseAdapter {
    Context context;
    List<WISAds> data;
    int idFriend;

    public PickPubAdapter(Context context, List<WISAds> data, int idFriend) {
        this.data = data;
        this.context = context;
        this.idFriend = idFriend;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_pick_pub, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        Tools.showLoader(context,context.getResources().getString(R.string.progress_loading));
        holder.ivPhoto.setImageResource(0);
        holder.tvTitle.setText(data.get(position).getTitle());
        if (data.get(position).getDesc() != null) {
            if (data.get(position).getDesc().length() > 50)
                holder.tvDesc.setText(data.get(position).getDesc().substring(0, 50) + "...");
            else
                holder.tvDesc.setText(data.get(position).getDesc());
        }
        if (data.get(position).getTypeObj().equals("video")) {
            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else {
            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPhoto(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        }
        holder.ivNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(position).isGroup()){
                    sendNotifToGroupUser(position, holder.progress);
                }else{
                    sendNotif(position, holder.progress);
                }

            }
        });


        try{
            Typeface font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            holder.tvDesc.setTypeface(font);
            holder.tvTitle.setTypeface(font);

        }catch (NullPointerException ex){
            Log.e("Exce",ex.getMessage());
        }


        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void sendNotif(int position, final ProgressBar progress) {
        progress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_ami", String.valueOf(idFriend));
            jsonBody.put("id_act", String.valueOf(data.get(position).getIdAct()));

            Log.e("amis id" ,String.valueOf(idFriend));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.notiffirend_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("response111",String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {
                                Toast.makeText(context, context.getString(R.string.friend_notified), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, context.getString(R.string.friend_already_notified), Toast.LENGTH_LONG).show();
                            }
                            progress.setVisibility(View.GONE);

                        } catch (JSONException e) {

                            progress.setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }


    private void sendNotifToGroupUser(int position, final ProgressBar progress) {
        progress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("user_id", Tools.getData(context, "idprofile"));
            jsonBody.put("group_id", String.valueOf(idFriend));
            jsonBody.put("id_act", String.valueOf(data.get(position).getIdAct()));

            Log.e("amis id1" ,String.valueOf(idFriend));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.share_publist_group), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("response",String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {
                                Log.e("response2",String.valueOf(response));
                                Toast.makeText(context, context.getString(R.string.friend_notified), Toast.LENGTH_LONG).show();
                            } else {
                                Log.e("response3",String.valueOf(response));
                                Toast.makeText(context, context.getString(R.string.friend_already_notified), Toast.LENGTH_LONG).show();
                            }
                            progress.setVisibility(View.GONE);

                        } catch (JSONException e) {

                            progress.setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }


    private static class ViewHolder {
        TextView tvTitle, tvDesc;
        ImageView ivPhoto, ivNotif;
        ProgressBar progress;

        public ViewHolder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
            ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            ivNotif = (ImageView) view.findViewById(R.id.ivNotif);
            progress = (ProgressBar) view.findViewById(R.id.loading);
            view.setTag(this);
        }
    }
}
