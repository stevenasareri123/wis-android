package com.oec.wis.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.andraskindler.quickscroll.Scrollable;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISUser;


import com.oec.wis.fragments.FragFriends;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FriendsAdapter extends BaseAdapter implements Filterable, Scrollable {
    Context context;
    List<WISUser> data;
    List<WISAds> ads;
    private List<WISUser>filteredData = null;
    private ItemFilter mFilter = new ItemFilter();
    private HashMap<String, Integer> alphaIndexer;
    private String[] sections;
    private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";


    public FriendsAdapter(Context context, List<WISUser> data) {
        this.data = data;
        this.context = context;
        this.filteredData =  data ;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return filteredData.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;


        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_friend, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPic.setImageResource(0);
        if(data.get(position).isPublishNewsFeed()!=null){
            if(data.get(position).isPublishNewsFeed().equals("0")){
            holder.showStatus.setImageResource(R.drawable.unchecked);
        }else{
            holder.showStatus.setImageResource(R.drawable.done);
        }


            }


        if(filteredData.get(position).getFullName()!=null){

            holder.tvName.setText(filteredData.get(position).getFullName());
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-R.ttf");
            holder.tvName.setTypeface(font);
        }else{
            holder.tvName.setText(filteredData.get(position).getFirstName() + " " + filteredData.get(position).getLastName());
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-R.ttf");
            holder.tvName.setTypeface(font);

        }




        holder.tvNFriend.setText(String.valueOf(filteredData.get(position).getnFriend()) + " Amis");
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + filteredData.get(position).getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPic.setImageResource(R.drawable.empty);
                retryImageDownload(holder, position);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPic.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPic.setImageResource(R.drawable.empty);
                }
            }
        });
        if (position % 2 == 0) {
            holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
            holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.white));
        }

        if(data.get(position).getGroupChatalive()!=null){

            if(data.get(position).getGroupChatalive()){

                holder.selecteFriends.setVisibility(View.VISIBLE);
                holder.selecteFriends.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }else{

                holder.selecteFriends.setVisibility(View.GONE);
            }
        }


        holder.bChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Chat.class);
                i.putExtra("id", filteredData.get(position).getId());
                context.startActivity(i);
            }
        });
        holder.bNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPubs(filteredData.get(position).getId());
            }
        });
        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ProfileView.class);
                i.putExtra("id", filteredData.get(position).getId());
                context.startActivity(i);
            }
        });

        final String currentUser = Tools.getData(context, "idprofile");
        final String amisId= String.valueOf(data.get(position).getId());

        final String objectIds= String.valueOf(data.get(position).getObjectId());

        final  String unfollowerId = data.get(position).getUnfollowerId();


        holder.showStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!currentUser.equals(unfollowerId)) {
                    if (data.get(position).isPublishNewsFeed().equals("1")) {

                        data.get(position).setPublishNewsFeed("0");
                        updateAmisView(objectIds, currentUser, amisId, "0",position);
                    } else {
                        data.get(position).setPublishNewsFeed("1");
                        updateAmisView(objectIds, currentUser, amisId, "1",position);
                    }
                }
                else{
                    Toast.makeText(context,"your friend unfollowed you", Toast.LENGTH_SHORT).show();
                }

            }
        });


            String s ="";
            if(data.get(position).getFirstName()!=null){

              s = data.get(position).getFirstName().substring(0,1);

            }else if(data.get(position).getFullName()!=null){
               s = data.get(position).getFullName().substring(0,1);
            }
            if(alphaIndexer!=null) {
                if (!alphaIndexer.containsKey(s))
                    alphaIndexer.put(s, position);
                Set<String> sectionLetters = alphaIndexer.keySet();
                ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);

                Collections.sort(sectionList);
                sections = new String[sectionList.size()];
                for (int i = 0; i < sectionList.size(); i++)
                    sections[i] = sectionList.get(i);
            }




        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (filteredData.size() > 1)
            return filteredData.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void dialogPubs(int idFriend) {
        final Dialog dialog = new Dialog(context);
        dialog.setTitle(context.getString(R.string.select_pub_tonotif));
        dialog.setContentView(R.layout.dialog_pickpub);
        ListView lvPubs = (ListView) dialog.findViewById(R.id.lvPubs);
        dialog.findViewById(R.id.bFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("pub dialog","dismissed");
                dialog.dismiss();
            }
        });
        dialog.show();
        loadMyAds(dialog, lvPubs, idFriend);
    }

    public void retryImageDownload(final ViewHolder holder,int position){

        try {

            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + filteredData.get(position).getPic(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPic.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPic.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPic.setImageResource(R.drawable.empty);
                    }
                }
            });

        }
        catch (IndexOutOfBoundsException ex){
            ex.printStackTrace();
        }


    }

    private void loadMyAds(final Dialog dialog, final ListView lvPubs, final int idFriend) {
        dialog.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        ads = new ArrayList<>();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.actpub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                Log.i("pub response", String.valueOf(response));
                                JSONArray data = response.getJSONArray("data");

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");

                                        } catch (Exception e) {
                                        }

                                        ads.add(new WISAds(obj.getInt("idpub"), obj.getInt("idact"), obj.getString("titre"), obj.getString("description"), obj.getString("photo"), obj.getString("type_obj"), thumb,false));
                                        Log.i("pub adapter count", String.valueOf(ads.size()));
                                        lvPubs.setAdapter(new PickPubAdapter(context, ads, idFriend));

                                    } catch (Exception e) {

                                    }
                                }
                            }
                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            dialog.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }



    public void updateAmisView(String objectIds, String currentUserId, final String amisId, final String  status, final int pos){

        ads = new ArrayList<>();
        JSONObject jsonBody = null;

        try {
            jsonBody =new JSONObject();
            jsonBody.put("user_id",currentUserId);
            jsonBody.put("object_id",objectIds);
            jsonBody.put("status",status);
            jsonBody.put("unfollower_id",amisId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("parms", String.valueOf(jsonBody));

        JsonObjectRequest reqPub = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.updatedAmisView), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
//                                JSONArray data = response.getJSONArray("data");

                               data.get(pos).setPublishNewsFeed(status);
                                data.get(pos).setUnfollowerId(amisId);


                                filteredData.get(pos).setPublishNewsFeed(status);
                                filteredData.get(pos).setUnfollowerId(amisId);



                                notifyDataSetChanged();


                            }


                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);

    }

    @Override
    public String getIndicatorForPosition(int childposition, int groupposition) {

        Log.i("quick scroll section","quick scroll view");

        Log.i("quick scroll section",Character.toString(data.get(childposition).getFullName().charAt(0)));
        return Character.toString(data.get(childposition).getFullName().charAt(0));
    }

    @Override
    public int getScrollPosition(int childposition, int groupposition) {
        return childposition;
    }

//    @Override
//    public int getPositionForSection(int section) {
//        // If there is no item for current section, previous section will be selected
//        for (int i = section; i >= 0; i--) {
//            for (int j = 0; j < getCount(); j++) {
//                if (i == 0) {
//
//                    // For numeric section
//                    for (int k = 0; k <= 9; k++) {
//                        if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(k)))
//                            return j;
//                    }
//                } else {
//                    if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(mSections.charAt(i))))
//                        return j;
//                }
//            }
//        }
//        return 0;
//    }
//
//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }
//
//    @Override
//    public Object[] getSections() {
//        String[] sections = new String[mSections.length()];
//        for (int i = 0; i < mSections.length(); i++)
//            sections[i] = String.valueOf(mSections.charAt(i));
//        return sections;
//    }



    private static class ViewHolder {
        TextView tvName, tvNFriend,time,createdDate;
        ImageView ivPic, bChat, bNotif,activeStateIV;
        ImageButton showStatus;
        LinearLayout llContent;
        CheckBox selecteFriends;



        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.uName);
            tvNFriend = (TextView) view.findViewById(R.id.fCount);
            ivPic = (ImageView) view.findViewById(R.id.ivPic);
            bChat = (ImageView) view.findViewById(R.id.bChat);
            bNotif = (ImageView) view.findViewById(R.id.bNotif);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
            showStatus = (ImageButton)view.findViewById(R.id.selectFriends);
            time = (TextView)view.findViewById(R.id.time);
            createdDate = (TextView)view.findViewById(R.id.tvDate);
            activeStateIV=(ImageView)view.findViewById(R.id.activeStateIV);



//            this on eonly that id u write code ok
//            this fragment ? tell me that activity name, this one adapter de,fragment name, ok tell FragFriends,
//            ok wait wait de
            /**
             * Intent intent = new intent(getActivity,  FragFriends)
             */



//            selecteFriends = (CheckBox)view.findViewById(R.id.selectFriends);


            view.setTag(this);
        }
    }
    // Filter Class
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<WISUser> tempList = new ArrayList<WISUser>();

                // search content in friend list
                for (WISUser user : data) {
                    if (user.getFullName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = data.size();
                filterResults.values = data;
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<WISUser>) results.values;
            notifyDataSetChanged();
        }

    }



}
