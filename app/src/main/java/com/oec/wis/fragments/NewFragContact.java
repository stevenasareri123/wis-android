package com.oec.wis.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.DiscuAdapter;
import com.oec.wis.adapters.GroupListAdapter;
import com.oec.wis.adapters.UserInvitAdapter;
import com.oec.wis.adapters.UserRInvitAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.MyContact;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.ListViewScrollable;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class NewFragContact extends Fragment {
    View view;
    ListView lvContact, lvRequest,chatListLV,groupChatListLV;
    EditText etFilter;
    List<WISUser> users, rinvit;
    TextView chatsTV,contactsTV,groupsTV,headerTitleTV;
    //    ListView chatListLV,groupChatListLV;
    GroupListAdapter groupListAdapter;
    List<WISUser> userList;
    List<WISChat> chatList,groupList;
    Button bMyContact;
    ImageButton searchButton;
    RelativeLayout searchRL,headerRL;
    ImageView searchBackButton,searchCloseButton;
    String getImagePath;
    DiscuAdapter discuAdapter;
    UserInvitAdapter userInvitAdapter;
    LinearLayout contactBottomLL;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));
            view = inflater.inflate(R.layout.new_contacts_page, container, false);


        } catch (InflateException e) {
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControls();
        loadUsers();
        setListener();
        loadInvits();
    }

    private void initControls() {
        lvContact = (ListView) view.findViewById(R.id.lvContacts);
//        lvContact.setExpanded(true);
        lvRequest = (ListView) view.findViewById(R.id.lvInvits);
//        lvRequest.setExpanded(true);
        etFilter = (EditText) view.findViewById(R.id.etFilter);
        chatsTV=(TextView)view.findViewById(R.id.chatsTV);
        contactsTV=(TextView)view.findViewById(R.id.contactsTV);
        groupsTV=(TextView)view.findViewById(R.id.groupsTV);
//        chatListLV=(ListView)view.findViewById(R.id.chatListLV);
//        groupChatListLV=(ListView)view.findViewById(R.id.groupChatListLV);
        chatListLV= (ListView) view.findViewById(R.id.chatListLV);
        groupChatListLV= (ListView) view.findViewById(R.id.groupChatListLV);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        bMyContact=(Button)view.findViewById(R.id.bMyContact);
        searchButton =(ImageButton)view.findViewById(R.id.searchButton);
        Tools.hideKeyboard(getActivity());
        searchRL =(RelativeLayout)view.findViewById(R.id.searchRL);
        headerRL =(RelativeLayout)view.findViewById(R.id.headerRL);
        contactBottomLL=(LinearLayout)view.findViewById(R.id.contactBottomLL);

        try{
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
            contactsTV.setTypeface(font);
            groupsTV.setTypeface(font);
            chatsTV.setTypeface(font);
            headerTitleTV.setTypeface(font);
            bMyContact.setTypeface(font);
            etFilter.setTypeface(font);

        }catch (NullPointerException ex){

        }

        searchBackButton=(ImageView) view.findViewById(R.id.searchBackButton);
        searchCloseButton =(ImageView) view.findViewById(R.id.searchCloseButton);
        contactsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.twenty_size));
        groupsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.twenty_size));
        chatsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.twenty_size));
    }


    private void setListener() {



        contactsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactBottomLL.setVisibility(View.VISIBLE);
                lvContact.setVisibility(View.VISIBLE);
                chatListLV.setVisibility(View.GONE);
                groupChatListLV.setVisibility(View.GONE);
                contactsTV.setBackgroundColor(getResources().getColor(R.color.blue));
                contactsTV.setTextColor(getResources().getColor(R.color.white));
                groupsTV.setBackgroundColor(getResources().getColor(R.color.white));
                groupsTV.setTextColor(getResources().getColor(R.color.blue));
                chatsTV.setBackgroundColor(getResources().getColor(R.color.white));
                chatsTV.setTextColor(getResources().getColor(R.color.blue));

            }
        });
        chatsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactBottomLL.setVisibility(View.GONE);
                lvContact.setVisibility(View.GONE);
                chatListLV.setVisibility(View.VISIBLE);
                groupChatListLV.setVisibility(View.GONE);

                chatsTV.setBackgroundColor(getResources().getColor(R.color.blue));
                chatsTV.setTextColor(getResources().getColor(R.color.white));
                groupsTV.setBackgroundColor(getResources().getColor(R.color.white));
                groupsTV.setTextColor(getResources().getColor(R.color.blue));
                contactsTV.setBackgroundColor(getResources().getColor(R.color.white));
                contactsTV.setTextColor(getResources().getColor(R.color.blue));
                loadData();
            }
        });

        groupsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactBottomLL.setVisibility(View.GONE);

                chatListLV.setVisibility(View.GONE);
                lvContact.setVisibility(View.GONE);
                groupChatListLV.setVisibility(View.VISIBLE);

                groupsTV.setBackgroundColor(getResources().getColor(R.color.blue));
                groupsTV.setTextColor(getResources().getColor(R.color.white));
                chatsTV.setBackgroundColor(getResources().getColor(R.color.white));
                chatsTV.setTextColor(getResources().getColor(R.color.blue));
                contactsTV.setBackgroundColor(getResources().getColor(R.color.white));
                contactsTV.setTextColor(getResources().getColor(R.color.blue));

                getGroupList();
            }
        });

        bMyContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MyContact.class));
            }
        });
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerRL.setVisibility(View.GONE);
                searchRL.setVisibility(View.VISIBLE);

                searchBackButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchRL.setVisibility(View.GONE);
                        headerRL.setVisibility(View.VISIBLE);

                    }
                });
                searchCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etFilter.getText().clear();
                    }
                });

            }
        });
        etFilter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //final int DRAWABLE_LEFT = 0;
                //final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                //final int DRAWABLE_BOTTOM = 3;

                try{
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (etFilter.getRight() - etFilter.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            loadUsers();
                            Tools.hideKeyboard(getActivity());
                            return true;
                        }
                    }
                }catch (NullPointerException ex){
                    System.out.println("Null Exception" +ex.getMessage());
                }


                return false;
            }
        });

        etFilter.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    loadUsers();
                    Tools.hideKeyboard(getActivity());
                }
                return true;
            }
        });



//        view.findViewById(R.id.bMyContact).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), MyContact.class));
//            }
//        });
    }

    private void loadUsers() {
        users = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("filtre", etFilter.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.filterprofile_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("contact list url",getString(R.string.server_url) + getString(R.string.filterprofile_meth));
//                            Log.i("contact parms", String.valueOf(jsonBody));

                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String name = "";
                                        try {
                                            name = obj.getString("firstname_prt") + " " + obj.getString("lastname_prt");
                                        } catch (Exception e) {
                                        }
                                        try {
                                            name += obj.getString("name");
                                        } catch (Exception e) {
                                        }
                                        users.add(new WISUser(obj.getInt("idprofile"), name, obj.getString("photo"), obj.getInt("nbr_amis_comm"), 0));
                                    } catch (Exception e) {
                                        String x = "";
                                    }
                                }
                                //if (users.size() > 0)
                                //view.findViewById(R.id.tvSuggetion).setVisibility(View.VISIBLE);
                                //else
                                //view.findViewById(R.id.tvSuggetion).setVisibility(View.GONE);


                                userInvitAdapter =new UserInvitAdapter(getActivity(),users);
                                lvContact.setAdapter(userInvitAdapter);
                                setUPFilterConfig(userInvitAdapter);

//                                lvContact.setAdapter(new UserInvitAdapter(getActivity(), users));

                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }

    private void loadInvits() {
        rinvit = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.invits_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String x = "";
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        int nf = 0;
                                        try {
                                            //nf=obj.getInt("liens_amitie");
                                        } catch (Exception e) {
                                        }
                                        rinvit.add(new WISUser(obj.getInt("id_profil"), obj.getString("name"), obj.getString("photo"), nf, obj.getInt("lien_amitie")));
                                    } catch (Exception e) {
                                    }
                                }
                                //if (rinvit.size() > 0)
                                //view.findViewById(R.id.tvInvit).setVisibility(View.VISIBLE);
                                //else
                                //view.findViewById(R.id.tvInvit).setVisibility(View.GONE);
                                lvRequest.setAdapter(new UserRInvitAdapter(getActivity(), rinvit));
                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }




    private void loadData() {
        userList = new ArrayList<>();
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDisc = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.alldiscution_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Tools.dismissLoader();
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        try {
                            if (response.getString("Message").contains("any historique")) {
                                Toast.makeText(getActivity(), getString(R.string.no_chat_historique), Toast.LENGTH_SHORT).show();
                            } else if (response.getString("result").equals("true")) {
                                chatList = new ArrayList<>();
                                JSONArray data = response.getJSONArray("data");

                                System.out.println("channel response");

                                Log.i("resp", String.valueOf(response));

                                JSONArray privateJsonArray = new JSONArray();

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        JSONObject oDisc = obj.getJSONObject("discution");

                                        //  get channel

                                        String privatechannel = "Private_".concat(obj.getString("channels"));

                                        privateJsonArray.put(i,privatechannel);

                                        JSONObject oFriend = obj.getJSONObject("amis");

                                        String firstname=oFriend.getString("firstname");
                                        System.out.println("firstname****" +firstname);


//                                        String Firstname =oFriend.getString("firstname");
//                                        String anis_id=oFriend.getString("lastname");
//                                        String photo=oFriend.getString("photo");
//                                        Date d=new Date();
//                                        Date now = new Date();

//                                        override date and time
                                        Date date=new Date();
                                        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        sourceFormat.format(date);
                                        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getActivity(),"timezone"));
                                        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        destFormat.setTimeZone(tz);
                                        String userTimeZoneFormateddate = destFormat.format(date);
                                        Log.i("resilt timezone ",userTimeZoneFormateddate);


                                        String created_at= String.valueOf(DateUtils.getRelativeTimeSpanString(getDateInMillis(oDisc.getString("created_at")),getDateInMillis(userTimeZoneFormateddate), 0));


                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date d1 = null;
                                        try {
                                            d1 = format.parse(oDisc.getString("created_at"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("chate data",created_at);

                                        chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami"),privatechannel));
                                    } catch (Exception e) {
                                        String x = "";
                                    }
                                }
//                                chatListLV.setAdapter(new DiscuAdapter(getActivity(),chatList));
                                discuAdapter =new DiscuAdapter(getActivity(),chatList);
                                chatListLV.setAdapter(discuAdapter);
                                Log.i("private", String.valueOf(privateJsonArray));
                                setUPFilterConfig(discuAdapter);
                                ChatApi.getInstance().removePrivatechannels();
                                Tools.getPrivateChannel(privateJsonArray);
                            }

                        } catch (JSONException e) {
                            Tools.dismissLoader();
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

//        getGroupList();


        ApplicationController.getInstance().addToRequestQueue(reqDisc);
    }

    public static long getDateInMillis(String srcDate) {

        Log.i("src date",srcDate);
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return 0;
    }


    public void getGroupList(){
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {


            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest getGroups = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.get_groupList), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (response.getString("result").equals("true")) {

                                Log.i("group chat", String.valueOf(response));
                                Tools.dismissLoader();
//                                view.findViewById(R.id.loading).setVisibility(View.GONE);
                                JSONArray data = response.getJSONArray("data");

                                ArrayList<List> groupchannelMember = new ArrayList<>();

                                ArrayList <String> publicid = new ArrayList();

                                groupList = new ArrayList<>();

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);

                                        String publicId =  "Public_".concat(obj.getString("group_id"));

                                        publicid.add(publicId);

                                        JSONArray groupMember = obj.getJSONArray("channel_group_members");

                                        JSONArray groupMemberDetails = obj.getJSONArray("membersDetails");

                                        System.out.println("group meber");

                                        Log.i("group count", String.valueOf(groupMember.length()));

                                        Log.i("group members", String.valueOf(groupMember));

                                        Log.i("groupMemberDetails", String.valueOf(groupMemberDetails));

                                        ChatApi.getInstance().setGroupMembersDetails(publicId,groupMemberDetails);

                                        Log.i("parsinng pMemberDetails", String.valueOf(ChatApi.getInstance().getGroupMembersBychannel(publicId)));


                                        List<String> memberlist = new ArrayList<String>();
                                        for (int m=0; m<groupMember.length(); m++) {
                                            memberlist.add(groupMember.getString(m) );
                                        }

                                        groupchannelMember.add(memberlist);

                                        String lastMessage = "",created_at = "";
                                        JSONObject message = null;
                                        if(obj.optJSONObject("message")!=null){
                                            message =obj.getJSONObject("message");
                                            if(message!=null){
                                                lastMessage = message.getString("message");
                                                created_at = message.getString("created_at");
                                            }

                                        }

                                        Log.i("group chat date",created_at);
//                                            chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami")));

                                        String currentchannel = "Public_".concat(String.valueOf(obj.getInt("group_id")));
                                        groupList.add(new WISChat(true,obj.getInt("group_id"),obj.getString("group_name"),obj.getString("photo"),obj.getInt("number_of_members"),lastMessage,created_at,currentchannel));

//
                                    } catch (Exception e) {

                                    }
                                }

                                Log.i("group chat size", String.valueOf(groupList.size()));
//                                    //is exist
                                groupListAdapter =new GroupListAdapter(getActivity(),groupList);
                                groupChatListLV.setAdapter(groupListAdapter);
                                setUPFilterConfig(groupListAdapter);


                                Log.i("gorup", String.valueOf(groupchannelMember));

                                Log.i("group", String.valueOf(publicid));


                                try{
                                    ChatApi.getInstance().removePublicchannels();

                                    ChatApi.getInstance().addPublicChannels(groupchannelMember,publicid);
                                }catch (IndexOutOfBoundsException ex){
                                    System.out.println("exce" +ex.getMessage());
                                }

                            }



//                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            Tools.dismissLoader();

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(getGroups);

    }
    private void setUPFilterConfig(final GroupListAdapter adapter){
        etFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    private void setUPFilterConfig(final UserInvitAdapter adapter){
        etFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }


    private void setUPFilterConfig(final DiscuAdapter adapter){
        try{
            etFilter.addTextChangedListener(new TextWatcherAdapter(){
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    super.beforeTextChanged(s, start, count, after);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    super.onTextChanged(s, start, before, count);

                    adapter.getFilter().filter(s.toString());

                }

                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);

                }
            });
        }catch(NullPointerException exce){
            Log.e("Exce",exce.getMessage());
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
