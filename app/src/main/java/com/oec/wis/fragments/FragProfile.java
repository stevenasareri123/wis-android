package com.oec.wis.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.CustomPhotoAdapter;
import com.oec.wis.adapters.CutomVideoAdapter;
import com.oec.wis.adapters.GalleryListener;
import com.oec.wis.adapters.ImagePickerListener;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.dialogs.ActuDetails;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.EditProfile;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISUser;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.tools.ItemClickSupport;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragProfile extends Fragment implements GalleryListener ,ImageChooserListener {
    View view;
    ImageView ivProfile, ivProfile2;
    TextView tvFName, tvJob, tvCountry,userAddress,emailIdTextTV,userNameTextTV;
    Bitmap oldPic;
    String imgPath = "";
    LinearLayoutManager actuLM;
    List<WISActuality> actuList;
    RecyclerView.Adapter adaptActu;
    RecyclerView lvActu;
    View editinflatedView;
    public final String  APP_TAG= "MyCustomAPP";
    public String photofilename="photo.jpg";
    static final int REQUEST_CAMERA = 2;
    static final int SELECT_FILE=3;
    ImageView  close_btn;
    public static List<WISPhoto> photos;
    public static  List<WISVideo> video;
    DialogPlus dialog;
    Boolean WISPHOTOSELECTED;
    String WISPHOTOPATH;
    Typeface font;
    TextView usenameTV,emailTV,facebookTV,googleTV,twitterTV,contactTextTV,postTextTV,friendsTextTV,phototextTV,headerTitleTV;
    private PopupWindow popWindow, editpopWindow;
    Button media_button,post_status;
    ImageView close_btn1,edit_activity_image;
    EditText message;
    TextView editTV;
    int position;
    int cPage = 0;
    String selectedObjectId="",selectedObjectType="";
    Popupadapter simpleAdapter, live_status_adapter;
    RelativeLayout contactRL,postRL,newsRL,chatRL;
    private final int SELECT_PHOTO = 1;

    CircularImageView userNotificationView;
    TextView notificationCount;
    ImageChooserManager imageChooserManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        Log.i("create","fragment view");

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));

//            Tools.showLoader(getActivity(),"Loading...");
            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));

            view = inflater.inflate(R.layout.frag_profile, container, false);
            loadControls();
            setListener();
            loadProfileData();
            getActivityLog();
            setFontTypeFace();
//            requestCountContact();
//            requestCountPub();
//            requestCountActu();
            loadActu();
        } catch (InflateException e) {
        }


        return view;
    }



    private void setListener() {

        view.findViewById(R.id.bEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EditProfile.class));
            }
        });
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
        View.OnClickListener pickPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMedia();
//                Intent i = new Intent(Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, 1);
            }
        };
        ivProfile.setOnClickListener(pickPhoto);
//        ivProfile2.setOnClickListener(pickPhoto);
        view.findViewById(R.id.bSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.llSave).setVisibility(View.GONE);
                if (!imgPath.equals("")){
                    new DoUpload().execute();
                }else if(WISPHOTOSELECTED ==Boolean.TRUE){

                    doUpdate(WISPHOTOPATH);

                }

            }
        });
        view.findViewById(R.id.bCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.llSave).setVisibility(View.GONE);
                ivProfile.setImageBitmap(oldPic);
//                ivProfile2.setImageBitmap(oldPic);
            }
        });

        view.findViewById(R.id.tvContacts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NewFragContact();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });


        contactRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NewFragContact();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();

            }
        });
        postRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FragMyPub();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });
        newsRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FragActu();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();

            }
        });
        chatRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserNotification.setDISCUSSIONSELECTED(Boolean.TRUE);
                Fragment fragment = new FragChat();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();

            }
        });
        view.findViewById(R.id.tvPub).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FragMyPub();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });

        view.findViewById(R.id.tvActu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FragActu();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });
        view.findViewById(R.id.tvGroup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               UserNotification.setDISCUSSIONSELECTED(Boolean.TRUE);
                Fragment fragment = new FragChat();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });
        ItemClickSupport.addTo(lvActu).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                AdaptActu.SELECTED = actuList.get(position);
//                startActivity(new Intent(getActivity(), ActuDetails.class));


                editPost(v,position);


            }
        });



        view.findViewById(R.id.tvCountry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserNotification.setLocationSelected(Boolean.TRUE);
                UserNotification.setViewType("DashBoard");
                String addr = Tools.getData(getActivity(),"state")+","+Tools.getData(getActivity(),"country");
                UserNotification.setAddress(addr);
                Fragment fragment = new FragMyLocation();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
            }
        });

    }


    public void  editPost(final View v,final int position){

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        editinflatedView = layoutInflater.inflate(R.layout.edit_activity, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        editpopWindow = new PopupWindow(editinflatedView, size.x - 35, size.y - 300, true);

        editpopWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        editpopWindow.setOutsideTouchable(false);

        editpopWindow.setAnimationStyle(R.style.animationName);

        this.view.setAlpha(0.4f);

        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,


        media_button = (Button) editinflatedView.findViewById(R.id.open_gallery);
        close_btn1 = (ImageView) editinflatedView.findViewById(R.id.close_btn);
        post_status = (Button) editinflatedView.findViewById(R.id.post_status);
        edit_activity_image = (ImageView) editinflatedView.findViewById(R.id.post_image);
        message = (EditText) editinflatedView.findViewById(R.id.edit_act_message);
        editTV=(TextView)editinflatedView.findViewById(R.id.wispopTV);
        try{
            font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            post_status.setTypeface(font);
            message.setTypeface(font);
            post_status.setTypeface(font);
            editTV.setTypeface(font);

        }catch (NullPointerException ex){
            System.out.println("exception===>" +ex.getMessage());
        }




        message.setText(actuList.get(position).getCmtActu());

        if(actuList.get(position).getpType().equals("partage_photo")){


            updateImage(position,edit_activity_image,actuList.get(position).getImg());

        }else if (actuList.get(position).getpType().equals("partage_video")){
            updateImage(position,edit_activity_image,actuList.get(position).getThumb());
        }

        close_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editpopWindow.dismiss();
                views.setAlpha(1);
            }
        });


        media_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMedia(v,position);
            }
        });

        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateActivity(position,message.getText().toString());

            }
        });


        editpopWindow.showAtLocation(v, Gravity.CENTER, 0, 100);




    }
    public void updateImage(final int position,ImageView image,String path){
        Picasso.with(getActivity())
                .load(getString(R.string.server_url3) + path)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(image);
    }


    public void updateActivity(final int act_pos,String editedMessage){

        String object_id="",object_type="";
        String id_act = String.valueOf(actuList.get(act_pos).getId());


        if(!selectedObjectId.equals("")){

            object_id = selectedObjectId;
            object_type = selectedObjectType;

        }else{
            object_type="exists";
            object_id = "exists";

        }


        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("act_id", String.valueOf(id_act));
            jsonBody.put("object_model",object_type);
            jsonBody.put("object_id",object_id);
            jsonBody.put("commentaire",editedMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqSave = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.updateActivity), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Tools.dismissLoader();

//                                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                                editpopWindow.dismiss();

                                view.setAlpha(1);

                                Toast.makeText(getActivity(),getString(R.string.updated_msg), Toast.LENGTH_SHORT).show();
                                cPage = 0;
//                                loadingMore = Boolean.FALSE;
                                loadActu();

//

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSave);

    }



    public void chooseMedia(final View view,final int pos){
        final CharSequence[] items = { getString(R.string.photo), getString(R.string.video),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choisissez Photo De");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(
                    DialogInterface dialog, int item) {
                if(items[item].equals(items[0])){
                    dialog.dismiss();

                    openWisMediaPopup(view,pos,"photo");
                }
                else if(items[item].equals(items[1])){
                    dialog.dismiss();
                    openWisMediaPopup(view,pos,"video");
                }

                else if(items[item].equals(items[2])){
                    dialog.dismiss();
                }
            }

        });
        builder.show();



    }


    public void openWisMediaPopup(final View v,final int position,String type){

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.media_gallery, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

        this.view.setAlpha(0.4f);

        final View views = this.view;


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);


        TextView header = (TextView)inflatedView.findViewById(R.id.headerTitleForWindow);




        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
            }
        });

        // show the popup at bottom of the screen and set some margin at bottom ie,


        final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);

        if(type.equals("photo")){
            header.setText(getString(R.string.photo));
            loadPhotos(gvMedia);
        }else{
            header.setText(getString(R.string.video));
            loadVideos(gvMedia);
        }


        popWindow.showAtLocation(v, Gravity.CENTER, 0, 100);

    }
    private void loadVideos(final GridView gridVideo) {
        video = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqVideos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        video.add(new WISVideo(obj.getInt("id"), "", getString(R.string.server_url3) + obj.getString("url_video"), obj.getString("created_at"), obj.getString("image_video")));
                                    } catch (Exception e) {
                                    }
                                }
                                if (video.size() == 0)
                                    Toast.makeText(getActivity(), getString(R.string.no_video_element), Toast.LENGTH_SHORT).show();
                                gridVideo.setAdapter(new CutomVideoAdapter(video,FragProfile.this));

                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                            Tools.dismissLoader();
                        } catch (JSONException e) {
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqVideos);
    }


    public void openMedia(){

        final ArrayList popupList = new ArrayList();

        popupList.add(new Popupelements(R.drawable.slide_wis_photo,"Pick Photos"));
        popupList.add(new Popupelements(R.drawable.slide_wis_photo,"Wis Photos"));
        popupList.add(new Popupelements(R.drawable.camera_grey,"Take Photos"));

        Popupadapter simpleAdapter = new Popupadapter(getActivity(), false, popupList);


       dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
                            Intent i = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 1);
                            dialog.dismiss();

                        }
                        else if(position ==1){

                           openWisMedia(view,0,"photo");


                        }
                        else if(position == 2){
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent,REQUEST_CAMERA);

                            takeImageFromCamera();
                            dialog.dismiss();



                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }

    private void takeImageFromCamera() {
        Log.e("Image custom profilee","Library");
        imageChooserManager = new ImageChooserManager(FragProfile.this, ChooserType.REQUEST_CAPTURE_PICTURE);
        imageChooserManager.setImageChooserListener(FragProfile.this);
        try {
            imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    oldPic = ((BitmapDrawable) ivProfile.getDrawable()).getBitmap();
                    Bitmap sImage = BitmapFactory.decodeStream(imageStream);
                    ivProfile.setImageBitmap(sImage);
//                    ivProfile2.setImageBitmap(sImage);
                    view.findViewById(R.id.llSave).setVisibility(View.VISIBLE);
                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = getActivity().managedQuery(selectedImage, proj, null, null,
                            null);
                    final int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    imgPath = cursor.getString(column_index);

                }
                break;
            case 2:
                if(resultCode == getActivity().RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    }catch (FileNotFoundException e){
                        e.printStackTrace();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    ivProfile.setImageBitmap(thumbnail);
//                    ivProfile2.setImageBitmap(thumbnail);
                    System.out.println("Welcome"+destination);
                    view.findViewById(R.id.llSave).setVisibility(View.VISIBLE);
                    //new DoUpload().execute();
                    imgPath= String.valueOf(destination);
                }
                break;
            case 3:


                Log.i("onActivityResult", "showImageLibraryProfile");


                Log.i("showImageLibrary profile", String.valueOf(Activity.RESULT_OK));

                Log.i("requestCode profile", String.valueOf(requestCode));

                if (resultCode ==  Activity.RESULT_OK &&
                        (requestCode == ChooserType.REQUEST_PICK_PICTURE||
                                requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
                    imageChooserManager.submit(requestCode, imageReturnedIntent);

                }
//                if (resultCode == getActivity().RESULT_OK) {
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    InputStream imageStream = null;
//                    try {
//                        imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    oldPic = ((BitmapDrawable) ivProfile.getDrawable()).getBitmap();
//                    Bitmap sImage = BitmapFactory.decodeStream(imageStream);
//                    ivProfile.setImageBitmap(sImage);
////                    ivProfile2.setImageBitmap(sImage);
//                    view.findViewById(R.id.llSave).setVisibility(View.VISIBLE);
//                    final String[] proj = {MediaStore.Images.Media.DATA};
//                    final Cursor cursor = getActivity().managedQuery(selectedImage, proj, null, null,
//                            null);
//                    final int column_index = cursor
//                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                    cursor.moveToLast();
//                    imgPath = cursor.getString(column_index);
//
//                }
//                break;
            default:
                break;
        }

    }
    public void openWisMedia(final View v,final int position,String type){

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.media_gallery, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

        this.view.setAlpha(0.4f);

        final View views = this.view;


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);



        TextView header = (TextView)inflatedView.findViewById(R.id.headerTitleForWindow);

         try{

             Typeface typeface =Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
             header.setTypeface(typeface);
         }catch(NullPointerException ex){
             Log.d("Exce" ,ex.getMessage());
         }


        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    popWindow.dismiss();
                    dialog.dismiss();
                    view.setAlpha(1);

                }catch (NullPointerException ex){
                    System.out.println("Exception" +ex.getMessage());
                }

            }
        });

        // show the popup at bottom of the screen and set some margin at bottom ie,


        final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);

        if(type.equals("photo")){
            header.setText(getString(R.string.photo));
            loadPhotos(gvMedia);
        }

        popWindow.showAtLocation(v, Gravity.CENTER, 0, 100);

    }
    private void loadPhotos(final GridView gridPhoto) {
        photos = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").contains("any image")) {
                                Toast.makeText(getActivity(), getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
                            } else {
                                if (response.getString("result").equals("true")) {

                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at")));
                                        } catch (Exception e) {
                                        }
                                    }
//                                    gridPhoto.setAdapter(new PhotoAdapter((FragmentActivity) getActivity(), photos));
                                    gridPhoto.setAdapter(new CustomPhotoAdapter(photos,FragProfile.this));
                                }
                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);
    }

    private void loadControls() {
        ivProfile = (ImageView) view.findViewById(R.id.ivProfile);
//        ivProfile2 = (ImageView) view.findViewById(R.id.ivProfile2);
        tvFName = (TextView) view.findViewById(R.id.tvFName);
        tvJob = (TextView) view.findViewById(R.id.tvJob);
        tvCountry = (TextView) view.findViewById(R.id.tvCountry);
//        userAddress = (TextView)view.findViewById(R.id.tvaddr);

        userNameTextTV=(TextView)view.findViewById(R.id.userNameTextTV);
        emailIdTextTV=(TextView)view.findViewById(R.id.emailIdTextTV);
        usenameTV=(TextView)view.findViewById(R.id.userNameTV);
        emailTV=(TextView)view.findViewById(R.id.emailTV);
//        facebookTV=(TextView)view.findViewById(R.id.facebookTV);
//        twitterTV=(TextView)view.findViewById(R.id.twitterTV);
//        googleTV=(TextView)view.findViewById(R.id.googleTV);
        contactTextTV=(TextView)view.findViewById(R.id.contactTextTV);
        postTextTV=(TextView)view.findViewById(R.id.postTextTV);
        friendsTextTV=(TextView)view.findViewById(R.id.friendsTextTV);
        phototextTV=(TextView)view.findViewById(R.id.photoTextTV);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        contactRL=(RelativeLayout) view.findViewById(R.id.contactRL);
        postRL=(RelativeLayout)view.findViewById(R.id.postRL);
        newsRL=(RelativeLayout)view.findViewById(R.id.newsRL);
        chatRL=(RelativeLayout)view.findViewById(R.id.chatRL);



        lvActu = (RecyclerView) view.findViewById(R.id.lvActu);
        lvActu.setHasFixedSize(true);
        actuLM = new LinearLayoutManager(getActivity());
        lvActu.setLayoutManager(actuLM);
        actuList = new ArrayList<>();



        notificationCount = (TextView) view.findViewById(R.id.target_view);
        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);

        downLoadProfileImage();
        if(new UserNotification().getBatchcount()==0)
        {
            notificationCount.setVisibility(View.INVISIBLE);
        }
        else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));

                }
            });
        }
        userNotificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Tools.callDialog(getActivity());
            }
        });
    }

    private void downLoadProfileImage() {
        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(userNotificationView);

    }

    private void setFontTypeFace() {

        try {
            font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            tvFName.setTypeface(font);
            tvJob.setTypeface(font);
            tvCountry.setTypeface(font);
            userNameTextTV.setTypeface(font);
            emailIdTextTV.setTypeface(font);
            usenameTV.setTypeface(font);
            emailTV.setTypeface(font);
//            facebookTV.setTypeface(font);
//            twitterTV.setTypeface(font);
//            googleTV.setTypeface(font);
            postTextTV.setTypeface(font);
            contactTextTV.setTypeface(font);
            phototextTV.setTypeface(font);
            friendsTextTV.setTypeface(font);
            headerTitleTV.setTypeface(font);

        }catch(NullPointerException ex){
            Log.d("Exception",ex.getMessage());
        }

    }




    private void loadProfileData() {




        if (Tools.getData(getActivity(), "name").replace("null", "").equals("")) {
            tvFName.setText(Tools.getData(getActivity(), "firstname_prt") + " " + Tools.getData(getActivity(), "lastname_prt"));
            userNameTextTV.setText(Tools.getData(getActivity(), "firstname_prt") + " " + Tools.getData(getActivity(), "lastname_prt"));
        }
        else {
            tvFName.setText(Tools.getData(getActivity(), "name").replace("null", ""));

            userNameTextTV.setText(Tools.getData(getActivity(), "name").replace("null", ""));

        }
//        String city_country = Tools.getData(getActivity(), "country").concat(" ").concat(Tools.getData(getActivity(), "state"));
        String city_country = Tools.getData(getActivity(), "country");

        tvCountry.setText(city_country);


        String country=Tools.getData(getActivity(),"country");

        System.out.println("country" +country);

        String email = Tools.getData(getActivity(), "email_pr");
        emailIdTextTV.setText(email);
        Log.d("email",email);


        String type = Tools.getData(getActivity(),"profiletype");
//
//        if(type.equals("profiletype")){
//            userAddress.setVisibility(View.INVISIBLE);
//
//        }else{
//            userAddress.setVisibility(View.VISIBLE);
//            userAddress.setText(Tools.getData(getActivity(),"tel_pr"));
//        }

        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                reDownloadImage();

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivProfile.setImageBitmap(response.getBitmap());
//                    ivProfile2.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    public void reDownloadImage(){
        Activity activity = getActivity();
        if(activity != null) {

            // etc ...


            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {



                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivProfile.setImageBitmap(response.getBitmap());
//                        ivProfile2.setImageBitmap(response.getBitmap());
                    }
                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(adaptActu!=null) {
            adaptActu.notifyDataSetChanged();
        }
    }

    private void doUpdate(final String photo) {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getActivity(), "idprofile") + "\",\"name_img\": \"" + photo + "\"}";
            jsonBody = new JSONObject(json);


            Log.e("frag profile do update", String.valueOf(jsonBody));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.update_img_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Tools.saveData(getActivity(), "photo", photo);
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void getActivityLog(){
        JSONObject jsonBody = null;

        try {
            String json = "{\"user_id\": \"" + Tools.getData(getActivity(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.activityLog), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                Log.i("actrivity response", String.valueOf(response));

                                TextView tvContact = (TextView) view.findViewById(R.id.tvContacts);
                                TextView tvPub = (TextView)view.findViewById(R.id.tvPub);
                                TextView tvAct = (TextView)view.findViewById(R.id.tvActu);
                                TextView tvgroup = (TextView)view.findViewById(R.id.tvGroup);

                                JSONObject json = response.getJSONObject("data");
                                Log.i("actrivity response json", String.valueOf(json));
                                tvContact.setText(json.getString("contact"));
                                tvPub.setText(json.getString("pub"));
                                tvAct.setText(json.getString("actuality"));
                                tvgroup.setText(json.getString("group"));

                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);

    }

    private void requestCountContact() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getActivity(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_contact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvContact = (TextView) view.findViewById(R.id.tvContacts);
                                if (count > 500)
                                    tvContact.setText("500+");
                                else
                                    tvContact.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
                        Tools.dismissLoader();
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void requestCountPub() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getActivity(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_pub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvPub = (TextView) view.findViewById(R.id.tvPub);
                                tvPub.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void requestCountActu() {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getActivity(), "idprofile") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.count_actu_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                int count = response.getInt("data");
                                TextView tvActu = (TextView) view.findViewById(R.id.tvActu);
                                tvActu.setText(String.valueOf(count));
                            }
                        } catch (Exception e) {

                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private void loadActu() {
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.myact_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("path_photo");
                                        } catch (Exception e) {
                                        }

                                        String video = "";
                                        try {
                                            video = obj.getString("path_video");
                                        } catch (Exception e) {
                                        }

                                        int nCmt = obj.getInt("nbr_comment");
                                        List<WISCmt> cmts = new ArrayList<>();
                                        if (nCmt > 0) {
                                            JSONArray aCmts = obj.getJSONArray("comment");
                                            for (int j = 0; j < aCmts.length(); j++) {
                                                try {
                                                    JSONObject ocmt = aCmts.getJSONObject(j);
                                                    JSONObject oUser = ocmt.getJSONObject("created_by");
                                                    cmts.add(new WISCmt(ocmt.getInt("id_comment"), new WISUser(oUser.getInt("idprofile"), oUser.getString("name"), oUser.getString("photo")), ocmt.getString("text"), ocmt.getString("last_edit_at"),"false"));
                                                } catch (Exception e2) {
                                                    String x = e2.getMessage();
                                                }
                                            }
                                        }
                                        String text = "";
                                        try {
                                            text = obj.getString("text");
                                        } catch (Exception e3) {
                                        }

                                        String cmtActu = "";
                                        try {
                                            cmtActu = obj.getString("commentair_act");
                                        } catch (Exception e3) {
                                        }

                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");
                                        } catch (Exception e3) {
                                        }
                                        actuList.add(new WISActuality(0,obj.getInt("id_act"), text, "", photo, video, thumb, obj.getInt("nbr_jaime"), obj.getInt("nbr_vue"), obj.getBoolean("like_current_profil"), obj.getString("type_act"), cmts, cmtActu, obj.getString("created_at"), new WISUser(obj.getJSONObject("created_by").getInt("idprofile"), obj.getJSONObject("created_by").getString("name"), obj.getJSONObject("created_by").getString("photo")),obj.getInt("dis_like"),obj.getBoolean("dislike_current_profil"),obj.getInt("share_count"),nCmt,null,null));

                                    } catch (Exception e) {

                                    }
                                }
                                adaptActu = new AdaptActu(actuList,null);

                                lvActu.setAdapter(adaptActu);
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.err_no_new_actu), Toast.LENGTH_SHORT).show();
                            }

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                   Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void pickedItems(String objectId, Bitmap imageDrawable, String type,String name) {
        try{


            popWindow.dismiss();
            dialog.dismiss();
            view.setAlpha(1);


            view.findViewById(R.id.llSave).setVisibility(View.VISIBLE);

            ivProfile.setImageBitmap(imageDrawable);
//        ivProfile2.setImageBitmap(imageDrawable);

            WISPHOTOSELECTED = Boolean.TRUE;
            WISPHOTOPATH = name;

        }catch (NullPointerException ex){
            System.out.println("Null Exception" +ex.getMessage());
        }




//         for popup inside save photo

        try{
            popWindow.dismiss();

            selectedObjectId = objectId;
            selectedObjectType = type;
            edit_activity_image=(ImageView)editinflatedView.findViewById(R.id.post_image);

            Log.i("photo id", String.valueOf(imageDrawable));
            edit_activity_image.setImageBitmap(imageDrawable);
            if(type.equals("partage_photo")){

                edit_activity_image.setImageBitmap(imageDrawable);
            }

        }catch(NullPointerException ex){
            System.out.println("exception" +ex.getMessage());
        }


    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {

        Log.e("choosen image", String.valueOf(chosenImage));
        Log.e("choosen image path", String.valueOf(chosenImage.getFilePathOriginal()));

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                final Uri uri = Uri.fromFile(new File(chosenImage.getFileThumbnail()));
               ivProfile.setImageURI(uri);
                imgPath = chosenImage.getFilePathOriginal();

            }
        });

    }

    @Override
    public void onError(final String s) {
        Log.e("onError image", s);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(getActivity(),s, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }


    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {

                Log.e("result profile",result);
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
//                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    Tools.dismissLoader();
                    Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {
            return Tools.doFileUpload(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.upload_profile_meth), imgPath, Tools.getData(getActivity(), "token"));
        }
    }
}
