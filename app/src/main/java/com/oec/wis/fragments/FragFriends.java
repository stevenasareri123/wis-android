package com.oec.wis.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.FloatRange;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.DiscuAdapter;
import com.oec.wis.adapters.FriendsAdapter;
import com.oec.wis.adapters.GroupListAdapter;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.adapters.WisAmisAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.MapsActivity;
import com.oec.wis.entities.EntryItem;
import com.oec.wis.entities.Item;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.SectionItem;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISUser;

import com.oec.wis.groupcall.GroupCallMainActivity;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import static android.view.Gravity.START;

public class FragFriends extends Fragment {
    View view;
    List<WISUser> friendList;
    ListView lvFriend,groupChatFriendList;
    EditText etFilter;
    public static final String PREFS_NAME = "CustomPermission";
    SharedPreferences settings;
    ArrayList<String>contacts;
    FriendsAdapter friendsAdapter;
    ImageView toogleBack;
    Boolean isCreateGroupChat;
    Button createGroupchat;
    EditText groupName,searchFilter;
    TextView createGroupTV;
    Button next;
    ImageButton close;
    ImageView groupIcon;
    LinkedHashMap mapIndex;
    LinearLayout indexLayout;
    ArrayList <Item>items;
    SelectFriendsAdapter selectFriendsAdapter;
    DialogPlus dialog;
    static final int REQUEST_CAMERA = 2;
    String imagePath;
    List<WISUser> userList;
    List<WISChat> chatList,groupList;

    ListView lvChat,chatListLV,groupChatListLV;

    TextView contactsTV,groupsTV,chatsTv,headerTitleTV;
    GroupListAdapter groupListAdapter;
    ImageView activeStateIV;
    String userActive;
    RelativeLayout rootRL;
    ImageButton searchButton;
    RelativeLayout searchRL,headerRL;
    ImageView searchCloseButton;
    Button searchBackButton;
    DiscuAdapter discuAdapter;
    ProgressDialog pd;
//    CircularImageView userNotificationView;
//    TextView notificationCount;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));

            view = inflater.inflate(R.layout.frag_friend_list, container, false);
            settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
            boolean dialogShown = settings.getBoolean("dialogShown", false);

;
//            displayIndex(container);

            if (!dialogShown) {
                // AlertDialog code here
                showcontactAlert();

            }else{
                loadFriends();


            }
            initControls();
            setListener();



        } catch (InflateException e) {
        }
        return view;
    }






    private void showGroupChatView(){

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.group_chat_view, null);


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();

        groupIcon = (ImageView)alert.findViewById(R.id.groupchatIcon);
        groupName = (EditText)dialogView.findViewById(R.id.groupName);
        next = (Button)alert.findViewById(R.id.bNext);
        close = (ImageButton)alert.findViewById(R.id.closeGroupchat);
        groupChatFriendList = (ListView)alert.findViewById(R.id.groupfriendList);
        searchFilter = (EditText)alert.findViewById(R.id.searchFilter);
        createGroupTV=(TextView)alert.findViewById(R.id.createGroupTV);



        loadFriendsList(groupChatFriendList);


        try{
            Typeface  typeface =Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            groupName.setTypeface(typeface);
            searchFilter.setTypeface(typeface);
            createGroupTV.setTypeface(typeface);
            next.setTypeface(typeface);

        }catch (NullPointerException ex)
        {
            Log.d("Excep" ,ex.getMessage());
        }

        dialogView.findViewById(R.id.groupName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                alert.dismiss();
//                openMedia();






            }

        });



        groupIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMedia();

            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!groupName.getText().toString().equals("")){
                    alert.dismiss();

                    selectFriendsAdapter.createGroupchat(groupName.getText().toString(),imagePath);
                }else{

                    Toast.makeText(getActivity(), getString(R.string.groupchatmessage), Toast.LENGTH_SHORT).show();
                }


            }
        });

    }



    public void openMedia() {

        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
//                boolean result=Utility.checkPermission(MainActivity.this);
                if (items[item].equals("Take Photo")) {
//                    userChoosenTask="Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
//                    userChoosenTask="Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();




    }

    private void  galleryIntent() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }


    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Log.i("showImageLibrary", "showImageLibrary");


        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            Uri uri = imageReturnedIntent.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                groupIcon.setImageBitmap(bitmap);

                final String[] proj = {MediaStore.Images.Media.DATA};
                final Cursor cursor = getActivity().managedQuery(uri, proj, null, null,
                        null);
                final int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToLast();
                imagePath = cursor.getString(column_index);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(requestCode == 2 && resultCode == Activity.RESULT_OK){
            Bitmap thumbnail = (Bitmap)imageReturnedIntent.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
            groupIcon.setImageBitmap(thumbnail);
            System.out.println("Welcome"+destination);
            //new DoUpload().execute();
            imagePath= String.valueOf(destination);
        }
    }


    private void loadFriendsList(ListView groupChatFriendList){

        selectFriendsAdapter = new SelectFriendsAdapter(getActivity(),friendList);
        groupChatFriendList.setAdapter(selectFriendsAdapter);
        selectFriendsAdapter.notifyDataSetChanged();
        loadFilterConfig(selectFriendsAdapter);
    }

    private void loadFilterConfig(final SelectFriendsAdapter adapter){
        searchFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    private void initControls() {
        lvFriend = (ListView) view.findViewById(R.id.friendList);
        lvFriend.setVisibility(View.VISIBLE);
        etFilter = (EditText) view.findViewById(R.id.etFilter);
        toogleBack = (ImageView)view.findViewById(R.id.toggle);
        createGroupchat = (Button)view.findViewById(R.id.groupChatBtn);
        chatsTv=(TextView)view.findViewById(R.id.chatsTV);
        groupsTV=(TextView)view.findViewById(R.id.groupsTV);
        contactsTV=(TextView)view.findViewById(R.id.contactsTV);
        chatListLV=(ListView)view.findViewById(R.id.chatListLV);
        groupChatListLV=(ListView)view.findViewById(R.id.groupChatListLV);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        rootRL=(RelativeLayout)view.findViewById(R.id.rootRL);
        searchButton =(ImageButton) view.findViewById(R.id.searchButton);

        searchRL=(RelativeLayout)view.findViewById(R.id.searchRL);
        headerRL=(RelativeLayout)view.findViewById(R.id.headerRL);

        searchBackButton=(Button) view.findViewById(R.id.searchBackButton);
        searchCloseButton =(ImageView) view.findViewById(R.id.searchCloseButton);

        try {
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
            contactsTV.setTypeface(font);
            groupsTV.setTypeface(font);
            chatsTv.setTypeface(font);
            headerTitleTV.setTypeface(font);
            createGroupchat.setTypeface(font);
            etFilter.setTypeface(font);
        }catch (NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }



        contactsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.twenty_size));
        groupsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.twenty_size));
        chatsTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.twenty_size));
        createGroupchat.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.twenty_size));



//        notificationCount = (TextView) view.findViewById(R.id.target_view);
//        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);
//        if(new UserNotification().getBatchcount()==0)
//        {
//            notificationCount.setVisibility(View.INVISIBLE);
//        }
//        else {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));
//
//                }
//            });
//        }
//        userNotificationView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Tools.callDialog(getActivity());
//            }
//        });
    }



    public void DisplaySideIndex(ViewGroup group){

//        indexLayout = (LinearLayout) view.findViewById(R.id.side_index);
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView sideTextView = new TextView(group.getContext());
        sideTextView.setBackgroundColor(Color.WHITE);
        sideTextView.setText("side index");
        if(view!=null){
            indexLayout.addView(sideTextView,lparams);
        }




    }



//  changed 45


//    private void displayIndex(ViewGroup container) {
//
//        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//

//        indexLayout = (LinearLayout)view.findViewById(R.id.side_index);
//        String []alphabet = getActivity().getResources().getStringArray(R.array.alphabet_list);
//        for(int i = 0;i<alphabet.length;i++){
//
//            TextView sideTextView = new TextView(container.getContext());
//            sideTextView.setText(alphabet[i]);
//            sideTextView.setTag(alphabet[i]);
//            indexLayout.addView(sideTextView,lparams);
//            sideTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    quickScroll(v);
//                }
//            });
//
//        }
//
//    }

    //     end 45
    private void getIndexList(String[] fruits) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < fruits.length; i++) {
            String fruit = fruits[i];
            String index = fruit.substring(0, 1);

            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }

    public void quickScroll(View v) {
        String alphabet = (String)v.getTag();
        //find the index of the separator row view
        int pos = 0;
        for(Item data : items){

            if(data.isSection()){
                SectionItem section =(SectionItem) data;
                Log.i("alphabet pos", String.valueOf(data.getTitle()));

                if(section.getTitle().matches(alphabet)){
                    pos = items.indexOf(section);
                    Log.i(" pos", String.valueOf(pos));

                }

            }

        }

        lvFriend.setSelection(pos);
    }

    private void setListener() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerRL.setVisibility(View.GONE);
                searchRL.setVisibility(View.VISIBLE);

                searchBackButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchRL.setVisibility(View.GONE);
                        headerRL.setVisibility(View.VISIBLE);

                    }
                });
                searchCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etFilter.getText().clear();
                    }
                });

            }
        });
        contactsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createGroupchat.setVisibility(View.VISIBLE);
                lvFriend.setVisibility(View.VISIBLE);
                chatListLV.setVisibility(View.GONE);
                groupChatListLV.setVisibility(View.GONE);
                contactsTV.setBackgroundColor(getResources().getColor(R.color.blue));
                contactsTV.setTextColor(getResources().getColor(R.color.white));
                groupsTV.setBackgroundColor(getResources().getColor(R.color.white));
                groupsTV.setTextColor(getResources().getColor(R.color.blue));
                chatsTv.setBackgroundColor(getResources().getColor(R.color.white));
                chatsTv.setTextColor(getResources().getColor(R.color.blue));

            }
        });
        chatsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createGroupchat.setVisibility(View.GONE);

                lvFriend.setVisibility(View.GONE);
                chatListLV.setVisibility(View.VISIBLE);
                groupChatListLV.setVisibility(View.GONE);
                rootRL.setBackground(getResources().getDrawable(R.drawable.friends_list_shape));


                chatsTv.setBackgroundColor(getResources().getColor(R.color.blue));
                chatsTv.setTextColor(getResources().getColor(R.color.white));
                groupsTV.setBackgroundColor(getResources().getColor(R.color.white));
                groupsTV.setTextColor(getResources().getColor(R.color.blue));
                contactsTV.setBackgroundColor(getResources().getColor(R.color.white));
                contactsTV.setTextColor(getResources().getColor(R.color.blue));

                loadData();
            }
        });

        groupsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createGroupchat.setVisibility(View.GONE);
                chatListLV.setVisibility(View.GONE);
                lvFriend.setVisibility(View.GONE);
                groupChatListLV.setVisibility(View.VISIBLE);

                groupsTV.setBackgroundColor(getResources().getColor(R.color.blue));
                groupsTV.setTextColor(getResources().getColor(R.color.white));
                chatsTv.setBackgroundColor(getResources().getColor(R.color.white));
                chatsTv.setTextColor(getResources().getColor(R.color.blue));
                contactsTV.setBackgroundColor(getResources().getColor(R.color.white));
                contactsTV.setTextColor(getResources().getColor(R.color.blue));

                getGroupList();
            }
        });


        createGroupchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showGroupChatView();
                isCreateGroupChat = Boolean.TRUE;


            }
        });

        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);

            }
        });

        groupChatListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //Intent i = new Intent(getActivity(), EditProfile.class);
                //getActivity().startActivity(i);

                Log.e("CLICKED","CLICEKD");


                try{

                    ImageView openChat = (ImageView)view.findViewById(R.id.bChat);

                    ImageView phoneChat = (ImageView)view.findViewById(R.id.bCall);

                    phoneChat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            redirectVideoScreen(groupList.get(position));
                        }
                    });

                    openChat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(getActivity(), Chat.class);
                            i.putExtra("id", groupList.get(position).getGroupId());
                            i.putExtra("is_group",Boolean.TRUE);
                            i.putExtra("groupName",groupList.get(position).getGroupName());

                            i.putStringArrayListExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(groupList.get(position).getCurrentchannel()));

                            i.putExtra("currentChannel",groupList.get(position).getCurrentchannel());

//                            set channels here



                            getActivity().startActivity(i);


                        }
                    });



                }catch (NullPointerException exception){
                    Log.e("Null pointer Exception" , exception.getMessage());
                }




            }
        });
    }

    public void redirectVideoScreen(WISChat data){

        Tools.updateUserNotification(getActivity(),"Groupe_video", String.valueOf(data.getGroupId()));

        Intent groupVideoCall = new Intent(getActivity(), GroupCallMainActivity.class);

        groupVideoCall.putExtra("id", data.getGroupId());
        groupVideoCall.putExtra("receiver_name",data.getGroupName());
        groupVideoCall.putExtra("image", data.getGroupIcon());
        groupVideoCall.putExtra("actual_channel", data.getCurrentchannel());

        ApplicationController.getInstance().setUserCurrentChannel(data.getCurrentchannel());

        groupVideoCall.putExtra("is_group", Boolean.TRUE);
        groupVideoCall.putExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(data.getCurrentchannel()));
        groupVideoCall.putExtra("senderName",data.getGroupName());

        String mememberArray = ChatApi.getInstance().getGroupMembersBychannel(data.getCurrentchannel()).get(0);


        Log.d("Current group channel",data.getCurrentchannel());

        Log.d("Current group dict", String.valueOf(ChatApi.getInstance().getGroupMembersDetails()));

        Log.d("Current members_details",ChatApi.getInstance().getGroupMembersBychannel(data.getCurrentchannel()).get(0));


        String[] stringArray = new String[] {mememberArray};

        groupVideoCall.putExtra("members_details",stringArray);

        groupVideoCall.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(groupVideoCall);
    }

    private void loadFriends() {
        friendList = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        System.out.println("response  on loadfriends" +response);

                        try {

                            if (response.getString("data").contains("Acun amis"))
                                Toast.makeText(getActivity(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
                            else {
                                if (response.getString("result").equals("true")) {
                                    JSONArray data = response.getJSONArray("data");
                                    Log.i("contacts list", String.valueOf(response));

                                    for (int i = 0; i <data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
                                            userActive= String.valueOf(obj.getString("activeNewsFeed"));
                                            Log.i("contacts list elemts1", userActive);
//                                              ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
//
//                                            UserAvilable(userActive, activeStateIV);
//                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
                                            friendList.add(new WISUser(isCreateGroupChat,obj.getString("blocked_by"),obj.getString("objectId"),obj.getString("activeNewsFeed"),obj.getInt("idprofile"), obj.getString("fullName"),obj.getString("photo"), obj.getInt("nbr_amis")));

//                                            if(userActive.equals("1")){
//
//                                               ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
//                                                activeStateIV.setBackground(getResources().getDrawable(R.drawable.green_label));
//
//                                            }
//                                            else
//                                            {
//                                                ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
//                                                activeStateIV.setBackground(getResources().getDrawable(R.drawable.lightred_label));
//                                            }

                                        } catch (Exception e) {

                                        }
//

                                    }




                                    Map<String, List<WISUser>> map = new HashMap<String, List<WISUser>>();

                                    for (WISUser student : friendList) {
                                        String key  = student.getFullName().substring(0,1).toUpperCase();
                                        if(map.containsKey(key)){
                                            List<WISUser> list = map.get(key);
                                            list.add(student);

                                        }else{
                                            List<WISUser> list = new ArrayList<WISUser>();
                                            list.add(student);
                                            map.put(key, list);
                                        }

                                    }

                                    Map<String, List<WISUser>> orderMap = new TreeMap<String,List<WISUser>>(map);


                                    Log.i("keys and values", String.valueOf(orderMap));

                                    Set<String> keyValues = orderMap.keySet();
                                    String [] arraykeys = keyValues.toArray(new String[keyValues.size()]);
                                    items = new ArrayList();

                                    for(int i =0;i<orderMap.size();i++){

//

                                        if(orderMap.containsKey(arraykeys[i])){
                                            String k = arraykeys[i];
//                                            header
                                            items.add(new SectionItem(k));




//                                            items

                                            for (WISUser user:map.get(k)) {

                                                items.add(new EntryItem(user));
                                            }



                                        }

                                    }


                                    Log.i("hash map key value", String.valueOf(items));

                                    WisAmisAdapter adapter = new WisAmisAdapter(getActivity(),items,arraykeys,orderMap);
                                    lvFriend.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    setUPFilterConfig(adapter);


                                }
                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();
                            //if (getActivity() != null)
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);
    }
    private void setUPFilterConfig(final WisAmisAdapter adapter){

        try{
            etFilter.addTextChangedListener(new TextWatcherAdapter(){
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    super.beforeTextChanged(s, start, count, after);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    super.onTextChanged(s, start, before, count);
                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);

                }
            });

        }catch(NullPointerException ex){
            System.out.println("Null pointer exception" +ex.getMessage());
        }

    }



    private void setUPFilterConfig(final GroupListAdapter adapter){
        etFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }



    private void setUPFilterConfig(final DiscuAdapter adapter){
        try{
            etFilter.addTextChangedListener(new TextWatcherAdapter(){
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    super.beforeTextChanged(s, start, count, after);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    super.onTextChanged(s, start, before, count);

                    adapter.getFilter().filter(s.toString());

                }

                @Override
                public void afterTextChanged(Editable s) {
                    super.afterTextChanged(s);

                }
            });
        }catch(NullPointerException exce){
            Log.e("Exce",exce.getMessage());
        }

    }

    private void loadData() {

        showLoader();


        userList = new ArrayList<>();
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDisc = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.alldiscution_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                        dismissLoader();

                        try {
                            if (response.getString("Message").contains("any historique")) {
                                Toast.makeText(getActivity(), getString(R.string.no_chat_historique), Toast.LENGTH_SHORT).show();
                            } else if (response.getString("result").equals("true")) {
                                chatList = new ArrayList<>();
                                JSONArray data = response.getJSONArray("data");

                                System.out.println("channel response");

                                Log.i("resp", String.valueOf(response));

                                JSONArray privateJsonArray = new JSONArray();

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        JSONObject oDisc = obj.getJSONObject("discution");

                                        //  get channel

                                        String privatechannel = "Private_".concat(obj.getString("channels"));

                                        privateJsonArray.put(i,privatechannel);

                                        JSONObject oFriend = obj.getJSONObject("amis");

                                        String firstname=oFriend.getString("firstname");
                                        System.out.println("firstname****" +firstname);


//                                        String Firstname =oFriend.getString("firstname");
//                                        String anis_id=oFriend.getString("lastname");
//                                        String photo=oFriend.getString("photo");
//                                        Date d=new Date();
//                                        Date now = new Date();

//                                        override date and time
                                        Date date=new Date();
                                        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        sourceFormat.format(date);
                                        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getActivity(),"timezone"));
                                        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        destFormat.setTimeZone(tz);
                                        String userTimeZoneFormateddate = destFormat.format(date);
                                        Log.i("resilt timezone ",userTimeZoneFormateddate);


                                        String created_at= String.valueOf(DateUtils.getRelativeTimeSpanString(getDateInMillis(oDisc.getString("created_at")),getDateInMillis(userTimeZoneFormateddate), 0));


                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date d1 = null;
                                        try {
                                            d1 = format.parse(oDisc.getString("created_at"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("chate data",created_at);

                                        chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami"),privatechannel));
                                    } catch (Exception e) {
                                        String x = "";
                                    }
                                }
                                discuAdapter =new DiscuAdapter(getActivity(),chatList);
                                chatListLV.setAdapter(discuAdapter);
                                setUPFilterConfig(discuAdapter);
//                                chatListLV.setAdapter(new DiscuAdapter(getActivity(),chatList));
                                Log.i("private", String.valueOf(privateJsonArray));
                                ChatApi.getInstance().removePrivatechannels();
                                Tools.getPrivateChannel(privateJsonArray);
                            }

                        } catch (JSONException e) {
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            dismissLoader();
                            Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                dismissLoader();
                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

//        getGroupList();


        ApplicationController.getInstance().addToRequestQueue(reqDisc);
    }

    public static long getDateInMillis(String srcDate) {

        Log.i("src date",srcDate);
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return 0;
    }


    public void getGroupList(){
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        showLoader();

        JSONObject jsonBody = null;
        try {


            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest getGroups = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.get_groupList), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            dismissLoader();

                            if (response.getString("result").equals("true")) {

                                Log.i("group chat", String.valueOf(response));
//                                view.findViewById(R.id.loading).setVisibility(View.GONE);
                                JSONArray data = response.getJSONArray("data");

                                ArrayList<List> groupchannelMember = new ArrayList<>();

                                ArrayList <String> publicid = new ArrayList();

                                groupList = new ArrayList<>();

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);

                                        String publicId =  "Public_".concat(obj.getString("group_id"));

                                        publicid.add(publicId);

                                        JSONArray groupMember = obj.getJSONArray("channel_group_members");

                                        JSONArray groupMemberDetails = obj.getJSONArray("membersDetails");

                                        System.out.println("group meber");

                                        Log.i("group count", String.valueOf(groupMember.length()));

                                        Log.i("group members", String.valueOf(groupMember));

                                        Log.i("groupMemberDetails", String.valueOf(groupMemberDetails));

                                        ChatApi.getInstance().setGroupMembersDetails(publicId,groupMemberDetails);

                                        Log.i("parsinng pMemberDetails", String.valueOf(ChatApi.getInstance().getGroupMembersBychannel(publicId)));


                                        List<String> memberlist = new ArrayList<String>();
                                        for (int m=0; m<groupMember.length(); m++) {
                                            memberlist.add(groupMember.getString(m) );
                                        }

                                        groupchannelMember.add(memberlist);

                                        String lastMessage = "",created_at = "";
                                        JSONObject message = null;
                                        if(obj.optJSONObject("message")!=null){
                                            message =obj.getJSONObject("message");
                                            if(message!=null){
                                                lastMessage = message.getString("message");
                                                created_at = message.getString("created_at");
                                            }

                                        }

                                        Log.i("group chat date",created_at);
//                                            chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami")));

                                        String currentchannel = "Public_".concat(String.valueOf(obj.getInt("group_id")));
                                        groupList.add(new WISChat(true,obj.getInt("group_id"),obj.getString("group_name"),obj.getString("photo"),obj.getInt("number_of_members"),lastMessage,created_at,currentchannel));

//
                                    } catch (Exception e) {

                                        dismissLoader();
                                        Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

                                    }
                                }

                                Log.i("group chat size", String.valueOf(groupList.size()));
//                                    //is exist
                                groupListAdapter =new GroupListAdapter(getActivity(),groupList);
                                groupChatListLV.setAdapter(groupListAdapter);
                                setUPFilterConfig(groupListAdapter);


                                Log.i("gorup", String.valueOf(groupchannelMember));

                                Log.i("group", String.valueOf(publicid));


                                try{
                                    ChatApi.getInstance().removePublicchannels();

                                    ChatApi.getInstance().addPublicChannels(groupchannelMember,publicid);
                                }catch (IndexOutOfBoundsException ex){
                                    System.out.println("exce" +ex.getMessage());
                                }

                            }



//                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

                            dismissLoader();
                            Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissLoader();
                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(getGroups);

    }


    public  void UserAvilable(String userActive, ImageView imageView){

        if(this.userActive.equals("1")){
//
//            ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
//            activeStateIV.setBackground(getResources().getDrawable(R.drawable.green_label));
            imageView.setBackground(getResources().getDrawable(R.drawable.green_label));
            //helotell ok va?

            // hey y u acess image view here ma? with out accessing how u get de
//            u pass param d wait
            //here u set color correct s ok this id pass fragment? xml file inside aa? no n
//                                                activeStateIV this one u access onther fragment this one fragment only., ok ok  where u have prblm , my problem is how can access the textview to fragment , getactivity calling na not working ,getContext not working
//                                                    u pass textview via intent and 2 page u get after u casting into textview.intent i am calling on adapter aa? yes y? y means what i tell ma?


        }
        else
        {
            imageView.setBackground(getResources().getDrawable(R.drawable.lightred_label));

        }

    }

    private void loadFFriends() {
        Tools.hideKeyboard(getActivity());

        friendList = new ArrayList<>();


        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"filtre\":\"" + etFilter.getText().toString() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.ffriends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
//                                        friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName")," ", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));

                                        friendList.add(new WISUser(isCreateGroupChat,obj.getString("blocked_by"),obj.getString("objectId"),obj.getString("activeNewsFeed"),obj.getInt("idprofile"), obj.getString("fullName"),obj.getString("photo"), obj.getInt("nbr_amis")));
                                    } catch (Exception e) {

                                    }
                                }
                                lvFriend.setAdapter(new FriendsAdapter(getActivity(), friendList));


                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //if (getActivity() != null)
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);
    }

    @Override
    public void onPause() {
        super.onPause();
        new UserNotification().setCREATEGROUPCHAT(Boolean.FALSE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    private void showcontactAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);

        final AlertDialog alert = builder.create();
//        Button cancelalert=(Button)alert.findViewById(R.id.cancel_alert);
//        Button acceptalert=(Button)alert.findViewById(R.id.accept_alert);
//        TextView alertMessage=(TextView)alert.findViewById(R.id.alertMessage);
//        cancelalert.setTypeface(font);
//        acceptalert.setTypeface(font);
//        alertMessage.setTypeface(font);

        alert.show();


        try{
            Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            ((TextView) alert.findViewById(R.id.cancel_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.accept_alert)).setTypeface(font);
            ((TextView)alert.findViewById(R.id.alertMessage)).setTypeface(font);
        }catch (NullPointerException ex){

        }

        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFriends();
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("dialogShown", true);
                editor.commit();
                alert.dismiss();
            }
        });
    }

    public void showLoader(){

        pd = new ProgressDialog(getActivity());
        pd.setMessage("loading");
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){

            pd.dismiss();
        }
    }



}

//
//package com.oec.wis.fragments;
//
//import android.app.Activity;
//import android.app.Fragment;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Environment;
//import android.provider.MediaStore;
//import android.provider.Settings;
//import android.support.annotation.FloatRange;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.AlertDialog;
//import android.text.Editable;
//import android.text.format.DateUtils;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.InflateException;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.widget.AdapterView;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.github.siyamed.shapeimageview.CircularImageView;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.R;
//import com.oec.wis.adapters.DiscuAdapter;
//import com.oec.wis.adapters.FriendsAdapter;
//import com.oec.wis.adapters.GroupListAdapter;
//import com.oec.wis.adapters.Popupadapter;
//import com.oec.wis.adapters.SelectFriendsAdapter;
//import com.oec.wis.adapters.WisAmisAdapter;
//import com.oec.wis.chatApi.ChatApi;
//import com.oec.wis.dialogs.Chat;
//import com.oec.wis.dialogs.MapsActivity;
//import com.oec.wis.entities.EntryItem;
//import com.oec.wis.entities.Item;
//import com.oec.wis.entities.Popupelements;
//import com.oec.wis.entities.SectionItem;
//import com.oec.wis.entities.UserNotification;
//import com.oec.wis.entities.WISChat;
//import com.oec.wis.entities.WISUser;
//
//import com.oec.wis.groupcall.GroupCallMainActivity;
//import com.oec.wis.tools.TextWatcherAdapter;
//import com.oec.wis.tools.Tools;
//import com.orhanobut.dialogplus.DialogPlus;
//import com.orhanobut.dialogplus.OnItemClickListener;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import java.util.Set;
//import java.util.TimeZone;
//import java.util.TreeMap;
//
//import static android.view.Gravity.START;
//
//public class FragFriends extends Fragment {
//    View view;
//    List<WISUser> friendList;
//    ListView lvFriend,groupChatFriendList;
//    EditText etFilter;
//    public static final String PREFS_NAME = "CustomPermission";
//    SharedPreferences settings;
//    ArrayList<String>contacts;
//    FriendsAdapter friendsAdapter;
//    ImageView toogleBack;
//    Boolean isCreateGroupChat;
//    Button createGroupchat;
//    EditText groupName,searchFilter;
//    TextView createGroupTV;
//    Button next;
//    ImageButton close;
//    ImageView groupIcon;
//    LinkedHashMap mapIndex;
//    LinearLayout indexLayout;
//    ArrayList <Item>items;
//    SelectFriendsAdapter selectFriendsAdapter;
//    DialogPlus dialog;
//    static final int REQUEST_CAMERA = 2;
//    String imagePath;
//    List<WISUser> userList;
//    List<WISChat> chatList,groupList;
//
//    ListView lvChat,chatListLV,groupChatListLV;
//
//    TextView contactsTV,groupsTV,chatsTv,headerTitleTV;
//    GroupListAdapter groupListAdapter;
//    ImageView activeStateIV;
//    String userActive;
//    RelativeLayout rootRL;
//    ImageButton searchButton;
//    RelativeLayout searchRL,headerRL;
//    ImageView searchCloseButton;
//    Button searchBackButton;
//    DiscuAdapter discuAdapter;
//    ProgressDialog pd;
////    CircularImageView userNotificationView;
////    TextView notificationCount;
//
//    ViewGroup add_container;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//
//        if (view != null) {
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null)
//                parent.removeView(view);
//        }
//        try {
//            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
//            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));
//
//            view = inflater.inflate(R.layout.frag_friend_list, container, false);
//            settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
//            boolean dialogShown = settings.getBoolean("dialogShown", false);
//
//
//            add_container = container;
//
//
//
//            if (!dialogShown) {
//                // AlertDialog code here
//                showcontactAlert();
//
//            }else{
//                loadFriends();
//
//
//            }
//            initControls();
//            setListener();
//
//
//
//        } catch (InflateException e) {
//        }
//        return view;
//    }
//
//
//
//
//
//
//    private void showGroupChatView(){
//
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.group_chat_view, null);
//
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setCancelable(false).setView(dialogView);
//
//        final AlertDialog alert = builder.create();
//        alert.show();
//
//        groupIcon = (ImageView)alert.findViewById(R.id.groupchatIcon);
//        groupName = (EditText)dialogView.findViewById(R.id.groupName);
//        next = (Button)alert.findViewById(R.id.bNext);
//        close = (ImageButton)alert.findViewById(R.id.closeGroupchat);
//        groupChatFriendList = (ListView)alert.findViewById(R.id.groupfriendList);
//        searchFilter = (EditText)alert.findViewById(R.id.searchFilter);
//        createGroupTV=(TextView)alert.findViewById(R.id.createGroupTV);
//
//
//
//        loadFriendsList(groupChatFriendList);
//
//
//        try{
//            Typeface  typeface =Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
//            groupName.setTypeface(typeface);
//            searchFilter.setTypeface(typeface);
//            createGroupTV.setTypeface(typeface);
//            next.setTypeface(typeface);
//
//        }catch (NullPointerException ex)
//        {
//            Log.d("Excep" ,ex.getMessage());
//        }
//
//        dialogView.findViewById(R.id.groupName).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                alert.dismiss();
////                openMedia();
//
//
//
//
//
//
//            }
//
//        });
//
//
//
//        groupIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openMedia();
//
//            }
//        });
//
//
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alert.dismiss();
//            }
//        });
//
//
//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(!groupName.getText().toString().equals("")){
//                    alert.dismiss();
//
//                    selectFriendsAdapter.createGroupchat(groupName.getText().toString(),imagePath);
//                }else{
//
//                    Toast.makeText(getActivity(), getString(R.string.groupchatmessage), Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });
//
//    }
//
//
//
//    public void openMedia() {
//
//        final CharSequence[] items = { "Take Photo", "Choose from Library",
//                "Cancel" };
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
////                boolean result=Utility.checkPermission(MainActivity.this);
//                if (items[item].equals("Take Photo")) {
////                    userChoosenTask="Take Photo";
//
//                    cameraIntent();
//                } else if (items[item].equals("Choose from Library")) {
////                    userChoosenTask="Choose from Library";
//
//                    galleryIntent();
//                } else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//
//
//
//
//    }
//
//    private void  galleryIntent() {
//
//        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("image/*");
//        startActivityForResult(photoPickerIntent, 1);
//    }
//
//
//    private void cameraIntent()
//    {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, REQUEST_CAMERA);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
//        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
//
//        Log.i("showImageLibrary", "showImageLibrary");
//
//
//        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
//
//            Uri uri = imageReturnedIntent.getData();
//
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                // Log.d(TAG, String.valueOf(bitmap));
//
//                groupIcon.setImageBitmap(bitmap);
//
//                final String[] proj = {MediaStore.Images.Media.DATA};
//                final Cursor cursor = getActivity().managedQuery(uri, proj, null, null,
//                        null);
//                final int column_index = cursor
//                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                cursor.moveToLast();
//                imagePath = cursor.getString(column_index);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        else if(requestCode == 2 && resultCode == Activity.RESULT_OK){
//            Bitmap thumbnail = (Bitmap)imageReturnedIntent.getExtras().get("data");
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//            File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
//            FileOutputStream fo;
//            try {
//                destination.createNewFile();
//                fo = new FileOutputStream(destination);
//                fo.write(bytes.toByteArray());
//                fo.close();
//            }catch (FileNotFoundException e){
//                e.printStackTrace();
//            }catch (IOException e){
//                e.printStackTrace();
//            }
//            groupIcon.setImageBitmap(thumbnail);
//            System.out.println("Welcome"+destination);
//            //new DoUpload().execute();
//            imagePath= String.valueOf(destination);
//        }
//    }
//
//
//    private void loadFriendsList(ListView groupChatFriendList){
//
//        selectFriendsAdapter = new SelectFriendsAdapter(getActivity(),friendList);
//        groupChatFriendList.setAdapter(selectFriendsAdapter);
//        selectFriendsAdapter.notifyDataSetChanged();
//        loadFilterConfig(selectFriendsAdapter);
//    }
//
//    private void loadFilterConfig(final SelectFriendsAdapter adapter){
//        searchFilter.addTextChangedListener(new TextWatcherAdapter(){
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                super.beforeTextChanged(s, start, count, after);
//            }
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                super.onTextChanged(s, start, before, count);
//                adapter.getFilter().filter(s.toString());
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//                super.afterTextChanged(s);
//
//            }
//        });
//    }
//
//    private void initControls() {
//        lvFriend = (ListView) view.findViewById(R.id.friendList);
//        lvFriend.setVisibility(View.VISIBLE);
//        indexLayout = (LinearLayout)view.findViewById(R.id.side_index);
//        indexLayout.setVisibility(View.VISIBLE);
//        displayIndex(add_container);
//        getIndexList();
//        etFilter = (EditText) view.findViewById(R.id.etFilter);
//        toogleBack = (ImageView)view.findViewById(R.id.toggle);
//        createGroupchat = (Button)view.findViewById(R.id.groupChatBtn);
//        chatsTv=(TextView)view.findViewById(R.id.chatsTV);
//        groupsTV=(TextView)view.findViewById(R.id.groupsTV);
//        contactsTV=(TextView)view.findViewById(R.id.contactsTV);
//        chatListLV=(ListView)view.findViewById(R.id.chatListLV);
//        groupChatListLV=(ListView)view.findViewById(R.id.groupChatListLV);
//        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
//        rootRL=(RelativeLayout)view.findViewById(R.id.rootRL);
//        searchButton =(ImageButton) view.findViewById(R.id.searchButton);
//
//        searchRL=(RelativeLayout)view.findViewById(R.id.searchRL);
//        headerRL=(RelativeLayout)view.findViewById(R.id.headerRL);
//
//        searchBackButton=(Button) view.findViewById(R.id.searchBackButton);
//        searchCloseButton =(ImageView) view.findViewById(R.id.searchCloseButton);
//
//        try {
//            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
//            contactsTV.setTypeface(font);
//            groupsTV.setTypeface(font);
//            chatsTv.setTypeface(font);
//            headerTitleTV.setTypeface(font);
//            createGroupchat.setTypeface(font);
//            etFilter.setTypeface(font);
//        }catch (NullPointerException ex){
//            System.out.println("Exception" +ex.getMessage());
//        }
//
//
//
//        contactsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
//                getResources().getDimension(R.dimen.twenty_size));
//        groupsTV.setTextSize(TypedValue.COMPLEX_UNIT_PX,
//                getResources().getDimension(R.dimen.twenty_size));
//        chatsTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
//                getResources().getDimension(R.dimen.twenty_size));
//        createGroupchat.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.twenty_size));
//
//
//
////        notificationCount = (TextView) view.findViewById(R.id.target_view);
////        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);
////        if(new UserNotification().getBatchcount()==0)
////        {
////            notificationCount.setVisibility(View.INVISIBLE);
////        }
////        else {
////            getActivity().runOnUiThread(new Runnable() {
////                @Override
////                public void run() {
////                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));
////
////                }
////            });
////        }
////        userNotificationView.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////
////                Tools.callDialog(getActivity());
////            }
////        });
//    }
//
//
//
//    private void getIndexList() {
//        String [] alphabet = getActivity().getResources().getStringArray(R.array.alphabet_list);
//        mapIndex = new LinkedHashMap();
//        for (int i = 0; i < alphabet.length; i++) {
//            String fruit = alphabet[i];
//            String index = fruit.substring(0, 1);
//
//            if (mapIndex.get(index) == null)
//                mapIndex.put(index, i);
//        }
//    }
//
//    private void displayIndex(ViewGroup container) {
//
//        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        String []alphabet = getActivity().getResources().getStringArray(R.array.alphabet_list);
//        for(int i = 0;i<alphabet.length;i++){
//
//            TextView sideTextView = new TextView(container.getContext());
//            sideTextView.setText(alphabet[i]);
//            sideTextView.setTag(alphabet[i]);
//            sideTextView.setTextColor(Color.BLUE);
//            indexLayout.addView(sideTextView,lparams);
//            sideTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    quickScroll(v);
//                }
//            });
//
//        }
//
//    }
//
//
//    public void quickScroll(View v) {
//        String alphabet = (String)v.getTag();
//        //find the index of the separator row view
//        int pos = 0;
//        for(Item data : items){
//
//            if(data.isSection()){
//                SectionItem section =(SectionItem) data;
//                Log.i("alphabet pos", String.valueOf(data.getTitle()));
//
//                if(section.getTitle().matches(alphabet)){
//                    pos = items.indexOf(section);
//                    Log.i(" pos", String.valueOf(pos));
//
//                }
//
//            }
//
//        }
//
//        lvFriend.setSelection(pos);
//    }
//
//    private void setListener() {
//        searchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                headerRL.setVisibility(View.GONE);
//                searchRL.setVisibility(View.VISIBLE);
//
//                searchBackButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        searchRL.setVisibility(View.GONE);
//                        headerRL.setVisibility(View.VISIBLE);
//
//                    }
//                });
//                searchCloseButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        etFilter.getText().clear();
//                    }
//                });
//
//            }
//        });
//        contactsTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                createGroupchat.setVisibility(View.VISIBLE);
//                lvFriend.setVisibility(View.VISIBLE);
//                indexLayout.setVisibility(View.VISIBLE);
//                groupChatListLV.setVisibility(View.GONE);
//                contactsTV.setBackgroundColor(getResources().getColor(R.color.blue));
//                contactsTV.setTextColor(getResources().getColor(R.color.white));
//                groupsTV.setBackgroundColor(getResources().getColor(R.color.white));
//                groupsTV.setTextColor(getResources().getColor(R.color.blue));
//                chatsTv.setBackgroundColor(getResources().getColor(R.color.white));
//                chatsTv.setTextColor(getResources().getColor(R.color.blue));
//                getIndexList();
//                displayIndex(add_container);
//
//            }
//        });
//        chatsTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                createGroupchat.setVisibility(View.GONE);
//
//                lvFriend.setVisibility(View.GONE);
//                indexLayout.setVisibility(View.GONE);
//                chatListLV.setVisibility(View.VISIBLE);
//                groupChatListLV.setVisibility(View.GONE);
//                rootRL.setBackground(getResources().getDrawable(R.drawable.friends_list_shape));
//
//
//                chatsTv.setBackgroundColor(getResources().getColor(R.color.blue));
//                chatsTv.setTextColor(getResources().getColor(R.color.white));
//                groupsTV.setBackgroundColor(getResources().getColor(R.color.white));
//                groupsTV.setTextColor(getResources().getColor(R.color.blue));
//                contactsTV.setBackgroundColor(getResources().getColor(R.color.white));
//                contactsTV.setTextColor(getResources().getColor(R.color.blue));
//
//                loadData();
//            }
//        });
//
//        groupsTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                createGroupchat.setVisibility(View.GONE);
//                chatListLV.setVisibility(View.GONE);
//                lvFriend.setVisibility(View.GONE);
//                indexLayout.setVisibility(View.GONE);
//                groupChatListLV.setVisibility(View.VISIBLE);
//
//                groupsTV.setBackgroundColor(getResources().getColor(R.color.blue));
//                groupsTV.setTextColor(getResources().getColor(R.color.white));
//                chatsTv.setBackgroundColor(getResources().getColor(R.color.white));
//                chatsTv.setTextColor(getResources().getColor(R.color.blue));
//                contactsTV.setBackgroundColor(getResources().getColor(R.color.white));
//                contactsTV.setTextColor(getResources().getColor(R.color.blue));
//
//                getGroupList();
//            }
//        });
//
//
//        createGroupchat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                showGroupChatView();
//                isCreateGroupChat = Boolean.TRUE;
//
//
//            }
//        });
//
//        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
//
//            }
//        });
//
//
//        chatListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                Intent i = new Intent(getActivity(), Chat.class);
//                i.putExtra("id", chatList.get(position).getUser().getId());
//                i.putExtra("name",chatList.get(position).getUser().getFirstName());
//                i.putExtra("image",chatList.get(position).getUser().getPic());
//                i.putExtra("is_group",Boolean.FALSE);
//                i.putExtra("currentChannel",chatList.get(position).getCurrentchannel());
//                getActivity().startActivity(i);
//            }
//        });
//        groupChatListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                //Intent i = new Intent(getActivity(), EditProfile.class);
//                //getActivity().startActivity(i);
//
//                Log.e("CLICKED","CLICEKD");
//
//
//                try{
//
//                    ImageView openChat = (ImageView)view.findViewById(R.id.bChat);
//
//                    ImageView phoneChat = (ImageView)view.findViewById(R.id.bCall);
//
//                    phoneChat.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            redirectVideoScreen(groupList.get(position));
//                        }
//                    });
//
//                    openChat.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(getActivity(), Chat.class);
//                            i.putExtra("id", groupList.get(position).getGroupId());
//                            i.putExtra("is_group",Boolean.TRUE);
//                            i.putExtra("groupName",groupList.get(position).getGroupName());
//
//                            i.putStringArrayListExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(groupList.get(position).getCurrentchannel()));
//
//                            i.putExtra("currentChannel",groupList.get(position).getCurrentchannel());
//
////                            set channels here
//
//
//
//                            getActivity().startActivity(i);
//
//
//                        }
//                    });
//
//
//
//                }catch (NullPointerException exception){
//                    Log.e("Null pointer Exception" , exception.getMessage());
//                }
//
//
//
//
//            }
//        });
//    }
//
//    public void redirectVideoScreen(WISChat data){
//
//        Tools.updateUserNotification(getActivity(),"Groupe_video", String.valueOf(data.getGroupId()));
//
//        Intent groupVideoCall = new Intent(getActivity(), GroupCallMainActivity.class);
//
//        groupVideoCall.putExtra("id", data.getGroupId());
//        groupVideoCall.putExtra("receiver_name",data.getGroupName());
//        groupVideoCall.putExtra("image", data.getGroupIcon());
//        groupVideoCall.putExtra("actual_channel", data.getCurrentchannel());
//
//        ApplicationController.getInstance().setUserCurrentChannel(data.getCurrentchannel());
//
//        groupVideoCall.putExtra("is_group", Boolean.TRUE);
//        groupVideoCall.putExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(data.getCurrentchannel()));
//        groupVideoCall.putExtra("senderName",data.getGroupName());
//
//        String mememberArray = ChatApi.getInstance().getGroupMembersBychannel(data.getCurrentchannel()).get(0);
//
//
//        Log.d("Current group channel",data.getCurrentchannel());
//
//        Log.d("Current group dict", String.valueOf(ChatApi.getInstance().getGroupMembersDetails()));
//
//        Log.d("Current members_details",ChatApi.getInstance().getGroupMembersBychannel(data.getCurrentchannel()).get(0));
//
//
//        String[] stringArray = new String[] {mememberArray};
//
//        groupVideoCall.putExtra("members_details",stringArray);
//
//        groupVideoCall.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        startActivity(groupVideoCall);
//    }
//
//    private void loadFriends() {
//        friendList = new ArrayList<>();
//
////        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
//        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        System.out.println("response  on loadfriends" +response);
//
//                        try {
//
//                            if (response.getString("data").contains("Acun amis"))
//                                Toast.makeText(getActivity(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
//                            else {
//                                if (response.getString("result").equals("true")) {
//                                    JSONArray data = response.getJSONArray("data");
//                                    Log.i("contacts list", String.valueOf(response));
//
//                                    for (int i = 0; i <data.length(); i++) {
//                                        try {
//                                            JSONObject obj = data.getJSONObject(i);
//                                            Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
//                                            userActive= String.valueOf(obj.getString("activeNewsFeed"));
//                                            Log.i("contacts list elemts1", userActive);
////                                              ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
////
////                                            UserAvilable(userActive, activeStateIV);
////                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
//                                            friendList.add(new WISUser(isCreateGroupChat,obj.getString("blocked_by"),obj.getString("objectId"),obj.getString("activeNewsFeed"),obj.getInt("idprofile"), obj.getString("fullName"),obj.getString("photo"), obj.getInt("nbr_amis")));
//
////                                            if(userActive.equals("1")){
////
////                                               ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
////                                                activeStateIV.setBackground(getResources().getDrawable(R.drawable.green_label));
////
////                                            }
////                                            else
////                                            {
////                                                ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
////                                                activeStateIV.setBackground(getResources().getDrawable(R.drawable.lightred_label));
////                                            }
//
//                                        } catch (Exception e) {
//
//                                        }
////
//
//                                    }
//
//
//
//
//                                    Map<String, List<WISUser>> map = new HashMap<String, List<WISUser>>();
//
//                                    for (WISUser student : friendList) {
//                                        String key  = student.getFullName().substring(0,1).toUpperCase();
//                                        if(map.containsKey(key)){
//                                            List<WISUser> list = map.get(key);
//                                            list.add(student);
//
//                                        }else{
//                                            List<WISUser> list = new ArrayList<WISUser>();
//                                            list.add(student);
//                                            map.put(key, list);
//                                        }
//
//                                    }
//
//                                    Map<String, List<WISUser>> orderMap = new TreeMap<String,List<WISUser>>(map);
//
//
//                                    Log.i("keys and values", String.valueOf(orderMap));
//
//                                    Set<String> keyValues = orderMap.keySet();
//                                    String [] arraykeys = keyValues.toArray(new String[keyValues.size()]);
//                                    items = new ArrayList();
//
//                                    for(int i =0;i<orderMap.size();i++){
//
////
//
//                                        if(orderMap.containsKey(arraykeys[i])){
//                                            String k = arraykeys[i];
////                                            header
//                                            items.add(new SectionItem(k));
//
//
//
//
////                                            items
//
//                                            for (WISUser user:map.get(k)) {
//
//                                                items.add(new EntryItem(user));
//                                            }
//
//
//
//                                        }
//
//                                    }
//
//
//                                    Log.i("hash map key value", String.valueOf(items));
//
//                                    WisAmisAdapter adapter = new WisAmisAdapter(getActivity(),items,arraykeys,orderMap);
//                                    lvFriend.setAdapter(adapter);
//                                    adapter.notifyDataSetChanged();
//                                    setUPFilterConfig(adapter);
//
//
//
//                                }
//                            }
////                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            Tools.dismissLoader();
//
//                        } catch (JSONException e) {
//
////                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            Tools.dismissLoader();
//                            //if (getActivity() != null)
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                Tools.dismissLoader();
//                //if (getActivity() != null)
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(req);
//    }
//    private void setUPFilterConfig(final WisAmisAdapter adapter){
//
//        try{
//            etFilter.addTextChangedListener(new TextWatcherAdapter(){
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    super.beforeTextChanged(s, start, count, after);
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    super.onTextChanged(s, start, before, count);
//                    adapter.getFilter().filter(s.toString());
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    super.afterTextChanged(s);
//
//                }
//            });
//
//        }catch(NullPointerException ex){
//            System.out.println("Null pointer exception" +ex.getMessage());
//        }
//
//    }
//
//
//
//    private void setUPFilterConfig(final GroupListAdapter adapter){
//        etFilter.addTextChangedListener(new TextWatcherAdapter(){
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                super.beforeTextChanged(s, start, count, after);
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                super.onTextChanged(s, start, before, count);
//                adapter.getFilter().filter(s.toString());
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                super.afterTextChanged(s);
//
//            }
//        });
//    }
//
//
//
//    private void setUPFilterConfig(final DiscuAdapter adapter){
//        try{
//            etFilter.addTextChangedListener(new TextWatcherAdapter(){
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    super.beforeTextChanged(s, start, count, after);
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    super.onTextChanged(s, start, before, count);
//
//                    adapter.getFilter().filter(s.toString());
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    super.afterTextChanged(s);
//
//                }
//            });
//        }catch(NullPointerException exce){
//            Log.e("Exce",exce.getMessage());
//        }
//
//    }
//
//    private void loadData() {
//
//        showLoader();
//
//
//        userList = new ArrayList<>();
////        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqDisc = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.alldiscution_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        dismissLoader();
//
//                        try {
//                            if (response.getString("Message").contains("any historique")) {
//                                Toast.makeText(getActivity(), getString(R.string.no_chat_historique), Toast.LENGTH_SHORT).show();
//                            } else if (response.getString("result").equals("true")) {
//                                chatList = new ArrayList<>();
//                                JSONArray data = response.getJSONArray("data");
//
//                                System.out.println("channel response");
//
//                                Log.i("resp", String.valueOf(response));
//
//                                JSONArray privateJsonArray = new JSONArray();
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj = data.getJSONObject(i);
//                                        JSONObject oDisc = obj.getJSONObject("discution");
//
//                                        //  get channel
//
//                                        String privatechannel = "Private_".concat(obj.getString("channels"));
//
//                                        privateJsonArray.put(i,privatechannel);
//
//                                        JSONObject oFriend = obj.getJSONObject("amis");
//
//                                        String firstname=oFriend.getString("firstname");
//                                        System.out.println("firstname****" +firstname);
//
//
////                                        String Firstname =oFriend.getString("firstname");
////                                        String anis_id=oFriend.getString("lastname");
////                                        String photo=oFriend.getString("photo");
////                                        Date d=new Date();
////                                        Date now = new Date();
//
////                                        override date and time
//                                        Date date=new Date();
//                                        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//                                        sourceFormat.format(date);
//                                        Log.i("smiley", String.valueOf(sourceFormat.format(date)));
//
//                                        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getActivity(),"timezone"));
//                                        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                        destFormat.setTimeZone(tz);
//                                        String userTimeZoneFormateddate = destFormat.format(date);
//                                        Log.i("resilt timezone ",userTimeZoneFormateddate);
//
//
//                                        String created_at= String.valueOf(DateUtils.getRelativeTimeSpanString(getDateInMillis(oDisc.getString("created_at")),getDateInMillis(userTimeZoneFormateddate), 0));
//
//
//                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                        Date d1 = null;
//                                        try {
//                                            d1 = format.parse(oDisc.getString("created_at"));
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Log.i("chate data",created_at);
//
//                                        chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami"),privatechannel));
//                                    } catch (Exception e) {
//                                        String x = "";
//                                    }
//                                }
//                                discuAdapter =new DiscuAdapter(getActivity(),chatList);
//                                chatListLV.setAdapter(discuAdapter);
//                                setUPFilterConfig(discuAdapter);
////                                chatListLV.setAdapter(new DiscuAdapter(getActivity(),chatList));
//                                Log.i("private", String.valueOf(privateJsonArray));
//                                ChatApi.getInstance().removePrivatechannels();
//                                Tools.getPrivateChannel(privateJsonArray);
//                            }
//
//                        } catch (JSONException e) {
////                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            dismissLoader();
//                            Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                dismissLoader();
//                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
////        getGroupList();
//
//
//        ApplicationController.getInstance().addToRequestQueue(reqDisc);
//    }
//
//    public static long getDateInMillis(String srcDate) {
//
//        Log.i("src date",srcDate);
//        SimpleDateFormat desiredFormat = new SimpleDateFormat(
//                "yyyy-MM-dd HH:mm:ss");
//
//        long dateInMillis = 0;
//        try {
//            Date date = desiredFormat.parse(srcDate);
//            dateInMillis = date.getTime();
//            return dateInMillis;
//        } catch (ParseException e) {
//
//            e.printStackTrace();
//        }
//
//        return 0;
//    }
//
//
//    public void getGroupList(){
////        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        showLoader();
//
//        JSONObject jsonBody = null;
//        try {
//
//
//            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest getGroups = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.get_groupList), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//
//                            dismissLoader();
//
//                            if (response.getString("result").equals("true")) {
//
//                                Log.i("group chat", String.valueOf(response));
////                                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                                JSONArray data = response.getJSONArray("data");
//
//                                ArrayList<List> groupchannelMember = new ArrayList<>();
//
//                                ArrayList <String> publicid = new ArrayList();
//
//                                groupList = new ArrayList<>();
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj = data.getJSONObject(i);
//
//                                        String publicId =  "Public_".concat(obj.getString("group_id"));
//
//                                        publicid.add(publicId);
//
//                                        JSONArray groupMember = obj.getJSONArray("channel_group_members");
//
//                                        JSONArray groupMemberDetails = obj.getJSONArray("membersDetails");
//
//                                        System.out.println("group meber");
//
//                                        Log.i("group count", String.valueOf(groupMember.length()));
//
//                                        Log.i("group members", String.valueOf(groupMember));
//
//                                        Log.i("groupMemberDetails", String.valueOf(groupMemberDetails));
//
//                                        ChatApi.getInstance().setGroupMembersDetails(publicId,groupMemberDetails);
//
//                                        Log.i("parsinng pMemberDetails", String.valueOf(ChatApi.getInstance().getGroupMembersBychannel(publicId)));
//
//
//                                        List<String> memberlist = new ArrayList<String>();
//                                        for (int m=0; m<groupMember.length(); m++) {
//                                            memberlist.add(groupMember.getString(m) );
//                                        }
//
//                                        groupchannelMember.add(memberlist);
//
//                                        String lastMessage = "",created_at = "";
//                                        JSONObject message = null;
//                                        if(obj.optJSONObject("message")!=null){
//                                            message =obj.getJSONObject("message");
//                                            if(message!=null){
//                                                lastMessage = message.getString("message");
//                                                created_at = message.getString("created_at");
//                                            }
//
//                                        }
//
//                                        Log.i("group chat date",created_at);
////                                            chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami")));
//
//                                        String currentchannel = "Public_".concat(String.valueOf(obj.getInt("group_id")));
//                                        groupList.add(new WISChat(true,obj.getInt("group_id"),obj.getString("group_name"),obj.getString("photo"),obj.getInt("number_of_members"),lastMessage,created_at,currentchannel));
//
////
//                                    } catch (Exception e) {
//
//                                        dismissLoader();
//                                        Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//
//                                    }
//                                }
//
//                                Log.i("group chat size", String.valueOf(groupList.size()));
////                                    //is exist
//                                groupListAdapter =new GroupListAdapter(getActivity(),groupList);
//                                groupChatListLV.setAdapter(groupListAdapter);
//                                setUPFilterConfig(groupListAdapter);
//
//
//                                Log.i("gorup", String.valueOf(groupchannelMember));
//
//                                Log.i("group", String.valueOf(publicid));
//
//
//                                try{
//                                    ChatApi.getInstance().removePublicchannels();
//
//                                    ChatApi.getInstance().addPublicChannels(groupchannelMember,publicid);
//                                }catch (IndexOutOfBoundsException ex){
//                                    System.out.println("exce" +ex.getMessage());
//                                }
//
//                            }
//
//
//
////                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//
//                            dismissLoader();
//                            Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dismissLoader();
//                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(getGroups);
//
//    }
//
//
//    public  void UserAvilable(String userActive, ImageView imageView){
//
//        if(this.userActive.equals("1")){
////
////            ImageView activeStateIV=(ImageView)getView().findViewById(R.id.activeStateIV);
////            activeStateIV.setBackground(getResources().getDrawable(R.drawable.green_label));
//            imageView.setBackground(getResources().getDrawable(R.drawable.green_label));
//            //helotell ok va?
//
//            // hey y u acess image view here ma? with out accessing how u get de
////            u pass param d wait
//            //here u set color correct s ok this id pass fragment? xml file inside aa? no n
////                                                activeStateIV this one u access onther fragment this one fragment only., ok ok  where u have prblm , my problem is how can access the textview to fragment , getactivity calling na not working ,getContext not working
////                                                    u pass textview via intent and 2 page u get after u casting into textview.intent i am calling on adapter aa? yes y? y means what i tell ma?
//
//
//        }
//        else
//        {
//            imageView.setBackground(getResources().getDrawable(R.drawable.lightred_label));
//
//        }
//
//    }
//
//    private void loadFFriends() {
//        Tools.hideKeyboard(getActivity());
//
//        friendList = new ArrayList<>();
//
//
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"filtre\":\"" + etFilter.getText().toString() + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.ffriends_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getString("result").equals("true")) {
//                                JSONArray data = response.getJSONArray("data");
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj = data.getJSONObject(i);
////                                        friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName")," ", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
//
//                                        friendList.add(new WISUser(isCreateGroupChat,obj.getString("blocked_by"),obj.getString("objectId"),obj.getString("activeNewsFeed"),obj.getInt("idprofile"), obj.getString("fullName"),obj.getString("photo"), obj.getInt("nbr_amis")));
//                                    } catch (Exception e) {
//
//                                    }
//                                }
//                                lvFriend.setAdapter(new FriendsAdapter(getActivity(), friendList));
//
//
//                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            //if (getActivity() != null)
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                //if (getActivity() != null)
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(req);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        new UserNotification().setCREATEGROUPCHAT(Boolean.FALSE);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
//        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
//    }
//
//    private void showcontactAlert(){
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setCancelable(false).setView(R.layout.custom_permission_alert);
//
//        final AlertDialog alert = builder.create();
////        Button cancelalert=(Button)alert.findViewById(R.id.cancel_alert);
////        Button acceptalert=(Button)alert.findViewById(R.id.accept_alert);
////        TextView alertMessage=(TextView)alert.findViewById(R.id.alertMessage);
////        cancelalert.setTypeface(font);
////        acceptalert.setTypeface(font);
////        alertMessage.setTypeface(font);
//
//        alert.show();
//
//
//        try{
//            Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
//            ((TextView) alert.findViewById(R.id.cancel_alert)).setTypeface(font);
//            ((TextView)alert.findViewById(R.id.accept_alert)).setTypeface(font);
//            ((TextView)alert.findViewById(R.id.alertMessage)).setTypeface(font);
//        }catch (NullPointerException ex){
//
//        }
//
//        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alert.dismiss();
//            }
//        });
//        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                loadFriends();
//                SharedPreferences.Editor editor = settings.edit();
//                editor.putBoolean("dialogShown", true);
//                editor.commit();
//                alert.dismiss();
//            }
//        });
//    }
//
//    public void showLoader(){
//
//        pd = new ProgressDialog(getActivity());
//        pd.setMessage("loading");
//        pd.show();
//    }
//
//    public void dismissLoader(){
//
//        if(pd!=null){
//
//            pd.dismiss();
//        }
//    }
//
//
//
//}