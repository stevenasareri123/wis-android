package com.oec.wis.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.location.places.GeoDataApi;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.oec.wis.ApplicationController;
//import com.oec.wis.Dashboard;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.adapters.UserLocationListener;
import com.oec.wis.adapters.WisAmisAdapter;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.entities.EntryItem;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.SectionItem;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.oec.wis.tools.UserLocation;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;

public class FragMyLocation extends Fragment implements OnMapReadyCallback {
    public static final String PREFS_NAME = "CustomPermission";
    SharedPreferences settings;
    private static View view;
    LatLng myLocation = null;
    private GoogleMap googleMap;
    Bitmap bitmap;
    Button addMapOption;
    ArrayList popupList;

    Popupadapter simpleAdapter;


    PopupWindow popWindow;

    Context context;
    double lng, lat;
    String address;

    ImageView close_btn;
    Button post_status;
    ImageView snapShot;
    TextView message;

    List<WISUser> friendList;

    ArrayList items;


    ListView groupChatFriendList;

    EditText searchFilter;

    SelectFriendsAdapter selectFriendsAdapter;


    String snapShotFilePath;

    double latitude, langitude;

    String currentLocationName;


    String curretnLocationAdress;

    String coordinates;
    SupportMapFragment mapFragment;

    View currentView;

    @Override
    public void onResume() {
        super.onResume();
        setupMapView();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_my_location, container, false);
            setupMapView();


        } catch (InflateException e) {
        }
        Boolean locationEnabled = Tools.showGPSDialog(getActivity());

        if (locationEnabled) {
            settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
            boolean dialogShown = settings.getBoolean("locationPermissionDialogShown", false);

            if (!dialogShown) {
                // AlertDialog code here
                showMapPermissionAlert();

            } else {
                loadFriends();

//                Log.i("CurrentLoc",curretnLocationAdress);
//                Log.i("CurrentLoc",coordinates);


            }
        }


        currentView = view;

        return view;
    }


    private void setupMapView() {
        try{
            loadFriends();


            addMapOption = (Button) view.findViewById(R.id.addMapOption);

            addMapOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //viewMoreOption();

                    CaptureMapScreen();
                }
            });


            if (UserNotification.getViewType().equals("ProfileViewTwo")) {
                System.out.println("ProfileViewTwo");
                mapFragment = (SupportMapFragment) ((ProfileViewTwo) getActivity()).getSupportFragmentManager()
                        .findFragmentById(R.id.mapFragmentView);
                mapFragment.getMapAsync(this);

            } else if (UserNotification.getViewType().equals("ProfileView")) {
                System.out.println("ProfileView");
                mapFragment = (SupportMapFragment) ((ProfileView) getActivity()).getSupportFragmentManager()
                        .findFragmentById(R.id.mapFragmentView);
                mapFragment.getMapAsync(this);

            } else if (UserNotification.getViewType().equals("DashBoard")) {
                System.out.println("dash board calling==================");
                System.out.println("DashBoard");
                mapFragment = (SupportMapFragment) ((Dashboard) getActivity()).getSupportFragmentManager()
                        .findFragmentById(R.id.mapFragmentView);
                mapFragment.getMapAsync(this);

            } else if (UserNotification.getViewType().equals("PubDetailView")) {
                System.out.println("PubdetailView");
                mapFragment = (SupportMapFragment) ((PubDetails) getActivity()).getSupportFragmentManager()
                        .findFragmentById(R.id.mapFragmentView);
                mapFragment.getMapAsync(this);

            }

        }catch (NullPointerException ex){
            System.out.println("Null pointer exception" +ex);
        }



    }


    private void showMapPermissionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);

        final AlertDialog alert = builder.create();
        alert.show();
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFriends();
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("locationPermissionDialogShown", true);
                editor.commit();
                alert.dismiss();
            }
        });
    }


    public void viewMoreOption(final Bitmap snapImageView) {


        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {


                        if (position == 0) {
                            dialog.dismiss();

                            shareToAmis(view, snapImageView);
//                            CaptureMapScreen();


                        } else if (position == 1) {
                            dialog.dismiss();


                            shareLocationToWisActuality();


                        } else if (position == 2) {
                            dialog.dismiss();
                            Log.i("WIS chat", "chat click");


                        } else if (position == 3) {
                            dialog.dismiss();


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }


    public void setupPopUPView() {

        popupList = new ArrayList<>();

//        popupList.add(new Popupelements(R.drawable.copy_link, getString(R.string.screenShot)));
        popupList.add(new Popupelements(R.drawable.grey_group_icon, getString(R.string.wiscontact)));
        popupList.add(new Popupelements(R.drawable.grey_news, getString(R.string.actuality)));

        simpleAdapter = new Popupadapter(getActivity(), false, popupList);

    }


    public String getMapUrlLink() {


        String locationUrl = curretnLocationAdress.concat(" http://www.google.com/maps/place/".concat(coordinates));


        return locationUrl;
    }


    public void shareToAmis(final View v, Bitmap snapedImage) {
        // StartAction


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.wis_amis_view, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);


        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);


        currentView.setAlpha(0.5f);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                popWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
            }
        });




        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);
        snapShot = (ImageView) inflatedView.findViewById(R.id.snapShot);

        groupChatFriendList = (ListView) inflatedView.findViewById(R.id.groupfriendList);
        searchFilter = (EditText) inflatedView.findViewById(R.id.searchFilter);

        message = (TextView) inflatedView.findViewById(R.id.map_link);

        TextView wispopTV = (TextView) inflatedView.findViewById(R.id.locationTV);

        message.setText(getMapUrlLink());

        currentLocationName = getMapUrlLink();


        Typeface  font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
        message.setTypeface(font);
        message.setTextColor(getResources().getColor(R.color.light_black));
        post_status.setTypeface(font);
        wispopTV.setTypeface(font);


        loadFriendsList(groupChatFriendList);


        snapShot.setImageBitmap(snapedImage);


        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                view.setAlpha(1);
            }
        });


        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!message.getText().toString().equals("")) {
                    popWindow.dismiss();


                    selectFriendsAdapter.transferTextToContacts("map_link", String.valueOf(""), message.getText().toString());

                    view.setAlpha(1);


                }


            }
        });


    }

    private void loadFriendsList(ListView groupChatFriendList) {

        selectFriendsAdapter = new SelectFriendsAdapter(getActivity(), friendList);
        groupChatFriendList.setAdapter(selectFriendsAdapter);
        selectFriendsAdapter.notifyDataSetChanged();
        loadFilterConfig(selectFriendsAdapter);


    }

    private void loadFilterConfig(final SelectFriendsAdapter adapter) {
        searchFilter.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }


    private void loadFriends() {
        friendList = new ArrayList<>();
        try {
            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

            JSONObject jsonBody = null;


            try {
                jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
            JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {

                                if (response.getString("data").contains("Acun amis"))
                                    Toast.makeText(getActivity(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
                                else {
                                    if (response.getString("result").equals("true")) {
                                        JSONArray data = response.getJSONArray("data");
                                        Log.i("contacts list", String.valueOf(response));


                                        for (int i = 0; i < data.length(); i++) {
                                            try {
                                                JSONObject obj = data.getJSONObject(i);
                                                Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
//                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
                                                friendList.add(new WISUser(Boolean.FALSE, obj.getString("blocked_by"), obj.getString("objectId"), obj.getString("activeNewsFeed"), obj.getInt("idprofile"), obj.getString("fullName"), obj.getString("photo"), obj.getInt("nbr_amis")));


                                            } catch (Exception e) {

                                            }
//

                                        }


                                        Map<String, List<WISUser>> map = new HashMap<String, List<WISUser>>();

                                        for (WISUser student : friendList) {
                                            String key = student.getFullName().substring(0, 1).toUpperCase();
                                            if (map.containsKey(key)) {
                                                List<WISUser> list = map.get(key);
                                                list.add(student);

                                            } else {
                                                List<WISUser> list = new ArrayList<WISUser>();
                                                list.add(student);
                                                map.put(key, list);
                                            }

                                        }

                                        Map<String, List<WISUser>> orderMap = new TreeMap<String, List<WISUser>>(map);


                                        Log.i("keys and values", String.valueOf(orderMap));

                                        Set<String> keyValues = orderMap.keySet();
                                        String[] arraykeys = keyValues.toArray(new String[keyValues.size()]);
                                        items = new ArrayList();

                                        for (int i = 0; i < orderMap.size(); i++) {

//

                                            if (orderMap.containsKey(arraykeys[i])) {
                                                String k = arraykeys[i];
//                                            header
                                                items.add(new SectionItem(k));


//                                            items

                                                for (WISUser user : map.get(k)) {

                                                    items.add(new EntryItem(user));
                                                }


                                            }

                                        }


                                        Log.i("hash map key value", String.valueOf(items));

                                        setupPopUPView();


                                    }
                                }
                                view.findViewById(R.id.loading).setVisibility(View.GONE);

                            } catch (JSONException e) {

                                view.findViewById(R.id.loading).setVisibility(View.GONE);
                                //if (getActivity() != null)
                                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    //if (getActivity() != null)
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("token", Tools.getData(getActivity(), "token"));
                    headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                    return headers;
                }
            };

            ApplicationController.getInstance().addToRequestQueue(req);


        } catch (NullPointerException ex) {
            System.out.println("Exception" + ex.getMessage());
        }

    }

    public void getCityName(double latitude, double longitude) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        new UserLocation(getActivity(), new UserLocationListener() {
            @Override
            public void updateUserLocationInfo(String formatted_address) {

                dialog.dismiss();


                curretnLocationAdress = formatted_address;

                Log.e("return response", formatted_address);
            }
        }).execute(String.valueOf(latitude), String.valueOf(longitude));
//        Geocoder geocoder;
//        List<android.location.Address> addresses;
//        geocoder = new Geocoder(getActivity(), Locale.getDefault());
//
//        try {
//            addresses = geocoder.getFromLocation(latitude, longitude, 1);
//            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String postalCode = addresses.get(0).getPostalCode();
//            knownName = addresses.get(0).getAddressLine(1);
//
//            Log.i("address", String.valueOf(addresses));
//            Log.i("address", city);
//
//            knownName = address.concat("/n").concat("/n").concat(city).concat("/").concat(state).concat("/n").concat(country);
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
    }


    public void shareLocationToWisActuality() {


        sendCurrentLocationToSerer(coordinates, currentLocationName);


    }


    public void sendCurrentLocationToSerer(String cooridinates, String name) {


        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
//            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\",coordinate\":\"" +cooridinates+ "\",location_name\":\"" + name+ "\"}");
            jsonBody = new JSONObject();
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("coordinates", cooridinates);
            jsonBody.put("location_name", name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("jsone body%@", jsonBody.toString());
        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.share_location), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                        Log.i("map response", response.toString());

                        Toast.makeText(getActivity(), getResources().getString(R.string.share_success_msg), Toast.LENGTH_SHORT).show();


//                        tracker.stopUsingGPS();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                tracker.stopUsingGPS();
                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };


        ApplicationController.getInstance().addToRequestQueue(reqActu);


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);

    }

    @Override
    public void onMapReady(GoogleMap Map) {

        googleMap = Map;

        if (UserNotification.getLocationSelected() == Boolean.TRUE) {
            System.out.println("from actuality");
            String add = UserNotification.getAddress();
            address = add;
            Log.e("address", address);
            Log.i("request url ", "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            DataLongOperationAsynchTask taks = new DataLongOperationAsynchTask();
            taks.execute(add);

        } else {

            System.out.println("current location");

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {


                    try {

                        if (myLocation == null) {


                            myLocation = new LatLng(location.getLatitude(),
                                    location.getLongitude());


                            latitude = location.getLatitude();

                            langitude = location.getLongitude();

//                        currentLocationName = getMapUrlLink();


                            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));

                            Log.i("current",coordinates);
                            System.out.println("------------- sttusdhjbs-------");
                            System.out.println(curretnLocationAdress);
                            System.out.println("------------- sttusdhjbs-------");


                            googleMap.addMarker(new MarkerOptions().position(myLocation).title("je suis ici")).showInfoWindow();
//                         googleMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                                    14));


                            getCityName(latitude, langitude);
                            new UserNotification().setTopub(Boolean.TRUE);
                            new UserNotification().setLattitude(latitude);
                            new UserNotification().setLangtitude(langitude);


                        }
                    }
                    catch(NullPointerException ex){

                        ex.printStackTrace();

                    }
                }
            });
        }



//        googleMap.addMarker()

    }







    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, LatLng> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected LatLng doInBackground(String... params) {
            JSONObject response1;
            LatLng responsData = null;
            try {
                responsData = getLocationFromString(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return responsData;

        }


        @Override
        protected void onPostExecute(LatLng result) {

            Log.e("resltaas", String.valueOf(result));





//                JSONObject jsonObject = new JSONObject(result);
//
//                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lng");
//
//               lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lat");
//
//                Log.d("latitude", "" + lat);
//                Log.d("longitude", "" + lng);
//
//
//                Log.i("user address",address);



            latitude = result.latitude;

            langitude =result.longitude;

            getCityName(latitude,langitude);

            final LatLng testAddr = new LatLng(lat,lng);




//                currentLocationName = getMapUrlLink();

            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));

            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude,langitude)).title("je suis ici")).showInfoWindow();
//                googleMap.moveCamera(CameraUpdateFactory.newLatLng(testAddr));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(result,
                    14));

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }


    public static LatLng getLocationFromString(String address)
            throws JSONException {

        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(
                    "http://maps.google.com/maps/api/geocode/json?address="
                            + URLEncoder.encode(address, "UTF-8") + "&ka&sensor=false");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject(stringBuilder.toString());

        double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                .getJSONObject("geometry").getJSONObject("location")
                .getDouble("lng");

        double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                .getJSONObject("geometry").getJSONObject("location")
                .getDouble("lat");

        return new LatLng(lat, lng);
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        }catch (java.net.SocketTimeoutException e) {

            Toast.makeText(getActivity(), getString(R.string.connection_timeout), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public static JSONObject getLocationInfo(String address) {

//        https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY
//        http://maps.google.com/maps/api/geocode/json?address="+address+"&sensor=false
        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?address=" +address+ "&sensor=false");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());

            Log.e("response",stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }


    public static JSONObject getCurrentLocationViaJSON(String address) {

        JSONObject jsonObj = getLocationInfo(address);

        Log.i("JSON string =>", jsonObj.toString());

//        String currentLocation = "testing";
//        String street_address = null;
//        String postal_code = null;
//
//        try {
//            String status = jsonObj.getString("status").toString();
//            Log.i("status", status);
//
//            if (status.equalsIgnoreCase("OK")) {
//                JSONArray results = jsonObj.getJSONArray("results");
//                int i = 0;
//                Log.i("i", i + "," + results.length()); //TODO delete this
//                do {
//
//                    JSONObject r = results.getJSONObject(i);
//                    JSONArray typesArray = r.getJSONArray("types");
//                    String types = typesArray.getString(0);
//
//                    if (types.equalsIgnoreCase("street_address")) {
//                        street_address = r.getString("formatted_address").split(",")[0];
//                        Log.i("street_address", street_address);
//                    } else if (types.equalsIgnoreCase("postal_code")) {
//                        postal_code = r.getString("formatted_address");
//                        Log.i("postal_code", postal_code);
//                    }
//
//                    if (street_address != null && postal_code != null) {
//                        currentLocation = street_address + "," + postal_code;
//                        Log.i("Current Location =>", currentLocation); //Delete this
//                        i = results.length();
//                    }
//
//                    i++;
//                } while (i < results.length());
//
//                Log.i("JSON Geo Locatoin =>", currentLocation);
//                return currentLocation;
//            }
//
//        } catch (JSONException e) {
//            Log.e("testing", "Failed to load JSON");
//            e.printStackTrace();
//        }
        return jsonObj;
    }


    @Override
    public void onPause() {
        super.onPause();
        UserNotification.setLocationSelected(Boolean.FALSE);
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (googleMap != null) {
            googleMap.clear();

//            getFragmentManager()
//                    .beginTransaction()
//                    .remove(getFragmentManager().findFragmentById(R.id.mapFragment))
//                    .commit();
        }
    }


    public void CaptureMapScreen()
    {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // TODO Auto-generated method stub
                bitmap = snapshot;

                Log.d("map snap", String.valueOf(bitmap));

                snapShotFilePath = "/mnt/sdcard/"
                        + "MyMapScreen" + System.currentTimeMillis()
                        + ".jpg";

                try {
                    FileOutputStream out = new FileOutputStream( snapShotFilePath);

                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement

                    Log.e("mappath", String.valueOf(out));

//                    File file = new (filePath);

                    Toast toast= Toast.makeText(getActivity(),
                            (R.string.capture_success), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();








                    Log.e("map filepath",snapShotFilePath);



                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);


//                    shareToAmis(view,bitmap);
                    viewMoreOption(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);


    }



    public void onDetach() {
        super.onDetach();
        googleMap = null;
    }


}