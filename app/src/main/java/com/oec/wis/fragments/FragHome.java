package com.oec.wis.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.adapters.NavDrawarAdapter;
import com.oec.wis.adapters.SearcMenuAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.chatApi.ChatListener;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class FragHome extends Fragment{
    View view;
    GridView navGrid;
    List<WISMenu> menuList;
    EditText etSearch;
    ListView lvSearch;
    List<WISMenu> searchMenu;
    SharedPreferences settings;
    public static final String PREFS_NAME = "CustomPermission";
    boolean dialogShown;
    String currentChannel;
    String message = null;

    public static Activity context;

    private static final int RC_CAMERA_PERM = 123;
    private static final int RC_LOCATION_CONTACTS_PERM = 124;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_home, container, false);
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);






            loadControls();
            setNavList();
            setListener();


        } catch (InflateException e) {
        }
        return view;
    }


    private void loadControls() {
        navGrid = (GridView) view.findViewById(R.id.nav_grid);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        lvSearch = (ListView) view.findViewById(R.id.lvSeach);

        settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        dialogShown = settings.getBoolean("weatherdialogShown", false);

        setUpChatApi();





    }





    private void setNavList() {
        menuList = new ArrayList<>();
        menuList.add(new WISMenu(getString(R.string.actuality), R.drawable.menu_feed));
        menuList.add(new WISMenu(getString(R.string.profile), R.drawable.menu_profile));
        menuList.add(new WISMenu(getString(R.string.friends), R.drawable.menu_friends));
        menuList.add(new WISMenu(getString(R.string.chat), R.drawable.menu_chat));
        menuList.add(new WISMenu(getString(R.string.calender), R.drawable.menu_calender));
        menuList.add(new WISMenu(getString(R.string.local), R.drawable.menu_local));
        menuList.add(new WISMenu(getString(R.string.pub), R.drawable.menu_pub));
        menuList.add(new WISMenu(getString(R.string.video), R.drawable.menu_video));
        menuList.add(new WISMenu(getString(R.string.photo), R.drawable.menu_pics));
        menuList.add(new WISMenu(getString(R.string.music), R.drawable.menu_music));
        menuList.add(new WISMenu(getString(R.string.notif), R.drawable.menu_notif));
        menuList.add(new WISMenu(getString(R.string.condition), R.drawable.info));
//        menuList.add(new WISMenu(getString(R.string.weather), R.drawable.weather_logo));

        navGrid.setAdapter(new NavDrawarAdapter(getActivity(), menuList));
    }

    private void displayFrag(int position) {
        Fragment fragment = null;
        ((Dashboard) getActivity()).cFrag = 1;
        switch (position) {
            case 0:
                fragment = new FragActu();
                break;
            case 1:
                fragment = new FragProfile();
                break;
            case 2:
                fragment = new FragFriends();
                break;
            case 3:
                fragment = new FragChat();
                UserNotification userNotification = new UserNotification();
                userNotification.setChatSelected(Boolean.FALSE);
                break;
            case 4:
                fragment = new FragCalendar();
                break;
            case 5:
                fragment = new FragMyLocation();


                UserNotification.setViewType("DashBoard");
                UserNotification.setLocationSelected(Boolean.FALSE);


                break;
            case 6:
                fragment = new FragMyPub();
                break;
            case 7:
                fragment = new FragVideo();
                break;
            case 8:
                fragment = new FragPhoto();
                break;
            case 9:
                Toast.makeText(getActivity(), getString(R.string.msg_soon), Toast.LENGTH_LONG).show();
                break;
            case 10:
                fragment = new FragNotif();
                break;
            case 11:
                fragment = new FragConditions();
                break;
            case 12:
                if (dialogShown) {
                    fragment = new FragWeather();
                } else {
                    showWeatherAlert();
                }

                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
            ((Dashboard) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Dashboard) getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        navGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayFrag(position);
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    searchMenu(etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        searchMenu(etSearch.getText().toString());
                        return true;
                    }
                }
                return false;
            }
        });
        lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((Dashboard) getActivity()).cFrag = 1;
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, searchMenu.get(i).getFregment()).commit();
            }
        });
    }

    private void searchMenu(String key) {
        searchMenu = new ArrayList<>();
        if (key.equals("")) {
            navGrid.setVisibility(View.VISIBLE);
            lvSearch.setVisibility(View.GONE);
        } else {
            navGrid.setVisibility(View.GONE);
            lvSearch.setVisibility(View.VISIBLE);
            for (WISMenu item : ApplicationController.MENUS) {
                if (item.getTitle().toLowerCase().contains(key.toLowerCase()))
                    searchMenu.add(item);
            }
            lvSearch.setAdapter(new SearcMenuAdapter(getActivity(), searchMenu));
            if (searchMenu.size() == 0) {
                Toast.makeText(getActivity(), getString(R.string.no_result), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    private void showWeatherAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);


        final AlertDialog alert = builder.create();
        alert.show();
        TextView content = (TextView) alert.findViewById(R.id.alertMessage);
        content.setText(R.string.weather_permission_msg);
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("weatherdialogShown", true);
                editor.commit();
                alert.dismiss();

//                rediret to fragment
                Fragment fragment = new FragWeather();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
                ((Dashboard) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);


            }
        });
    }


//    PubNubChat APi


    public void setUpChatApi() {

//  ChatApi.getInstance().setContext(getActivity());




//        ChatApi.getInstance(this).subscribe();

//        ChatApi.getInstance(this);

        Log.i("get channels", "method called");

        String apiUrl = getString(R.string.server_url).concat(getString(R.string.getChannels));

        JSONObject jsonParms = new JSONObject();

        try {
            jsonParms.put("user_id", Tools.getData(getActivity(), "idprofile"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest reqDisc = new JsonObjectRequest(apiUrl, jsonParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("channel response", String.valueOf(response));

                        try {

                            if(response.has("private_members")){


                                JSONArray privateArray = response.getJSONArray("private_members");
                                Tools.getPrivateChannel(privateArray);
                            }



                            if(response.has("public_mebmers")){


                                JSONArray publicArray = response.getJSONArray("public_mebmers");

                                Tools.getPublicChannel(publicArray);

                            }



                            if(response.has("pub_channels")){

                                JSONArray pub_channels = response.getJSONArray("pub_channels");

                                Tools.getPrivateChannel(pub_channels);
                            }



//                            List<String> privatelist = new ArrayList<String>();
//                            for (int i=0; i<privateArray.length(); i++) {
//                                privatelist.add( privateArray.getString(i) );
//                            }
//
//                            ChatApi.getInstance().addPrivateChannel(privatelist);





                            Tools.doSubScribe();

//                            JSONArray publicArray = response.getJSONArray("public_mebmers");
//
//                            ArrayList <String>publicID = new ArrayList<>();
//
//                            ArrayList<List>memersID= new ArrayList<>();
//
//                            for (int i = 0;i<publicArray.length();i++){
//
//                                JSONObject json = publicArray.getJSONObject(i);
//
//                                String publicId = json.getString("public_id");
//
//                                publicID.add(publicId);
//
//                                JSONArray channelGroupArray = json.getJSONArray("channel_group_members");
//
//                                List<String> memberlist = new ArrayList<String>();
//                                for (int m=0; m<channelGroupArray.length(); m++) {
//                                    memberlist.add( channelGroupArray.getString(m) );
//                                }
//
//
//
//                                memersID.add(memberlist);
//
//
//
//                            }
//
//
//
//                            ChatApi.getInstance().addPublicChannels(memersID,publicID);
//
//
//                            ChatApi.getInstance().subscribe();


//                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDisc);


    }




    //camera permission
    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void cameraTask() {
        if (EasyPermissions.hasPermissions(getActivity(),android.Manifest.permission.CAMERA)) {
            // Have permission, do the thing!
            Toast.makeText(getActivity(), "TODO: Camera things", Toast.LENGTH_LONG).show();

        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again),
                    RC_CAMERA_PERM, android.Manifest.permission.CAMERA);
        }
    }


}



