package com.oec.wis.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.etsy.android.grid.StaggeredGridView;
//import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.PhotoAdapter;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.dialogs.PhotoGallery;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.oec.wis.R.id.post_image;

public class FragPhoto extends Fragment implements ImageChooserListener {
    public static List<WISPhoto> photos;
    View view;
//    AsymmetricGridView gridPhoto;
//    GridView gridPhoto;
    StaggeredGridView gridPhoto;

    DialogPlus dialog;

    private final int SELECT_PHOTO = 1;

    private final int REQUEST_CAMERA  =2;

     TextView headerTitleTV;
    PhotoAdapter pAdapter;

    ImageChooserManager imageChooserManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            if (isAdded())
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));
            view = inflater.inflate(R.layout.frag_photo, container, false);

            initControls();
            setListener();
            if (isAdded())
            loadPhotos();

        } catch (InflateException e) {
        }
        return view;
    }

    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded())
                    ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });


        gridPhoto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isAdded()) {
                    Intent i = new Intent(getActivity(), PhotoGallery.class);
                    i.putExtra("p", position);
                    getActivity().startActivity(i);
                }
            }
        });
        view.findViewById(R.id.bAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(Intent.createChooser(i, getString(R.string.pick_photo)), 1);

                showMediaoption();
            }
        });
    }

//    Photo

    public void showMediaoption(){



            final ArrayList popupList = new ArrayList();

            popupList.add(new Popupelements(R.drawable.slide_wis_photo,"Pick Photos"));
            popupList.add(new Popupelements(R.drawable.camera_grey,"Take Photos"));

            Popupadapter simpleAdapter = new Popupadapter(getActivity(), false, popupList);


            dialog = DialogPlus.newDialog(getActivity())
                    .setAdapter(simpleAdapter)
                    .setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                            if(position ==0){
                                Intent i = new Intent(Intent.ACTION_PICK,
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, SELECT_PHOTO);
                                dialog.dismiss();

                            }

                            else if(position == 1){

                                takeImageFromCamera();

//                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                                startActivityForResult(intent,REQUEST_CAMERA);
//                                dialog.dismiss();

                            }

                        }
                    })
                    .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                    .create();
            dialog.show();
    }

    private void takeImageFromCamera() {

        Log.e("Image custom","Library");
        imageChooserManager = new ImageChooserManager(FragPhoto.this, ChooserType.REQUEST_CAPTURE_PICTURE);
        imageChooserManager.setImageChooserListener(FragPhoto.this);
        try {
            imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);


        if (resultCode ==  Activity.RESULT_OK &&
                (requestCode == ChooserType.REQUEST_PICK_PICTURE||
                        requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            imageChooserManager.submit(requestCode, imageReturnedIntent);

        }

        switch (requestCode) {
            case 1:
                if (isAdded()) {



                    if (resultCode == getActivity().RESULT_OK && requestCode == SELECT_PHOTO) {
                        Uri selectedVideo = imageReturnedIntent.getData();


                        final String[] proj = {MediaStore.Images.Media.DATA};
                        final Cursor cursor = getActivity().managedQuery(selectedVideo, proj, null, null,
                                null);
                        final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                        cursor.moveToLast();
                        String photoPath = cursor.getString(column_index);
                        new DoUpload().execute(photoPath);
                    }


                }
                break;


            case REQUEST_CAMERA:
                if(resultCode == getActivity().RESULT_OK && requestCode == REQUEST_CAMERA){
                    System.out.println("finalFile");


                    System.out.println("finalFile");

                    Log.d("request code", String.valueOf(requestCode));

                    Log.d("response", String.valueOf(getActivity().RESULT_OK));


                    Bitmap thumbnail = (Bitmap) imageReturnedIntent.getExtras().get("data");


//                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
//                    Uri tempUri = getImageUri(getActivity(), thumbnail);
//
//                    // CALL THIS METHOD TO GET THE ACTUAL PATH
//                    File finalFile = new File(getRealPathFromURI(tempUri));
//
//                    System.out.println(finalFile);

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    }catch (FileNotFoundException e){
                        e.printStackTrace();
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                    System.out.println("Welcome"+destination);

                    //new DoUpload().execute();
                    new DoUpload().execute(String.valueOf(destination));
                }
                break;
            default:
                break;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void initControls() {
        gridPhoto = (StaggeredGridView) view.findViewById(R.id.gridPhoto);

        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
        headerTitleTV.setTypeface(font);
//        gridPhoto = (AsymmetricGridView) view.findViewById(R.id.gridPhoto);

    }

    private void loadPhotos() {
        photos = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").contains("any image")) {
                                Toast.makeText(getActivity(), getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
                            } else {
                                if (response.getString("result").equals("true")) {

                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at")));
                                        } catch (Exception e) {
                                        }
                                    }
                                    try{
                                        gridPhoto.setAdapter(new PhotoAdapter((FragmentActivity) getActivity(), photos));

                                    }catch(NullPointerException ex){
                                        System.out.println("Exce" +ex.getMessage());
                                    }
//                                    gridPhoto.setAdapter(pAdapter);


                                }
                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
                            Tools.dismissLoader();

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);
    }

    private void doAddPhoto(final String photo) {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getActivity(), "idprofile") + "\",\"photo\": \"" + photo + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.add_photo_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String x = "";
                            if (response.getBoolean("result")) {
                                DateFormat df = new android.text.format.DateFormat();
                                String dt = DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
                                photos.add(0, new WISPhoto(response.getInt("data"), "", photo, dt));
                                gridPhoto.setAdapter(new PhotoAdapter((FragmentActivity) getActivity(), photos));
                            }
                        } catch (Exception e) {
                            String x = "";
                        }
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        Tools.dismissLoader();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);

                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    public void shareContent(String filePath){
        new DoUpload().execute(filePath);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {




                new DoUpload().execute(String.valueOf(chosenImage.getFilePathOriginal()));

            }
        });

    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }

    public class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result")) {
                        if (isAdded())
                            doAddPhoto(img.getString("data"));
                    } else {
                        //if (isAdded())
                        //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                }

            } else {
                //if (isAdded())
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                return Tools.doFileUpload(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.upload_meth), urls[0], Tools.getData(getActivity(), "token"));
            } catch (Exception e) {
                return "";
            }
        }
    }
}
