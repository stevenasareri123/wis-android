package com.oec.wis.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.GPSTracker;
import com.oec.wis.R;
import com.oec.wis.adapters.SearcMenuAdapter;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.tools.Tools;
import com.squareup.picasso.Picasso;

import org.joda.time.YearMonth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by asareri08 on 17/08/16.
 */
public class FragWeather extends Fragment{

    private static View view;
    private LocationManager locationManager;
    CircularImageView userNotificationView;
    TextView notificationCount;

    TextView country,city,temp_max,temp_min,lever,coucher,description,totaltempDegreeTV,mph_textView,headerTitleTV,monthdisplayTV,dateDisplayTV;
    EditText etSearch;
    ImageView refreshIv;
    Button currentDateButton;
    ImageView weatherIcon;
    String icon;
    GPSTracker tracker;
    String getIconUrl;
    Drawable weatherDrawable=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_weather, container, false);
            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));

        } catch (InflateException e) {
        }

        setUpcontrollers();
        setupWeatherConfig();
        setTypeFace();

        return view;
    }

    private void setTypeFace() {
        Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
        city.setTypeface(font);
        country.setTypeface(font);
        temp_max.setTypeface(font);
        temp_min.setTypeface(font);
        etSearch.setTypeface(font);
        description.setTypeface(font);
        mph_textView.setTypeface(font);
        etSearch.setTypeface(font);
        headerTitleTV.setTypeface(font);
        monthdisplayTV.setTypeface(font);
        dateDisplayTV.setTypeface(font);

        Typeface typeface =Typeface.createFromAsset(getActivity().getAssets(),"fonts/CHANTICL.TTF");
        totaltempDegreeTV.setTypeface(typeface);

    }


    private void setUpcontrollers(){
        country = (TextView)view.findViewById(R.id.country);
        city = (TextView)view.findViewById(R.id.city);
        temp_max = (TextView)view.findViewById(R.id.temp_max_txt);
        temp_min = (TextView)view.findViewById(R.id.temp_min_txt);
//        lever = (TextView)view.findViewById(R.id.lever_txt);
//        coucher = (TextView)view.findViewById(R.id.coucher_txt);
        etSearch = (EditText)view.findViewById(R.id.etSearch);
        weatherIcon = (ImageView)view.findViewById(R.id.temps_icon);
        description = (TextView)view.findViewById(R.id.description);
//        currentDateButton=(Button)view.findViewById(R.id.currentDateButton);
        totaltempDegreeTV=(TextView)view.findViewById(R.id.totaltempDegreeTV);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        mph_textView=(TextView)view.findViewById(R.id.mph_textView);
        refreshIv=(ImageView) view.findViewById(R.id.refreshIV);
        monthdisplayTV=(TextView)view.findViewById(R.id.monthdisplayTV);
        dateDisplayTV=(TextView)view.findViewById(R.id.dateDisplayTV);
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });

        notificationCount = (TextView) view.findViewById(R.id.target_view);
        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);

        downLoadProfileImage();
        if(new UserNotification().getBatchcount()==0)
        {
            notificationCount.setVisibility(View.INVISIBLE);
        }
        else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));

                }
            });
        }
        userNotificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Tools.callDialog(getActivity());
            }
        });


        SimpleDateFormat sdf = new SimpleDateFormat("MMM");
        String currentDateandTime = sdf.format(new Date());
//        currentDateButton.setText(currentDateandTime);

        monthdisplayTV.setText(currentDateandTime);

        System.out.println("current month" +currentDateandTime);
        final Calendar c = Calendar.getInstance();

        String  mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
//        SimpleDateFormat sdf2=new SimpleDateFormat("DD");
//        String currentdate = sdf2.format(new Date());
        System.out.println("current date" +mDay);
        dateDisplayTV.setText(mDay);
        refreshIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupWeatherConfig();
            }
        });
        try{
            etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        searchMenu(etSearch.getText().toString());
                        return true;
                    }
                    return false;
                }
            });
            etSearch.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int DRAWABLE_RIGHT = 2;

//                    if (event.getAction() == MotionEvent.ACTION_UP) {
//                        if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                            searchMenu(etSearch.getText().toString());
//                            return true;
//                        }
//                    }
                    return false;
                }
            });

        }catch(NullPointerException  exception){
            System.out.println("Null pointer Exception" +exception.getMessage());
        }





    }

    private void downLoadProfileImage() {

        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(userNotificationView);

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private  void updateWeatherReport(String pays, String ville, double t_max, double t_min, long sunrise, long sunset, String icon, String desc, String windSpeed){

        Locale obj = new Locale("",pays);

        System.out.println("Country Code = " + obj.getCountry()
                + ", Country Name = " + obj.getDisplayCountry());


        country.setText(obj.getDisplayCountry());


        city.setText(ville.concat(","));

//        city.setText(ville.concat("  ").concat(String.valueOf(currentDateandTime)));
        String t_M = String.valueOf(t_min).split("\\.", 2)[0];
        String t_Ma = String.valueOf(t_max).split("\\.", 2)[0];

//        temp_min.setText(String.format("%.0f", t_min).concat("°C"));

        temp_min.setText(String.format("%.0f", t_min).concat("%"));

        temp_max.setText(String.format("%.0f", t_max).concat("°%"));

        mph_textView.setText(windSpeed.concat(" km/h"));

//        String s = Double.toString(t_max);
        String maxTemp=String.format("%.0f", t_max);

        totaltempDegreeTV.setText(maxTemp);

        long UTC_TIMEZONE_SUNRISE=sunrise;
        String OUTPUT_DATE_FORMATE="hh:mm a";

//        lever.setText(getDateCurrentTimeZone(UTC_TIMEZONE_SUNRISE,OUTPUT_DATE_FORMATE));
//        long UTC_TIMEZONE_SUNSET=sunset;
//        coucher.setText(getDateCurrentTimeZone(UTC_TIMEZONE_SUNSET,OUTPUT_DATE_FORMATE));

        String weatherIconUrl = "http://openweathermap.org/img/w/";
//D

//        if(icon.equals("01d"))
//        {
//            weatherDrawable = getActivity().getDrawable(R.drawable.clearsky_d);
//
//            Log.e("icon" , String.valueOf(weatherDrawable));
////            weatherIcon.setBackgroundResource(R.drawable.clearsky_d);
//        }
//        else if (icon.equals("01n")){
////            weatherIcon.setBackgroundResource(R.drawable.clearsky_n);
//
//            getIconUrl  = String.valueOf(R.drawable.clearsky_n);
//        }
//        else if(icon.equals("02d"))
//        {
//
//            getIconUrl  = String.valueOf(R.drawable.fewclouds_d);
//
//        }else if(icon.equals("02n")) {
//            getIconUrl  = String.valueOf(R.drawable.fewclouds_n);
//
//        }
//
//       else if(icon.equals("03d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.scattered_d);
//
//        }
//        else if (icon.equals("03n")){
////            weatherIcon.setBackgroundResource(R.drawable.scattered_n);
//            getIconUrl  = String.valueOf(R.drawable.scattered_n);
//
//        }
//        else if(icon.equals("04d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.broken_d);
//        }else if(icon.equals("04n"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.broken_n);
//        }
//
//        if(icon.equals("09d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.shower_d);
//        }
//        else if (icon.equals("09n")){
//            getIconUrl  = String.valueOf(R.drawable.shower_n);
//        }
//        else if(icon.equals("10d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.rain_d);
//        }else if(icon.equals("10n")) {
//            getIconUrl  = String.valueOf(R.drawable.rain_n);
//        }
//
//        else if(icon.equals("11d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.thunderstorm_d);
//        }
//        else if (icon.equals("11n")){
//            getIconUrl  = String.valueOf(R.drawable.thunderstorm_n);
//        }
//        else if(icon.equals("13d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.snow_d);
//        }else if(icon.equals("13n"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.snow_n);
//        }
//
//        else if(icon.equals("50d"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.mist_d);
//        }else if(icon.equals("50n"))
//        {
//            getIconUrl  = String.valueOf(R.drawable.mist_n);
//        }
//
//        else
//        {
////            weatherIcon.setBackgroundResource(R.drawable.logo_wis);
//            Log.e("else ","else");
//        }
//        weatherIconUrl = weatherIconUrl.concat(icon).concat(".png");


//        Picasso.with(getActivity())
//                .load(weatherIconUrl)
//                .into(weatherIcon);


//        weatherIcon.setImageDrawable(weatherDrawable);


        description.setText(desc);




    }


    private void setupWeatherConfig(){


//        setup location data
         locationManager = (LocationManager)getActivity().getSystemService(getActivity().LOCATION_SERVICE);
         boolean enabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);


        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        else{

            tracker = new GPSTracker(getActivity());

            if (tracker.canGetLocation()) {

                 String lat = String.valueOf(tracker.getLatitude());
                String lon = String.valueOf(tracker.getLongitude());

                Log.e("LAT",lat);
                Log.e("LONG",lon);

                getWeatherReport(lat,lon,null);

            } else {

                tracker.showSettingsAlert();
            }


        }


    }



    public void getWeatherReport(final String lat, final String lan,String city){
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        String weatherApiUrl="";
        String lang = Tools.getData(getActivity(),"lang_pr");
        if(city!=null){

            weatherApiUrl = "http://api.openweathermap.org/data/2.5/weather?".concat("lang=").concat(lang).concat("&").concat("units=metric").concat("&").concat("type=accurate").concat("&").concat("q").concat("=").concat(city).concat("&").concat("APPID").concat("=").concat(getActivity().getString(R.string.weather_id));

        }else{

            weatherApiUrl = "http://api.openweathermap.org/data/2.5/weather?".concat("lang=").concat(lang).concat("&").concat("units=metric").concat("&").concat("type=accurate").concat("&").concat("lat").concat("=").concat(lat).concat("&").concat("lon").concat("=").concat(lan).concat("&").concat("APPID").concat("=").concat(getActivity().getString(R.string.weather_id));
        }


        System.out.println(weatherApiUrl);

        final JsonObjectRequest weatherRequest = new JsonObjectRequest(weatherApiUrl, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(JSONObject response) {
//                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                Tools.dismissLoader();

                System.out.println("weather report");


                System.out.println(response.toString());


                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    int responseCode = jsonObject.getInt("cod");
                    if(responseCode==200) {
//                        {"coord":{"lon":80.24,"lat":13.07},"weather":[{"id":802,"main":"Clouds","description":"partiellement ensoleillé","icon":"03d"}],
//                            "base":"stations","main":{"temp":32,"pressure":1013,"humidity":59,"temp_min":32,"temp_max":32},"visibility":6000,
//                                "wind":{"speed":2.1,"deg":140},"clouds":{"all":40},"dt":1490763600,"sys":{"type":1,"id":7834,"message":0.0262,"country":"IN","sunrise":1490747828,"sunset":1490791828},"id":1274394,"name":"Chetput","cod":200}

                        JSONArray weatherArray = jsonObject.getJSONArray("weather");
                        JSONObject weatherJson = weatherArray.getJSONObject(0);
                        String desc = weatherJson.getString("description");
                        String city = jsonObject.getString("name");
                        JSONObject mainData = jsonObject.getJSONObject("main");
                        JSONObject windData = jsonObject.getJSONObject("wind");
                        JSONObject sysData = jsonObject.getJSONObject("sys");
                        double temp_min = mainData.getDouble("temp_min");
                        double temp_max = mainData.getDouble("temp_max");
                        long sunrise = sysData.getLong("sunrise");
                        long sunset = sysData.getLong("sunset");
                        String country = sysData.getString("country");

                         icon = weatherJson.getString("icon");

                        if(icon.equals("01d"))
                        {
                            Log.e("weather icon inside ", icon);

                            weatherDrawable =getActivity().getDrawable(R.drawable.clearsky_d);
                        }else if(icon.equals("01n")){
                            weatherDrawable =getActivity().getDrawable(R.drawable.clearsky_n);

                        }
                        else if(icon.equals("02d"))
                         {

                         weatherDrawable =getActivity().getDrawable(R.drawable.fewclouds_d);

                      }else if(icon.equals("02n")) {
                            weatherDrawable =getActivity().getDrawable(R.drawable.fewclouds_n);

                      }
                       else if(icon.equals("03d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.scattered_d);
                        }
                        else if (icon.equals("03n")){
                //            weatherIcon.setBackgroundResource(R.drawable.scattered_n);
                          weatherDrawable =getActivity().getDrawable(R.drawable.scattered_n);
                        }
                        else if(icon.equals("04d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.broken_d);

                        }else if(icon.equals("04n"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.broken_n);
                        }

                        if(icon.equals("09d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.shower_d);

                        }
                        else if (icon.equals("09n")){
                            weatherDrawable =getActivity().getDrawable(R.drawable.shower_n);
                        }
                        else if(icon.equals("10d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.rain_d);
                        }else if(icon.equals("10n")) {
                            weatherDrawable =getActivity().getDrawable(R.drawable.rain_n);
                        }

                        else if(icon.equals("11d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.thunderstorm_d);
                        }
                        else if (icon.equals("11n")){
                            weatherDrawable =getActivity().getDrawable(R.drawable.thunderstorm_n);
                        }
                        else if(icon.equals("13d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.snow_d);
                        }else if(icon.equals("13n"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.snow_n);
                        }

                        else if(icon.equals("50d"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.mist_d);
                        }else if(icon.equals("50n"))
                        {
                            weatherDrawable =getActivity().getDrawable(R.drawable.mist_n);
                        }


                        weatherIcon.setImageDrawable(weatherDrawable);





                        Log.e("weather icon", icon);
                        Log.e("weather array", String.valueOf(weatherArray));
                        String windSpedd = windData.getString("speed");


                        updateWeatherReport(country, city, temp_max, temp_min, sunrise, sunset, icon, desc,windSpedd);

                    }
                    else{

                        String error = jsonObject.getString("message");

                        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                Tools.dismissLoader();


                Log.i("error", String.valueOf(error));

            }
        });


        ApplicationController.getInstance().addToRequestQueue(weatherRequest);


    }

    public JSONObject getLocationName(double lattitude, double longitude) {

        String cityName = "Not Found";
        String countryName = "Not Found";
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("city",cityName);
            jsonData.put("country",countryName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
        try {

            List<Address> addresses = gcd.getFromLocation(lattitude, longitude,
                    10);

            if (addresses != null) {
                String city = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                if (city != null && !city.equals("") || countryName != null && !countryName.equals("")) {
                    cityName = city;
                    countryName = country;
                    System.out.println("city ::  " + cityName);
                    try {
                        jsonData.put("city", cityName);
                        jsonData.put("country", countryName);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }
            }
            }catch(IOException e){
                e.printStackTrace();
            }

        return jsonData;
    }


    public  String getDateCurrentTimeZone(long timestamp,String mDateFormate) {
        long unixSeconds = timestamp;
        Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat(mDateFormate); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        System.out.println(formattedDate);
        return formattedDate;
    }
    @Override
    public void onResume() {
        super.onResume();
//        if(tracker != null){
//            if (tracker.canGetLocation()) {
//                String lat = String.valueOf(tracker.getLatitude());
//                String lon = String.valueOf(tracker.getLongitude());
//                getWeatherReport(lat, lon);
//            }
//
//        }

    }

    private void searchMenu(String key) {


        getWeatherReport(null,null,key);


    }
}
