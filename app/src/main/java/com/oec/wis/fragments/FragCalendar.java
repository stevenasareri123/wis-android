package com.oec.wis.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceEnter;
import com.flyco.animation.FadeEnter.FadeEnter;
import com.flyco.animation.NewsPaperEnter;
import com.flyco.animation.SlideEnter.SlideBottomEnter;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.games.event.Events;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.DeleteEventListener;
import com.oec.wis.adapters.EventsAdapter;
import com.oec.wis.adapters.MyAdsAdapter;
import com.oec.wis.adapters.YMAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.CustomBaseDialog;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.entities.Event;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISMY;
import com.oec.wis.entities.WisEvents;
import com.oec.wis.tools.ListViewScrollable;
import com.oec.wis.tools.Tools;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class FragCalendar extends Fragment implements DeleteEventListener{
    GridView monthGrid, yearGrid;
    MaterialCalendarView cWidget;
    List<WISMY> m, y;
    YMAdapter mAdapter, yAdapter;
    TextView tvSMonth, tvSYear;
    ListViewScrollable lvMyPub;
    List<WISAds> pubList;
    TextView tvEmpty;
    private View view;
    private Button bDay, bMonth, bYear;
    Context context;
    Boolean isClickable;

    PopupWindow popWindow;

    ImageView close_btn;
    Button post_status;
    EditText message;

    TextView datelabel;

    List<WisEvents>eventsData;

    EventsAdapter adapter;

    TextView dateLabelHeaderTV;
    Event event;
    TextView headerTitleTV,headerTitleTVPopup,notificationCount;

    CircularImageView userNotificationView ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_calendar, container, false);
            loadControls();
            loadCalendar();
            setListener();
            ChatApi.getInstance().setContext(getActivity());


        } catch (InflateException e) {
        }
        return view;
    }

    private void loadControls() {

        eventsData = new ArrayList<>();
//
//        bDay = (Button) view.findViewById(R.id.bDay);
//        bMonth = (Button) view.findViewById(R.id.bMonth);
//        bYear = (Button) view.findViewById(R.id.bYear);
        cWidget = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        monthGrid = (GridView) view.findViewById(R.id.monthGrid);
        yearGrid = (GridView) view.findViewById(R.id.yearGrid);
        tvSMonth = (TextView) view.findViewById(R.id.tvSmonth);
        tvSYear = (TextView) view.findViewById(R.id.tvSYear);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);
        notificationCount = (TextView) view.findViewById(R.id.target_view);
        downLoadProfileImage();
        if(new UserNotification().getBatchcount()==0)
        {
            notificationCount.setVisibility(View.INVISIBLE);
        }
        else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));

                }
            });
        }

        userNotificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Tools.callDialog(getActivity());
            }
        });


//        tvEmpty = (TextView) view.findViewById(R.id.tvEmpty);
        lvMyPub = (ListViewScrollable) view.findViewById(R.id.lvMyAds);
        lvMyPub.setExpanded(true);
        m = new ArrayList<>();
        y = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        String[] aMonth = getResources().getStringArray(R.array.month_new_list);
        String[] aYear = getResources().getStringArray(R.array.year_list);

        for (int i = 0; i < aYear.length; i++) {
            if (String.valueOf(c.get(Calendar.YEAR)).equals(aYear[i].replace("_", ""))) {
                y.add(new WISMY(aYear[i], true));
                tvSYear.setText(y.get(i).getName().replace("_", ""));
            } else {
                y.add(new WISMY(aYear[i], false));
            }
        }
        for (int i = 0; i < aMonth.length; i++) {
            if (c.get(Calendar.MONTH) == i) {
                m.add(new WISMY(aMonth[i], true));
                tvSMonth.setText(m.get(i).getName().replace("_", "") + " " + tvSYear.getText().toString());
            } else
                m.add(new WISMY(aMonth[i], false));
        }


        mAdapter = new YMAdapter(getActivity(), m);
        monthGrid.setAdapter(mAdapter);
        yAdapter = new YMAdapter(getActivity(), y);
        yearGrid.setAdapter(yAdapter);


         Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);

        }

    private void downLoadProfileImage() {

        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(userNotificationView);
    }

    private void setListener() {

        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
//        bDay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Log.i("bday clicke","d");
//
//                lvMyPub.setVisibility(View.VISIBLE);
//
//                resetIndic();
//                resetLayout();
//                resetButtonBg();
//                view.findViewById(R.id.dayIndic).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.llDay).setVisibility(View.VISIBLE);
//                bDay.setBackgroundColor(getResources().getColor(R.color.trans_white));
//
//            }
//        });
//        bMonth.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lvMyPub.setVisibility(View.INVISIBLE);
//                resetIndic();
//                resetLayout();
//                resetButtonBg();
//                view.findViewById(R.id.monthIndic).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.llMonth).setVisibility(View.VISIBLE);
//                bMonth.setBackgroundColor(getResources().getColor(R.color.trans_white));
//
//                tvSMonth.setText(tvSMonth.getText().toString().split(" ")[0] + " " + tvSYear.getText().toString());
//            }
//        });
//        bYear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lvMyPub.setVisibility(View.INVISIBLE);
//                resetIndic();
//                resetLayout();
//                resetButtonBg();
//                view.findViewById(R.id.yearIndic).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.llYear).setVisibility(View.VISIBLE);
//                bYear.setBackgroundColor(getResources().getColor(R.color.trans_white));
//            }
//        });
        monthGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (m.get(position).isSelected()) {
                    /*resetIndic();
                    resetLayout();
                    resetButtonBg();
                    view.findViewById(R.id.dayIndic).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.llDay).setVisibility(View.VISIBLE);
                    bDay.setBackgroundColor(getResources().getColor(R.color.trans_white));*/
                    requestFeeds(2, tvSYear.getText().toString() + "-" + String.format("%02d", position + 1));
                } else {
                    for (WISMY item : m)
                        item.setSelected(false);

                    m.get(position).setSelected(true);
                    mAdapter.notifyDataSetChanged();
                    monthGrid.invalidateViews();
                    tvSMonth.setText(m.get(position).getName().replace("_", "") + " " + tvSYear.getText().toString());
                }
            }
        });
        yearGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (y.get(position).isSelected()) {
                    /*
                    resetIndic();
                    resetLayout();
                    resetButtonBg();
                    view.findViewById(R.id.monthIndic).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.llMonth).setVisibility(View.VISIBLE);
                    bMonth.setBackgroundColor(getResources().getColor(R.color.trans_white));
                    tvSMonth.setText(tvSMonth.getText().toString().split(" ")[0] + " " + tvSYear.getText().toString());

                    for (WISMY item : m)
                        item.setSelected(false);
                    m.get(0).setSelected(true);
                    mAdapter.notifyDataSetChanged();
                    monthGrid.invalidateViews();
                    tvSMonth.setText(m.get(position).getName().replace("_", "") + " " + tvSYear.getText().toString());
                    */
                    requestFeeds(3, y.get(position).getName().replace("_", ""));
                } else {
                    for (WISMY item : y)
                        item.setSelected(false);
                    y.get(position).setSelected(true);
                    yAdapter.notifyDataSetChanged();
                    yearGrid.invalidateViews();
                    tvSYear.setText(y.get(position).getName().replace("_", ""));
                }
            }
        });

        view.findViewById(R.id.ivMRight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int s = -1;
                for (WISMY item : m) {
                    if (item.isSelected()) {
                        s = m.indexOf(item);
                    }
                }
                if (s != -1 && s < 11) {
                    m.get(s).setSelected(false);
                    m.get(s + 1).setSelected(true);
                    mAdapter.notifyDataSetChanged();
                    monthGrid.invalidateViews();
                    tvSMonth.setText(m.get(s + 1).getName().replace("_", "") + " " + tvSYear.getText().toString());
                }
            }
        });
        view.findViewById(R.id.ivMLeft).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int s = -1;
                for (WISMY item : m) {
                    if (item.isSelected()) {
                        s = m.indexOf(item);
                    }
                }
                if (s != -1 && s > 0) {
                    m.get(s).setSelected(false);
                    m.get(s - 1).setSelected(true);
                    mAdapter.notifyDataSetChanged();
                    monthGrid.invalidateViews();
                    tvSMonth.setText(m.get(s - 1).getName().replace("_", "") + " " + tvSYear.getText().toString());
                }
            }
        });

        view.findViewById(R.id.ivYRight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int s = -1;
                for (WISMY item : y) {
                    if (item.isSelected()) {
                        s = y.indexOf(item);
                    }
                }
                if (s != -1 && s < 11) {
                    y.get(s).setSelected(false);
                    y.get(s + 1).setSelected(true);
                    yAdapter.notifyDataSetChanged();
                    yearGrid.invalidateViews();
                    tvSYear.setText(y.get(s + 1).getName().replace("_", ""));
                }
            }
        });
        view.findViewById(R.id.ivYLeft).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int s = -1;
                for (WISMY item : y) {
                    if (item.isSelected()) {
                        s = y.indexOf(item);
                    }
                }
                if (s != -1 && s > 0) {
                    y.get(s).setSelected(false);
                    y.get(s - 1).setSelected(true);
                    yAdapter.notifyDataSetChanged();
                    yearGrid.invalidateViews();
                    tvSYear.setText(y.get(s - 1).getName().replace("_", ""));
                }
            }
        });
//        lvMyPub.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (pubList.size() > 0) {
//                    Intent i = new Intent(getActivity(), PubDetails.class);
//                    i.putExtra("id", pubList.get(position).getId());
//                    startActivity(i);
//                }
//            }
//        });
    }

//    private void resetIndic() {
//        view.findViewById(R.id.dayIndic).setVisibility(View.INVISIBLE);
//        view.findViewById(R.id.monthIndic).setVisibility(View.INVISIBLE);
//        view.findViewById(R.id.yearIndic).setVisibility(View.INVISIBLE);
//    }
//
//    private void resetLayout() {
//        view.findViewById(R.id.llDay).setVisibility(View.GONE);
//        view.findViewById(R.id.llMonth).setVisibility(View.GONE);
//        view.findViewById(R.id.llYear).setVisibility(View.GONE);
//    }
//
//    private void resetButtonBg() {
//        bDay.setBackgroundColor(getResources().getColor(R.color.blue4));
//        bMonth.setBackgroundColor(getResources().getColor(R.color.blue4));
//        bYear.setBackgroundColor(getResources().getColor(R.color.blue4));
//    }



    public String getCurretTime(String format){

        Calendar calander = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat(format);

        String time = simpleDateFormat.format(calander.getTime());

        return time;
    }

    private void loadCalendar() {



        cWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("touch working","tocudfjbv");
//                cWidget.setSelectionColor(R.color.black);
            }
        });



        cWidget.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                cWidget.setSelectionColor(R.color.black);

                return true;
            }
        });

//        cWidget.setFirstDayOfWeek(Calendar.MONDAY);
        cWidget.setOnDateChangedListener(new OnDateSelectedListener() {
                                             @Override
                                             public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay calendarDay, boolean selected) {

//                                                 generateSelector();
                                                 Log.d("calender day ", String.valueOf(calendarDay));
                                                 Log.d("selected date", String.valueOf(calendarDay.getDate()));
                                                 Log.d("event date", String.valueOf(calendarDay.getDate()));

                                                 List<CalendarDay> list = new ArrayList<CalendarDay>();
                                                 Calendar calendar = Calendar.getInstance();

                                                 try{

                                                     list.add(calendarDay);
                                                     cWidget.addDecorator(new EventDecorator(Color.RED,list));

//                                                     CalendarDay calendarDate= CalendarDay.from(calendarDay.getDate());


//                                                     Log.d("event date======>", String.valueOf(calendarDate));

//                                                 cWidget.setBackgroundColor(getResources().getColor(R.color.white));

//                                                 HashSet<CalendarDay> dates=calendarDate;
//                                                     if(isClickable=true){
//                                                         cWidget.addDecorator(new EventDecorator(Color.RED,list));
//                                                     }else
//                                                     {
//                                                         cWidget.addDecorator(new EventDecorator(R.color.blue_color,list));
//
//                                                     }

//                                                         cWidget.addDecorator(new EventDecorator(Color.RED,list));
//                                                         cWidget.addDecorator(new EventDecorator(R.color.blue_color,list));

                                                 }catch (NullPointerException ex){
                                                     System.out.println("Null Ex===>" +ex.getMessage());
                                                 }

//                                                 try {
//
//                                                     cWidget.addDecorator(new EventDecorator3(calendarDay,ChatApi.getInstance().getContext()));
//
//                                                 }catch (NullPointerException ex){
//                                                     System.out.println("Exception===>" +ex.getMessage());
//                                                 }


                                                 android.text.format.DateFormat df = new android.text.format.DateFormat();
                                                 String date = DateFormat.format("dd-MM-yyyy", calendarDay.getDate()).toString();

                                                 String displaydate = DateFormat.format("dd/MM/yyyy", calendarDay.getDate()).toString();

                                                 String displayTime =getCurretTime("HH").concat("h");

                                                 addEvents(date,getCurretTime("HH:mm:ss"),view,displaydate,displayTime);

                                                 getEvents(date);
                                             }

//                                             @Override
//                                             public void onDateChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
//
//
////                                                 cWidget.setBackground(R.drawable.outline);
//
////                                                 cWidget.addDecorator(new EventDecorator(type2, this));
//
//                                                 try{
//                                                     Log.d("selected date decorator", String.valueOf(calendarDay.getDate()));
//
//                                                     cWidget.addDecorator(new EventDecorator1(calendarDay,getContext()));
//
////                                                     EventDecorator eventDecorator = new EventDecorator(calendarDay, event.getDate(), event.getColor());
////                                                     calendarDay.addDecorator(eventDecorator);
//                                                 }catch (ClassCastException exception){
//
//                                                     System.out.println("Exception" +exception.getMessage());
//                                                 }
//
//
//                                                 Log.d("selected date", String.valueOf(calendarDay.getDate()));
//                                                 Log.d("event date", String.valueOf(calendarDay.getDate()));
//
////                                                 cWidget.setBackgroundColor(getResources().getColor(R.color.white));
//
//
//                                                 android.text.format.DateFormat df = new android.text.format.DateFormat();
//                                                 String date = DateFormat.format("dd-MM-yyyy", calendarDay.getDate()).toString();
//
//                                                 String displaydate = DateFormat.format("dd/MM/yyyy", calendarDay.getDate()).toString();
//
//                                                 String displayTime =getCurretTime("HH").concat("h");
//
//                                                 addEvents(date,getCurretTime("HH:mm:ss"),view,displaydate,displayTime);
//
//                                                 getEvents(date);
//                                             }
                                         }
        );



//        cWidget.setOnDateChangedListener(new OnDateChangedListener() {
//                                             @Override
//                                             public void onDateChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
//
//
////                                                 cWidget.setBackground(R.drawable.outline);
//
////                                                 cWidget.addDecorator(new EventDecorator(type2, this));
//
//                                                 try{
//                                                     Log.d("selected date decorator", String.valueOf(calendarDay.getDate()));
//
//                                                     cWidget.addDecorator(new EventDecorator1(calendarDay,getContext()));
//
////                                                     EventDecorator eventDecorator = new EventDecorator(calendarDay, event.getDate(), event.getColor());
////                                                     calendarDay.addDecorator(eventDecorator);
//                                                 }catch (ClassCastException exception){
//
//                                                     System.out.println("Exception" +exception.getMessage());
//                                                 }
//
//
//                                                 Log.d("selected date", String.valueOf(calendarDay.getDate()));
//                                                 Log.d("event date", String.valueOf(calendarDay.getDate()));
//
////                                                 cWidget.setBackgroundColor(getResources().getColor(R.color.white));
//
//
//                                                 android.text.format.DateFormat df = new android.text.format.DateFormat();
//                                                 String date = DateFormat.format("dd-MM-yyyy", calendarDay.getDate()).toString();
//
//                                                 String displaydate = DateFormat.format("dd/MM/yyyy", calendarDay.getDate()).toString();
//
//                                                 String displayTime =getCurretTime("HH").concat("h");
//
//                                                 addEvents(date,getCurretTime("HH:mm:ss"),view,displaydate,displayTime);
//
//                                                 getEvents(date);
//                                             }
//                                         }
//        );
//        cWidget.setShowOtherDates(true);

        Calendar calendar = Calendar.getInstance();
        cWidget.setSelectedDate(calendar.getTime());

        calendar.set(calendar.get(Calendar.YEAR), Calendar.JANUARY, 1);

        Calendar c = Calendar.getInstance();
        c.set(2010, 0, 1);
//        cWidget.setMinimumDate(c.getTime());

        calendar.set(calendar.get(Calendar.YEAR), Calendar.DECEMBER, 31);
        c.set(2021, 11, 31);
//        cWidget.setMaximumDate(c.getTime());


        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String date = DateFormat.format("dd-MM-yyyy", cWidget.getCurrentDate().getDate()).toString();


        System.out.println("Current date");

        Log.i("current date", String.valueOf(cWidget.getCurrentDate().getDate()));


        String d = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        getEvents(d);
//        requestFeeds(1, date);



    }


    private Drawable generateSelector() {
        Log.d(" drawable calling","called");
        StateListDrawable drawable = new StateListDrawable();
        drawable.setExitFadeDuration(200);
        drawable.addState(new int[]{android.R.attr.state_checked}, getResources().getDrawable(R.drawable.outline));
        drawable.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(R.drawable.camera));
        drawable.addState(new int[]{}, getResources().getDrawable(R.drawable.red_icon));
        return drawable;
    }

    public class EventDecorator implements DayViewDecorator {

        private final int color;
        private final HashSet<CalendarDay> dates;

        public EventDecorator(int color, Collection<CalendarDay> dates) {
            this.color = color;
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new DotSpan(5, color));
        }
    }



    private void requestFeeds(int mode, String date) {
        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        String url = "", json = "";
        if (mode == 1) {
            url = getString(R.string.server_url) + getString(R.string.calendarbyday_meth);
            json = "{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"day\":\"" + date + "\"}";
        } else if (mode == 2) {
            url = getString(R.string.server_url) + getString(R.string.calendarbymonth_meth);
            json = "{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"month\":\"" + date + "\"}";
        } else if (mode == 3) {
            url = getString(R.string.server_url) + getString(R.string.calendarbyyear_meth);
            json = "{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"year\":\"" + date + "\"}";
        }
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pubList = new ArrayList<>();
        lvMyPub.setAdapter(new MyAdsAdapter(getActivity(), pubList));
        JsonObjectRequest reqPub = new JsonObjectRequest(url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (response.getString("result").equals("true")) {
                                if (response.getString("Message").equals("")) {
                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            String visib = "0";
                                            visib = obj.getString("distance");
                                            String video_thumb = "";
                                            try {
                                                video_thumb = obj.getString("photo_video");
                                            } catch (Exception e) {
                                            }
                                            pubList.add(new WISAds(obj.getInt("id"), obj.getString("description"), obj.getString("titre"), obj.getInt("etat"), 0, visib, "", "", obj.getString("created_at"), obj.getString("photo"), obj.getString("type_obj"), false, false, obj.getString("startTime"), obj.getString("endTime"), video_thumb));

                                        } catch (Exception e) {
                                        }
                                    }
                                    lvMyPub.setAdapter(new MyAdsAdapter(getActivity(), pubList));
                                }
                                if (pubList.size() == 0) {
//                                    tvEmpty.setVisibility(View.VISIBLE);
                                } else {
//                                    tvEmpty.setVisibility(View.GONE);
                                }
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }



//    Events View


//    Add Events


//    public void addEvents(final String selectedDate, final String selectedTime, final View v,String displaydate,String displayTime) {
//        // StartAction
//
//
//        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // inflate the custom popup layout
//        final View inflatedView = layoutInflater.inflate(R.layout.event_dialogue, null, false);
//
//
//        // get device size
//        Display display = getActivity().getWindowManager().getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
////        mDeviceHeight = size.y;
//
//
//        // set height depends on the device size
//        popWindow = new PopupWindow(inflatedView, size.x - 70, size.y - 450, true);
//
//
//        popWindow.setTouchable(true);
//
//
//        popWindow.setOutsideTouchable(false);
//
//        popWindow.setAnimationStyle(R.style.animationName);
//
////        new SlideInAnimation(popWindow.getContentView()).setDirection(com.easyandroidanimations.library.Animation.DIRECTION_UP)
////                .animate();
////        this.view.setAlpha(0.4f);
//
//
////        getActivity().getV
//
//
////        final View views = this.view;
//
//        // show the popup at bottom of the screen and set some margin at bottom ie,
//
//
//        view.setAlpha(0.5f);
//
//
//
//        popWindow.showAtLocation(v, Gravity.CENTER, 0, 80);
//
////
//
////
////        new AlertDialog.Builder(getActivity())
////                .setView(R.layout.write_status_popup)
////                .show();
//
//
//        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
//        post_status = (Button) inflatedView.findViewById(R.id.post_status);
//         message= (EditText) inflatedView.findViewById(R.id.eventMesage);
//
//        datelabel = (TextView)inflatedView.findViewById(R.id.datelabel);
//
//        datelabel.setText(displaydate.concat(" ").concat(displayTime));
//
//
//
//
////        clear previous data
//
//
//
//
//
//
//        close_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popWindow.dismiss();
//                view.setAlpha(1);
//            }
//        });
//
//
//
//        post_status.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if (!message.getText().toString().equals("")) {
//                    popWindow.dismiss();
//
//                    view.setAlpha(1);
//
//
//                    sendEventsToServer(message.getText().toString(),selectedDate,selectedTime);
//
//
//
//                }
//
//
//            }
//        });
//
//
//
//    }


    public void addEvents(final String selectedDate, final String selectedTime, final View v,String displaydate,String displayTime) {
        // StartAction


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.event_dialogue, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 70, size.y - 450, true);


        popWindow.setTouchable(true);


        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);

//        new SlideInAnimation(popWindow.getContentView()).setDirection(com.easyandroidanimations.library.Animation.DIRECTION_UP)
//                .animate();
//        this.view.setAlpha(0.4f);


//        getActivity().getV


//        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,


        view.setAlpha(0.5f);



        popWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

//

//
//        new AlertDialog.Builder(getActivity())
//                .setView(R.layout.write_status_popup)
//                .show();


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);
        message= (EditText) inflatedView.findViewById(R.id.eventMesage);
        headerTitleTVPopup =(TextView)inflatedView.findViewById(R.id.headerTitleTVPopup);
        datelabel = (TextView)inflatedView.findViewById(R.id.datelabel);
        dateLabelHeaderTV =(TextView) inflatedView.findViewById(R.id.dateLabelHeaderTV);
        datelabel.setText(displaydate.concat(" ").concat(displayTime));




//        clear previous data


        try{
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            post_status.setTypeface(font);
            message.setTypeface(font);
            datelabel.setTypeface(font);
            headerTitleTVPopup.setTypeface(font);
            dateLabelHeaderTV.setTypeface(font);

        }catch(NullPointerException ex){
            Log.e("Exception" ,ex.getMessage());
        }





        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                view.setAlpha(1f);
            }
        });



        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!message.getText().toString().equals("")) {
                    popWindow.dismiss();

                    view.setAlpha(1f);


                    sendEventsToServer(message.getText().toString(),selectedDate,selectedTime);



                }


            }
        });



    }


//    private List<Events> loadMyEvents(Date date) {
//        // TODO Load events from Date and return list
//        return events;
//    }


    public void showProgress(int status){


        if(status==1){

            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        else{

            view.findViewById(R.id.loading).setVisibility(View.GONE);
        }


    }

    public void sendEventsToServer(String message,String date,String time) {


        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);


        String urls = getString(R.string.server_url) + getString(R.string.addEventUrl);

        JSONObject json = new JSONObject();

        try {
            json.put("user_id", Tools.getData(getActivity(), "idprofile"));

            json.put("event_date", date);
            json.put("event_time", time);
            json.put("description",message);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.i("parms", String.valueOf(json));

        JsonObjectRequest reqEvents = new JsonObjectRequest(urls, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        Log.i("events", response.toString());

                        eventsData.clear();

                        try {
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject jsonObject = data.getJSONObject(i);


                                eventsData.add(new WisEvents(jsonObject.getInt("id"), jsonObject.getString("eventDate"), jsonObject.getString("eventTime"), jsonObject.getString("comments"), jsonObject.getString("status")));
                            }


                            adapter = new EventsAdapter(getActivity(), eventsData,FragCalendar.this);

                            lvMyPub.setAdapter(adapter);

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqEvents);


    }

    public class EventDecorator1 implements DayViewDecorator {

        public  HashSet<CalendarDay> dates;
        public  Drawable drawable1;

        public EventDecorator1(Collection<CalendarDay> dates, Context context) {
            this.dates = new HashSet<>(dates);
            drawable1 = ContextCompat.getDrawable(context, R.drawable.outline);

            Log.d("drawable", String.valueOf(drawable1));
        }
        @Override
        public boolean shouldDecorate(CalendarDay day) {

            return dates.contains(day);
        }
        @Override
        public void decorate(DayViewFacade view) {

            view.setBackgroundDrawable(drawable1);
        }
    }




    public class EventDecorator3 implements DayViewDecorator {

//        public  HashSet<CalendarDay> dates;
        public CalendarDay calendarDay;
        public  Drawable drawable1;

        public EventDecorator3(CalendarDay calendarDay, Context context) {
            this.calendarDay = calendarDay;
            drawable1 = ContextCompat.getDrawable(context, R.drawable.outline);

            Log.d("drawable", String.valueOf(drawable1));
        }
        @Override
        public boolean shouldDecorate(CalendarDay calendarDay) {

            return true;
        }
        @Override
        public void decorate(DayViewFacade view) {

            view.setBackgroundDrawable(drawable1);
        }
    }


    public class EventDecorator4 implements DayViewDecorator {

        //        public  HashSet<CalendarDay> dates;
        public String calendarDay;
        public  Drawable drawable1;

        public EventDecorator4(String calendarDay, Context context) {
            this.calendarDay = calendarDay;
            drawable1 = ContextCompat.getDrawable(context, R.drawable.outline);

            Log.d("drawable", String.valueOf(drawable1));
        }
        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return false;
        }

        @Override
        public void decorate(DayViewFacade view) {

            view.setBackgroundDrawable(drawable1);
        }
    }
    public class EventDecorator2 implements DayViewDecorator {

        private final Drawable drawable;
        private final CalendarDay day;
        private final int color;

        public EventDecorator2(MaterialCalendarView view, Date date, int color) {
            this.day = CalendarDay.from(date);
            this.color = color;
            this.drawable = createTintedDrawable(view.getContext(), color);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            if (this.day.equals(day)) {
                return true;
            }
            return false;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setSelectionDrawable(drawable);
        }

        private  Drawable createTintedDrawable(Context context, int color) {
            return applyTint(createBaseDrawable(context), color);
        }

        private  Drawable applyTint(Drawable drawable, int color) {
            Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(wrappedDrawable, color);
            return wrappedDrawable;
        }

        private  Drawable createBaseDrawable(Context context) {
            return ContextCompat.getDrawable(context, R.drawable.outline);
        }
    }


//    public void  addEvents(String selectedDate){
//
//
////        new CustomBaseDialog(getActivity()).showAnim(new FadeEnter()).show();
//
//
//
//
//
//    }


//    get Events


    public void getEvents(String selectedDate){



        String urls = getString(R.string.server_url) +getString(R.string.getEvent);

        JSONObject json = new JSONObject();


        try {
            json.put("user_id",Tools.getData(getActivity(),"idprofile"));

            json.put("date",selectedDate);



        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.i("parms", String.valueOf(json));

        JsonObjectRequest reqEvents = new JsonObjectRequest(urls, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        Log.i("events",response.toString());

                        eventsData.clear();

//                        List<WisEvents> eventsList = new ArrayList<>();

                        try {
                            JSONArray data = response.getJSONArray("data");

                            for(int i= 0;i<data.length();i++){

                                JSONObject jsonObject = data.getJSONObject(i);



                                eventsData.add(new WisEvents(jsonObject.getInt("id"),jsonObject.getString("eventDate"),jsonObject.getString("eventTime"),jsonObject.getString("comments"),jsonObject.getString("status")));
                            }


                            adapter = new EventsAdapter(getActivity(),eventsData,FragCalendar.this);

                            lvMyPub.setAdapter(adapter);

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqEvents);



    }

    public void removeEvents(final int index, String eventId){


        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);


        String urls = getString(R.string.server_url) + getString(R.string.delEvent);

        JSONObject json = new JSONObject();

        try {
            json.put("user_id", Tools.getData(getActivity(), "idprofile"));

            json.put("event_id", eventId);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.i("parms", String.valueOf(json));

        JsonObjectRequest reqEvents = new JsonObjectRequest(urls, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                        try {
                            JSONObject object = new JSONObject(response.toString());

                            if(object.getInt("data")==1){


                                eventsData.remove(index);

                                Log.i("events results ", String.valueOf(response));
//                                if(eventsData.size() == 1){
//
//                                    eventsData.clear();
//                                }
//
//                                else {
//
//                                    eventsData.remove(index);
//
//
//                                }

                                adapter.notifyDataSetChanged();

                                Log.i("events size ", String.valueOf(eventsData.size()));


                            }else{

                                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }





                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqEvents);



    }

    @Override
    public void deleteEventSucess(final int position, final String eventId) {

        Log.i("int pos", String.valueOf(position));





       AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.msg_confirm_delete_post))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.i("remove pos", String.valueOf(position));
                        removeEvents(position,eventId);
                    }

                })
                .setNegativeButton(getString(R.string.no), null)
                .show();



        try{
            TextView textView = (TextView) dialog.findViewById(android.R.id.message);
            Typeface face=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            button1.setTypeface(face);
            Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            textView.setTypeface(face);
            button2.setTypeface(face);
            textView.setTextSize(20);//to change font size
            button1.setTextSize(20);
            button2.setTextSize(20);
        }catch (NullPointerException ex){
            Log.e("Exception",ex.getMessage());
        }

    }

}




