package com.oec.wis.fragments;

import android.app.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.NotifAdapter;
import com.oec.wis.adapters.NotificationDeleteListener;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISNotif;
import com.oec.wis.tools.Tools;
import com.oec.wis.wisdirect.WisDirect;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragNotif extends Fragment implements NotificationDeleteListener {
    View view;
    List<WISNotif> notifs;
    ListView lvNotif;
    TextView headerTitleTV;
    CircularImageView userNotificationView;
    TextView notificationCount;
    Button deleteAll;
    ProgressDialog dialog;
    NotifAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            Tools.showLoader(getActivity(),getResources().getString(R.string.progress_loading));
            view = inflater.inflate(R.layout.frag_notif, container, false);

            initControls();
            setListener();
            loadNotifs();

        } catch (InflateException e) {
        }
        return view;
    }

    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
        userNotificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Tools.callDialog(getActivity());
            }
        });

        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notifs.size() >0){
                    deleteDialog(null,-1,"Yes");
                }else{
                    Log.e("else","notify call");
                }


            }
        });


//        lvNotif.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//           @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//               Log.e("Calling","Notify");
//               WISNotif notif = notifs.get(position);
//               updateNotify(String.valueOf(notif.getId()));
//               if(notif.getType().equals("wis_direct")){
//                  Intent direct = new Intent(getActivity(), WisDirect.class);
//                   direct.putExtra("is_publisher","YES");
//                   startActivity(direct);
//
//                  }
//               }
//            });
    }

    private void updateNotify(String directId) {
        notifs = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
            jsonBody.put("direct_id",directId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("Jsonbody", String.valueOf(jsonBody));
        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.updateDirect), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Notificatiin", String.valueOf(response));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                Tools.dismissLoader();
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }


    private void initControls() {
        lvNotif = (ListView) view.findViewById(R.id.lvNotif);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        deleteAll =(Button) view.findViewById(R.id.deleteAll);
        dialog= new ProgressDialog(getActivity());
        adapter = new NotifAdapter(getActivity(),notifs);




        notificationCount = (TextView) view.findViewById(R.id.target_view);
        userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);


        try{
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
            headerTitleTV.setTypeface(font);
            deleteAll.setTypeface(font);
        }catch (NullPointerException ex){

        }


        downloadProfileImage();
        if(new UserNotification().getBatchcount()==0)
        {
            notificationCount.setVisibility(View.INVISIBLE);
        }
        else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notificationCount.setText(String.valueOf(new UserNotification().getBatchcount()));

                }
            });
        }
    }

    private void downloadProfileImage() {
        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(userNotificationView);

    }

    private void loadNotifs() {
        notifs = new ArrayList<>();

//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("Jsonbody", String.valueOf(jsonBody));
        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.notifhisto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("response==>", String.valueOf(response));

                        try {
                            if (response.getString("result").equals("true")) {

                                Log.d("response DATA==>", String.valueOf(response));
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);

                                        notifs.add(new WISNotif(obj.getInt("id_pub"), obj.getString("title"), obj.getString("desc"), obj.getString("sent_at"), obj.getString("photo"),obj.getString("type")));
                                    } catch (Exception e) {
                                    }
                                }
                                lvNotif.setAdapter(new NotifAdapter(getActivity(), notifs));
                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Tools.dismissLoader();

                        } catch (JSONException e) {
                            Tools.dismissLoader();

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Tools.dismissLoader();
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }
    private void deleteDialog(final String notifid, final int position, final String is_deleteAll) {
        new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.msg_confirm_delete_post))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAllNotif(notifid,position,is_deleteAll);
                    }

                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }

    private void deleteAllNotif(String notifid, final int position,String is_deleteAll) {

        if(notifid == null)
            notifid = "";


        dialog.setMessage("Please wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("id_notif",notifid);
            jsonBody.put("is_deleteAll",is_deleteAll);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("delete res", String.valueOf(jsonBody));

        Log.d("delete postion", String.valueOf(position));
        JsonObjectRequest reqDelete = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.deletenotif_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            if (response.getBoolean("result")) {

//                                if(position==-1){
//                                    notifs.clear();
//                                }
//                                else{
//
//                                }

                                notifs.clear();
                                adapter.notifyDataSetChanged();

                                if(notifs.size()>0){

                                    deleteAll.setVisibility(View.VISIBLE);
                                }
                                else{

                                    deleteAll.setVisibility(View.INVISIBLE);
                                }

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //if (context != null)
                Toast.makeText(getActivity(),getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void updateDeleteResponse(int position, int notifyid) {


        deleteDialog(String.valueOf(notifyid),position,"No");

    }

}