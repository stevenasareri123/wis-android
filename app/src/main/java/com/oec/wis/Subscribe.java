package com.oec.wis;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.adapters.PopupadApterText;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.entities.PopupElementsText;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.WISLanguage;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class Subscribe extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    EditText etName, etEmail, etPwd;
    CircularImageView ivPhoto;
    String photoPath = "";
    int position;
    DialogPlus dialog;
    RelativeLayout companyRL,langRL;
    TextView headerTitleTV,companyTV,langTV;
    ImageView  companyIV,langIV;
    TextView tvPhone;
    Button b_subs;
    private static final int RC_LOCATION_CONTACTS_PERM = 124;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        loadControls();
        loadListener();
        checkPermissions();
        setTypeFace();
    }


    private void loadControls() {
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPwd = (EditText) findViewById(R.id.etPwd);
        ivPhoto = (CircularImageView) findViewById(R.id.ivPhoto);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
        companyRL=(RelativeLayout)findViewById(R.id.companyRL);
        companyTV=(TextView)findViewById(R.id.companyTV);
        companyIV=(ImageView)findViewById(R.id.companyIV);
        langRL=(RelativeLayout)findViewById(R.id.langRL);
        langTV=(TextView)findViewById(R.id.langTV);
        langIV=(ImageView)findViewById(R.id.langIV);
        tvPhone=(TextView)findViewById(R.id.tvPhone);
        b_subs=(Button)findViewById(R.id.b_subs);
    }


    private void setTypeFace() {
        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
        etName.setTypeface(font);
        etEmail.setTypeface(font);
        etPwd.setTypeface(font);
        companyTV.setTypeface(font);
        langTV.setTypeface(font);
        tvPhone.setTypeface(font);
        b_subs.setTypeface(font);
    }


    private void loadListener() {


        langRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langDropDown();
            }
        });
        langTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langDropDown();
            }
        });
        langIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langDropDown();
            }
        });
        companyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyDropDown();
            }
        });
        companyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyDropDown();
            }
        });
        companyIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyDropDown();
            }
        });
        findViewById(R.id.b_subs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    //switchActivity(position);

                    switchActivity(companyTV.getText().toString());

                }
            }
        });
        View.OnClickListener pickPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        };
        ivPhoto.setOnClickListener(pickPhoto);
        findViewById(R.id.tvPhone).setOnClickListener(pickPhoto);
    }

    private void langDropDown() {

        final ArrayList popupList = new ArrayList();

        String[] menuArray;

        menuArray = getResources().getStringArray(R.array.country_list);
        for(int i=0;i<menuArray.length;i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(menuArray[i]));
        }




        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            langTV.setText(list.get(position).getTitle());
                            langTV.setTextColor(getResources().getColor(R.color.black));
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }


    private void companyDropDown() {


        final ArrayList popupList = new ArrayList();

        String[] menuArray;

        menuArray = getResources().getStringArray(R.array.new_orga_list);
        for(int i=0;i<menuArray.length;i++){
            //popupList.add(menuArray[i]);
            popupList.add(new PopupElementsText(menuArray[i]));
        }



        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {


                        ArrayList<PopupElementsText> list = popupList;

                        for(int i = 0; i < list.size(); i++)
                        {

                            dialog.dismiss();
                            companyTV.setText(list.get(position).getTitle());
                            companyTV.setTextColor(getResources().getColor(R.color.black));
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bitmap sImage = BitmapFactory.decodeStream(imageStream);
                    ivPhoto.setImageBitmap(sImage);

                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = managedQuery(selectedImage, proj, null, null,
                            null);
                    final int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    photoPath = cursor.getString(column_index);
                }
                break;
            default:
                break;
        }
    }



    private void switchActivity(String type) {
        Intent intent = null;
        //String lang = WISLanguage.fr_FR.toString();
        String lang = "";

        if(type.equals(getString(R.string.new_title_campany)))
        {
            intent = new Intent(getApplicationContext(), SubsCampany.class);
            lang = WISLanguage.fr_FR.toString();
            Log.e("calling","SubsCampany");
        }
        else if(type.equals(getString(R.string.new_particular)))
        {
            intent = new Intent(getApplicationContext(), SubsParticular.class);
            lang = WISLanguage.en_US.toString();
            Log.e("calling","SubsParticular");
        }
        else
        {
            intent = new Intent(getApplicationContext(), SubsAsso.class);
            lang = WISLanguage.es_ES.toString();
            Log.e("calling","SubsAsso");
        }
//        switch (type) {
//            case "ENTERPRISE":
//                i = new Intent(getApplicationContext(), SubsCampany.class);
//                lang = WISLanguage.fr_FR.toString();
//                Log.e("calling","SubsCampany");
//                break;
//            case "PARTICULAR":
//                i = new Intent(getApplicationContext(), SubsParticular.class);
//                lang = WISLanguage.en_US.toString();
//                Log.e("calling","SubsParticular");
//                break;
//            case "ASSOCIATIONS/ORGANIZATIONS/OTHERS":
//                i = new Intent(getApplicationContext(), SubsAsso.class);
//                lang = WISLanguage.es_ES.toString();
//                Log.e("calling","SubsAsso");
//                break;
//        }
        if (intent != null) {
            intent.putExtra("name", etName.getText().toString());
            intent.putExtra("email", etEmail.getText().toString());
            intent.putExtra("pwd", etPwd.getText().toString());
            intent.putExtra("lang", lang);
            intent.putExtra("photo", photoPath);
            startActivity(intent);
            Log.e("calling","Going");
        }
    }
    private boolean check() {
        boolean check = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            check = false;
            etName.setError(getString(R.string.msg_empty_name));
        }
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            check = false;
            etEmail.setError(getString(R.string.msg_empty_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            check = false;
            etEmail.setError(getString(R.string.msg_invalid_email));
        }
        if (TextUtils.isEmpty(etPwd.getText().toString())) {
            check = false;
            etPwd.setError(getString(R.string.msg_empty_pwd));
        }
        return check;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        Toast.makeText(this, "onPermissionsGranted", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {


        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
//            new AppSettingsDialog.Builder(((Activity)context).build().show();
        }
    }

    @AfterPermissionGranted(RC_LOCATION_CONTACTS_PERM)
    public void checkPermissions() {
        String[] perms = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_CONTACTS, android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.INTERNET};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Have permissions, do the thing!
//            Toast.makeText(getActivity(), "TODO: Location and Contacts things", Toast.LENGTH_LONG).show();
        } else {
            // Ask for both permissions
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again),
                    RC_LOCATION_CONTACTS_PERM, perms);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
