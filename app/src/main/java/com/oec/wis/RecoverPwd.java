package com.oec.wis;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RecoverPwd extends AppCompatActivity {
    EditText etEmail;
    Button bSend;
    TextView headerTitleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_pwd);
        loadControls();
        setListener();
        setTypeFace();
    }



    private void loadControls() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        bSend = (Button) findViewById(R.id.bSend);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
    }
    private void setTypeFace() {

        Typeface font=Typeface.createFromAsset(RecoverPwd.this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
        etEmail.setTypeface(font);
        bSend.setTypeface(font);
    }
    private void setListener() {
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkData())
                    sendEmail(etEmail.getText().toString());
            }
        });
//        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }

    private void sendEmail(String email) {

        Log.i("server url",getString(R.string.server_url) + getString(R.string.recov_meth));
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        disableControls();

        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"email\":\"" + email + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqRecover = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.recov_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                if (response.getString("Message").contains("Invalid Mail"))
                                    Toast.makeText(getApplicationContext(), getString(R.string.msg_invalid_email), Toast.LENGTH_SHORT).show();
                                else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.msg_email_sent), Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.msg_invalid_email), Toast.LENGTH_SHORT).show();
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            enableControls();
                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            enableControls();
                            //Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                enableControls();
            }
        });
        ApplicationController.getInstance().addToRequestQueue(reqRecover);
    }

    private void disableControls() {
        etEmail.setEnabled(true);
        bSend.setEnabled(true);
    }

    private void enableControls() {
        etEmail.setEnabled(true);
        bSend.setEnabled(true);
    }

    private boolean checkData() {
        boolean check = true;
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            check = false;
            etEmail.setError(getString(R.string.msg_empty_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            check = false;
            etEmail.setError(getString(R.string.msg_invalid_email));
        }
        return check;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
