package com.oec.wis.singlecall;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.CountryAdapter;
import com.oec.wis.chatApi.CallListener;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.entities.WISCountry;
import com.oec.wis.fragments.FragHome;
import com.oec.wis.tools.Tools;
import com.opentok.android.OpentokError;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.SubscriberKit;
import com.tokbox.android.accpack.OneToOneCommunication;
import com.tokbox.android.logging.OTKAnalytics;
import com.tokbox.android.logging.OTKAnalyticsData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class SingleCallMainActivity extends AppCompatActivity implements SubscriberKit.SubscriberListener,Session.SessionListener,CallListener,OneToOneCommunication.Listener, PreviewControlFragment.PreviewControlCallbacks, RemoteControlFragment.RemoteControlCallbacks, PreviewCameraFragment.PreviewCameraCallbacks {
    private final String LOGTAG = SingleCallMainActivity.class.getSimpleName();

    private final String[] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA};
    private final int permsRequestCode = 200;

    //OpenTok calls
    private OneToOneCommunication mComm;

    private RelativeLayout mPreviewViewContainer;
    private RelativeLayout mRemoteViewContainer;
    private RelativeLayout mAudioOnlyView;
    private RelativeLayout mLocalAudioOnlyView;
    private RelativeLayout.LayoutParams layoutParamsPreview;
    private TextView mAlert;
    private ImageView mAudioOnlyImage;

    ImageView subscriberFlagIV;



    //UI fragments
    private PreviewControlFragment mPreviewFragment;
    private RemoteControlFragment mRemoteFragment;
    private PreviewCameraFragment mCameraFragment;
    private FragmentTransaction mFragmentTransaction;

    private OTKAnalyticsData mAnalyticsData;
    private OTKAnalytics mAnalytics;

    private boolean mAudioPermission = false;
    private boolean mVideoPermission = false;

    public static boolean makeRejectCall=false;

    private String currentChannel;


    public static String receiverFirstName;
    public static Activity context;
    public static  String receiverImage;

    String senderId;

    int fId;

    int receiverId;

    String  receverStringId;


    Chronometer myChronometer;

    ProgressDialog pd;


    Bundle currentsavedInstanceState;

    Boolean is_group;
    TextView headerTitileTV;
    Typeface font;


    RelativeLayout subscriberCountryRL,publisherCountryRL;
    TextView subscriberCountryNameTV,publisherCountryNameTV;
    LinearLayout dividerView;
    ImageView backButton,cameraBtn;



    @Override
    public void connectStreamingToOwner() {

        Log.d("SIngleCall","Now Start Video");


        ApplicationController.getInstance().stopPreviewScreenTimer();
        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                connectStreaming();
            }
        });


    }

    @Override
    public void groupCallAcceptedFromReceiver(String receiverId) {

    }



    @Override
    public void onBackPressed(){

        showAlert();
    }

    public void showAlert(){

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        new AlertDialog.Builder(this)
                .setTitle("Wis")
                .setMessage("Are you sure you want to exit this session?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        ChatApi.getInstance().stopRingTone();
                        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

//                        disconnectSession();

                        disConnectRemoteViewSession();

//                        if(hasAdmin){
//
//                            makePublisherCalsForStopVideo();
//                        }else{
//
//
//                        }




                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOGTAG, "onCreate");

        this.currentsavedInstanceState= savedInstanceState;


        ChatApi.getInstance().setPublisherContext(this);

        ChatApi.getInstance().setCallListener(this);

        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

        ApplicationController.getInstance().isInputEventOccurred = false;


        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_single_call_main);


        String OwnerId=Tools.getData(this, "idprofile");

        String senderId = Tools.getData(this, "idprofile");

        Bundle b = getIntent().getExtras();


        backButton = (ImageView)findViewById(R.id.close);

//        cameraBtn = (ImageView)findViewById(R.id.camera);






        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAlert();
            }
        });

//
//        cameraBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onCameraSwap();
//            }
//        });


        try {
            headerTitileTV=(TextView)findViewById(R.id.phoneHeaderTitle);

            Typeface font= Typeface.createFromAsset(getAssets(),"fonts/Harmattan-R.ttf");

            headerTitileTV.setTypeface(font);

            subscriberCountryNameTV=(TextView)findViewById(R.id.subscriberCountryNameTV);
            publisherCountryNameTV=(TextView)findViewById(R.id.publisherCountryNameTV);

            dividerView=(LinearLayout)findViewById(R.id.dividerView);


            font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            headerTitileTV.setTypeface(font);

            subscriberCountryNameTV.setTypeface(font);
            publisherCountryNameTV.setTypeface(font);

            dividerView.setVisibility(View.INVISIBLE);
            subscriberCountryRL.setVisibility(View.INVISIBLE);
            publisherCountryRL.setVisibility(View.INVISIBLE);

        }catch (NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }


        if(b!=null){

            if(b.getString("senderID",null)!=null){

                senderId=b.getString("senderID");
                System.out.println("senderId  if "  + senderId);

            }else{

                senderId = Tools.getData(this, "idprofile");
                System.out.println("senderId  else "  + senderId);

            }
        }



        if(senderId.equals(OwnerId)){


            showLoader();
            initSession(this.currentsavedInstanceState);

        }else{

            System.out.println("OPENTOK API");

            Log.e("Session",ChatApi.getInstance().getOPENTOKSESSIONID());

            Log.e("token",ChatApi.getInstance().getOPENTOKTOKEN());

            Log.e("Api key",ChatApi.getInstance().getOPENTOKAPIKEY());


            connectSession(this.currentsavedInstanceState);
        }

    }


    public void initSession(final Bundle savedInstanceState){

        System.out.println("INITSession");


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                final JSONObject sessionResponse = Tools.createSessionAndToken(SingleCallMainActivity.this.getString(R.string.server_url)+SingleCallMainActivity.this.getString(R.string.create_opentok_session),Tools.getData(SingleCallMainActivity.this, "idprofile"),Tools.getData(SingleCallMainActivity.this, "token"));


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {



                        if(sessionResponse!=null){

                            dismissLoader();

                            try {
                                if(sessionResponse.getBoolean("result")){

                                    JSONObject opentok_response = sessionResponse.getJSONObject("data");

                                    ChatApi.getInstance().setOPENTOKSESSIONID(opentok_response.getString("session_id"));

                                    ChatApi.getInstance().setOPENTOKTOKEN(opentok_response.getString("token"));

                                    ChatApi.getInstance().setOPENTOKAPIKEY(opentok_response.getString("api_key"));



                                    connectSession(savedInstanceState);






                                }else{

                                    dismissLoader();

                                    Toast.makeText(SingleCallMainActivity.this,"Unable to Create session Please try again",Toast.LENGTH_SHORT);
                                }
                            } catch (JSONException e) {

                                dismissLoader();
                                Toast.makeText(SingleCallMainActivity.this,"Unable to Create session Please try again",Toast.LENGTH_SHORT);
                                e.printStackTrace();
                            }
                        }
                        else{

                            dismissLoader();

                            Toast.makeText(SingleCallMainActivity.this,"Unable to Create session Please try again",Toast.LENGTH_SHORT);
                        }

                    }
                });
            }
        });


    }


    public void connectSession(Bundle savedInstanceState){

        //init 1to1 communication object
        mComm = new OneToOneCommunication(SingleCallMainActivity.this, ChatApi.getInstance().getOPENTOKSESSIONID(), ChatApi.getInstance().getOPENTOKTOKEN(), ChatApi.getInstance().getOPENTOKAPIKEY());
        mComm.setSubscribeToSelf(OpenTokConfig.SUBSCRIBE_TO_SELF);

        //set listener to receive the communication events, and add UI to these events
        mComm.setListener(this);



        //Init the analytics logging for internal use
        String source = this.getPackageName();

        SharedPreferences prefs = this.getSharedPreferences("opentok", Context.MODE_PRIVATE);
        String guidVSol = prefs.getString("guidVSol", null);
        if (null == guidVSol) {
            guidVSol = UUID.randomUUID().toString();
            prefs.edit().putString("guidVSol", guidVSol).commit();
        }

        mAnalyticsData = new OTKAnalyticsData.Builder(OpenTokConfig.LOG_CLIENT_VERSION, source, OpenTokConfig.LOG_COMPONENTID, guidVSol).build();
        mAnalytics = new OTKAnalytics(mAnalyticsData);

        //add INITIALIZE attempt log event
        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_ATTEMPT);


        mPreviewViewContainer = (RelativeLayout) findViewById(R.id.publisherview);
        mRemoteViewContainer = (RelativeLayout) findViewById(R.id.subscriberview);
        mAlert = (TextView) findViewById(R.id.quality_warning);
        mAudioOnlyView = (RelativeLayout) findViewById(R.id.audioOnlyView);
        mLocalAudioOnlyView = (RelativeLayout) findViewById(R.id.localAudioOnlyView);
        subscriberCountryRL=(RelativeLayout)findViewById(R.id.subscriberCountryRL);
        publisherCountryRL=(RelativeLayout)findViewById(R.id.publisherCountryRL);

        //request Marshmallow camera permission
        //request Marshmallow camera permission
        if (ContextCompat.checkSelfPermission(this,permissions[1]) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,permissions[0]) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, permsRequestCode);
            }
        }
        else {
            mVideoPermission = true;
            mAudioPermission = true;
        }



//        if(mVideoPermission&&mAudioPermission){
//
//            showLoader();
//
//            initSession();
//        }


        //init controls fragments
        if (savedInstanceState == null) {
            mFragmentTransaction = getSupportFragmentManager().beginTransaction();
            initCameraFragment(); //to swap camera
            initPreviewFragment(); //to enable/disable local media
            initRemoteFragment(); //to enable/disable remote media
            mFragmentTransaction.commitAllowingStateLoss();
        }

        //add INITIALIZE attempt log event
        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_SUCCESS);


//        get current channel

        Bundle b = getIntent().getExtras();

        if(b!=null){

            currentChannel = b.getString("actual_channel");

            ApplicationController.getInstance().setUserCurrentChannel(currentChannel);

            senderId =  b.getString("senderID");


            receverStringId =b.getString("receiverId");




            if(b.getString("activate_reject_btn",null)!=null){

                if((b.getString("activate_reject_btn").equals("YES"))){

                    makeRejectCall = true;

                }else{

                    makeRejectCall  = false;
                }


            }





            Log.d("receverStringId::", receverStringId);

            initObjects();
        }


//        addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//        mComm.start();


//        setEnabled(mActivity.getComm().isStarted());




        if (mPreviewFragment != null) {

            Log.d("on video enabled","video updated");
            mPreviewFragment.setEnabled(true);
        }


    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        System.out.println("configuration changed" +  "calling");

        if (mCameraFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(mCameraFragment).commit();
            initCameraFragment();


        }

        if (mPreviewFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(mPreviewFragment).commit();
            initPreviewFragment();
        }

        if (mRemoteFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(mRemoteFragment).commit();
            initRemoteFragment();
        }

        if (mComm != null) {
            mComm.reloadViews(); //reload the local preview and the remote views
        }

    }

    @Override
    public void onRequestPermissionsResult(final int permsRequestCode, final String[] permissions,
                                           int[] grantResults) {
        switch (permsRequestCode) {

            case 200:
                mVideoPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                mAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;


                if ( !mVideoPermission || !mAudioPermission ){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(SingleCallMainActivity.this);
                    builder.setTitle(getResources().getString(R.string.permissions_denied_title));
                    builder.setMessage(getResources().getString(R.string.alert_permissions_denied));
                    builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            showLoader();

                            initSession(currentsavedInstanceState);

                        }
                    });
                    builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permissions, permsRequestCode);
                            }
                        }
                    });
                    builder.show();
                }

                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d("ONSTop","VIDEO MAIN ACTIVITY");

        clearVideoSession();

    }


    @Override
    protected void onPause() {
        super.onStop();

        Log.d("onPause","VIDEO MAIN ACTIVITY");

        clearVideoSession();

    }

    @Override
    protected void onDestroy() {
        Log.d("DES", "onDestroy");

        clearVideoSession();

        super.onDestroy();
    }

    private void initPreviewFragment() {

        System.out.println("fragmen"+ "Preview Fragment");
        Bundle bundle = getIntent().getExtras();
//        bundle.putString("edttext", "From Activity");
// set Fragmentclass Argumentso



        mPreviewFragment = new PreviewControlFragment();
        mPreviewFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.actionbar_preview_fragment_container, mPreviewFragment).commit();

        mPreviewFragment.setEnabled(true);

        mPreviewFragment.setCameraAction();




//        mControlCallbacks.onCall
    }


    public void initObjects(){


        if (getIntent().getExtras() != null) {
            fId = getIntent().getExtras().getInt("id");
            receiverFirstName = getIntent().getExtras().getString("receiver_name");
//            otherUserNameTv.setText(receiverFirstName);
            receiverImage = getIntent().getExtras().getString("image");
            currentChannel = getIntent().getExtras().getString("actual_channel");
            is_group = getIntent().getExtras().getBoolean("is_group");

//            receiverId =getIntent().getExtras().getInt("receiverId");

            System.out.println("Sender name"+getIntent().getExtras().getString("senderName"));


            System.out.println("receiverName===>" + receiverFirstName);
//            System.out.println("receiverimage===>" + receiverImage);
            System.out.println("group" + is_group);
            System.out.println("receiverId=====>" + fId);
            System.out.println("current channel=====>" + currentChannel);

        }
    }

    private void initRemoteFragment() {
        System.out.println("fragmen"    + "REmote Fragment");
        mRemoteFragment = new RemoteControlFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.actionbar_remote_fragment_container, mRemoteFragment).commit();


    }

    private void initCameraFragment() {

        Bundle bundle = getIntent().getExtras();
        mCameraFragment = new PreviewCameraFragment();
        mCameraFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.camera_preview_fragment_container, mCameraFragment).commit();





    }

    public OneToOneCommunication getComm() {
        return mComm;
    }

    //Local control callbacks
    @Override
    public void onDisableLocalAudio(boolean audio) {

        Log.d("Audio button","Clicked");
        if (mComm != null) {
            Log.d("inside","mComm");
            mComm.enableLocalMedia(OneToOneCommunication.MediaType.AUDIO, audio);
            onDisableRemoteAudio(audio);
        }
    }

    @Override
    public void onDisableLocalVideo(boolean video) {
        if (mComm != null) {
            mComm.enableLocalMedia(OneToOneCommunication.MediaType.VIDEO, video);

            Log.e("onDisableLocalVideo","onDisableLocalVideo");

            if (mComm.isRemote()) {
                if (!video) {
                    mAudioOnlyImage = new ImageView(this);
                    mAudioOnlyImage.setImageResource(R.drawable.avatar);

//                    mAudioOnlyImage.getLayoutParams().height = 150;
//
//                    mAudioOnlyImage.getLayoutParams().width = 150;
//
//                    mAudioOnlyImage.setScaleType(ImageView.ScaleType.FIT_XY);

                    mAudioOnlyImage.setBackgroundResource(R.drawable.bckg_audio_only);
                    mPreviewViewContainer.addView(mAudioOnlyImage);

                    Log.e("onDisableLocalVideo","mComm if");

//

                } else {
                    mPreviewViewContainer.removeView(mAudioOnlyImage);
                    Log.e("onDisableLocalVideo","mComm else");
                }
            } else {
                if (!video) {
                    mLocalAudioOnlyView.setVisibility(View.VISIBLE);
                    mPreviewViewContainer.addView(mLocalAudioOnlyView);
                    Log.e("onDisableLocalVideo","if");

                } else {
                    mLocalAudioOnlyView.setVisibility(View.GONE);
                    mPreviewViewContainer.removeView(mLocalAudioOnlyView);

                    Log.e("onDisableLocalVideo", "else");
                }
            }
        }
    }


    public Boolean isUserAvailable(){


        return false;
    }



    public void connectStreaming(){

        ChatApi.getInstance().stopRingTone();

        ApplicationController.getInstance().setCallConnected(Boolean.TRUE);
        if (mComm != null && mComm.isStarted()) {
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mComm.end();
            cleanViewsAndControls();
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
        } else {
            addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mComm.start();
//            start timer

//            setupTimer();

            ApplicationController.getInstance().startTimer(SingleCallMainActivity.this,"Amis", String.valueOf(receverStringId),currentChannel);

            if (mPreviewFragment != null) {
                mPreviewFragment.setEnabled(true);

            }

            if(mCameraFragment!=null){

                mCameraFragment.disableCustomPreview();
            }
        }
    }


    @Override
    public void onCall() {


//        connectSession(this.currentsavedInstanceState);



        makeRejectCall = true;

        ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);


//        showLoader();

//        initSession();






        Log.d("onCall","Single main activity");
        System.out.println("oncall" +"calling====>");

        subscriberCountryRL.setVisibility(View.VISIBLE);
        publisherCountryRL.setVisibility(View.VISIBLE);

//        ApplicationController.getInstance().setTimer();





//        check user status
//
//        if (mComm != null && mComm.isStarted()) {
//            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//            mComm.end();
//            cleanViewsAndControls();
//            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
//        } else {
//            addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//            mComm.start();
//
//
////            start timer
//
////            setupTimer();
//
//
//
//            if (mPreviewFragment != null) {
//                mPreviewFragment.setEnabled(true);
//
//            }
//
//            if(mCameraFragment!=null){
//
//                mCameraFragment.disableCustomPreview();
//            }
//        }
    }


    public void enableVideoToolOption(){

        if (mPreviewFragment != null) {
            mPreviewFragment.setEnabled(true);

            Toast.makeText(SingleCallMainActivity.this,"Video option tabbed",Toast.LENGTH_SHORT).show();

            System.out.println("onCall " + "else execute");


        }
    }


//        roopa changed+


//             if (mComm != null && mComm.isStarted()) {
//
//
////            end call
//
//                addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//                mComm.end();
//                cleanViewsAndControls();
//                addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
//                System.out.println("onCall "+ "if execute");
//
//        }
//        else {
////            start call
//            addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//            mComm.start();
//            if (mPreviewFragment != null) {
//                mPreviewFragment.setEnabled(true);
//                System.out.println("onCall "+ "else execute");
//            }
//        }
//    }


    public void customRejectCall(){

        if (mComm != null && mComm.isStarted()) {

//            end call
            ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mComm.end();
            cleanViewsAndControls();
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
            System.out.println("oncustomRejectCall " + "customRejectCall");

        }
    }

    public void clearVideoSession(){

//        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        Log.d("Show voic popup::", String.valueOf(ApplicationController.getInstance().isShowVoicePopup()));

        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

        ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

        ApplicationController.getInstance().stopPreviewScreenTimer();

        ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

//        ApplicationController.getInstance().setUserCurrentChannel("");

        if (mComm != null && mComm.isStarted()) {

            makeRejectCall = false;

            ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

//            end call

            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mComm.end();
            cleanViewsAndControls();
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
//            mCameraFragment.getView().
            myChronometer = (Chronometer)findViewById(R.id.chronometer);
            myChronometer.stop();
            myChronometer.setVisibility(View.GONE);

        }
    }

    @Override
    public void rejectCall() {

        Log.e("reject call" , "singl call main ");


//        ApplicationController.getInstance().stopTimer();


        disConnectRemoteViewSession();





    }

    @Override
    public void oncameraViewClicked() {

        Log.i("onCmaera view","clicked");

        onCameraSwap();

    }

    public void clearSession(){

        Log.e("makeRejectCall","Not access");

        ChatApi.getInstance().stopRingTone();

        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);



        ChatApi.getInstance().setOPENTOKSESSIONID("");

        ChatApi.getInstance().setOPENTOKTOKEN("");

        ChatApi.getInstance().setOPENTOKAPIKEY("");


        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        finish();
    }



    public void disConnectRemoteViewSession(){

        ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

        ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);


        Log.e("MAKE REJECT CALL STAUTS", String.valueOf(makeRejectCall));



        if(makeRejectCall) {

            ChatApi.getInstance().stopRingTone();

            ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);




            if (mComm != null && mComm.isStarted()) {




//            end call
                addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
                mComm.end();
                cleanViewsAndControls();
                addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
                System.out.println("onCall " + " reject call");


                Log.e("reject call", "main activity");

                Map<String, String> json = new HashMap<>();
                json.put("message", "RejectOnetoONE");
                json.put("Name", "RejectOnetoONE");
                json.put("actual_channel", currentChannel);

                json.put("ReceiverName", "dummy");

                json.put("senderID", senderId);
                json.put("receiverID", receverStringId);

                json.put("contentType", "text");

                json.put("senderName", Tools.getData(this, "name"));
//                        json.put("ReceiverName", receiverFirstName);
                json.put("chatType", "amis");
                json.put("isVideoCalling", "yes");
                json.put("type", "RejectOnetoONE");
                String senderPhoto = Tools.getData(this, "photo");
                json.put("senderImage", senderPhoto);
                json.put("ReceiverImage", senderPhoto);
                json.put("senderCountry", Tools.getData(this, "country"));
                json.put("ReceiverCountry", "India");


                json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
                json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
                json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());

                ChatApi.getInstance().sendMessage(json, currentChannel);

//            finish();


            } else {

                Log.d("CLEAR::", "VIDEO SESSION");


                Map<String, String> json = new HashMap<>();
                json.put("message", "RejectOnetoONE");
                json.put("Name", "RejectOnetoONE");
                json.put("actual_channel", currentChannel);

                json.put("ReceiverName", "dummy");

                json.put("senderID", senderId);
                json.put("receiverID", receverStringId);

                json.put("contentType", "text");

                json.put("senderName", Tools.getData(this, "name"));
                json.put("chatType", "amis");
                json.put("isVideoCalling", "yes");
                json.put("type", "RejectOnetoONE");
                String senderPhoto = Tools.getData(this, "photo");
                json.put("senderImage", senderPhoto);
                json.put("ReceiverImage", senderPhoto);
                json.put("senderCountry", Tools.getData(this, "country"));
                json.put("ReceiverCountry", "India");

                json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
                json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
                json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());

                ChatApi.getInstance().sendMessage(json, currentChannel);

//            finish();
            }

        }
        else{

            Log.e("makeRejectCall","Not access");

            ChatApi.getInstance().stopRingTone();

            ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);



            ChatApi.getInstance().setOPENTOKSESSIONID("");

            ChatApi.getInstance().setOPENTOKTOKEN("");

            ChatApi.getInstance().setOPENTOKAPIKEY("");


            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

            finish();

//            onBackPressed();


        }

        ApplicationController.getInstance().stopPreviewScreenTimer();
    }

    //Remote control callbacks
    @Override
    public void onDisableRemoteAudio(boolean audio) {
        if (mComm != null) {
            mComm.enableRemoteMedia(OneToOneCommunication.MediaType.AUDIO, audio);
        }
    }

    @Override
    public void onDisableRemoteVideo(boolean video) {
        if (mComm != null) {
            mComm.enableRemoteMedia(OneToOneCommunication.MediaType.VIDEO, video);
        }
    }



    public void showRemoteControlBar(View v) {
        if (mRemoteFragment != null && mComm.isRemote()) {
            mRemoteFragment.show();
        }
    }

    //Camera control callback
    @Override
    public void onCameraSwap() {

        Log.e("ON SWAP CAMERA CALLED","CAMERA");

        if (mComm != null) {
            mComm.swapCamera();
        }
    }

    //cleans views and controls
    private void cleanViewsAndControls() {


        Log.e("reStartFragment","SingleCallMain Activity");
        mPreviewFragment.restartFragment(true);
    }

    @Override
    public void onInitialized() {
        Log.i(LOGTAG, "OneToOne communication has been initialized.");

        addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
    }

    //OneToOneCommunication callbacks
    @Override
    public void onError(String error) {
        Log.i(LOGTAG, "Error: "+error);

        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        makeRejectCall = false;
        ApplicationController.getInstance().setCallConnected(Boolean.FALSE);
        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
        ApplicationController.getInstance().stopPreviewScreenTimer();
        mComm.end(); //end communication
        cleanViewsAndControls(); //restart views
    }

    @Override
    public void onQualityWarning(boolean warning) {
        Log.i(LOGTAG, "The quality has degraded");

        if (warning) { //quality warning
            mAlert.setBackgroundResource(R.color.quality_warning);
            mAlert.setTextColor(this.getResources().getColor(R.color.warning_text));
        } else { //quality alert
            mAlert.setBackgroundResource(R.color.quality_alert);
            mAlert.setTextColor(this.getResources().getColor(R.color.white));
        }
        mAlert.bringToFront();
        mAlert.setVisibility(View.VISIBLE);
        mAlert.postDelayed(new Runnable() {
            public void run() {
                mAlert.setVisibility(View.GONE);
            }
        }, 7000);
    }

    @Override
    public void onAudioOnly(boolean enabled) {
        if (enabled) {
            Log.i(LOGTAG, "Audio only is enabled");
            mAudioOnlyView.setVisibility(View.VISIBLE);
            publisherCountryRL.setVisibility(View.VISIBLE);
            subscriberCountryRL.setVisibility(View.VISIBLE);
        }
        else {
            Log.i(LOGTAG, "Audio only is disabled");
            mAudioOnlyView.setVisibility(View.GONE);
            publisherCountryRL.setVisibility(View.GONE);
            subscriberCountryRL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPreviewReady(View preview) {
        mPreviewViewContainer.removeAllViews();
        if (preview != null) {
            Log.i(LOGTAG, "The preview view is ready to be attached");
            layoutParamsPreview = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            if (mComm.isRemote()) {
//                layoutParamsPreview.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
//                        RelativeLayout.TRUE);
//                layoutParamsPreview.addRule(RelativeLayout.ALIGN_PARENT_LEFT,
//                        RelativeLayout.TRUE);
//                layoutParamsPreview.width = (int) getResources().getDimension(R.dimen.preview_width);
//                layoutParamsPreview.height = (int) getResources().getDimension(R.dimen.preview_height);
//                layoutParamsPreview.rightMargin = (int) getResources().getDimension(R.dimen.preview_rightMargin);
//                layoutParamsPreview.bottomMargin = (int) getResources().getDimension(R.dimen.preview_bottomMargin);


//                layoutParamsPreview.width = (int) getResources().getDimension(R.dimen.preview_width_new);
//                layoutParamsPreview.height = (int) getResources().getDimension(R.dimen.preview_height_new);
//                layoutParamsPreview.rightMargin = (int) getResources().getDimension(R.dimen.preview_rightMargin_new);
//                layoutParamsPreview.bottomMargin = (int) getResources().getDimension(R.dimen.preview_bottomMargin_new);

//                layoutParamsPreview
            }
            mPreviewViewContainer.addView(preview);
            if (!mComm.getLocalVideo()){
                onDisableLocalVideo(false);
            }
        }
    }
    @Override
    public void onConnected(Session session){

        Toast.makeText(this, "session connected", Toast.LENGTH_LONG).show();


    }

    @Override
    public void onDisconnected(Session session) {

        Toast.makeText(this, "session disconnected", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {

        Toast.makeText(this, "onStreamReceived", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {

        Toast.makeText(this, "onStreamDropped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {

        Toast.makeText(this, "onError opentokErro", Toast.LENGTH_LONG).show();
    }

    Boolean isSessionDropped = Boolean.FALSE;
    @Override
    public void onRemoteViewReady(View remoteView) {

        Log.i(LOGTAG, "update preview when a new participant joined to the communication.");

        //update preview when a new participant joined to the communication
        if (mPreviewViewContainer.getChildCount() > 0) {

            onPreviewReady(mPreviewViewContainer.getChildAt(0)); //main preview view
        }
        if (!mComm.isRemote()) {
            //clear views
            onAudioOnly(false);
            mRemoteViewContainer.removeView(remoteView);
            mRemoteViewContainer.setClickable(false);
            Toast.makeText(this, R.string.call_disconnect, Toast.LENGTH_LONG).show();

            if(!isSessionDropped){
                isSessionDropped = Boolean.TRUE;
                clearSession();
            }





        }
        else {
            Log.i(LOGTAG, "The remote view is ready to be attached.");
            //show remote view
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    this.getResources().getDisplayMetrics().widthPixels, this.getResources()
                    .getDisplayMetrics().heightPixels);

//            Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(),
//                    R.anim.up_from_bottom);
//            remoteView.setAnimation(animation1);
//            remoteView.startAnimation(animation1);


//            layoutParams
            mRemoteViewContainer.removeView(remoteView);
            mRemoteViewContainer.addView(remoteView);



            mRemoteViewContainer.setClickable(true);

            if(mPreviewFragment!=null){

                mPreviewFragment.setCameraAction();
            }




            ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);


            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
            ApplicationController.getInstance().setCallConnected(Boolean.TRUE);

            ApplicationController.getInstance().stopPreviewScreenTimer();



            makeRejectCall = true;

//            ApplicationController.getInstance().startCountdown();

              dividerView.setVisibility(View.VISIBLE);
              subscriberCountryRL.setVisibility(View.VISIBLE);

              publisherCountryRL.setVisibility(View.VISIBLE);
//            myChronometer.setFormat("H:MM:SS");

            if(mCameraFragment!=null) {
                try {
//                    mCameraFragment.getView()
                    myChronometer = (Chronometer)findViewById(R.id.chronometer);

                    myChronometer.setVisibility(View.VISIBLE);

                    myChronometer.setBase(SystemClock.elapsedRealtime());

                    myChronometer.setTextColor(Color.WHITE);

                    myChronometer.start();

                } catch (NullPointerException ex) {

                    Log.e("exception", ex.getLocalizedMessage());
                }
            }

        }

        if(mPreviewFragment!=null){

            mPreviewFragment.dismissLoader();
        }



    }

    @Override
    public void onReconnecting() {
        Log.i(LOGTAG, "The session is reconnecting.");
        Toast.makeText(this, R.string.reconnecting, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onReconnected() {
        Log.i(LOGTAG, "The session reconnected.");
        Toast.makeText(this, R.string.reconnected, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCameraChanged(int newCameraId) {
        Log.i(LOGTAG, "The camera changed. New camera id is: "+newCameraId);
    }

    private void addLogEvent(String action, String variation){
        if ( mAnalytics!= null ) {
            mAnalytics.logEvent(action, variation);
        }



    }




    //    show loader

    public void showLoader(){

        pd = new ProgressDialog(this);
        pd.setMessage("loading");
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){

            pd.dismiss();
        }
    }

    @Override
    public void onConnected(SubscriberKit subscriberKit) {

        Toast.makeText(this,"onConnected onConnected", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDisconnected(SubscriberKit subscriberKit) {

        Toast.makeText(this,"onDisconnected onDisconnected", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(SubscriberKit subscriberKit, OpentokError opentokError) {

        Toast.makeText(this,"onError onError", Toast.LENGTH_LONG).show();
    }
}

