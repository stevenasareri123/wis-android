package com.oec.wis.singlecall;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.chatApi.CallListener;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.chatApi.ChatListener;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.groupcall.GroupCallMainActivity;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class PreviewControlFragment extends Fragment {

    private static final String LOGTAG = PreviewControlFragment.class.getSimpleName();

    private SingleCallMainActivity mActivity;

    private RelativeLayout mContainer;

    View rootView;

    private ImageView mAudioBtn;
    public ImageView mVideoBtn;
    private ImageView mCallBtn;
    private ImageView buttonCallReject;

    private ImageView grenThumbIV;
    private PreviewControlCallbacks mControlCallbacks = previewCallbacks;
    ImageView redArrowIV,greenArrowIV,cameraView;






    String senderId,OwnerId,receiverFirstName,currentChannel,receiver_image;

    int receiverAmisId;

    String  receverStringId;

    Boolean is_group,isConnectedToStream=Boolean.FALSE;

    int fId;

    ProgressDialog pd;





    public interface PreviewControlCallbacks {

        public void onDisableLocalAudio(boolean audio);

        public void onDisableLocalVideo(boolean video);

        public void onCall();
        public void rejectCall();

        public void oncameraViewClicked();


    }

    private static PreviewControlCallbacks previewCallbacks = new PreviewControlCallbacks() {
        @Override
        public void onDisableLocalAudio(boolean audio) {

            Log.e("onDisableLocalAudio","Called here");
        }

        @Override
        public void onDisableLocalVideo(boolean video) {

            Log.e("onDisableLocalVideo","Called here");
        }

        @Override
        public void onCall() {

            Log.e("onCall","Called here");
        }

        @Override
        public void rejectCall() {


            Log.e("rejectCall","Called here");

            ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

        }

        @Override
        public void oncameraViewClicked() {

            Log.e("onCall","oncameraView");

        }
    };

    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.e("button click ","Called here");

            Log.e("button id::", String.valueOf(v.getId()));
            switch (v.getId()) {
                case R.id.localAudio:
                    updateLocalAudio();
                    break;

                case R.id.localVideo:


//                    mControlCallbacks.onDisableLocalVideo(true);
//                    mControlCallbacks.onDisableLocalAudio(false);

                    updateLocalVideo();
                    break;

                case R.id.call:
//                    updateCall();


                    Log.e("button Status::", String.valueOf(isConnectedToStream));

                    if(ApplicationController.getInstance().isNetworkConnected()) {

                        if (!isConnectedToStream) {



                            isConnectedToStream = Boolean.TRUE;

                            mControlCallbacks.onCall();

                            makePubCalls();


//                            JSONObject sessionResponse = Tools.createSessionAndToken(getActivity().getString(R.string.create_opentok_session),Tools.getData(getActivity(), "idprofile"),Tools.getData(getActivity(), "token"));
//
//                            if(sessionResponse!=null){
//
//                                try {
//                                    if(sessionResponse.getBoolean("result")){
//
//                                       JSONObject opentok_response = sessionResponse.getJSONObject("data");
//
//                                        ChatApi.getInstance().setOPENTOKSESSIONID(opentok_response.getString("session_id"));
//
//                                        ChatApi.getInstance().setOPENTOKTOKEN(opentok_response.getString("token"));
//
//                                        ChatApi.getInstance().setOPENTOKAPIKEY(opentok_response.getString("api_key"));
//
//
//                                        isConnectedToStream = Boolean.TRUE;
//
//                                        mControlCallbacks.onCall();
//
//                                        makePubCalls();
//
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }

//
                        }
                    }else{

                        Toast.makeText(getActivity(),"Check your Internet Connection",Toast.LENGTH_SHORT);

                    }




                    break;
                case R.id.buttonCallReject:

                    if(ApplicationController.getInstance().isNetworkConnected()) {

                        mControlCallbacks.rejectCall();

                    }
                    else{

                        Toast.makeText(getActivity(),"Check your Internet Connection",Toast.LENGTH_SHORT);

                    }



                    break;

                case R.id.swapCamera:

                    Log.e("CAMERAVIEw","CCCLICJED");

                    mControlCallbacks.oncameraViewClicked();

                    break;

            }
        }
    };



    public void parseValues(){

        OwnerId=Tools.getData(getActivity(), "idprofile");

        fId =  getArguments().getInt("id");

        Log.d("PARSED OBJEt::", String.valueOf(fId));

        receiverFirstName =  getArguments().getString("receiver_name");

        currentChannel =  getArguments().getString("actual_channel");

        ChatApi.getInstance().setUserCurrentChannel(currentChannel);

        is_group = getArguments().getBoolean("is_group");

//      receiverAmisId = getArguments().getInt("receiverId");


        receverStringId =getArguments().getString("receiverId");

        if(getArguments().getString("senderID",null)!=null){

            senderId=getArguments().getString("senderID");
            System.out.println("senderId  if "  + senderId);

        }else{

            senderId = Tools.getData(getActivity(), "idprofile");
            System.out.println("senderId  else "  + senderId);

        }

        if(getArguments().getString("receiver_image",null)!=null){

            receiver_image=getArguments().getString("receiver_image");


        }else{

            receiver_image ="";


        }

        if(senderId.equals(OwnerId)){


        }else{

            ApplicationController.getInstance().startPreviewScreenTimer();
        }

//      if(getArguments())
        if(senderId.equals(OwnerId)) {
            greenArrowIV.setVisibility(View.VISIBLE);
            redArrowIV.setVisibility(View.GONE);

        }else{
             redArrowIV.setVisibility(View.VISIBLE);
            greenArrowIV.setVisibility(View.GONE);

        }


        Log.d("ON RECEIVER ID", String.valueOf(fId));

        Log.d("ON RECEIVER ID", String.valueOf(receiverAmisId));
    }
    public void connectStreamingCall(){

        mControlCallbacks.onCall();
    }

    public void makePubCalls(){


        showLoader();



        if(senderId.equals(OwnerId)){

//             sender side

            redArrowIV.setVisibility(View.GONE);
            greenArrowIV.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Calling", Toast.LENGTH_LONG).show();
            Map<String, String> json = new HashMap<>();
            json.put("message", "videoCalling");
            json.put("receiverID", receverStringId);
            json.put("contentType", "text");
            json.put("senderID", Tools.getData(getActivity(), "idprofile"));
            json.put("senderName", Tools.getData(getActivity(), "name"));
            json.put("ReceiverName", "receiverFirstName");
            json.put("chatType", "amis");
            json.put("isVideoCalling","yes");
            json.put("Name","OnetoOnePopupViewShow");
            json.put("type","Call Connecting");
            json.put("actual_channel", currentChannel);
            String senderPhoto = Tools.getData(getActivity(), "photo");
            json.put("senderImage", senderPhoto);
            json.put("ReceiverImage", senderPhoto);
            json.put("senderCountry", Tools.getData(getActivity(), "country"));
            json.put("ReceiverCountry", "India");

            json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
            json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
            json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());

            System.out.println("REceiver Id==>"+fId);
            System.out.println("json ==========>" +json);
            ChatApi.getInstance().sendMessage(json, currentChannel);

            ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

            Tools.sendMessgaeAlertToServer(getActivity(),receverStringId,"Amis_video");

            ApplicationController.getInstance().startTimer(getActivity(),"Amis", String.valueOf(receverStringId),currentChannel);

        }
        else
        {

//            receiver side


            redArrowIV.setVisibility(View.GONE);
            greenArrowIV.setVisibility(View.GONE);


            Log.e("else calling", "phone page");

            Toast.makeText(getActivity(), "Connecting.....", Toast.LENGTH_LONG).show();

            ApplicationController.getInstance().setCallConnected(Boolean.TRUE);

            ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

            Map<String, String> json = new HashMap<>();
            json.put("message", "Call Connected");

            json.put("receiverID", receverStringId);
            json.put("contentType", "text");
            json.put("senderID", Tools.getData(getActivity(), "idprofile"));
            json.put("senderName", Tools.getData(getActivity(), "name"));
            json.put("ReceiverName", receiverFirstName);
            json.put("chatType", "amis");
            json.put("actual_channel",currentChannel);
            json.put("type","Call Accepted");
            json.put("isAcceptCall","yes");
            String senderPhoto = Tools.getData(getActivity(), "photo");
            json.put("Name","connecting");
            json.put("senderImage", senderPhoto);
            json.put("ReceiverImage",receiver_image);
            json.put("senderCountry", Tools.getData(getActivity(), "country"));
            json.put("ReceiverCountry", "India");
            json.put("isVideoCalling","yes");

            json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
            json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
            json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());

            System.out.println("json params==========>" +json);

            System.out.println("REceiver Id else ==>"+fId);
            System.out.println("REceiver Amis Id ==>"+receverStringId);
            System.out.println("current channel ==>"+currentChannel);
            ChatApi.getInstance().sendMessage(json,currentChannel);

//            ApplicationController.getInstance().= Boolean.TRUE;
            Tools.updateUserNotification(getActivity(),"Amis_video",receverStringId);




        }

    }

    @Override
    public void onAttach(Context context) {
        Log.i(LOGTAG, "OnAttach PreviewControlFragment");

        super.onAttach(context);

        this.mActivity = (SingleCallMainActivity) context;
        this.mControlCallbacks = (PreviewControlCallbacks) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            this.mActivity = (SingleCallMainActivity) activity;
            this.mControlCallbacks = (PreviewControlCallbacks) activity;
        }
    }

    public void disableBackgroudView(){


    }

    @Override
    public void onDetach() {
        Log.i(LOGTAG, "onDetach PreviewControlFragment");

        super.onDetach();

        mControlCallbacks = previewCallbacks;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(LOGTAG, "OnCreate PreviewControlFragment");



        rootView = inflater.inflate(R.layout.preview_actionbar_fragment, container, false);

        mContainer = (RelativeLayout) this.mActivity.findViewById(R.id.actionbar_preview_fragment_container);
        mAudioBtn = (ImageView) rootView.findViewById(R.id.localAudio);
        mVideoBtn = (ImageView) rootView.findViewById(R.id.localVideo);
        mCallBtn = (ImageView) rootView.findViewById(R.id.call);
        redArrowIV=(ImageView)rootView.findViewById(R.id.redarrowIV);
        buttonCallReject=(ImageView)rootView.findViewById(R.id.buttonCallReject);
        greenArrowIV=(ImageView)getActivity().findViewById(R.id.greenarrowIV);
        redArrowIV =(ImageView)getActivity().findViewById(R.id.redarrowIV);
        cameraView = (ImageView)rootView.findViewById(R.id.swapCamera);

        cameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Camera View","Clicked");

                mControlCallbacks.oncameraViewClicked();

            }
        });


//        greenArrowIV.setVisibility(View.VISIBLE);
//        redArrowIV.setVisibility(View.GONE);


//        initOpenTok();

        updateSettings();



        parseValues();


//        grenThumbIV=(ImageView)getActivity().findViewById(R.id.phoneGreenThumbIv);
//        grenThumbIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(), " sample app",Toast.LENGTH_SHORT).show();
//            }
//        });


        return rootView;

    }


    public void updateSettings(){



        mAudioBtn.setImageResource(mActivity.getComm().getLocalAudio()
                ? R.drawable.new_speaker
                : R.drawable.new_speaker_disable_icon);

        mVideoBtn.setImageResource(mActivity.getComm().getLocalVideo()
                ? R.drawable.new_camera
                : R.drawable.new_video_disable_icon);

//
//        mCallBtn.setImageResource(mActivity.getComm().isStarted()
//                ? R.drawable.hang_up
//                : R.drawable.start_call);
//
//        mCallBtn.setBackgroundResource(mActivity.getComm().isStarted()
//                ? R.drawable.end_call_button
//                : R.drawable.initiate_call_button);

        mCallBtn.setOnClickListener(mBtnClickListener);
//
//        if(cameraView!=null){
//
//            cameraView.setOnClickListener(mBtnClickListener);
//        }




        buttonCallReject.setOnClickListener(mBtnClickListener);
        setEnabled(mActivity.getComm().isStarted());


    }



    //    public void updateRemoteAudio(){
//        if(!mActivity.getComm().getRemoteAudio()){
//            mControlCallbacks.onDisableRemoteAudio(true);
//            mAudioBtn.setImageResource(R.drawable.audio);
//        }
//        else {
//            mControlCallbacks.onDisableRemoteAudio(false);
//            mAudioBtn.setImageResource(R.drawable.no_audio);
//        }
//    }
    public void updateLocalAudio() {



        if (!mActivity.getComm().getLocalAudio()) {
            mControlCallbacks.onDisableLocalAudio(true);
            mActivity.onDisableLocalAudio(true);
            mAudioBtn.setImageResource(R.drawable.new_speaker);
        } else {
            mControlCallbacks.onDisableLocalAudio(false);
            mAudioBtn.setImageResource(R.drawable.new_speaker_disable_icon);
        }
    }

    public void updateLocalVideo() {
        Toast.makeText(getActivity(),"Video disable clicked",Toast.LENGTH_SHORT).show();

        if (!mActivity.getComm().getLocalVideo()) {
            mControlCallbacks.onDisableLocalVideo(true);



            mVideoBtn.setImageResource(R.drawable.new_camera);
        } else {
            mControlCallbacks.onDisableLocalVideo(false);
            mVideoBtn.setImageResource(R.drawable.new_video_disable_icon);
        }
    }

    public void updateCall() {
        mCallBtn.setImageResource(!mActivity.getComm().isStarted()
                ? R.drawable.hang_up
                : R.drawable.start_call);

        mCallBtn.setBackgroundResource(!mActivity.getComm().isStarted()
                ? R.drawable.end_call_button
                : R.drawable.initiate_call_button);

        mControlCallbacks.onCall();
    }


    public void setCameraAction() {

        Log.e("Cameraview set","Listebnr");

        if(cameraView!=null){

            cameraView.setOnClickListener(mBtnClickListener);
        }



    }
    public void setEnabled(boolean enabled) {
        System.out.println("_____________________");



//        mCallBtn = (ImageView) rootView.findViewById(R.id.call);
//
//        mCallBtn.setOnClickListener(mBtnClickListener);
//        buttonCallReject.setOnClickListener(mBtnClickListener);

        Log.d("SET ENABLED","CALED");
        if (mVideoBtn != null && mAudioBtn != null) {
            if (enabled) {
                mAudioBtn.setOnClickListener(mBtnClickListener);
                mVideoBtn.setOnClickListener(mBtnClickListener);
            } else {

                Log.d("SET ENABLED","ELSE CALED");
                mAudioBtn.setOnClickListener(null);
                mVideoBtn.setOnClickListener(null);
                mAudioBtn.setImageResource(R.drawable.new_speaker);
                mVideoBtn.setImageResource(R.drawable.new_camera);
            }
        }
    }

    public void restartFragment(boolean restart){
        if ( restart ) {
            setEnabled(false);

            System.out.println("reStartFragment" + " reStartFragment");
            System.out.println("=====" +"fragment");
//            mCallBtn.setBackgroundResource(R.drawable.initiate_call_button);
            mCallBtn.setImageResource(R.drawable.new_phone_green);

        }
    }

//    public  void sendMessgaeToServer(String groupId) {
//
//        JSONObject jsonBody = new JSONObject();
////        findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        try {
//            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
//            jsonBody.put("id_profil_dest",groupId);
//            jsonBody.put("message",Tools.getData(getActivity(), "name")+" "+"Missed a video call");
//            jsonBody.put("type_message","text");
//            jsonBody.put("chat_type", "Amis_video");
//
//            Log.i("json", jsonBody.toString());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest reqSend = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.sendmsg_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        Log.d("SETMESSAGE RES:", String.valueOf(response));
//                        try {
//                            if (response.getString("result").equals("true")) {
//
////
//                            } else {
//                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                            }
//
//                        } catch (JSONException e) {
//
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqSend);
//    }


    public void initOpenTok(){

        JSONObject sessionResponse = Tools.createSessionAndToken(getActivity().getString(R.string.create_opentok_session),Tools.getData(getActivity(), "idprofile"),Tools.getData(getActivity(), "token"));

        if(sessionResponse!=null){

            dismissLoader();

            try {
                if(sessionResponse.getBoolean("result")){

                    JSONObject opentok_response = sessionResponse.getJSONObject("data");

                    ChatApi.getInstance().setOPENTOKSESSIONID(opentok_response.getString("session_id"));

                    ChatApi.getInstance().setOPENTOKTOKEN(opentok_response.getString("token"));

                    ChatApi.getInstance().setOPENTOKAPIKEY(opentok_response.getString("api_key"));



                    updateSettings();


//                    isConnectedToStream = Boolean.TRUE;
//
//                    mControlCallbacks.onCall();
//
//                    makePubCalls();

                }
            } catch (JSONException e) {
                dismissLoader();
                e.printStackTrace();
            }
        }else{

            dismissLoader();

            Toast.makeText(getActivity(),"Unable to Create session Please try again",Toast.LENGTH_SHORT);
        }
    }

    //    show loader

    public void showLoader(){

        pd = new ProgressDialog(getActivity());
        pd.setMessage(getString(R.string.call_connecting_alert));
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){

            pd.dismiss();
        }
    }


}
