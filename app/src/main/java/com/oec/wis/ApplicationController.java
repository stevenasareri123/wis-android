//package com.oec.wis;
//
//import android.app.Activity;
//import android.app.Application;
//import android.app.Dialog;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.media.AudioManager;
//import android.media.MediaPlayer;
//import android.media.MediaRecorder;
//import android.media.RingtoneManager;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.Looper;
//import android.support.multidex.MultiDex;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.View;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.android.internal.http.multipart.MultipartEntity;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.ImageLoader;
//import com.android.volley.toolbox.Volley;
//import com.oec.wis.chatApi.CallListener;
//import com.oec.wis.chatApi.ChatApi;
//import com.oec.wis.chatApi.ChatListener;
//import com.oec.wis.dialogs.Chat;
//import com.oec.wis.dialogs.Phone;
//import com.oec.wis.entities.WISChat;
//import com.oec.wis.entities.WISMenu;
//import com.oec.wis.fragments.FragActu;
//import com.oec.wis.fragments.FragCalendar;
//import com.oec.wis.fragments.FragChat;
//import com.oec.wis.fragments.FragConditions;
//import com.oec.wis.fragments.FragFriends;
//import com.oec.wis.fragments.FragMyLocation;
//import com.oec.wis.fragments.FragMyPub;
//import com.oec.wis.fragments.FragNotif;
//import com.oec.wis.fragments.FragPhoto;
//import com.oec.wis.fragments.FragProfile;
//import com.oec.wis.fragments.FragVideo;
//import com.oec.wis.groupcall.GroupCallMainActivity;
//import com.oec.wis.singlecall.OpenTokConfig;
//import com.oec.wis.singlecall.PreviewControlFragment;
//import com.oec.wis.singlecall.SingleCallMainActivity;
//import com.oec.wis.tools.LruBitmapCache;
//import com.oec.wis.tools.Tools;
//import com.paypal.android.sdk.payments.PayPalConfiguration;
//import com.paypal.android.sdk.payments.PayPalPayment;
//import com.pubnub.api.PNConfiguration;
//import com.pubnub.api.PubNub;
//import com.pubnub.api.callbacks.SubscribeCallback;
//import com.pubnub.api.enums.PNLogVerbosity;
//import com.pubnub.api.enums.PNStatusCategory;
//import com.pubnub.api.models.consumer.PNStatus;
//import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
//import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
//
//import net.danlew.android.joda.JodaTimeAndroid;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.mime.content.InputStreamBody;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.params.CoreProtocolPNames;
//import org.joda.time.LocalDate;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.ByteArrayInputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.lang.reflect.Array;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//import java.util.UUID;
//
//import static android.Manifest.permission.RECORD_AUDIO;
//import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
//
//public class ApplicationController extends Application implements ChatListener {
//
//    Thread t;
//    // PayPal app configuration
//    //ligne 1 sandbox ligne 2 production
//    //public static final String PAYPAL_CLIENT_ID = "ATxzhKN6Bwyf4f7JgXAPCmOqc6x_--gc6M1K031MCaf6MY70EM8JjGjcph2-H48KykVcMWlTLksDEYAc";
//    public static final String PAYPAL_CLIENT_ID = "Abo2qqy1NPxRFxuwrCC_2X10DL6_62spPa5mh0Bq4pTqctJR2faFyI4GnUmka7lACmxL4VaPOALme8L0";
//    //public static final String PAYPAL_CLIENT_SECRET = "ENc6Qnk9wxSyvI9ZOGGsTwWM-GcLVqeKda0SzE-mYtvRvyzjbbGN85xGHXEja9Q2Iy6FzWz-lNNW3-kT";
//
//    public static final String PAYPAL_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
//    public static final String PAYMENT_INTENT = PayPalPayment.PAYMENT_INTENT_SALE;
//    public static final String DEFAULT_CURRENCY = "EUR";
//    // PayPal server urls
//    public static final String TAG = "VolleyPatterns";
//    public static List<WISMenu> MENUS;
//
//
//    String currentChannel;
//    String message = null;
//
//
//
//    CountDownTimer waitTimer;
//
//
//    String senderName = null;
//    String receiverId = null;
//    String currentUserId = null;
//    String receiverName = null;
//    String chatType = null;
//    String senderIcon = null;
//    String receiverImage = null;
//    String Name = null;
//    String receiverCountry = null;
//    String type = null;
//    String imgPath = "";
//    ArrayList<String>memberArray = new ArrayList<>();
//
//    String memberids;
//
//
//    String senderCountry = null;
//    String contentType = null;
//    String groupMemberDetails;
//    /**
//     * A singleton instance of the application class for easy access in other places
//     */
//    private static ApplicationController sInstance;
//    /**
//     * Global request queue for Volley
//     */
//
//
//    public boolean showVoicePopup;
//
//    public boolean isShowVoicePopup() {
//        return showVoicePopup;
//    }
//
//    public void setShowVoicePopup(boolean showVoicePopup) {
//        this.showVoicePopup = showVoicePopup;
//    }
//    private RequestQueue mRequestQueue;
//    private ImageLoader mImageLoader;
//    public static Dialog dialoge;
//    String AudioSavePathInDevice = null;
//    public static String fileUrl;
//    MediaRecorder mediaRecorder;
//    Random random = null;
//    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
//    public static final int RequestPermissionCode = 1;
//    MediaPlayer mediaPlayer;
//    String senderId= null;
//    String receiverAmisId;
//
//    Boolean callConnected =Boolean.FALSE;
//
//    public String userCurrentChannel;
//
//    public String getUserCurrentChannel() {
//        return userCurrentChannel;
//    }
//
//    public void setUserCurrentChannel(String userCurrentChannel) {
//        this.userCurrentChannel = userCurrentChannel;
//    }
//
//
//    public Boolean getCallConnected() {
//        return callConnected;
//    }
//
//    public void setCallConnected(Boolean callConnected) {
//        this.callConnected = callConnected;
//    }
//
//    PubNub pubnub;
//
//    /**
//     * @return ApplicationController singleton instance
//     */
//    public static synchronized ApplicationController getInstance() {
//        return sInstance;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
////        WISChat wisChat=new WISChat();
////        ChatApi.getInstance().setListenerMain(this);
//
//        ChatApi.getInstance().setListenerMain(this);
//        ChatApi.getInstance().setContext(getApplicationContext());
//        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//
////        currentUserId = Tools.getData(this, "idprofile");
////
////        String currentChannel= chatList.get(position).getCurrentchannel();
////        System.out.println("current channel" + currentChannel);
//
//
////
////        if (getIntent().getExtras() != null) {
////
////
////            currentChannel = getIntent().getExtras().getString("currentChannel");
////            System.out.println("current channel" + currentChannel);
////
////        }
//
//        // initialize the singleton
//        sInstance = this;
//        JodaTimeAndroid.init(this);
//        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
//        MENUS = new ArrayList<>();
//        MENUS.add(new WISMenu(getString(R.string.actuality), 1, new FragActu(), null));
//
//        MENUS.add(new WISMenu(getString(R.string.profile), 1, new FragProfile(), null));
//        MENUS.add(new WISMenu(getString(R.string.friends), 1, new FragFriends(), null));
//        MENUS.add(new WISMenu(getString(R.string.chat), 1, new FragChat(), null));
//        MENUS.add(new WISMenu(getString(R.string.calender), 1, new FragCalendar(), null));
//        MENUS.add(new WISMenu(getString(R.string.local), 1, new FragMyLocation(), null));
//        MENUS.add(new WISMenu(getString(R.string.pub), 1, new FragMyPub(), null));
//        MENUS.add(new WISMenu(getString(R.string.video), 1, new FragVideo(), null));
//        MENUS.add(new WISMenu(getString(R.string.photo), 1, new FragPhoto(), null));
//        MENUS.add(new WISMenu(getString(R.string.notif), 1, new FragNotif(), null));
//        MENUS.add(new WISMenu(getString(R.string.condition), 1, new FragConditions(), null));
//
//
//    }
//
//    /**
//     * @return The Volley Request queue, the queue will be created if it is null
//     */
//    public RequestQueue getRequestQueue() {
//        // lazy initialize the request queue, the queue instance will be
//        // created when it is accessed for the first time
//        if (mRequestQueue == null) {
//            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
//        }
//
//        return mRequestQueue;
//    }
//
//    /**
//     * Adds the specified request to the global queue, if tag is specified
//     * then it is used else Default TAG is used.
//     *
//     * @param req
//     * @param tag
//     */
//    public <T> void addToRequestQueue(Request<T> req, String tag) {
//        // set the default_ringtone tag if tag is empty
//        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
//
//        VolleyLog.d("Adding request to queue: %s", req.getUrl());
//
//        getRequestQueue().add(req);
//    }
//
//    /**
//     * Adds the specified request to the global queue using the Default TAG.
//     *
//     * @param req
//     */
//    public <T> void addToRequestQueue(Request<T> req) {
//        // set the default_ringtone tag if tag is empty
//        req.setTag(TAG);
//        req.setRetryPolicy(new DefaultRetryPolicy(
//                10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        getRequestQueue().add(req);
//    }
//
//    /**
//     * Cancels all pending requests by the specified TAG, it is important
//     * to specify a TAG so that the pending/ongoing requests can be cancelled.
//     *
//     * @param tag
//     */
//    public void cancelPendingRequests(Object tag) {
//        if (mRequestQueue != null) {
//            mRequestQueue.cancelAll(tag);
//        }
//    }
//
//    public ImageLoader getImageLoader() {
//        getRequestQueue();
//        if (mImageLoader == null) {
//            mImageLoader = new ImageLoader(this.mRequestQueue,
//                    new LruBitmapCache());
//        }
//        return this.mImageLoader;
//    }
//
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }
//
//    public void setUpchat() {
//
//        PNConfiguration pnConfiguration = new PNConfiguration();
//
//        pnConfiguration.setSubscribeKey("sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe");
//        pnConfiguration.setPublishKey("pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f");
//
//        pnConfiguration.setLogVerbosity(PNLogVerbosity.BODY);
//
//        String uniqueID = UUID.randomUUID().toString();
//        pnConfiguration.setUuid(uniqueID);
//
//        pubnub = new PubNub(pnConfiguration);
//
//
//        pubnub.addListener(new SubscribeCallback() {
//            @Override
//            public void status(PubNub pubnub, PNStatus status) {
//
//                Log.e("PUNUB", status.toString());
//
//
//                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
//                    // This event happens when radio / connectivity is lost
//
//
//                    Log.e("PUNUB", String.valueOf(status.getErrorData()));
//                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
//
//                    // Connect event. You can do stuff like publish, and know you'll get it.
//                    // Or just use the connected event to confirm you are subscribed for
//                    // UI / internal notifications, etc
//
//                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
//
//                        Log.e("PUNUB CONECT", String.valueOf(pubnub.getConfiguration()));
//
//
//                    }
//                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {
//
//                    // Happens as part of our regular operation. This event happens when
//                    // radio / connectivity is lost, then regained.
//
//                    Log.e("connectivity is lost", "PNReconnectedCategory");
//                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {
//
//                    // Handle messsage decryption error. Probably client configured to
//                    // encrypt messages and on live data feed it received plain text.
//                }
//            }
//
//            @Override
//            public void message(PubNub pubnub, PNMessageResult message) {
//                // Handle new message stored in message.data.message
//                if (message.getActualChannel() != null) {
//                    // Message has been received on channel group stored in
//                    // message.getActualChannel()
//                } else {
//                    // Message has been received on channel stored in
//                    // message.getSubscribedChannel()
//                }
//
//            /*
//                log the following items with your favorite logger
//                    - message.getMessage()
//                    - message.getSubscribedChannel()
//                    - message.getTimetoken()
//            */
//                System.out.println("chat Api response");
//
//
//                Log.i("received message Main", String.valueOf(message.getMessage()));
//
//                Log.i("subscribedchannel Main", String.valueOf(message.getChannel()));
//            }
//
//            @Override
//            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
//
//                Log.i("presence", presence.toString());
//
//                Log.i("presence channel", presence.getChannel());
//
//            }
//        });
//
//        pubnub.subscribe().channels(Arrays.asList("awesomeChannel")).execute();
//
//
//    }
//
//    @Override
//    public void didReceiveMessage(PNMessageResult messageResult) {
//
//
//        String receivedChannel = messageResult.getChannel();
//
//        System.out.println("listner for message MainActivity Page");
//
//        Log.i("ChatMessage :::MainPage", messageResult.getMessage().toString());
//
////        if (receivedChannel.equals(currentChannel)) {
//
//        try {
//
//
//            JSONObject jsonData = new JSONObject(messageResult.getMessage().toString());
//
//
//            Log.d("json res on main page", String.valueOf(jsonData));
//
//
//            senderId = jsonData.getString("senderID");
//
//            receiverId = jsonData.getString("receiverID");
//
//
//
//
//            currentUserId = Tools.getData(this, "idprofile");
//
//
//            senderName = jsonData.getString("senderName");
//
////            receiverName = jsonData.getString("ReceiverName");
//
//
//
//            message = jsonData.getString("message");
//
//
//            chatType = jsonData.getString("chatType");
//
//
//            receiverCountry = jsonData.getString("ReceiverCountry");
//
//            senderIcon = jsonData.getString("senderImage");
//            receiverImage = jsonData.getString("ReceiverImage");
//
//            contentType = jsonData.getString("contentType");
//
//            Name = jsonData.getString("Name");
//            senderCountry = jsonData.getString("senderCountry");
//
//
//            currentChannel = jsonData.getString("actual_channel");
//
//
//
//
//
//
//            System.out.println("message Main page------->" + message);
//            System.out.println("Name on mainpage------->" + Name);
//
//
//            System.out.println("senderId Main page------->" + senderId);
//
//            System.out.println("receiverId Main  page------->" + receiverId);
//
//            System.out.println("ownerId Main page------->" + currentUserId);
//
//
//            final String currentUser =Tools.getData(this, "idprofile");
//
//            if(chatType.equals("Groupe")){
//
//                memberids = jsonData.getString("group_mebers");
//
//
//
//
//                Log.e("memberids",memberids);
//
//                if(memberids.contains(currentUser)){
//
//                    receiverId = currentUser;
//                }
//            }
//
//
//
//
//            if (Name.equals("RejectOnetoONE")) {
//
//                ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//                ChatApi.getInstance().stopRingTone();
//                Log.i("Main Page", "RejectOnetoONE");
//
//                Handler h = new Handler(Looper.getMainLooper());
//                h.post(new Runnable() {
//                    public void run() {
//
//                        Log.d("OnCurretn Context", String.valueOf(ChatApi.getInstance().getPublisherContext()));
//
////                        Intent it=new Intent(ApplicationController.this, Chat.class);
////                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        startActivity(it);
//                            if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                            }else{
//
//                                Log.e("ERROR ON","CONTEXT");
//                            }
//
//
////                          new SingleCallMainActivity().customRejectCall();
//
////                          ((SingleCallMainActivity())getBaseContext()).finish();
//
//                        Toast.makeText(getApplicationContext(), "Call Disconnected", Toast.LENGTH_LONG).show();
//                    }
//                });
//
//
//            }
//            else if (Name.equals("RejectConferenceVideoCall")) {
//
//                ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//                ChatApi.getInstance().stopRingTone();
//                Log.i("Main Page", "RejectConferenceVideoCall");
//
//                Handler h = new Handler(Looper.getMainLooper());
//                h.post(new Runnable() {
//                    public void run() {
//
////                            Log.d("OnCurretn Context", String.valueOf(ChatApi.getInstance().getPublisherContext()));
////
////                        Intent it=new Intent(ApplicationController.this, Chat.class);
////                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        startActivity(it);
//                            if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                            }else{
//
//                                Log.e("ERROR ON","CONTEXT");
//                            }
//
//
//
//
////                          ((SingleCallMainActivity())getBaseContext()).finish();
//
//                        Toast.makeText(getApplicationContext(), "Call Rejected", Toast.LENGTH_LONG).show();
//                    }
//                });
//
//
//            }
//            else if(currentUser.equals(receiverId)) {
//
//
//
//
////                Private_951
//
////                Private_966
//
//                if (Name.equals("callerBusyMode")) {
//
//                    Log.d("Type", "callerBusyMode");
//
//                    Handler h = new Handler(Looper.getMainLooper());
//                    h.post(new Runnable() {
//                        public void run() {
//
////                        Intent it=new Intent(ApplicationController.this, Chat.class);
////                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        startActivity(it);
//
//                            Log.d("SENDER ID::",senderId);
//
//                            Log.d("CURRENT USER ID::",Tools.getData(getApplicationContext(),"idprofile"));
//
//
//
//                            if (senderId.equals(Tools.getData(getApplicationContext(),"idprofile"))) {
//
//                                System.out.println("STOP VIDEO SESSION");
//
//                                Log.d("CURRENT USER ID::",Tools.getData(getApplicationContext(),"idprofile"));
//
//                                if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                    ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                                }
//
//                                Toast.makeText(getApplicationContext(), "Busy Mode", Toast.LENGTH_LONG).show();
//
//
//
//
//
//                            }
//
//
//
//                        }
//                    });
////
////
//                }
//
//
//                 else if (Name.equals("OnetoOnePopupViewShow")) {
//
//                    if (senderId.equals(currentUserId)) {
//                        System.out.println("if caaling");
//
//                        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
//
//
//                    } else {
//
//                        System.out.println("USER STATUS" + ChatApi.getInstance().getUserIsAvailable());
//
//
//                        if (ChatApi.getInstance().getUserIsAvailable()) {
//
//                            ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
//
//                            System.out.println("user state" + ChatApi.getInstance().getUserIsAvailable());
//
//
//                            Log.e("USER AVAILABLE", String.valueOf(ChatApi.getInstance().getUserIsAvailable()));
//
////                        true
//
//                            ChatApi.getInstance().setUpRingTone(getApplicationContext());
//                            ChatApi.getInstance().playRingTone();
////                        Intent i = new Intent(this, Phone.class);
////                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        i.putExtra("receiver_name",senderName);
////                        i.putExtra("receiverID",receiverId);
////                        i.putExtra("actual_channel",currentChannel);
////                        i.putExtra("senderID",senderId);
//
//
////
////                        Map<String, String> json = new HashMap<>();
////                        json.put("message", " Call Connecting");
////                        json.put("receiverID", "216");
////                        json.put("Name","callerBusyMode");
////                        json.put("contentType", "text");
////                        json.put("senderID", Tools.getData(this, "idprofile"));
////                        json.put("senderName", Tools.getData(this, "name"));
//////                        json.put("ReceiverName", receiverFirstName);
////                        json.put("chatType", "amis");
////                        json.put("isVideoCalling","yes");
////                        json.put("type","Call Connecting");
////                        json.put("actual_channel", currentChannel);
////                        String senderPhoto = Tools.getData(this, "photo");
////                        json.put("senderImage", senderPhoto);
////                        json.put("ReceiverImage", senderPhoto);
////                        json.put("senderCountry", Tools.getData(this, "country"));
////                        json.put("ReceiverCountry", "India");
////
////                        System.out.println("json ==========>" +json);
////
////
////                        i.putExtra("senderID", senderId);
////                        startActivity(i);
//
//                            Log.i("Main page", "OneToOnePopupViewShow");
//
//
////                        review screen
//
//                            Intent it = new Intent(this, SingleCallMainActivity.class);
//                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            it.putExtra("receiver_name", senderName);
//                            it.putExtra("receiverId", receiverId);
//                            it.putExtra("actual_channel", currentChannel);
//                            it.putExtra("senderID", senderId);
//
//                            it.putExtra("message", "CallBusy");
//                            it.putExtra("contentType", "text");
//                            it.putExtra("senderName", senderId);
////                        json.put("ReceiverName", receiverFirstName);
//                            it.putExtra("chatType", "amis");
//                            it.putExtra("isVideoCalling", "yes");
//                            it.putExtra("type", "Connecting");
//                            String senderPhoto = Tools.getData(this, "photo");
//                            it.putExtra("senderImage", senderPhoto);
//                            it.putExtra("ReceiverImage", senderPhoto);
//                            it.putExtra("senderCountry", Tools.getData(this, "country"));
//                            it.putExtra("ReceiverCountry", "India");
//
//
//                            startActivity(it);
//
//                        } else {
//
//                            Map<String, String> json = new HashMap<>();
//                            json.put("message", "CallBusy");
//                            json.put("receiverID", receiverId);
//                            json.put("Name", "callerBusyMode");
//                            json.put("contentType", "text");
//                            json.put("senderID", Tools.getData(this, "idprofile"));
//                            json.put("senderName", Tools.getData(this, "name"));
////                        json.put("ReceiverName", receiverFirstName);
//                            json.put("chatType", "amis");
//                            json.put("isVideoCalling", "yes");
//                            json.put("type", "Call Connecting");
//                            json.put("actual_channel", currentChannel);
//                            String senderPhoto = Tools.getData(this, "photo");
//                            json.put("senderImage", senderPhoto);
//                            json.put("ReceiverImage", senderPhoto);
//                            json.put("senderCountry", Tools.getData(this, "country"));
//                            json.put("ReceiverCountry", "India");
//
//                            System.out.println("json ==========>" + json);
//                            ChatApi.getInstance().sendMessage(json, currentChannel);
//
//
//                        }
//
//
//                    }
//                }
//
////            E/else calling: phone page
////            E/calling: called
////            I/System.out: json params==========>{actual_channel=Private_951, ReceiverName=null, chatType=amis, senderID=225, contentType=text, senderImage=img_pub1482331044.png, ReceiverCountry=India, senderName=chat USER M, senderCountry=Antarctica, isAcceptCall=yes, message=Call Connected, ReceiverImage=img_pub1482331044.png, type=Call Accepted, Name=connecting, receiverID=216}
////            I/System.out: REceiver Id else ==>0
////            I/System.out: REceiver Amis Id ==>
////            I/System.out: current channel ==>0
////            D/sende message called: {actual_channel=Private_951, ReceiverName=null, chatType=amis, senderID=225, contentType=text, senderImage=img_pub1482331044.png, ReceiverCountry=India, senderName=chat USER M, senderCountry=Antarctica, isAcceptCall=yes, message=Call Connected, ReceiverImage=img_pub1482331044.png, type=Call Accepted, Name=connecting, receiverID=216}
//
//                else if (Name.equals("connecting")) {
//
//                    ChatApi.getInstance().stopRingTone();
//
//                    ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
//
//
//                    if (senderId.equals(currentUserId)) {
//
//                        System.out.println("if connected");
//
//                        System.out.println("if caaling");
//
//
//                    } else {
//
//
//                        Intent it = new Intent(this, SingleCallMainActivity.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        it.putExtra("receiver_name", senderName);
//                        it.putExtra("receiverId", receiverId);
//                        it.putExtra("actual_channel", currentChannel);
//                        it.putExtra("senderID", senderId);
//
//
//                        it.putExtra("message", "CallBusy");
//                        it.putExtra("contentType", "text");
//                        it.putExtra("senderName", senderId);
////                        json.put("ReceiverName", receiverFirstName);
//                        it.putExtra("chatType", "amis");
//                        it.putExtra("isVideoCalling", "yes");
//                        it.putExtra("type", "Connecting");
//                        String senderPhoto = Tools.getData(this, "photo");
//                        it.putExtra("senderImage", senderPhoto);
//
//                        it.putExtra("senderImage", senderPhoto);
//                        it.putExtra("ReceiverImage", senderPhoto);
//                        it.putExtra("senderCountry", Tools.getData(this, "country"));
//                        it.putExtra("ReceiverCountry", "India");
//
//                    }
////
////
////                startActivity(it);
//
////                    if(senderId.equals(currentUserId)){
////
////                    Intent intent = new Intent(this, SingleCallMainActivity.class);
////                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                    startActivity(intent);
////
////
////                }
//
//                }
//
//
////
//
//               else if (Name.equals("RejectOnetoONE")) {
//
//                    ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//                    ChatApi.getInstance().stopRingTone();
//                    Log.i("Main Page", "RejectOnetoONE");
//
//                    Handler h = new Handler(Looper.getMainLooper());
//                    h.post(new Runnable() {
//                        public void run() {
//
//                            Log.d("OnCurretn Context", String.valueOf(ChatApi.getInstance().getPublisherContext()));
//
//                          Intent it=new Intent(ApplicationController.this, Chat.class);
//                          it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                          startActivity(it);
////                            if (ChatApi.getInstance().getPublisherContext() != null) {
////
////                                ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
////                            }else{
////
////                                Log.e("ERROR ON","CONTEXT");
////                            }
//
//
////                          new SingleCallMainActivity().customRejectCall();
//
////                          ((SingleCallMainActivity())getBaseContext()).finish();
//
//                            Toast.makeText(getApplicationContext(), "Call Rejected", Toast.LENGTH_LONG).show();
//                        }
//                    });
//
//
//                }
//
//                else if(Name.equals("MultiCall")){
//
//                    Log.d("GROPUCALL","INITIALZED");
//
//                    if(ChatApi.getInstance().getUserIsAvailable()){
//
//                        Log.e("GROPUCALL","USER IS Available");
//
//                    }else{
//
//                        Log.e("GROPUCALL","USER IS NOT Available");
//                    }
//
//                    if(senderId.equals(currentUserId)){
//
//
//
//                        Log.d("GROPUCALL","IS OWNER OF THE CALL");
//                    }
//
//                    else{
//
//                        if(ChatApi.getInstance().getUserIsAvailable()){
//
////                        alert receiver
//
//                        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
//
//                        ChatApi.getInstance().setUpRingTone(getApplicationContext());
//                        ChatApi.getInstance().playRingTone();
//                        ChatApi.getInstance().setUserIsAvailable(false);
//
////                        make pub nub to intimate  receiver
//
//
//
//                        Intent it = new Intent(this,GroupCallMainActivity.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        it.putExtra("receiver_name", senderName);
//                        it.putExtra("receiverID", receiverId);
//                        it.putExtra("actual_channel", currentChannel);
//                        it.putExtra("senderID", senderId);
//                        it.putExtra("Name","connecting Multiple Call");
//
//                        it.putExtra("message", "connecting Multiple Call");
//                        it.putExtra("contentType", "text");
//                        it.putExtra("senderName",senderName);
////                        json.put("ReceiverName", receiverFirstName);
//                        it.putExtra("chatType", "Groupe");
//                        it.putExtra("isVideoCalling", "yes");
//                        it.putExtra("type", "Connecting");
//                        String senderPhoto = Tools.getData(this, "photo");
//                        it.putExtra("senderImage", senderPhoto);
//                        it.putExtra("receiverGroup","");
//                        it.putExtra(" GroupNewCount","2");
//                        it.putExtra("senderImage", senderPhoto);
//                        it.putExtra("ReceiverImage", senderPhoto);
//                        it.putExtra("senderCountry", Tools.getData(this, "country"));
//                        it.putExtra("ReceiverCountry", "India");
//
//                        String mememberArray = jsonData.getString("receiverGroup");
//
//                        String[] strArray = new String[] {mememberArray};
//
//                        Log.d(" mememberArray", String.valueOf(strArray));
//
////                        JSONArray groupMemberDetails = jsonData.getJSONArray("receiverGroup");
////
//
//
//                        it.putExtra("members_details",strArray);
//
//
//                        startActivity(it);
//
//
//                        //                    end here
//
//                        }else{
//
//                            Log.e("GROPUCALL","USER IS NOT Available");
//
//                            if(senderId.equals(Tools.getData(getApplicationContext(),"idprofile"))){
//
//                                Toast.makeText(getApplicationContext(), "Caller Busy", Toast.LENGTH_LONG).show();
//                            }
//
//
//
//                            Map<String, String> json = new HashMap<>();
//                            json.put("message", "CallBusy");
//                            json.put("receiverID", receiverId);
//                            json.put("Name", "callerBusyMode");
//                            json.put("contentType", "text");
//                            json.put("senderID",senderId);
//                            json.put("senderName", Tools.getData(this, "name"));
////                        json.put("ReceiverName", receiverFirstName);
//                            json.put("chatType", "Groupe");
//                            json.put("isVideoCalling", "yes");
//                            json.put("type", "Call Connecting");
//                            json.put("actual_channel", currentChannel);
//                            String senderPhoto = Tools.getData(this, "photo");
//                            json.put("senderImage", senderPhoto);
//                            json.put("ReceiverImage", senderPhoto);
//                            json.put("senderCountry", Tools.getData(this, "country"));
//                            json.put("ReceiverCountry", "India");
//
//
//
//                            json.put("group_mebers",jsonData.getString("group_mebers"));
//                            json.put("GroupCount","");
//                            json.put("currenIDs",Tools.getData(getApplicationContext(),"idprofile"));
//
//                            json.put("receiverGroup","");
//
//                            System.out.println("json ==========>" + json);
//                            ChatApi.getInstance().sendMessageForGroupVideo(json, currentChannel,null);
//
//                        }
//
//
//                    }
//
//
//
//
//
//                }
//
//                else if(Name.equals("connecting Multiple Call")){
//
//
//
//                    if(senderId.equals(currentUserId)){
//
//                        Log.d("GROPUCALL","IS OWNER OF THE CALL");
//
//                        ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver("1");
//
//
//                    }else{
//
//                        Log.d("USERCONNECTED","TO GROUPCALLS");
//
//                        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
//
//                        ChatApi.getInstance().stopRingTone();
//
////                        ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver("0");
//
////                        redirect to GroupVideoView
//
////                        Intent it = new Intent(this,GroupCallMainActivity.class);
////                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        it.putExtra("receiver_name", senderName);
////                        it.putExtra("receiverID", receiverId);
////                        it.putExtra("actual_channel", currentChannel);
////                        it.putExtra("senderID", senderId);
////                        it.putExtra("Name","connecting Multiple Call");
////
////                        it.putExtra("message", "connecting Multiple Call");
////                        it.putExtra("contentType", "text");
////                        it.putExtra("senderName", senderId);
//////                        json.put("ReceiverName", receiverFirstName);
////                        it.putExtra("chatType", "Groupe");
////                        it.putExtra("isVideoCalling", "yes");
////                        it.putExtra("type", "Connecting");
////                        String senderPhoto = Tools.getData(this, "photo");
////                        it.putExtra("senderImage", senderPhoto);
////                        it.putExtra("receiverGroup","");
////                        it.putExtra(" GroupNewCount","2");
////                        it.putExtra("senderImage", senderPhoto);
////                        it.putExtra("ReceiverImage", senderPhoto);
////                        it.putExtra("senderCountry", Tools.getData(this, "country"));
////                        it.putExtra("ReceiverCountry", "India");
////
////                        startActivity(it);
//
//
//                    }
//                }
//
//                else if (Name.equals("Reject")) {
//
////                    ChatApi.getInstance().stopRingTone();
////                    Log.i("Main Page", "Reject");
////                    Intent it = new Intent(ApplicationController.this, Chat.class);
////                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                    startActivity(it);
//
//
//                } else {
//
//                    Log.i("Name", "else calling");
//                    Log.e("name", "else call");
//                }
//
//            }
//            else{
//
//                System.out.println("_____________********************__________");
//
//                Log.e("WIS AUDIO CALL", "DO NOT SHOW POP");
//            }
//
//
//        } catch (JSONException e) {
//
//            e.printStackTrace();
//        }
//
//
//    }
//
////    compile group: 'org.apache.httpcomponents', name: 'httpmime', version: '4.5.2'
//
//
//    @Override
//    public void didReceivePresenceEvent(PNPresenceEventResult eventResult) {
//
//
//        Log.d("presenceEvent:Main Page", eventResult.getChannel());
//
//        Log.d("presenceEvent: MainPage", eventResult.getState().toString());
//
//    }
//
//    @Override
//    public void onPublishSuccess(PNStatus status) {
//
//        if (!status.isError()) {
//
//            Log.d("message on MainPage ", String.valueOf(status));
//
//
//        }
//
//
//    }
//
//    public void showAlert(String message, String senderName) {
//
//        Log.i("notification", "alert");
//
//        Tools.showNotif2(this, 0, senderName, message);
//
//    }
//
//    public void setTimer() {
//
//
//
////        showpopup();
//
////        new CountDownTimer(30, 1000) {
////
////            public void onTick(long millisUntilFinished) {
//////                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
////
////                Toast.makeText(getApplicationContext(), "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_LONG).show();
////
////
////
////
////            }
////
////            public void onFinish() {
//////                mTextField.setText("done!");
////
//////                showpopup();
////
////
////                if (ChatApi.getInstance().getPublisherContext() != null) {
////
////                    ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
////
////
////                    showpopup();
////
////
////                }else{
////
////                    Log.e("ERROR ON","CONTEXT");
////                }
////
////
//////                Toast.makeText(getApplicationContext(), "onFinish: ", Toast.LENGTH_LONG).show();
////            }
////        }.start();
//
//
//    }
//
//    public void stopTimer(){
//
//        t.interrupt();
//
//    }
//
//    public void startTimer(final Context context){
//
//        final long timeBeforeThreadStart = System.currentTimeMillis();
//
//        final boolean isInputEventOccurred = false;
//        t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                while ((System.currentTimeMillis() - timeBeforeThreadStart) < (30 * 1000)) {
//                    // you add sleep here if you want instead of loop
//
////                    Log.d("TIMER INIT","TIMER START");
//                }
//                if (!isInputEventOccurred) {
//                    // write code to close app
//
////                    Log.d("TIMER STOPED","TIMER STOPED");
//
////                    clearVideoSession();
//
//                    if(context!=null){
//
//                        if(!callConnected){
//
////                            ((Activity)context).finish();
//
//                            showpopup(context);
//
//                        }else{
//
//                            Log.d("Call","connected");
//                        }
//
//
//                    }else{
//
//                        Log.e("ERROR ON","CONTEXT");
//                    }
//
//
//                }
//
//            }
//        });
//        t.start();
//    }
//    public void showpopup(final Context context) {
//
//
//        Handler h = new Handler(Looper.getMainLooper());
//        h.post(new Runnable() {
//                   public void run() {
//
//
//
//
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
//
//        // Setting Dialog Title
//        alertDialog.setTitle("Audio Record...");
//
//        // Setting Dialog Message
//        alertDialog.setMessage("Are you sure you want  record your voice");
//
//        // Setting Icon to Dialog
////        alertDialog.setIcon(R.drawable.delete);
//
//        // Setting Positive "Yes" Button
//        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//
//
//                pickMediaAudio(context);
////                audioRecordMethod();
//                // Write your code here to invoke YES event
//                Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        // Setting Negative "NO" Button
//        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                // Write your code here to invoke NO event
//                Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
//                dialog.cancel();
//
//                ((Activity)context).finish();
//            }
//        });
//
//        // Showing Alert Message
//                       if(!((Activity) context).isFinishing())
//                       {
//                           //show dialog
//                           alertDialog.show();
//                       }
//
//
//                   }
//        });
//
//    }
//
//
//
//        private void pickMediaAudio(final Context context) {
//
//            // custom dialog
//            dialoge = new Dialog(context);
//            dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//            dialoge.setContentView(R.layout.dialog_share_actu);
//            // Custom Android Allert Dialog Title
//            dialoge.setTitle("Custom Dialog Example");
//
//            final Button buttonstop = (Button) dialoge.findViewById(R.id.buttonstop);
//            Button buttonplay = (Button) dialoge.findViewById(R.id.buttonplay);
//            final Button buttonsendfile = (Button) dialoge.findViewById(R.id.buttonsendfile);
//            final Button startButton = (Button) dialoge.findViewById(R.id.startButton);
//
//            final ImageView microphoneIV = (ImageView) dialoge.findViewById(R.id.microphoneIV);
//            final ImageView no_microphoneIV = (ImageView) dialoge.findViewById(R.id.no_microphoneIV);
//
//
//            // Click cancel to dismiss android custom dialog box
//            microphoneIV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    microphoneIV.setVisibility(View.GONE);
//                    no_microphoneIV.setVisibility(View.VISIBLE);
////                 mediaRecorder.stop();
////                microphoneIV.setEnabled(false);
////                startButton.setEnabled(true);
//
//                    Toast.makeText(ApplicationController.this, "Recording stopped",
//                            Toast.LENGTH_LONG).show();
//                }
//            });
//            startButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (checkPermission()) {
//
//                        AudioSavePathInDevice =
//                                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
//                                        CreateRandomAudioFileName(5) + "AudioRecording.mp3";
//
//
//                        File AudioFile = new File(AudioSavePathInDevice);
//                        System.out.println("Audio File" + AudioFile);
//
//
////                    MediaRecorderReady();
//
//
//                        mediaRecorder = new MediaRecorder();
//                        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//                        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//                        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
//                        mediaRecorder.setOutputFile(AudioSavePathInDevice);
//
//                        System.out.println("Audio path" + AudioSavePathInDevice);
//
//                        try {
//                            mediaRecorder.prepare();
//                            mediaRecorder.start();
//                        } catch (IllegalStateException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//
//                        startButton.setEnabled(false);
//                        buttonstop.setEnabled(true);
//                        microphoneIV.setEnabled(true);
//
//                        Toast.makeText(ApplicationController.this, "Recording started",
//                                Toast.LENGTH_LONG).show();
//                    } else {
//                        requestPermission();
//                    }
//                }
//            });
//            buttonstop.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    mediaRecorder.stop();
//                    buttonstop.setEnabled(false);
////                buttonPlayLastRecordAudio.setEnabled(true);
//                    startButton.setEnabled(true);
////                buttonStopPlayingRecording.setEnabled(false);
//
//                    Toast.makeText(ApplicationController.this, "Recording Completed",
//                            Toast.LENGTH_LONG).show();
////                dialog.dismiss();
////                Toast.makeText(getApplicationContext(),"button stop",Toast.LENGTH_LONG).show();
//                }
//            });
//
//            // Your android custom dialog ok action
//            // Action for custom dialog ok button click
//            buttonplay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    buttonstop.setEnabled(false);
//                    startButton.setEnabled(false);
////                buttonStopPlayingRecording.setEnabled(true);
//
//                    mediaPlayer = new MediaPlayer();
//                    try {
//                        mediaPlayer.setDataSource(AudioSavePathInDevice);
//                        mediaPlayer.prepare();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    mediaPlayer.start();
//                    Toast.makeText(ApplicationController.this, "Recording Playing",
//                            Toast.LENGTH_LONG).show();
////                Toast.makeText(getApplicationContext(),"button play",Toast.LENGTH_LONG).show();
//
////                dialog.dismiss();
//                }
//            });
//
//
//            // Action for custom dialog ok button click
//            buttonsendfile.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    Toast.makeText(getApplicationContext(), "button send file", Toast.LENGTH_LONG).show();
//                    dialoge.dismiss();
//
////                    rejectSingleCall(receiver,currentChannel,type);
//
////                    new DoAudioUpload().execute(AudioSavePathInDevice);
//
//                    if(AudioSavePathInDevice==null){
//
//                        Toast.makeText(getApplicationContext(), "No Recording Found", Toast.LENGTH_LONG).show();
//
//                        ((Activity)ChatApi.getInstance().getPublisherContext()).finish();
//
//                    }else {
//
//                        AsyncTask.execute(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                JSONObject jsonObject = Tools.uploadImage(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.uploadVoiceMessage), AudioSavePathInDevice, Tools.getData(ApplicationController.this, "token"), Tools.getData(ApplicationController.this, "idprofile"),"",type,"");
//
//                                if (jsonObject != null) {
//
//
//
//                                    Log.d("RESPONSE ", "HERE");
//
//                                    Log.d("RESPONSE", String.valueOf(jsonObject));
//
//                                    ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//
//                                    ((Activity)ChatApi.getInstance().getPublisherContext()).finish();
//
//                                    ((Activity)ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            Toast.makeText(getApplicationContext(), "Voice Message Sent", Toast.LENGTH_LONG).show();
//                                        }
//                                    });
//
//
//                                }
//                            }
//                        });
//
//                    }
//
//
//
//
//
//
//                }
//            });
//
//            if(!((Activity) context).isFinishing())
//            {
//                //show dialog
//                dialoge.show();
//            }
//
//
//
//
//
//
//    }
//
//    public void makePubCallsforVoicmessage(Context con,final String type, final String receiver, final String currentChannel){
//
////        {"actual_channel":"Private_1008","senderID":"263","ReceiverID":"265","Name":"DisconnectCallBothuser"}
//
//        Log.e("makePubCallsfo","Called");
//
//        Map<String, String> json = new HashMap<>();
//        json.put("ReceiverID",receiver);
//        json.put("Name","DisconnectCallBothuser");
//        json.put("actual_channel", currentChannel);
//        json.put("chatType",type);
//        json.put("senderID",Tools.getData(con, "idprofile"));
//
//        ChatApi.getInstance().sendMessage(json,currentChannel);
//
//    }
//
//    private void requestPermission() {
//
////        ActivityCompat.requestPermissions(ApplicationController.this, new
////                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
//    }
//
//    public String CreateRandomAudioFileName(int string) {
//        StringBuilder stringBuilder = new StringBuilder(string);
//        int i = 0;
//        try {
//
//            while (i < string) {
//
//                stringBuilder.append(RandomAudioFileName.
//                        charAt(random.nextInt(RandomAudioFileName.length())));
//
//                i++;
//            }
//
//
//        } catch (NullPointerException exce) {
//            System.out.println("exce ==>" + exce.getMessage());
//        }
//        return stringBuilder.toString();
//    }
//
//
//
//
//
//
//
//    public boolean checkPermission() {
//        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
//                WRITE_EXTERNAL_STORAGE);
//        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
//                RECORD_AUDIO);
//        return result == PackageManager.PERMISSION_GRANTED &&
//                result1 == PackageManager.PERMISSION_GRANTED;
//    }
//
//
//
//
//    public void stopPreviewScreenTimer(){
//
//
//        if(waitTimer!=null){
//
//            Log.e("wistimer","Stopped called");
//            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
//            waitTimer.cancel();
//            waitTimer = null;
//
//
//        }
//
//    }
//    public void startTimer(final Context con, final String type, final String receiver, final String currentChannel){
//
//        Log.d("TIMER STARTED","TIMER STARTEd");
//
//        final long timeBeforeThreadStart = System.currentTimeMillis();
//
//        final boolean isInputEventOccurred = false;
//        t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                while ((System.currentTimeMillis() - timeBeforeThreadStart) < (30 * 1000)) {
//                    // you add sleep here if you want instead of loop
//
////                    Log.d("TIMER INIT","TIMER START");
//                }
//                if (!isInputEventOccurred) {
//                    // write code to close app
//
//                    Log.d("TIMER STOPED","TIMER STOPED");
//
//
//
////                    clearVideoSession();
//
//
//
//                    if(con!=null){
//
//                        if(!ApplicationController.getInstance().getCallConnected()){
//
//                            Log.d("Call not connected::","show pop");
//
//                            ApplicationController.getInstance().setShowVoicePopup(Boolean.TRUE);
//
//
//                            ((Activity)con).finish();
//
//                            ChatApi.getInstance().claerBusyModeUsers();
//
//
//
//
////                            showpopup(con,type,receiver,currentChannel);
//
//
//
//
//
////                            rejectSingleCall(receiver,"Private_".concat(receiver));
//
////                            Tools.sendMessgaeAlertToServer(con,receiver);
//
//                        }else{
//
//                            Log.d("Call","connected");
//
//                            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
//                        }
//
//
//                    }else{
//
//                        Log.e("ERROR ON","CONTEXT");
//                    }
//
//
//                }else{
//
//                    Log.e("Timer::","isInputEventOccurred else part");
//                }
//
//            }
//        });
//        t.start();
//    }
//
//    public class DoAudioUpload extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
////            findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//
//            System.out.println("result on Audio =======>" + result);
//            if (!result.equals("")) {
//                try {
//                    JSONObject img = new JSONObject(result);
//                    if (img.getBoolean("result"))
//                        Log.i("image results%@", String.valueOf(img));
//                    String type;
////                    if (is_group) {
////                        type = "Groupe";
////                    } else {
////                        type = "Amis";
////                    }
//
//
//                    System.out.println("fileurl=====>" + AudioSavePathInDevice);
//                    Map<String, String> json = new HashMap<>();
//                    json.put("message", img.getString("data"));
//                    json.put("chatType", "Amis");
//                    json.put("receiverID", receiverId);
//                    json.put("contentType", "Audio");
//                    json.put("senderID", Tools.getData(ApplicationController.this, "idprofile"));
//                    json.put("senderName", Tools.getData(ApplicationController.this, "name"));
//                    String senderPhoto = Tools.getData(ApplicationController.this, "photo");
//
//                    json.put("senderImage", senderPhoto);
//
//                    json.put("actual_channel", currentChannel);
//
//
//                    System.out.println("senderId" + Tools.getData(ApplicationController.this, "idprofile"));
//
//
//                    ChatApi.getInstance().sendMessage(json, currentChannel);
//
//
//
////                    Chat.sendMsg(img.getString("data"), Chat.fId, "photo", "",type);
//
//
////                        doUpdate(img.getString("data"));
//                    //else
//                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
////                    findViewById(R.id.loading).setVisibility(View.GONE);
//                    Toast.makeText(ApplicationController.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                }
//
//            } else {
////                findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }
//
//
//        @Override
//        protected String doInBackground(String... urls) {
//            ArrayList<String> images = new ArrayList<>();
//            images.add(imgPath);
//
//            Log.d("Aduo recording filePath",urls[0]);
//
////            ArrayList<String> audiourls = new ArrayList<>();
////            images.add(fileUrl);
//            return Tools.doFileUploadForChat(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.savePhotoAction),urls[0], Tools.getData(ApplicationController.this, "token"), Tools.getData(ApplicationController.this, "idprofile"), "", "");
//
//
////            /storage/sdcard0/JCIENAudioRecording.3gp
//        }
//
//    }
//
//
//
//
//
//}
package com.oec.wis;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.internal.http.multipart.MultipartEntity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.oec.wis.chatApi.CallListener;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.chatApi.ChatListener;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.fragments.FragActu;
import com.oec.wis.fragments.FragCalendar;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragConditions;
import com.oec.wis.fragments.FragFriends;
import com.oec.wis.fragments.FragMyLocation;
import com.oec.wis.fragments.FragMyPub;
import com.oec.wis.fragments.FragNotif;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.fragments.FragProfile;
import com.oec.wis.fragments.FragVideo;
import com.oec.wis.groupcall.GroupCallMainActivity;
import com.oec.wis.singlecall.OpenTokConfig;
import com.oec.wis.singlecall.PreviewControlFragment;
import com.oec.wis.singlecall.SingleCallMainActivity;
import com.oec.wis.tools.LruBitmapCache;
import com.oec.wis.tools.Tools;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNLogVerbosity;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import net.danlew.android.joda.JodaTimeAndroid;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;


import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ApplicationController extends Application implements ChatListener {

    Thread t;
    // PayPal app configuration
    //ligne 1 sandbox ligne 2 production
    //public static final String PAYPAL_CLIENT_ID = "ATxzhKN6Bwyf4f7JgXAPCmOqc6x_--gc6M1K031MCaf6MY70EM8JjGjcph2-H48KykVcMWlTLksDEYAc";
    public static final String PAYPAL_CLIENT_ID = "Abo2qqy1NPxRFxuwrCC_2X10DL6_62spPa5mh0Bq4pTqctJR2faFyI4GnUmka7lACmxL4VaPOALme8L0";
    //public static final String PAYPAL_CLIENT_SECRET = "ENc6Qnk9wxSyvI9ZOGGsTwWM-GcLVqeKda0SzE-mYtvRvyzjbbGN85xGHXEja9Q2Iy6FzWz-lNNW3-kT";

    public static final String PAYPAL_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    public static final String PAYMENT_INTENT = PayPalPayment.PAYMENT_INTENT_SALE;
    public static final String DEFAULT_CURRENCY = "EUR";
    // PayPal server urls
    public static final String TAG = "VolleyPatterns";
    public static List<WISMenu> MENUS;


    String currentChannel;
    String message = null;



//   Handler popupHandler;

    CountDownTimer waitTimer;

    String senderName = null;
    String receiverId = null;
    String currentUserId = null;
    String receiverName = null;
    String chatType = null;
    String senderIcon = null;
    String receiverImage = null;
    String Name = null;
    String receiverCountry = null;
    String type = null;
    String imgPath = "";
    ArrayList<String>memberArray = new ArrayList<>();

    String memberids;


    String senderCountry = null;
    String contentType = null;
    String groupMemberDetails;
    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static ApplicationController sInstance;

    public boolean isInputEventOccurred = false;

    public boolean getIsInputEventOccurred() {
        return isInputEventOccurred;
    }

    public void setInputEventOccurred(boolean inputEventOccurred) {
        isInputEventOccurred = inputEventOccurred;
    }

    public boolean showVoicePopup;

    public boolean isShowVoicePopup() {
        return showVoicePopup;
    }

    public void setShowVoicePopup(boolean showVoicePopup) {
        this.showVoicePopup = showVoicePopup;
    }

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    public static Dialog dialoge;
    String AudioSavePathInDevice = null;
    public static String fileUrl;
    MediaRecorder mediaRecorder;
    Random random = null;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer;
    String senderId= null;
    String receiverAmisId;

    Boolean callConnected =Boolean.FALSE;

    public String userCurrentChannel;

    public String getUserCurrentChannel() {
        return userCurrentChannel;
    }

    public void setUserCurrentChannel(String userCurrentChannel) {
        this.userCurrentChannel = userCurrentChannel;
    }

    public Boolean getCallConnected() {
        return callConnected;
    }

    public void setCallConnected(Boolean callConnected) {
        this.callConnected = callConnected;
    }

    PubNub pubnub;


    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized ApplicationController getInstance() {
        return sInstance;
    }

    @Override
    public void onTerminate(){
        super.onTerminate();


        System.out.println("*********");
        Log.d("APP STATUS","WIS APP TERMINATED");


//        ChatApi.getInstance().unSubscribeChannels();

    }


    @Override
    public void onCreate() {
        super.onCreate();


//        registerActivityLifecycleCallbacks(ApplicationController.this);

//        WISChat wisChat=new WISChat();
//        ChatApi.getInstance().setListenerMain(this);

        ChatApi.getInstance().setListenerMain(this);
        ChatApi.getInstance().setContext(getApplicationContext());
        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

        ChatApi.getInstance().setWisDirectContext(this);


//        Fabric.with(ApplicationController.this, new Crashlytics());

//        currentUserId = Tools.getData(this, "idprofile");
//
//        String currentChannel= chatList.get(position).getCurrentchannel();
//        System.out.println("current channel" + currentChannel);


//
//        if (getIntent().getExtras() != null) {
//
//
//            currentChannel = getIntent().getExtras().getString("currentChannel");
//            System.out.println("current channel" + currentChannel);
//
//        }

        // initialize the singleton
        sInstance = this;
        JodaTimeAndroid.init(this);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        MENUS = new ArrayList<>();
        MENUS.add(new WISMenu(getString(R.string.actuality), 1, new FragActu(), null));

        MENUS.add(new WISMenu(getString(R.string.profile), 1, new FragProfile(), null));
        MENUS.add(new WISMenu(getString(R.string.friends), 1, new FragFriends(), null));
        MENUS.add(new WISMenu(getString(R.string.chat), 1, new FragChat(), null));
        MENUS.add(new WISMenu(getString(R.string.calender), 1, new FragCalendar(), null));
        MENUS.add(new WISMenu(getString(R.string.local), 1, new FragMyLocation(), null));
        MENUS.add(new WISMenu(getString(R.string.pub), 1, new FragMyPub(), null));
        MENUS.add(new WISMenu(getString(R.string.video), 1, new FragVideo(), null));
        MENUS.add(new WISMenu(getString(R.string.photo), 1, new FragPhoto(), null));
        MENUS.add(new WISMenu(getString(R.string.notif), 1, new FragNotif(), null));
        MENUS.add(new WISMenu(getString(R.string.condition), 1, new FragConditions(), null));


        Log.e("APPLICATION STARTED","ABCDEFGH");

        if (TextUtils.isEmpty(Tools.getData(ApplicationController.this, "token"))){

            setUpChatApi();

            Tools.loadNotifs(ApplicationController.this);

        }




    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default_ringtone tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default_ringtone tag if tag is empty
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void setUpchat() {

        PNConfiguration pnConfiguration = new PNConfiguration();

        pnConfiguration.setSubscribeKey("sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe");
        pnConfiguration.setPublishKey("pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f");

        pnConfiguration.setLogVerbosity(PNLogVerbosity.BODY);

        String uniqueID = UUID.randomUUID().toString();
        pnConfiguration.setUuid(uniqueID);

        pubnub = new PubNub(pnConfiguration);


        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                Log.e("PUNUB", status.toString());


                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost


                    Log.e("PUNUB", String.valueOf(status.getErrorData()));
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

                        Log.e("PUNUB CONECT", String.valueOf(pubnub.getConfiguration()));


                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.

                    Log.e("connectivity is lost", "PNReconnectedCategory");
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
                // Handle new message stored in message.data.message
                if (message.getActualChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getActualChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscribedChannel()
                }

            /*
                log the following items with your favorite logger
                    - message.getMessage()
                    - message.getSubscribedChannel()
                    - message.getTimetoken()
            */
                System.out.println("chat Api response");


                Log.i("received message Main", String.valueOf(message.getMessage()));

                Log.i("subscribedchannel Main", String.valueOf(message.getChannel()));
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

                Log.i("presence", presence.toString());

                Log.i("presence channel", presence.getChannel());

            }
        });

        pubnub.subscribe().channels(Arrays.asList("awesomeChannel")).execute();


    }

    @Override
    public void didReceiveMessage(PNMessageResult messageResult) {


        Boolean isVideoCalling = Boolean.FALSE;

        System.out.println("listner for message MainActivity Page");

        Log.i("ChatMessage :::MainPage", messageResult.getMessage().toString());

//        if (receivedChannel.equals(currentChannel)) {

        try {


            final JSONObject jsonData = new JSONObject(messageResult.getMessage().toString());

            currentUserId = Tools.getData(this, "idprofile");
            Log.d("json res on main page", String.valueOf(jsonData));

            if(jsonData.has("senderID")){

                senderId = jsonData.getString("senderID");
            }


            if(jsonData.has("chatType")){

                chatType = jsonData.getString("chatType");
            }

            if(jsonData.has("Name")){

                Name = jsonData.getString("Name");
            }

            if(jsonData.has("actual_channel")){

                currentChannel = jsonData.getString("actual_channel");
            }




            if(jsonData.has("isVideoCalling")){

                if(jsonData.getString("isVideoCalling").equals("yes")){

                    isVideoCalling = Boolean.TRUE;
                }
                else{

                    isVideoCalling = Boolean.FALSE;
                }

            }
//

//            roopa
//            else if(jsonData.getString("Name").equals("WISDirectInvite")){
//
//
//                connectToBroadCastView(jsonData);
//            }









            System.out.println("message Main page------->" + message);
            System.out.println("Name on mainpage------->" + Name);


            System.out.println("senderId Main page------->" + senderId);

            System.out.println("receiverId Main  page------->" + receiverId);

            System.out.println("ownerId Main page------->" + currentUserId);


            System.out.println("ChatApi.getInstance().getUserCurrentChannel()------->" + ChatApi.getInstance().getUserCurrentChannel());


            if(isVideoCalling) {

                final String currentUser = Tools.getData(this, "idprofile");

                contentType = "empty";

                if (chatType.equals("Groupe")) {


                    if (jsonData.has("group_mebers")) {

                        memberids = jsonData.getString("group_mebers");
                    } else if (jsonData.has("ReceiverID")) {

                        memberids = jsonData.getString("ReceiverID");
                    }


                    Log.e("memberids", memberids);

                    if (memberids.contains(currentUser)) {

                        receiverId = currentUser;

//                    Log.d("CURRENT GroupCount::",jsonData.getString("GroupCount"));
                    }
                } else {


                    if (!Name.equals("RejectOnetoONE")) {

                        if (jsonData.has("receiverID")) {

                            receiverId = jsonData.getString("receiverID");

                        } else if (jsonData.has("ReceiverID")) {


                            receiverId = jsonData.getString("ReceiverID");
                        }

                        if (jsonData.has("ReceiverCountry")) {

                            receiverCountry = jsonData.getString("ReceiverCountry");
                        }


                        if (jsonData.has("ReceiverImage")) {

                            receiverImage = jsonData.getString("ReceiverImage");
                        }

                        if (jsonData.has("senderCountry")) {

                            senderCountry = jsonData.getString("senderCountry");
                        }

                        if (jsonData.has("senderImage")) {

                            senderIcon = jsonData.getString("senderImage");
                        }

                        if (jsonData.has("senderName")) {

                            senderName = jsonData.getString("senderName");
                        }


                    }

//                contentType = jsonData.getString("contentType");
                }


                if (Name.equals("RejectOnetoONE") || Name.equals("PublisherRejectOnetoONE")) {
                    ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
                    ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
                    ChatApi.getInstance().stopRingTone();


//                ApplicationController.getInstance().stopTimer();
//               ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

//                ApplicationController.getInstance().stopPoupHandler();

                    Log.i("Main Page", "RejectOnetoONE");

                    Log.e("InputEventOccurred", String.valueOf(ApplicationController.getInstance().getIsInputEventOccurred()));

                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        public void run() {

                            Log.d("OnCurretn Context", String.valueOf(ChatApi.getInstance().getPublisherContext()));

//                        Intent it=new Intent(ApplicationController.this, Chat.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(it);
                            if (ChatApi.getInstance().getPublisherContext() != null) {

                                ChatApi.getInstance().setOPENTOKSESSIONID("");

                                ChatApi.getInstance().setOPENTOKTOKEN("");

                                ChatApi.getInstance().setOPENTOKAPIKEY("");


                                ((Activity) ChatApi.getInstance().getPublisherContext()).finish();

                                ApplicationController.getInstance().stopPreviewScreenTimer();


//                                if(senderId.equals(currentUserId)){
//
//                                    if(!callConnected){
//
//                                        showpopup(ChatApi.getInstance().getPublisherContext(),"Amis",receiverId);
//                                    }
//                                }else{
//
//
//                                }


                            } else {

                                Log.e("ERROR ON", "CONTEXT");
                            }


//                          new SingleCallMainActivity().customRejectCall();

//                          ((SingleCallMainActivity())getBaseContext()).finish();

                            Toast.makeText(getApplicationContext(), "Call Disconnected", Toast.LENGTH_LONG).show();
                        }
                    });


                } else if (Name.equals("RejectConferenceVideoCall")||(Name.equals("PubRejectConferenceVideoCall"))) {


                    Handler h = new Handler(Looper.getMainLooper());
                    h.post(new Runnable() {
                        public void run() {

//                            Log.d("OnCurretn Context", String.valueOf(ChatApi.getInstance().getPublisherContext()));
//
//                        Intent it=new Intent(ApplicationController.this, Chat.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(it);


                            Log.e("CurrentUser Chanel", String.valueOf(ApplicationController.getInstance().currentChannel));

                            try {

                                if (ApplicationController.getInstance().getUserCurrentChannel().equals(currentChannel)) {

                                    ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

                                    ChatApi.getInstance().setOPENTOKSESSIONID("");

                                    ChatApi.getInstance().setOPENTOKTOKEN("");

                                    ChatApi.getInstance().setOPENTOKAPIKEY("");

                                    ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

                                    ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
                                    ChatApi.getInstance().stopRingTone();
                                    Log.i("Main Page", "RejectConferenceVideoCall");



                                    System.out.println("Curent session Action");

                                    Log.d("Channel", currentChannel);

                                    if (ChatApi.getInstance().getPublisherContext() != null) {

                                        ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
                                        Toast.makeText(getApplicationContext(), "Call disconnected", Toast.LENGTH_LONG).show();


                                    } else {

                                        Log.e("ERROR ON", "CONTEXT");
                                    }


                                } else {

                                    System.out.println("No Current session Action");

                                    Log.d("Channel", currentChannel);

//                                if (ChatApi.getInstance().getPublisherContext() != null) {
//
//
//
//                                    ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                                    Toast.makeText(getApplicationContext(),"Call disconnected",Toast.LENGTH_LONG).show();
//
//
//
//                                }else{
//
//                                    Log.e("ERROR ON","CONTEXT");
//                                }

                                }


//                            if (ChatApi.getInstance().getPublisherContext() != null) {
//
//
//
//                                    ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                                    Toast.makeText(getApplicationContext(),"Call disconnected",Toast.LENGTH_LONG).show();
//
//
//
//                            }else{
//
//                                Log.e("ERROR ON","CONTEXT");
//                            }


//                          ((SingleCallMainActivity())getBaseContext()).finish();

//                        Toast.makeText(getApplicationContext(), "Call Rejected", Toast.LENGTH_LONG).show();
                            } catch (NullPointerException ex) {

                                Log.e("Exception", "NULL");
                            }

                        }


                    });


                }
//            currentUser.equals(receiverId)
                else if (currentChannel != null) {


//                Private_951

//                Private_966

                    if (Name.equals("callerBusyMode")) {

                        Log.d("Type", "callerBusyMode");

                        Handler h = new Handler(Looper.getMainLooper());
                        h.post(new Runnable() {
                            public void run() {

//                        Intent it=new Intent(ApplicationController.this, Chat.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(it);

//                            Log.d("SENDER ID::",senderId);
//
//                            Log.d("CURRENT USER ID::",Tools.getData(getApplicationContext(),"idprofile"));


                                if (receiverId.equals(Tools.getData(getApplicationContext(), "idprofile"))) {

                                    System.out.println("STOP VIDEO SESSION");

                                    Log.d("CURRENT USER ID::", Tools.getData(getApplicationContext(), "idprofile"));


                                    if (chatType.equals("Groupe")) {


                                        System.out.println(ChatApi.getInstance().rejectedMemberArry.size());

                                        System.out.println(memberids.length() - 1);

                                        Log.e("CurrentUser Status", String.valueOf(ChatApi.getInstance().getUserIsAvailable()));

                                        Log.e("rejectedMemberArry", String.valueOf(ChatApi.getInstance().rejectedMemberArry.size()));


                                        Log.e("memberids", String.valueOf(memberArray.size()));

//                                    Log.e("membe", String.valueOf(memberArray.size()));


//                                   memberids

                                        String sessionid = "";

                                        try {
                                            if(jsonData.has("opentok_session_id")){

                                                sessionid =jsonData.getString("opentok_session_id");
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            if (currentChannel != null) {
                                                if (ApplicationController.getInstance().getUserCurrentChannel().equals(currentChannel) && sessionid.equals(ChatApi.getInstance().getOPENTOKSESSIONID())) {

                                                    if (!ApplicationController.getInstance().getCallConnected()) {

                                                        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

                                                        ((Activity) ChatApi.getInstance().getPublisherContext()).finish();

                                                        ((Activity) ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(getApplicationContext(), "Busy Mode", Toast.LENGTH_LONG).show();
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                        catch (NullPointerException ex){

                                            Log.e("EXCEPT APPLICA::",ex.getLocalizedMessage());
                                        }

//                                    for(int i=0;i<memberList.size();i++){
//
//                                        if(callOwnerId.equals(memberList.get(i))){
//
//
//                                        }
//                                    }

//                                    Log.e("totalMemberCount", String.valueOf(totalMemberCount));
//
//                                    if(ChatApi.getInstance().rejectedMemberArry.size()==totalMemberCount-2){
//
//
//                                        if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                            ChatApi.getInstance().claerBusyModeUsers();
//
////                                            ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                                        }
//                                    }
//                                    else{
//
//                                        try {
//                                            String rejetcedMeberIds = jsonData.getString("currenIDs");
//
//                                            ChatApi.getInstance().addbusyModeUser(rejetcedMeberIds);
//
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }

                                    } else {


                                        if (ChatApi.getInstance().getPublisherContext() != null) {

                                            ((Activity) ChatApi.getInstance().getPublisherContext()).finish();

                                            ((Activity) ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Busy Mode", Toast.LENGTH_LONG).show();
                                                }
                                            });


                                        }
                                    }


                                } else {

//                                    if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                        ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                                    }
                                }


                            }
                        });
//
//
                    } else if (Name.equals("OnetoOnePopupViewShow")) {

                        if (senderId.equals(currentUserId)) {
                            System.out.println("if caaling");

                            ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);


                        } else {

                            System.out.println("USER STATUS" + ChatApi.getInstance().getUserIsAvailable());


                            if (ChatApi.getInstance().getUserIsAvailable()) {

                                ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

                                System.out.println("user state" + ChatApi.getInstance().getUserIsAvailable());


                                Log.e("USER AVAILABLE", String.valueOf(ChatApi.getInstance().getUserIsAvailable()));


                                ChatApi.getInstance().setOPENTOKSESSIONID(jsonData.getString("opentok_session_id"));

                                ChatApi.getInstance().setOPENTOKTOKEN(jsonData.getString("opentok_token"));

                                ChatApi.getInstance().setOPENTOKAPIKEY(jsonData.getString("opentok_apikey"));

//                        true

                                ChatApi.getInstance().setUpRingTone(getApplicationContext());
                                ChatApi.getInstance().playRingTone();






//                        Intent i = new Intent(this, Phone.class);
//                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        i.putExtra("receiver_name",senderName);
//                        i.putExtra("receiverID",receiverId);
//                        i.putExtra("actual_channel",currentChannel);
//                        i.putExtra("senderID",senderId);


//
//                        Map<String, String> json = new HashMap<>();
//                        json.put("message", " Call Connecting");
//                        json.put("receiverID", "216");
//                        json.put("Name","callerBusyMode");
//                        json.put("contentType", "text");
//                        json.put("senderID", Tools.getData(this, "idprofile"));
//                        json.put("senderName", Tools.getData(this, "name"));
////                        json.put("ReceiverName", receiverFirstName);
//                        json.put("chatType", "amis");
//                        json.put("isVideoCalling","yes");
//                        json.put("type","Call Connecting");
//                        json.put("actual_channel", currentChannel);
//                        String senderPhoto = Tools.getData(this, "photo");
//                        json.put("senderImage", senderPhoto);
//                        json.put("ReceiverImage", senderPhoto);
//                        json.put("senderCountry", Tools.getData(this, "country"));
//                        json.put("ReceiverCountry", "India");
//
//                        System.out.println("json ==========>" +json);
//
//
//                        i.putExtra("senderID", senderId);
//                        startActivity(i);

                                Log.i("Main page", "OneToOnePopupViewShow");


//                        review screen

//                            ApplicationController.getInstance().setInputEventOccurred(Boolean.FALSE);

                                final Intent it = new Intent(this, SingleCallMainActivity.class);
                                it.putExtra("activate_reject_btn", "YES");
                                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                it.putExtra("receiver_name", senderName);
                                it.putExtra("receiverId", receiverId);
                                it.putExtra("actual_channel", currentChannel);
                                it.putExtra("senderID", senderId);

                                it.putExtra("message", "CallBusy");
                                it.putExtra("contentType", "text");
                                it.putExtra("senderName", senderId);
//                        json.put("ReceiverName", receiverFirstName);
                                it.putExtra("chatType", "amis");
                                it.putExtra("isVideoCalling", "yes");
                                it.putExtra("type", "Connecting");
                                String senderPhoto = Tools.getData(this, "photo");
                                it.putExtra("senderImage", senderPhoto);
                                it.putExtra("ReceiverImage", senderPhoto);
                                it.putExtra("senderCountry", Tools.getData(this, "country"));
                                it.putExtra("ReceiverCountry", "India");
                                startActivity(it);

//                            ((Activity)ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    ApplicationController.getInstance().startPreviewScreenTimer();
//                                    startPopUpTimer();
//
//                                }
//                            });


//                            startPoupHandler();


                            } else {

                                Map<String, String> json = new HashMap<>();
                                json.put("message", "CallBusy");
                                json.put("receiverID", senderId);
                                json.put("ReceiverID", senderId);
                                json.put("Name", "callerBusyMode");
                                json.put("contentType", "text");
                                json.put("senderID", senderId);
                                json.put("senderName", Tools.getData(this, "name"));
//                        json.put("ReceiverName", receiverFirstName);
                                json.put("chatType", "amis");
                                json.put("isVideoCalling", "yes");
                                json.put("type", "Call Connecting");
                                json.put("actual_channel", currentChannel);
                                String senderPhoto = Tools.getData(this, "photo");
                                json.put("senderImage", senderPhoto);
                                json.put("ReceiverImage", senderPhoto);
                                json.put("senderCountry", Tools.getData(this, "country"));
                                json.put("ReceiverCountry", "India");


                                json.put("opentok_session_id",jsonData.getString("opentok_session_id"));
                                json.put("opentok_token",jsonData.getString("opentok_session_id"));
                                json.put("opentok_apikey",jsonData.getString("opentok_session_id"));

                                System.out.println("json ==========>" + json);
                                ChatApi.getInstance().sendMessage(json, currentChannel);


                            }


                        }
                    }



//            E/else calling: phone page
//            E/calling: called
//            I/System.out: json params==========>{actual_channel=Private_951, ReceiverName=null, chatType=amis, senderID=225, contentType=text, senderImage=img_pub1482331044.png, ReceiverCountry=India, senderName=chat USER M, senderCountry=Antarctica, isAcceptCall=yes, message=Call Connected, ReceiverImage=img_pub1482331044.png, type=Call Accepted, Name=connecting, receiverID=216}
//            I/System.out: REceiver Id else ==>0
//            I/System.out: REceiver Amis Id ==>
//            I/System.out: current channel ==>0
//            D/sende message called: {actual_channel=Private_951, ReceiverName=null, chatType=amis, senderID=225, contentType=text, senderImage=img_pub1482331044.png, ReceiverCountry=India, senderName=chat USER M, senderCountry=Antarctica, isAcceptCall=yes, message=Call Connected, ReceiverImage=img_pub1482331044.png, type=Call Accepted, Name=connecting, receiverID=216}

                    else if (Name.equals("connecting")) {


                        Log.e("NAME TYPE :::", "Connecting");

                        ChatApi.getInstance().stopRingTone();

                        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

                        ChatApi.getInstance().getCallListener().connectStreamingToOwner();

                        ApplicationController.getInstance().setCallConnected(Boolean.TRUE);


                        if (senderId.equals(currentUserId)) {

                            System.out.println("if connected");

                            System.out.println("if caaling");


                        } else {


//                        Intent it = new Intent(this, SingleCallMainActivity.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        it.putExtra("receiver_name", senderName);
//                        it.putExtra("receiverId", receiverId);
//                        it.putExtra("actual_channel", currentChannel);
//                        it.putExtra("senderID", senderId);
//
//
//                        it.putExtra("message", "CallBusy");
//                        it.putExtra("contentType", "text");
//                        it.putExtra("senderName", senderId);
////                        json.put("ReceiverName", receiverFirstName);
//                        it.putExtra("chatType", "amis");
//                        it.putExtra("isVideoCalling", "yes");
//                        it.putExtra("type", "Connecting");
//                        String senderPhoto = Tools.getData(this, "photo");
//                        it.putExtra("senderImage", senderPhoto);
//
//                        it.putExtra("senderImage", senderPhoto);
//                        it.putExtra("ReceiverImage", senderPhoto);
//                        it.putExtra("senderCountry", Tools.getData(this, "country"));
//                        it.putExtra("ReceiverCountry", "India");

                        }
//
//
//                startActivity(it);

//                    if(senderId.equals(currentUserId)){
//
//                    Intent intent = new Intent(this, SingleCallMainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//
//
//                }

                    }


//

                    else if (Name.equals("RejectOnetoONE")) {

                        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
                        ChatApi.getInstance().stopRingTone();
                        Log.i("Main Page", "RejectOnetoONE");

                        ChatApi.getInstance().setOPENTOKSESSIONID("");

                        ChatApi.getInstance().setOPENTOKTOKEN("");

                        ChatApi.getInstance().setOPENTOKAPIKEY("");



                        Handler h = new Handler(Looper.getMainLooper());
                        h.post(new Runnable() {
                            public void run() {

                                Log.d("OnCurretn INSIDEContext", String.valueOf(ChatApi.getInstance().getPublisherContext()));
                                ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

//                            stopTimer();

                                Intent it = new Intent(ApplicationController.this, Chat.class);
                                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(it);
                                stopPreviewScreenTimer();

//                            if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//                            }else{
//
//                                Log.e("ERROR ON","CONTEXT");
//                            }


//                          new SingleCallMainActivity().customRejectCall();

//                          ((SingleCallMainActivity())getBaseContext()).finish();

                                Toast.makeText(getApplicationContext(), "Call Rejected", Toast.LENGTH_LONG).show();
                            }
                        });


                    } else if (Name.equals("MultiCall")) {

                        Log.d("GROPUCALL", "INITIALZED");

                        if (ChatApi.getInstance().getUserIsAvailable()) {

                            Log.e("GROPUCALL", "USER IS Available");

                        } else {

                            Log.e("GROPUCALL", "USER IS NOT Available");
                        }

                        if (senderId.equals(currentUserId)) {


                            Log.d("GROPUCALL", "IS OWNER OF THE CALL");
                        } else {

                            if (ChatApi.getInstance().getUserIsAvailable()) {

//                        alert receiver



                                ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

                                ChatApi.getInstance().setOPENTOKSESSIONID(jsonData.getString("opentok_session_id"));

                                ChatApi.getInstance().setOPENTOKTOKEN(jsonData.getString("opentok_token"));

                                ChatApi.getInstance().setOPENTOKAPIKEY(jsonData.getString("opentok_apikey"));



                                ChatApi.getInstance().setUpRingTone(getApplicationContext());
                                ChatApi.getInstance().playRingTone();
                                ChatApi.getInstance().setUserIsAvailable(false);


//                        make pub nub to intimate  receiver

//                            ApplicationController.getInstance().setInputEventOccurred(Boolean.FALSE);

//                            ApplicationController.getInstance().startPreviewScreenTimer();
//                            startPopUpTimer();

//                            startPoupHandler();


                                Intent it = new Intent(this, GroupCallMainActivity.class);
                                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                it.putExtra("receiver_name", senderName);
                                it.putExtra("receiverID", receiverId);
                                it.putExtra("actual_channel", currentChannel);
                                it.putExtra("senderID", senderId);
                                it.putExtra("Name", "connecting Multiple Call");

                                it.putExtra("message", "connecting Multiple Call");
                                it.putExtra("contentType", "text");
                                it.putExtra("senderName", senderName);
//                        json.put("ReceiverName", receiverFirstName);
                                it.putExtra("chatType", "Groupe");
                                it.putExtra("isVideoCalling", "yes");
                                it.putExtra("type", "Connecting");
                                String senderPhoto = Tools.getData(this, "photo");
                                it.putExtra("senderImage", senderPhoto);
                                it.putExtra("receiverGroup", "");
                                it.putExtra(" GroupNewCount", "2");
                                it.putExtra("senderImage", senderPhoto);
                                it.putExtra("ReceiverImage", senderPhoto);
                                it.putExtra("senderCountry", Tools.getData(this, "country"));
                                it.putExtra("ReceiverCountry", "India");

                                String mememberArray = jsonData.getString("receiverGroup");

                                String[] strArray = new String[]{mememberArray};

                                Log.d(" mememberArray", String.valueOf(strArray));

//                        JSONArray groupMemberDetails = jsonData.getJSONArray("receiverGroup");
//


                                it.putExtra("members_details", strArray);


                                startActivity(it);


                                //                    end here

                            } else {

                                Log.e("GROPUCALL", "USER IS NOT Available");

                                if (senderId.equals(Tools.getData(getApplicationContext(), "idprofile"))) {

                                    Toast.makeText(getApplicationContext(), "Caller Busy", Toast.LENGTH_LONG).show();
                                }


                                Map<String, String> json = new HashMap<>();
                                json.put("message", "CallBusy");
                                json.put("receiverID", receiverId);
                                json.put("Name", "callerBusyMode");
                                json.put("contentType", "text");
                                json.put("senderID", senderId);
                                json.put("senderName", Tools.getData(this, "name"));
//                        json.put("ReceiverName", receiverFirstName);
                                json.put("chatType", "Groupe");
                                json.put("isVideoCalling", "yes");
                                json.put("type", "Call Connecting");
                                json.put("actual_channel", currentChannel);
                                String senderPhoto = Tools.getData(this, "photo");
                                json.put("senderImage", senderPhoto);
                                json.put("ReceiverImage", senderPhoto);
                                json.put("senderCountry", Tools.getData(this, "country"));
                                json.put("ReceiverID", jsonData.getString("group_mebers"));
                                json.put("ReceiverCountry", "India");
                                json.put("callOwnerId", senderId);


                                json.put("group_mebers", jsonData.getString("group_mebers"));
                                json.put("GroupCount", String.valueOf(memberArray.size()));
                                json.put("currenIDs", Tools.getData(getApplicationContext(), "idprofile"));


                                json.put("opentok_session_id",jsonData.getString("opentok_session_id"));
                                json.put("opentok_token",jsonData.getString("opentok_session_id"));
                                json.put("opentok_apikey",jsonData.getString("opentok_session_id"));

                                json.put("receiverGroup", "");

                                System.out.println("json ==========>" + json);
                                ChatApi.getInstance().sendMessageForGroupVideo(json, currentChannel, null);

                            }


                        }


                    }
//                "senderID":"226"

//                "senderID":"212"

                    else if (Name.equals("connecting Multiple Call")) {

//                    senderId.equals(currentUserId)

                        Log.e("Connection Status", String.valueOf(ChatApi.getInstance().isStreamingconnected));


//                    if(currentUserId.equals(senderId)){
//
//
//                    }


                        if (senderId.equals(currentUserId)) {

                            ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver(senderId);

                        } else {

//                        ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver(receiverId);
                        }


//                    if(senderId.equals(currentUserId)){
//
//                        Log.d("GROPUCALL","IS OWNER OF THE CALL");
//
//                        if(!ChatApi.getInstance().isStreamingconnected){
//
//                            ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver("1");
//
//                        }else{
//
//                            Log.e("STreamig conected","Live In Progress");
//                        }
//
//
////                        212,220,216,137,225,218,218
//
//                    }else{
//
//                        Log.d("USERCONNECTED","TO GROUPCALLS");
//
//                        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
//
//                        ChatApi.getInstance().stopRingTone();
//
//
//                        if(!ChatApi.getInstance().isStreamingconnected){
//
//                            ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver("1");
//
//                        }else{
//
//                            Log.e("STreamig conected","Live In Progress");
//                        }


//                        if(!ChatApi.getInstance().isStreamingconnected){
//
//                            ChatApi.getInstance().getCallListener().connectStreamingToOwner();
//                        }else{
//
//                            Log.e("STreamig conected","Live In Progress");
//                        }

//                        ChatApi.getInstance().getCallListener().groupCallAcceptedFromReceiver("0");

//                        redirect to GroupVideoView

//                        Intent it = new Intent(this,GroupCallMainActivity.class);
//                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        it.putExtra("receiver_name", senderName);
//                        it.putExtra("receiverID", receiverId);
//                        it.putExtra("actual_channel", currentChannel);
//                        it.putExtra("senderID", senderId);
//                        it.putExtra("Name","connecting Multiple Call");
//
//                        it.putExtra("message", "connecting Multiple Call");
//                        it.putExtra("contentType", "text");
//                        it.putExtra("senderName", senderId);
////                        json.put("ReceiverName", receiverFirstName);
//                        it.putExtra("chatType", "Groupe");
//                        it.putExtra("isVideoCalling", "yes");
//                        it.putExtra("type", "Connecting");
//                        String senderPhoto = Tools.getData(this, "photo");
//                        it.putExtra("senderImage", senderPhoto);
//                        it.putExtra("receiverGroup","");
//                        it.putExtra(" GroupNewCount","2");
//                        it.putExtra("senderImage", senderPhoto);
//                        it.putExtra("ReceiverImage", senderPhoto);
//                        it.putExtra("senderCountry", Tools.getData(this, "country"));
//                        it.putExtra("ReceiverCountry", "India");
//
//                        startActivity(it);


//                    }
                    } else if (Name.equals("Reject")) {

//                    ChatApi.getInstance().stopRingTone();
//                    Log.i("Main Page", "Reject");
//                    Intent it = new Intent(ApplicationController.this, Chat.class);
//                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(it);


                    } else {

                        Log.i("Name", "else calling");
                        Log.e("name", "else call");
                    }

                } else {

                    System.out.println("_____________********************__________");

                    Log.e("WIS AUDIO CALL", "DO NOT SHOW POP");
                }


            }

            else{
                System.out.println("______________________");

                Log.d("ISCHATVIEW","Redirect To ChatView");

            }


        } catch (JSONException e) {

            e.printStackTrace();
        }





    }

//    compile group: 'org.apache.httpcomponents', name: 'httpmime', version: '4.5.2'


    @Override
    public void didReceivePresenceEvent(PNPresenceEventResult eventResult) {


        Log.d("presenceEvent:Main Page", eventResult.getChannel());

        Log.d("presenceEvent: MainPage", eventResult.getState().toString());

    }

    @Override
    public void onPublishSuccess(PNStatus status) {

        if (!status.isError()) {

            Log.d("message on MainPage ", String.valueOf(status));


        }


    }

    public void showAlert(String message, String senderName) {

        Log.i("notification", "alert");

        Tools.showNotif2(this, 0, senderName, message);

    }

    public void setTimer() {



//        showpopup();

//        new CountDownTimer(30, 1000) {
//
//            public void onTick(long millisUntilFinished) {
////                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
//
//                Toast.makeText(getApplicationContext(), "seconds remaining: " + millisUntilFinished / 1000, Toast.LENGTH_LONG).show();
//
//
//
//
//            }
//
//            public void onFinish() {
////                mTextField.setText("done!");
//
////                showpopup();
//
//
//                if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                    ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//
//
//                    showpopup();
//
//
//                }else{
//
//                    Log.e("ERROR ON","CONTEXT");
//                }
//
//
////                Toast.makeText(getApplicationContext(), "onFinish: ", Toast.LENGTH_LONG).show();
//            }
//        }.start();


    }

//    public void stopTimer(){
//
//        Log.d("Stop timer thread","timer");
//
//        if(t!=null){
//            t.currentThread().interrupt();
//
//            Log.e("Thread","Inside");
//            Thread t1 = t;
//            t = null;
//            t1.interrupt();
//
//
//
//        }
//
//
//    }

    public void startCountdown(){

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
//                mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);

                Log.d("seconds remaining", String.valueOf(millisUntilFinished / 1000));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {

            }

        }.start();
    }

    public void startTimer(final Context con, final String type, final String receiver, final String currentChannel){

        Log.d("TIMER STARTED","TIMER STARTEd");

        final long timeBeforeThreadStart = System.currentTimeMillis();

        final boolean isInputEventOccurred = false;
        t = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while ((System.currentTimeMillis() - timeBeforeThreadStart) < (30 * 1000)) {
                    // you add sleep here if you want instead of loop

//                    Log.d("TIMER INIT","TIMER START");
                }
                if (!isInputEventOccurred) {
                    // write code to close app

                    Log.d("TIMER STOPED","TIMER STOPED");



//                    clearVideoSession();



                    if(con!=null){

                        if(!ApplicationController.getInstance().getCallConnected()){

                            Log.d("Call not connected::","show pop");

                            ApplicationController.getInstance().setShowVoicePopup(Boolean.TRUE);


                            ((Activity)con).finish();

                            ChatApi.getInstance().claerBusyModeUsers();




//                            showpopup(con,type,receiver,currentChannel);





//                            rejectSingleCall(receiver,"Private_".concat(receiver));

//                            Tools.sendMessgaeAlertToServer(con,receiver);

                        }else{

                            Log.d("Call","connected");

                            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
                        }


                    }else{

                        Log.e("ERROR ON","CONTEXT");
                    }


                }else{

                    Log.e("Timer::","isInputEventOccurred else part");
                }

            }
        });
        t.start();
    }

    public void makePubCallsforVoicmessage(Context con,final String type, final String receiver, final String currentChannel){

//        {"actual_channel":"Private_1008","senderID":"263","ReceiverID":"265","Name":"DisconnectCallBothuser"}

        Log.e("makePubCallsfo","Called");

        Map<String, String> json = new HashMap<>();
        json.put("ReceiverID",receiver);
        json.put("Name","DisconnectCallBothuser");
        json.put("actual_channel", currentChannel);
        json.put("chatType",type);
        json.put("senderID",Tools.getData(con, "idprofile"));

        ChatApi.getInstance().sendMessage(json,currentChannel);

    }


    public void rejectSingleCall(String receverStringId,String currentChannel,String type){

        Map<String, String> json = new HashMap<>();
        json.put("message", "RejectOnetoONE");
        json.put("Name","RejectOnetoONE");
        json.put("actual_channel", currentChannel);

        json.put("ReceiverName","dummy");

        json.put("senderID",Tools.getData(this, "idprofile"));
        json.put("receiverID",receverStringId);

        json.put("contentType", "text");

        json.put("senderName", Tools.getData(this, "name"));
//                        json.put("ReceiverName", receiverFirstName);
        json.put("chatType", type);
        json.put("isVideoCalling","yes");
        json.put("type","RejectOnetoONE");
        String senderPhoto = Tools.getData(this, "photo");
        json.put("senderImage", senderPhoto);
        json.put("ReceiverImage", senderPhoto);
        json.put("senderCountry", Tools.getData(this, "country"));
        json.put("ReceiverCountry", "India");

        ChatApi.getInstance().sendMessage(json, currentChannel);
    }


    public void makePubCallsforVoicmessage(final String type, final String receiver, final String currentChannel){

//        {"actual_channel":"Private_1008","senderID":"263","ReceiverID":"265","Name":"DisconnectCallBothuser"}

        Log.e("makePubCallsfo","Called");

        Map<String, String> json = new HashMap<>();
        json.put("ReceiverID",receiver);
        json.put("Name","DisconnectCallBothuser");
        json.put("actual_channel", currentChannel);
        json.put("chatType",type);
        json.put("senderID",currentUserId);

        ChatApi.getInstance().sendMessage(json,currentChannel);

    }
    public void showpopup(final Context context, final String type, final String receiver, final String currentChannel) {

        Log.e("Show voice Message::","Show popup");

        makePubCallsforVoicmessage(type,receiver,currentChannel);

        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

//                       alertDialog.set


                // Setting Dialog Title
                alertDialog.setTitle("Audio Record...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want  record your voice");

                // Setting Icon to Dialog
//        alertDialog.setIcon(R.drawable.delete);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onClick(DialogInterface dialog, int which) {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                                Log.v(TAG, "Permission is granted");
                                //File write logic here

                                pickMediaAudio(context, type, receiver, currentChannel);


                            } else {

//                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.}, REQUEST_CODE);

                                Toast.makeText(getApplicationContext(), "Please make  your storage and microphone permission enabled", Toast.LENGTH_SHORT).show();

                            }
                        }else{


//                    Toast.makeText(getApplicationContext(), "Please make  your storage and microphone permission enabled", Toast.LENGTH_SHORT).show();

                            pickMediaAudio(context, type, receiver, currentChannel);
                        }


//                audioRecordMethod();
                        // Write your code here to invoke YES event
//                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();

//                ((Activity)context).onBackPressed();
                    }
                });

                // Showing Alert Message
                if(!((Activity) context).isFinishing())
                {
                    //show dialog
                    alertDialog.show();
                }


            }
        });

    }


//        MediaRecorder  mediaRecorder;

    private void pickMediaAudio(final Context context, final String type, final String receiver, final String currentChannel) {



//            MediaRecorder mediaRecorder;

        // custom dialog
        dialoge = new Dialog(context);
        dialoge.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialoge.setCancelable(false);

        dialoge.setContentView(R.layout.dialog_share_actu);
        // Custom Android Allert Dialog Title
        dialoge.setTitle("Custom Dialog Example");

        final Button buttonstop = (Button) dialoge.findViewById(R.id.buttonstop);
        Button buttonplay = (Button) dialoge.findViewById(R.id.buttonplay);
        final Button buttonsendfile = (Button) dialoge.findViewById(R.id.buttonsendfile);
        final Button startButton = (Button) dialoge.findViewById(R.id.startButton);

        final ImageView microphoneIV = (ImageView) dialoge.findViewById(R.id.microphoneIV);
        final ImageView no_microphoneIV = (ImageView) dialoge.findViewById(R.id.no_microphoneIV);

        buttonstop.setEnabled(false);


        // Click cancel to dismiss android custom dialog box
        microphoneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                microphoneIV.setVisibility(View.GONE);
                no_microphoneIV.setVisibility(View.VISIBLE);
//                 mediaRecorder.stop();
//                microphoneIV.setEnabled(false);
//                startButton.setEnabled(true);

                Toast.makeText(ApplicationController.this, "Recording stopped",
                        Toast.LENGTH_LONG).show();
            }
        });
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("Audio record","Start");





                Toast.makeText(ApplicationController.this, "Recording started",
                        Toast.LENGTH_LONG).show();

                AudioSavePathInDevice =
                        Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                CreateRandomAudioFileName(5) + "AudioRecording.mp3";


                File AudioFile = new File(AudioSavePathInDevice);
                System.out.println("Audio File" + AudioFile);


//                    MediaRecorderReady();



                mediaRecorder = new MediaRecorder();
                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                mediaRecorder.setOutputFile(AudioSavePathInDevice);
                System.out.println("Audio path" + AudioSavePathInDevice);

                try {

                    mediaRecorder.prepare();
                    mediaRecorder.start();



                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                startButton.setEnabled(false);
                buttonstop.setEnabled(true);
                microphoneIV.setEnabled(true);

                Toast.makeText(ApplicationController.this, "Recording started",
                        Toast.LENGTH_LONG).show();

            }
        });
        buttonstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mediaRecorder!=null) {

                    try {

                        mediaRecorder.stop();
                        mediaRecorder.release();



                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }



                    buttonstop.setEnabled(false);
//                buttonPlayLastRecordAudio.setEnabled(true);
                    startButton.setEnabled(true);
//                buttonStopPlayingRecording.setEnabled(false);

                    Toast.makeText(ApplicationController.this, "Recording Completed",
                            Toast.LENGTH_LONG).show();
                }else{

                    Toast.makeText(ApplicationController.this, "Recording not Completed Check Your Storage Permission",
                            Toast.LENGTH_LONG).show();
                }


//                dialog.dismiss();
//                Toast.makeText(getApplicationContext(),"button stop",Toast.LENGTH_LONG).show();
            }
        });

        // Your android custom dialog ok action
        // Action for custom dialog ok button click
        buttonplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonstop.setEnabled(false);
                startButton.setEnabled(false);
//                buttonStopPlayingRecording.setEnabled(true);


                if(AudioSavePathInDevice!=null) {
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(AudioSavePathInDevice);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    mediaPlayer.start();
                    Toast.makeText(ApplicationController.this, "Recording Playing",
                            Toast.LENGTH_LONG).show();
                }
//                Toast.makeText(getApplicationContext(),"button play",Toast.LENGTH_LONG).show();

//                dialog.dismiss();
            }
        });


        // Action for custom dialog ok button click
        buttonsendfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "button send file", Toast.LENGTH_LONG).show();
                dialoge.dismiss();

//                    rejectSingleCall(receiver,currentChannel,type);

//                    new DoAudioUpload().execute(AudioSavePathInDevice);

                if(AudioSavePathInDevice==null){

                    Toast.makeText(getApplicationContext(), "No Recording Found", Toast.LENGTH_LONG).show();

                    ((Activity)ChatApi.getInstance().getPublisherContext()).finish();

                }else {

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            JSONObject jsonObject = Tools.uploadImage(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.uploadVoiceMessage), AudioSavePathInDevice, Tools.getData(ApplicationController.this, "token"), Tools.getData(ApplicationController.this, "idprofile"),"",type,receiver);

                            if (jsonObject != null) {



                                Log.d("RESPONSE ", "HERE");

                                Log.d("RESPONSE", String.valueOf(jsonObject));

                                ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

                                ((Activity)ChatApi.getInstance().getPublisherContext()).finish();

                                ((Activity)ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        Toast.makeText(getApplicationContext(), "Voice Message Sent", Toast.LENGTH_LONG).show();
                                    }
                                });


                            }
                        }
                    });

                }






            }
        });

        if(!((Activity) context).isFinishing())
        {
            //show dialog
            dialoge.show();
        }






    }

    private void requestPermission() {

//        ActivityCompat.requestPermissions((), new
//                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    public String CreateRandomAudioFileName(int string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int i = 0;
        try {

            while (i < string) {

                stringBuilder.append(RandomAudioFileName.
                        charAt(random.nextInt(RandomAudioFileName.length())));

                i++;
            }


        } catch (NullPointerException exce) {
            System.out.println("exce ==>" + exce.getMessage());
        }
        return stringBuilder.toString();
    }







    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

//    @Override
//    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//
//    }
//
//    @Override
//    public void onActivityStarted(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityResumed(Activity activity) {
//
//        Log.e("WIS APP","onActivityResumed");
//    }
//
//    @Override
//    public void onActivityPaused(Activity activity) {
//
//        Log.e("WIS APP","onActivityPaused");
//    }
//
//    @Override
//    public void onActivityStopped(Activity activity) {
//
//        try {
//            boolean foreground = new ForegroundCheckTask().execute(getApplicationContext()).get();
//            if(!foreground) {
//                //App is in Background - do what you want
//
//
//                Log.e("WIS APP","BACKROUND MODE");
//            }
//
//            else{
//
//                Log.e("WIS APP","APP CLOSED");
//
////                ChatApi.getInstance().unSubscribeChannels();
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
//
//        Log.e("WIS APP","SaveInstanceState");
//    }
//
//    @Override
//    public void onActivityDestroyed(Activity activity) {
//
//        Log.e("WIS APP","APP Destroyed");
//    }


    public class DoAudioUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            System.out.println("result on Audio =======>" + result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image results%@", String.valueOf(img));
                    String type;
//                    if (is_group) {
//                        type = "Groupe";
//                    } else {
//                        type = "Amis";
//                    }


                    System.out.println("fileurl=====>" + AudioSavePathInDevice);
                    Map<String, String> json = new HashMap<>();
                    json.put("message", img.getString("data"));
                    json.put("chatType", "Amis");
                    json.put("receiverID", receiverId);
                    json.put("contentType", "Audio");
                    json.put("senderID", Tools.getData(ApplicationController.this, "idprofile"));
                    json.put("senderName", Tools.getData(ApplicationController.this, "name"));
                    String senderPhoto = Tools.getData(ApplicationController.this, "photo");

                    json.put("senderImage", senderPhoto);

                    json.put("actual_channel", currentChannel);


                    System.out.println("senderId" + Tools.getData(ApplicationController.this, "idprofile"));


                    ChatApi.getInstance().sendMessage(json, currentChannel);



//                    Chat.sendMsg(img.getString("data"), Chat.fId, "photo", "",type);


//                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
//                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(ApplicationController.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {
            ArrayList<String> images = new ArrayList<>();
            images.add(imgPath);

            Log.d("Aduo recording filePath",urls[0]);

//            ArrayList<String> audiourls = new ArrayList<>();
//            images.add(fileUrl);
            return Tools.doFileUploadForChat(getApplicationContext().getString(R.string.server_url) + getApplicationContext().getString(R.string.savePhotoAction),urls[0], Tools.getData(ApplicationController.this, "token"), Tools.getData(ApplicationController.this, "idprofile"), "", "");


//            /storage/sdcard0/JCIENAudioRecording.3gp
        }



    }

    public boolean validateMicAvailability(){
        Boolean available = true;
        AudioRecord recorder =
                new AudioRecord(MediaRecorder.AudioSource.MIC, 44100,
                        AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_DEFAULT, 44100);
        try{
            if(recorder.getRecordingState() != AudioRecord.RECORDSTATE_STOPPED ){
                available = false;

            }

            recorder.startRecording();
            if(recorder.getRecordingState() !=
                    AudioRecord.RECORDSTATE_RECORDING){
                recorder.stop();
                available = false;

            }
            recorder.stop();
        } finally{
            recorder.release();
            recorder = null;
        }

        return available;
    }


//    public void startPoupHandler(){
//
//        ((Activity)ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//                popupHandler  = new Handler();
//                final Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//
//                        if(!callConnected) {
//
//                            Log.d("Stop timer","call connected");
//
//                            if (ChatApi.getInstance().getPublisherContext() != null) {
//
//                                ((Activity) ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//
//                                        ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);
//
//                                        Toast.makeText(ChatApi.getInstance().getPublisherContext(), "Call Disconnected.", Toast.LENGTH_LONG).show();
//                                    }
//                                });
//
//
//                                ChatApi.getInstance().stopRingTone();
//
//                                ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);
//
//                                ((Activity) ChatApi.getInstance().getPublisherContext()).finish();
//
//
////                            rejectSingleCall(receiver,"Private_".concat(receiver));
//
////                            Tools.sendMessgaeAlertToServer(con,receiver);
//
//                            } else {
//
//                                Log.d("Call", "connected");
//                            }
//
//                        }else{
//
//                            Log.d("Call", "connected");
//                        }
//                    }
//                };
//
//                popupHandler.postDelayed(runnable, 30000);
//            }
//        });
//
//    }
//    public void stopPoupHandler(){
//        if(popupHandler!=null){
//
//            popupHandler.removeCallbacks(null);
//        }
//
//    }

    public void startPreviewScreenTimer(){


        Log.e("startPreviewScreenTimer","started");
        waitTimer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                //called every 300 milliseconds, which could be used to
                //send messages or some other action
                Log.e("Clok started","00");
            }

            public void onFinish() {

                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;


                    if (!callConnected) {

                        Log.d("Stop timer", "call connected");

                        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

                        if (ChatApi.getInstance().getPublisherContext() != null) {

                            ((Activity) ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

                                    Toast.makeText(ChatApi.getInstance().getPublisherContext(), "Call Disconnected.", Toast.LENGTH_LONG).show();
                                }
                            });


                            ChatApi.getInstance().stopRingTone();

                            ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

                            ((Activity) ChatApi.getInstance().getPublisherContext()).finish();


//                            rejectSingleCall(receiver,"Private_".concat(receiver));

//                            Tools.sendMessgaeAlertToServer(con,receiver);

                        } else {

                            Log.d("Call", "connected");
                        }

                    } else {

                        Log.d("Call", "connected");
                    }


                    Log.e("wistimer", "Stopped");
                    //After 60000 milliseconds (60 sec) finish current
                    //if you would like to execute something when time finishes
                }
            }
        }.start();
    }

    public void stopPreviewScreenTimer(){


        if(waitTimer!=null){

            Log.e("wistimer","Stopped called");
            ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);
            waitTimer.cancel();
            waitTimer = null;


        }

    }

    public void startPopUpTimer(){

        System.out.println("startpopup called");

        final long timeBeforeThreadStart = System.currentTimeMillis();



        t = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while ((System.currentTimeMillis() - timeBeforeThreadStart) < (30 * 1000)) {
                    // you add sleep here if you want instead of loop

                    Log.d("TIMER INIT","TIMER START");


                }
                if (!ApplicationController.getInstance().getIsInputEventOccurred()) {
                    // write code to close app
                    Log.e("TIMER STOPED", String.valueOf(ApplicationController.getInstance().getIsInputEventOccurred()));

                    Log.d("TIMER STOPED","TIMER STOPED");

                    ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

//                    clearVideoSession();

                    t.interrupt();

                    Log.d("Stop timer","timer");

                    if(!callConnected) {

                        Log.d("Stop timer","call connected");

                        if (ChatApi.getInstance().getPublisherContext() != null) {

                            ((Activity) ChatApi.getInstance().getPublisherContext()).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

                                    Toast.makeText(ChatApi.getInstance().getPublisherContext(), "Call Disconnected.", Toast.LENGTH_LONG).show();
                                }
                            });


                            ChatApi.getInstance().stopRingTone();

                            ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

                            ((Activity) ChatApi.getInstance().getPublisherContext()).finish();


//                            rejectSingleCall(receiver,"Private_".concat(receiver));

//                            Tools.sendMessgaeAlertToServer(con,receiver);

                        } else {

                            Log.d("Call", "connected");
                        }

                    }else{

                        Log.d("Call", "connected");
                    }


                }else{

                    Log.e("ERROR ON","CONTEXT");

                    Log.e("Timer::","isInputEventOccurred else part");
                }


            }


        });
        t.start();
    }


    class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0];
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



//    BroadCast

//    public void connectToBroadCastView(JSONObject response){
//
//        System.out.println("=======connectToBroadCastView++++++++++");
//
//        Intent i = new Intent(ApplicationController.this, WisDirect.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        try {
//            i.putExtra("token",response.getString("token"));
//            i.putExtra("session_id",response.getString("session_id"));
//            i.putExtra("is_publisher","NO");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        startActivity(i);
//    }


    public void setUpChatApi() {



        Log.i("get channels", "method called");

        String apiUrl = getString(R.string.server_url).concat(getString(R.string.getChannels));

        JSONObject jsonParms = new JSONObject();

        try {
            jsonParms.put("user_id", Tools.getData(ApplicationController.this, "idprofile"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest reqDisc = new JsonObjectRequest(apiUrl, jsonParms,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("channel response", String.valueOf(response));

                        try {
//
//                            JSONArray privateArray = response.getJSONArray("private_members");
//
//                            JSONArray publicArray = response.getJSONArray("public_mebmers");

                            if(response.has("private_members")){


                                JSONArray privateArray = response.getJSONArray("private_members");
                                Tools.getPrivateChannel(privateArray);
                            }



                            if(response.has("public_mebmers")){


                                JSONArray publicArray = response.getJSONArray("public_mebmers");

                                Tools.getPublicChannel(publicArray);

                            }



                            if(response.has("pub_channels")){

                                JSONArray pub_channels = response.getJSONArray("pub_channels");

                                Tools.getPrivateChannel(pub_channels);
                            }




//                            Tools.getPrivateChannel(privateArray);
//
//                            Tools.getPublicChannel(publicArray);

                            Tools.doSubScribe();


                        } catch (JSONException e) {

//                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ApplicationController.this, "token"));
                headers.put("lang", Tools.getData(ApplicationController.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDisc);


    }



}
