package com.oec.wis.chatApi;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by asareri08 on 16/09/16.
 */
public  class UserData {

    private String senderName;

    private  String senderIcon;

    private String senderId ;

    private int receiverId;

    private String contentType;

    private String chatType;

    private String actualChannel;



    JSONObject obj;


    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderIcon() {
        return senderIcon;
    }

    public void setSenderIcon(String senderIcon) {
        this.senderIcon = senderIcon;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getActualChannel() {
        return actualChannel;
    }

    public void setActualChannel(String actualChannel) {
        this.actualChannel = actualChannel;
    }

    public  UserData(String senderName, String senderIcon, String senderId, int receiverId, String contentType, String chatType, String actualChannel){

        this.senderName = senderName;

        this.senderIcon = senderIcon;

        this.senderId = senderId;

        this.receiverId = receiverId;

        this.chatType = chatType;

        this.contentType = contentType;

        this.actualChannel = actualChannel;


    }

    public JSONObject getObj() {
        return obj;
    }

    public void setObj(JSONObject obj) {
        this.obj = obj;
    }

    public UserData(JSONObject obj){


        this.obj = obj;


    }
}
