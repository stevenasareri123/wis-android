package com.oec.wis.wismusic;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragNotif;
import com.oec.wis.fragments.FragWeather;
import com.oec.wis.wismusic.Player;
import com.oec.wis.R;
import com.oec.wis.adapters.ChatIndivisualListener;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.RefreshListener;
import com.oec.wis.adapters.ShareAlbumAdapter;
import com.oec.wis.adapters.ShareAlbumListener;
import com.oec.wis.adapters.ShowDialogAdapter;
import com.oec.wis.database.DatabaseHandler;
import com.oec.wis.dialogs.NewAds;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.PubMusic;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISAlbum;
import com.oec.wis.entities.WISAlbumPlaylist;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.entities.WISMusic;
import com.oec.wis.entities.WISPlaylist;
import com.oec.wis.entities.WISPlaylist1;
import com.oec.wis.entities.WISPlaylistAlbum;
import com.oec.wis.entities.WISPlaylistSongs;
import com.oec.wis.entities.WISSongs;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class FragPlaylist extends Fragment implements ChatIndivisualListener{
    View view;
    TextView albumTextView,songTextView,albumlist;
    EditText playlist;
    List<WISPlaylistAlbum> album;
    List<WISPlaylistSongs> songs;
    GridView gridMusic;
    GridView gridMusic1;
    TextView target_view;
    GridView navGrid;
    ImageView userNotificationView;
    RelativeLayout album_layout;
    RelativeLayout songs_layout;
    ImageView back;
    ArrayList<Popupelements> popupList;
    Popupadapter simpleAdapter;

    ArrayList<PubMusic> pubMusics;
    List<WISAds> banAds;

    ListView publist;
    DatabaseHandler db;


    List<WISPlaylistAlbum> playlistalbums;
    Typeface font;
    List<WISPlaylistSongs> playlistsongs;

    boolean isTimerRunning = false;
    int cPub = 0;

    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            loadPubBanner(cPub);
        }
    };

    TextView tvBanTitle, tvBanDesc;
    ImageView ivBanPhoto;

    PlaylistAlbumAdapter adapter;
    PlaylistAudioAdapter adapter1;

    ProgressDialog dialog;

    ListView shareSongsList;

    ImageButton close;
    List<WISSongs> shareAlbumlist;
    TextView headerTitleTv;
    ImageView closeIV,BackArrow;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {

            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.playerlist_activity, container, false);

            showLoader();
            initControls();
            setListener();
            fetchPlaylistData();
            loadBannerPub();


            gridMusic.setVisibility(View.VISIBLE);

        } catch (InflateException e) {
        }
        return view;
    }

    public void showLoader(){

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void fetchLocalAlbumData(){


        //List<WISAlbum> wisalbums = playlistalbums.get(0).getAlbumList();
        adapter = new PlaylistAlbumAdapter(getActivity(), playlistalbums, new ChatIndivisualListener() {
            @Override
            public void showPopUP(int id, int position, String type, String path) {
                //showsharePopUPView(id, position, type, UserNotification.getMusicChatMessage());

                showsharePopAlbum(id, position, type, UserNotification.getMusicChatMessage());
            }
        }, new RefreshListener() {
            @Override
            public void refreshData() {
                Log.i("calling","refreshData");
                db.deleteAllPlaylistAlbumFiles();
                playlistalbums.clear();
                loadPlaylist();
                adapter.notifyDataSetChanged();
            }
        });


        if(adapter!=null){

            gridMusic.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            loadFilterConfig();
        }
    }

    private void showsharePopAlbum(final int object_id, final int position, final String type, final String path) {

        List<WISAlbum> wisalbums = playlistalbums.get(position).getAlbumList();
        final List<WISSongs> songArray = new ArrayList<WISSongs>();
        for(int i=0;i<wisalbums.size();i++)
        {
            List<WISSongs> wisSongs = wisalbums.get(i).getListdata();

            for (int j=0;j<wisSongs.size();j++)
            {
                songArray.add(new WISSongs(wisSongs.get(j).getId(),wisSongs.get(j).getName(),wisSongs.get(j).getA(),wisSongs.get(j).getDuration(),wisSongs.get(j).getPath(),wisSongs.get(j).getThumb(),wisSongs.get(j).getCreated_at()));;
            }
        }

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.grey_chat_icon,getActivity().getResources().getString(R.string.chat_indivisul)));
        popupList.add(new Popupelements(R.drawable.grey_group_icon,getActivity().getResources().getString(R.string.chat_by_group)));
        popupList.add(new Popupelements(R.drawable.grey_news,getActivity().getResources().getString(R.string.field_actuality)));
        popupList.add(new Popupelements(R.drawable.grey_pub_icon,getActivity().getResources().getString(R.string.pub)));
        Log.i("popsize", String.valueOf(popupList));
        simpleAdapter = new Popupadapter(getActivity(),false,popupList);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int i) {


                        if(popupList.size()==4){

                            if(i == 0){


                                dialog.dismiss();

                                showGroupChatView(object_id,"single",position,songArray);




                            }

                            if(i ==1){
                                dialog.dismiss();

                                showGroupChatView(object_id,"group",position,songArray);



//                                Toast.makeText(context, data.get(position).getId()+data.get(position).getPath()+type, Toast.LENGTH_SHORT).show();

                            }
                            if(i ==2){
                                dialog.dismiss();
                                showGroupChatView(object_id,"shareActuality",position,songArray);




                            }
                            if(i==3)
                            {
                                dialog.dismiss();
                                showGroupChatView(object_id,"pub",position,songArray);




                            }

                        }
                        else if(popupList.size()==4){
                            dialog.dismiss();

                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }

    private void showGroupChatView(final int objectid, final String share, final int positions, List<WISSongs> songArray){

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.share_songs, null);


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        close = (ImageButton)alert.findViewById(R.id.closeGroupchat);
        shareSongsList = (ListView)alert.findViewById(R.id.groupfriendList);


//        db.getAlbumSongs(objectid);
        shareSongsList.setAdapter(new ShareAlbumAdapter(getActivity(), songArray, new ShareAlbumListener() {
            @Override
            public void showlist(int id, int position, String type, String path) {
                alert.dismiss();
                if(share.equals("single"))
                {
                    FragChat fragment = new FragChat();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                    new UserNotification().setObject(id);
                    new UserNotification().setAudio(path);
                    new UserNotification().setType(type);

                    new UserNotification().setFROMMUSIC(true);
                    new UserNotification().setChatSelected(true);
                    new UserNotification().setDISCUSSIONSELECTED(false);
                }
                else if(share.equals("group"))
                {
                    FragChat fragment = new FragChat();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                    new UserNotification().setObject(id);
                    new UserNotification().setAudio(path);
                    new UserNotification().setType(type);

                    new UserNotification().setFROMMUSIC(true);
                    new UserNotification().setDISCUSSIONSELECTED(true);
                    new UserNotification().setChatSelected(false);
                }
                else if(share.equals("shareActuality"))
                {
                    shareActuality(id,type);
                }
                else {
                    startActivity(new Intent(getActivity(), NewAds.class));
                    new UserNotification().setFROMMUSIC(true);
                    new UserNotification().setAudio(path);

                    Log.i("selectedPath",path);
                    new UserNotification().setObject(id);
                }
                //showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
            }
        }));
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });



    }

    public void fetchLocalsongData(){

        adapter1 = new PlaylistAudioAdapter(getActivity(), playlistsongs, new ChatIndivisualListener() {
            @Override
            public void showPopUP(int id, int position, String type, String path) {


                System.out.print(")())))))))))))))");

                Log.e("LIST OF SONGS", String.valueOf(playlistsongs.get(0).getSongsList()));

                shareSongList(id, position, type, UserNotification.getMusicChatMessage(),playlistsongs.get(position).getSongsList());

//                    showsharePopUPView(id, position, type, UserNotification.getMusicChatMessage());
            }
        }, new RefreshListener() {
            @Override
            public void refreshData() {
                Log.i("calling","refreshData");
                db.deleteAllPlaylistSongFiles();
                playlistsongs.clear();
                loadPlaylist();
                adapter1.notifyDataSetChanged();
            }
        });
        if(adapter1!=null){

            gridMusic1.setAdapter(adapter1);
            adapter1.notifyDataSetChanged();
            loadFilterConfig();
        }
    }

    public void refereshPalylist(){


        Log.e("Playlist::","referesh data called");

        db = new DatabaseHandler(getActivity());

//        playlistalbums.clear();
//        playlistsongs.clear();
//        //db.deleteAllPlaylist();

        playlistalbums = db.getAllPlaylistAlbums();
        playlistsongs = db.getAllPlaylistSongs();



        // album data

        if (playlistalbums.size()>0)
        {
            fetchLocalAlbumData();
        }
        // song data

        if (playlistsongs.size()>0)
        {
            fetchLocalsongData();
        }
//        else {
//            fetchPlaylistData();
//        }
    }


    public void fetchPlaylistData(){

        System.out.println("Fetch Playlist data called");

        loadPlaylist();
    }

    private void loadFilterConfig(){
        playlist.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                String text = playlist.getText().toString().toLowerCase(Locale.getDefault());
                try{

                    if(adapter!=null){

                        adapter.filter(text);
                    }

                    if(adapter1!=null){

                        adapter1.filter1(text);
                    }

                }
                catch (Exception e)
                {
                    //Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }



    private void initControls() {
        gridMusic = (GridView) view.findViewById(R.id.gridPhoto);
        gridMusic1 = (GridView) view.findViewById(R.id.gridPhoto1);
        playlist = (EditText) view.findViewById(R.id.playlist_serach);
        albumlist = (TextView) view.findViewById(R.id.album_search);
        album_layout = (RelativeLayout) view.findViewById(R.id.type_album);
        songs_layout = (RelativeLayout) view.findViewById(R.id.type_songs);
        albumTextView= (TextView) view.findViewById(R.id.album);
        songTextView= (TextView) view.findViewById(R.id.songs);
        back = (ImageView) view.findViewById(R.id.close);
        BackArrow = (ImageView) view.findViewById(R.id.closeView);





        BackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(Gravity.START);

                Log.e("ONBACKE","CLICKED");

//                FragmentManager fm = getFragmentManager();
//                if (fm.getBackStackEntryCount() > 0) {
//                    Log.i("MainActivity", "popping backstack");
//                    fm.popBackStack();
//                } else {
//                    Log.i("MainActivity", "nothing on backstack, calling super");
//
//                }



            }
        });
        target_view = (TextView) view.findViewById(R.id.target_view);

        tvBanTitle = (TextView) view.findViewById(R.id.tvPubTitle);
        tvBanDesc = (TextView) view.findViewById(R.id.tvPubDesc);
        ivBanPhoto = (ImageView) view.findViewById(R.id.ivPubPhoto);
        headerTitleTv=(TextView)view.findViewById(R.id.headerTitleTV1);
        closeIV =(ImageView) view.findViewById(R.id.closeIV);






        db = new DatabaseHandler(getActivity());


        playlistalbums = db.getAllPlaylistAlbums();
        playlistsongs = db.getAllPlaylistSongs();

        userNotificationView = (ImageView) view.findViewById(R.id.userNotificationView);

        //target_view.setText(String.valueOf(new UserNotification().getBatchcount()));


        if(new UserNotification().getBatchcount()==0)
        {
            target_view.setVisibility(View.INVISIBLE);
        }
        else {
            target_view.setText(String.valueOf(new UserNotification().getBatchcount()));
        }

        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                userNotificationView.setImageResource(R.drawable.logo_wis);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    userNotificationView.setImageBitmap(response.getBitmap());
                }
            }
        });

        try{
            font= Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            playlist.setTypeface(font);
            albumTextView.setTypeface(font);
            songTextView.setTypeface(font);
            target_view.setTypeface(font);
            tvBanDesc.setTypeface(font);
            tvBanTitle.setTypeface(font);
            headerTitleTv.setTypeface(font);
        }catch (NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }




    }
    private void setListener() {

        try {
//            view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(Gravity.START);
//
//                    FragMusic fragment = new FragMusic();
//                    FragmentManager fragmentManager = getFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();

//                }
//            });

            closeIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }catch (NullPointerException exception){
            System.out.println("Null pointer exception" +exception.getMessage());
        }


        albumTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                album_layout.setVisibility(View.VISIBLE);
                gridMusic.setVisibility(View.VISIBLE);
                songs_layout.setVisibility(View.GONE);
                albumTextView.setBackgroundColor(getResources().getColor(R.color.blue_color));
                songTextView.setBackgroundColor(getResources().getColor(R.color.light_blue_color));
                playlist.setText("");

                fetchLocalAlbumData();

//                loadFilterConfig();
//                onResume();



            }
        });
        songTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songs_layout.setVisibility(View.VISIBLE);
                gridMusic1.setVisibility(View.VISIBLE);
                album_layout.setVisibility(View.GONE);
                songTextView.setBackgroundColor(getResources().getColor(R.color.blue_color));
                albumTextView.setBackgroundColor(getResources().getColor(R.color.light_blue_color));
                playlist.setText("");

                fetchLocalsongData();

//                loadFilterConfig();




            }
        });
        gridMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


//                List<WISPlaylist1> array = album.get(position).getListdata().get(0).getListdata();
//                Log.i("songsData",String.valueOf(array));
//                Uri uri = Uri.parse(album.get(position).getListdata().get(0).getListdata().get(0).getAudio_path());
//                new UserNotification().setPlayAlbum(array);
//                new UserNotification().setSelected(0);
//                Intent intent = new Intent(getActivity(),Player.class);
//                intent .putExtra("uri_Str", uri);
//                intent.putExtra("type","playlist1");
////                intent.putExtra("songIndex",album.get(position).getListdata().get(0).getListdata().get(0).toString());
//                intent.putExtra("songIndex",0);
//                intent.putExtra("songname",album.get(position).getName());
//                intent.putExtra("songIcon",album.get(position).getListdata().get(0).getListdata().get(0).getThumbnail());
////                intent.putExtra("artist",album.get(position).getListdata().get(0).getAlbum_artists());
//                intent.putExtra("duration",album.get(position).getListdata().get(0).getListdata().get(0).getDuration());
//                startActivity(intent);



                try{


                Log.e("Selected Playlisalbum::", String.valueOf(playlistalbums.get(position).getListdata()));

                List<WISAlbum> wisalbums = playlistalbums.get(position).getAlbumList();
                Log.e("AlbumSize", String.valueOf(wisalbums.size()));
                List<WISSongs> songArray = new ArrayList<WISSongs>();
                for(int i=0;i<wisalbums.size();i++)
                {
                    List<WISSongs> wisSongs = wisalbums.get(i).getListdata();

                    for (int j=0;j<wisSongs.size();j++)
                    {
                        songArray.add(new WISSongs(wisSongs.get(j).getId(),wisSongs.get(j).getName(),wisSongs.get(j).getA(),wisSongs.get(j).getDuration(),wisSongs.get(j).getPath(),wisSongs.get(j).getThumb(),wisSongs.get(j).getCreated_at()));;
                    }
                }

                Log.e("AlbumSongSize", String.valueOf(songArray.size()));
                System.out.println("______WIS ALBUM____");

                Log.e("Album::", String.valueOf(wisalbums));
                Log.e("selected playlissongs::", String.valueOf(songArray));

                List<WISSongs> array =wisalbums.get(0).getListdata();
                Log.e("AlbumSize", String.valueOf(wisalbums.size()));
                Log.i("songsData", String.valueOf(array));
                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                Uri uri = Uri.parse(PATH+array.get(0).getPath());
                Log.i("albumUri", String.valueOf(uri));
                new UserNotification().setSongs(songArray);
                new UserNotification().setSelected(0);
                Intent intent = new Intent(getActivity(),Player.class);
                intent .putExtra("uri_Str", uri);
                intent.putExtra("thumbnail",array.get(0).getThumb());
                intent.putExtra("songIndex",0);
                intent.putExtra("type","album");
                intent.putExtra("songname",playlistalbums.get(position).getName());
                intent.putExtra("artist",array.get(0).getA());
                intent.putExtra("duration",array.get(0).getDuration());

                Log.d("Pass Data","ok");
                    new UserNotification().setSelectedPosition(position);
                    new UserNotification().setClicked(true);

                startActivity(intent);

                }
                catch (ArrayIndexOutOfBoundsException exception){


                    Toast.makeText(getActivity(),getString(R.string.mediaplayer_error),Toast.LENGTH_LONG).show();

                }



//                List<WISAlbumPlaylist> playlistalb = db.getAllPlaylistAddAlbums(playlistalbums.get(position).getId());
//                if(playlistalb.size() > 0) {
//                    List<WISPlaylist1> playlistalbum = db.getPlaylistAlbumsByAlbumId(playlistalb.get(0).getAlbum_id());
//                    String PATH = Environment.getExternalStorageDirectory()
//                            + "/wismusic/";
//                    Uri uri = Uri.parse(PATH + playlistalbum.get(0).getAudio_path());
//                    Log.i("albumData", String.valueOf(playlistalbum));
//                    new UserNotification().setPlayAlbum(playlistalbum);
//                    new UserNotification().setSelected(0);
//                    Intent intent = new Intent(getActivity(), Player.class);
//                    intent.putExtra("uri_Str", uri);
//                    intent.putExtra("type", "playlist1");
//                    intent.putExtra("songIndex", 0);
//                    intent.putExtra("songname", playlistalbums.get(position).getName());
//                    intent.putExtra("songIcon", playlistalbum.get(0).getThumbnail());
//                    intent.putExtra("duration", playlistalbum.get(0).getDuration());
//                    startActivity(intent);
//                } else {
//                    Toast.makeText(getActivity(), "No Songs", Toast.LENGTH_SHORT).show();
//                }






            }
        });

        userNotificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Tools.callDialog(getActivity());

            }
        });

        gridMusic1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                List<WISPlaylist> array = songs.get(position).getListdata();
//                Log.i("songsData",String.valueOf(array));
//                Uri uri = Uri.parse(songs.get(position).getListdata().get(0).getAudio_path());
//                new UserNotification().setPlaySongs(array);
//                new UserNotification().setSelected(0);
//                Intent intent = new Intent(getActivity(),Player.class);
//                intent .putExtra("uri_Str", uri);
//                intent.putExtra("type","playlist");
////                intent.putExtra("songIndex",songs.get(position).getListdata().get(0).toString());
//                intent.putExtra("songIndex",0);
//                intent.putExtra("songname",songs.get(position).getName());
//                intent.putExtra("songIcon",songs.get(position).getListdata().get(0).getThumbnail());
//                intent.putExtra("duration",songs.get(position).getListdata().get(0).getDuration());
//                startActivity(intent);


                try {



                List<WISMusic> allSongs = playlistsongs.get(position).getSongsList();

                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                Uri uri = Uri.parse(PATH + allSongs.get(0).getPath());

                Log.e("audio path", String.valueOf(uri));

                new UserNotification().setSonglist(allSongs);
                new UserNotification().setSelected(0);
                Intent intent = new Intent(getActivity(), Player.class);
                intent.putExtra("uri_Str", uri);
                intent.putExtra("thumbnail", allSongs.get(0).getThumb());
                intent.putExtra("songIndex", allSongs.get(0).toString());
                //intent.putExtra("songIndex",position);
                intent.putExtra("type", "songs");
                intent.putExtra("songname", playlistsongs.get(position).getName());
                intent.putExtra("artist", allSongs.get(0).getArtists());
                intent.putExtra("duration", allSongs.get(0).getDuration());

                Log.d("Pass Data", "ok");

                    new UserNotification().setSelectedPosition(position);
                    new UserNotification().setClicked(true);

                startActivity(intent);
            }

                catch (ArrayIndexOutOfBoundsException exception){


                    Toast.makeText(getActivity(),getString(R.string.mediaplayer_error),Toast.LENGTH_LONG).show();

                }

//                List<WISPlaylist> array = db.getPlaylistSongs(playlistsongs.get(position).getId());
//                if(playlistsongs.size()>0)
//                {
//                    Log.i("songsData",String.valueOf(array));
//                    String PATH = Environment.getExternalStorageDirectory()
//                            + "/wismusic/";
//                    Uri uri = Uri.parse(PATH+array.get(0).getAudio_path());
//                    new UserNotification().setPlaySongs(array);
//                    new UserNotification().setSelected(0);
//                    Intent intent = new Intent(getActivity(),Player.class);
//                    intent .putExtra("uri_Str", uri);
//                    intent.putExtra("type","playlist");
//                    intent.putExtra("songIndex",0);
//                    intent.putExtra("songname",playlistsongs.get(position).getName());
//                    intent.putExtra("songIcon",array.get(0).getThumbnail());
//                    intent.putExtra("duration",array.get(0).getDuration());
//                    startActivity(intent);
//                }






            }
        });


        if(new UserNotification().getUpdateplaylist()== Boolean.TRUE)
        {
            gridMusic.invalidateViews();
            gridMusic1.invalidateViews();
        }


    }

    private void loadBannerPub() {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
            jsonBody.put("type","music");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        banAds = new ArrayList<>();
        pubMusics = new ArrayList<>();
        JsonObjectRequest reqBan = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.banner_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                try {
                                    JSONArray data = response.getJSONArray("data");

                                    Log.i("pubdata", String.valueOf(data));

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("photo");
                                        } catch (Exception e) {
                                        }
                                        String video_thumb = "";
                                        try {
                                            video_thumb = obj.getString("photo_video");
                                        } catch (Exception e) {
                                        }
                                        banAds.add(new WISAds(obj.getInt("id"), obj.getString("title"), obj.getString("description"), 0, 0, "", "", "", "", photo, "", false, false, "", obj.getString("type_obj"), video_thumb));
                                        pubMusics.add(new PubMusic(obj.getInt("id"), obj.getString("title"), obj.getString("description")));
                                    }
                                    Log.i("pubmusiclist", String.valueOf(banAds));

                                    if (banAds.size() > 0) {
                                        showBanContent(0);
                                        view.findViewById(R.id.llPubBanner).setVisibility(View.VISIBLE);
                                        if (banAds.size() > 1)
                                            startBannerShow();
                                    } else {
                                        view.findViewById(R.id.llPubBanner).setVisibility(View.GONE);
                                    }

                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String x = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));

                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqBan);
    }

    private void loadPubBanner(final int i) {
        if (getActivity() != null) {
            Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out);
            a.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    showBanContent(i);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.findViewById(R.id.llPubBanner).startAnimation(a);
        }
    }

    private void startBannerShow() {
        isTimerRunning = true;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (cPub < (banAds.size() - 1))
                    cPub++;
                else
                    cPub = 0;
                mHandler.obtainMessage(cPub).sendToTarget();
            }
        }, 0, 8000);
    }

    private void showBanContent(int i) {
        tvBanTitle.setText(banAds.get(i).getTitle());
        tvBanDesc.setText(banAds.get(i).getDesc());
        final int id = banAds.get(i).getId();
        final int idact = banAds.get(i).getIdAct();
        final int advid = banAds.get(i).getAdvertiser();
        final String advname = banAds.get(i).getAdvertiserName();

        Log.i("going","pub :"+id+","+idact+","+advid+","+advname);


        tvBanTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubDetails.class);

                i.putExtra("id",id);
                i.putExtra("idact", idact);
                i.putExtra("advertiser_id", advid);
                i.putExtra("advertiser_name", advname);
                i.putExtra("type","music");

                startActivity(i);


            }
        });
        tvBanDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubDetails.class);

                i.putExtra("id",id);
                i.putExtra("idact", idact);
                i.putExtra("advertiser_id", advid);
                i.putExtra("advertiser_name", advname);
                i.putExtra("type","music");

                startActivity(i);


            }
        });
        if (!banAds.get(i).getTypeObj().equals("video")) {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + banAds.get(i).getPhoto(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivBanPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivBanPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivBanPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + banAds.get(i).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivBanPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivBanPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivBanPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        }
    }

    private void loadPlaylist() {


        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            Log.i("jsonBody", String.valueOf(jsonBody));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqMusic = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.get_playlist), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {


//                                clear playlist table data

                                if(db!=null){

                                    db.deleteAllPlaylist();
                                }

                                Log.i("playlist", String.valueOf(response));

                                JSONArray data = response.getJSONArray("album_files");

                                Log.i("albumData", String.valueOf(data));

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj1 = data.getJSONObject(i);
                                        //create albumlist array
                                        JSONArray albumArray = obj1.getJSONArray("albums");

                                        List<WISAlbumPlaylist> albumlist = new ArrayList<>();

                                        for (int pa=0;pa<albumArray.length();pa++)
                                        {

                                            JSONObject albumdict = albumArray.getJSONObject(pa);

//                                            create songlist array

                                            JSONArray songArray = albumdict.getJSONArray("songs");

                                            List<WISPlaylist1> songsList = new ArrayList<>();

                                            for(int ps=0;ps<songArray.length();ps++)
                                            {

                                                JSONObject songDict = songArray.getJSONObject(ps);
//                                                save songs

                                                songsList.add(new WISPlaylist1(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
                                                db.putPlaylistAlbumSongs(new WISPlaylist1(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
                                                Log.i("songsList", String.valueOf(songsList));
                                            }
//                                            save albums
                                            albumlist.add(new WISAlbumPlaylist(obj1.getInt("id"),albumdict.getString("album_name"),albumdict.getInt("album_id"),albumdict.getString("album_artists"),albumdict.getString("created_at"),songsList));
                                            db.addPlaylistAddAlbums(new WISAlbumPlaylist(obj1.getInt("id"),albumdict.getString("album_name"),albumdict.getInt("album_id"),albumdict.getString("album_artists"),albumdict.getString("created_at"),songsList));

                                            Log.i("albumlist", String.valueOf(albumlist));
                                        }

//                                        save playlist
                                        //album.add(new WISPlaylistAlbum(obj1.getInt("id"),obj1.getString("type"),obj1.getString("created_at"),obj1.getString("created_by"),obj1.getString("name"),albumlist));
                                        db.addPlaylistAllAlbums(new WISPlaylistAlbum(obj1.getInt("id"),obj1.getString("type"),obj1.getString("created_at"),obj1.getString("created_by"),obj1.getString("name"),albumlist));




                                    } catch (Exception e) {
                                    }


                                }


//                                if (album.size() == 0)
//                                    Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
////                                gridMusic.setAdapter(new PlaylistAlbumAdapter(getActivity(), album,"album",new ChatIndivisualListener() {
////                                    @Override
////                                    public void showPopUP(int id, int position, String type,String path) {
////                                        showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
////                                    }
////                                }));
//                                adapter = new PlaylistAlbumAdapter(getActivity(), album, new ChatIndivisualListener() {
//                                    @Override
//                                    public void showPopUP(int id, int position, String type, String path) {
//                                        showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
//                                    }
//                                });
//                                gridMusic.setAdapter(adapter);
//                                adapter.notifyDataSetChanged();
//                                loadFilterConfig();

                            }


                        } catch (JSONException e) {

                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

                        }

                        try {
                            if (response.getBoolean("result")) {

                                JSONArray data = response.getJSONArray("songs_files");

                                Log.i("songsData", String.valueOf(data));

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);


                                        JSONArray songArray = obj.getJSONArray("songs");

                                        List<WISPlaylist> songsList = new ArrayList<>();

                                        for(int ps=0;ps<songArray.length();ps++)
                                        {

                                            JSONObject songDict = songArray.getJSONObject(ps);
//                                                save songs

                                            songsList.add(new WISPlaylist(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),getString(R.string.server_url3)+songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
                                            db.putPlaylistSongs(new WISPlaylist(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),obj.getInt("id")));
                                            Log.i("songsList", String.valueOf(songsList));
                                        }
                                        // save playlist song
                                        db.addPlaylistSongs(new WISPlaylistSongs(obj.getInt("id"),obj.getString("type"),obj.getString("created_at"),obj.getString("created_by"),obj.getString("name"),songsList));
                                    } catch (Exception e) {
                                    }
                                }
//                                if (songs.size() == 0)
//                                    Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
//                                adapter1 = new PlaylistAudioAdapter(getActivity(), songs, new ChatIndivisualListener() {
//                                    @Override
//                                    public void showPopUP(int id, int position, String type, String path) {
//                                        showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
//                                    }
//                                });
//                                gridMusic1.setAdapter(adapter1);
//                                adapter1.notifyDataSetChanged();
//                                loadFilterConfig();
                            }


//                            referesh Playlist Data




                        } catch (JSONException e) {

                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                        System.out.print("referes Data::");

                        if(dialog!=null){
                            dialog.dismiss();
                        }

                        refereshPalylist();

                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if(dialog!=null){
                            dialog.dismiss();
                        }
                        refereshPalylist();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqMusic);
    }


//    private void loadMusic() {
//        album = new ArrayList<>();
//        songs = new ArrayList<>();
//        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        JSONObject jsonBody = new JSONObject();
//
//        try {
//            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
//            Log.i("jsonBody",String.valueOf(jsonBody));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqMusic = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.get_playlist), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//
//                                Log.i("playlist", String.valueOf(response));
//
//                                JSONArray data = response.getJSONArray("album_files");
//
//                                Log.i("albumData", String.valueOf(data));
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj1 = data.getJSONObject(i);
//                                        //create albumlist array
//                                        JSONArray albumArray = obj1.getJSONArray("albums");
//
//                                        List<WISAlbumPlaylist> albumlist = new ArrayList<>();
//                                        db.addPlaylistAllAlbums(new WISPlaylistAlbum(obj1.getInt("id"),obj1.getString("name"),obj1.getString("created_at"),obj1.getString("type")));
//
//                                        Log.d("Album Size ::", String.valueOf(albumArray.length()));
//
//                                        for (int pa=0;pa<albumArray.length();pa++)
//                                        {
//
//                                            JSONObject albumdict = albumArray.getJSONObject(pa);
//
//                                            Log.e("Playlist IDSS:", String.valueOf(obj1.getInt("id")));
//
//                                            db.addPlaylistAddAlbums(new WISAlbumPlaylist(albumdict.getInt("album_id"),albumdict.getString("album_name"),albumdict.getString("album_artists"),albumdict.getString("created_at"),obj1.getInt("id")));
//
//
////                                            create songlist array
//
//                                            JSONArray songArray = albumdict.getJSONArray("songs");
//
//                                            List<WISPlaylist1> songsList = new ArrayList<>();
//
//                                            for(int ps=0;ps<songArray.length();ps++)
//                                            {
//
//                                                JSONObject songDict = songArray.getJSONObject(ps);
////                                                save songs
//
//                                                songsList.add(new WISPlaylist1(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),getString(R.string.server_url3)+songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
//                                                db.putPlaylistAlbumSongs(new WISPlaylist1(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
//                                            }
//////                                            save albums
////                                            albumlist.add(new WISAlbumPlaylist(albumdict.getString("album_name"),albumdict.getInt("album_id"),albumdict.getString("album_artists"),albumdict.getString("created_at"),songsList));
//                                            Log.i("albumlist",String.valueOf(albumlist));
//                                        }
//
////                                        save playlist
//                                        //album.add(new WISPlaylistAlbum(obj1.getInt("id"),obj1.getString("type"),obj1.getString("created_at"),obj1.getString("created_by"),obj1.getString("name"),albumlist));
//                                        //searchlist.add(new WISSearch1(obj1.getInt("id"),obj1.getString("type"),obj1.getString("created_at"),obj1.getString("created_by"),obj1.getString("name"),albumlist));
//
//
//
//
//                                    } catch (Exception e) {
//                                    }
//
//
//                                }
////                                if (album.size() == 0)
////                                    Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
////                                adapter = new PlaylistAlbumAdapter(getActivity(), album, new ChatIndivisualListener() {
////                                    @Override
////                                    public void showPopUP(int id, int position, String type, String path) {
////                                        showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
////                                    }
////                                });
////                                gridMusic.setAdapter(adapter);
////                                adapter.notifyDataSetChanged();
////                                loadFilterConfig();
//
//                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                        try {
//                            if (response.getBoolean("result")) {
//
//                                JSONArray data = response.getJSONArray("songs_files");
//
//                                Log.i("songsData", String.valueOf(data));
//
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj = data.getJSONObject(i);
//
//
//                                        JSONArray songArray = obj.getJSONArray("songs");
//
//                                        List<WISPlaylist> songsList = new ArrayList<>();
//
//                                        db.addPlaylistSongs(new WISPlaylistSongs(obj.getInt("id"),obj.getString("name"),obj.getString("type"),obj.getString("created_at")));
//                                        int songid = obj.getInt("id");
//                                        for(int ps=0;ps<songArray.length();ps++)
//                                        {
//
//                                            JSONObject songDict = songArray.getJSONObject(ps);
////                                                save songs
//
//                                            songsList.add(new WISPlaylist(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),getString(R.string.server_url3)+songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
//                                            Log.i("songsList",String.valueOf(songsList));
//                                            db.putPlaylistSongs(new WISPlaylist(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songid));
//                                        }
//                                        // save playlist songs
//                                        songs.add(new WISPlaylistSongs(obj.getInt("id"),obj.getString("type"),obj.getString("created_at"),obj.getString("created_by"),obj.getString("name"),songsList));
//                                        //searchlist1.add(new WISSearchs1(obj.getInt("id"),obj.getString("type"),obj.getString("created_at"),obj.getString("created_by"),obj.getString("name"),songsList));
//                                    } catch (Exception e) {
//                                    }
//                                }
////                                if (songs.size() == 0)
////                                    Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
////                                adapter1 = new PlaylistAudioAdapter(getActivity(), songs, new ChatIndivisualListener() {
////                                    @Override
////                                    public void showPopUP(int id, int position, String type, String path) {
////                                        showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
////                                    }
////                                });
////                                gridMusic1.setAdapter(adapter1);
////                                adapter1.notifyDataSetChanged();
////                                loadFilterConfig();
//                            }
//
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                        } catch (JSONException e) {
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqMusic);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    private void shareSongList(final int object_id, final int position, final String type, final String path, List<WISMusic> wisSongs) {

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.grey_chat_icon,getActivity().getResources().getString(R.string.chat_indivisul)));
        popupList.add(new Popupelements(R.drawable.grey_group_icon,getActivity().getResources().getString(R.string.chat_by_group)));
        popupList.add(new Popupelements(R.drawable.grey_news,getActivity().getResources().getString(R.string.field_actuality)));
        popupList.add(new Popupelements(R.drawable.grey_pub_icon,getActivity().getResources().getString(R.string.pub)));
        Log.i("popsize", String.valueOf(popupList));
        simpleAdapter = new Popupadapter(getActivity(),false,popupList);

        final List<WISSongs> songArray = new ArrayList<>();

        for (int j=0;j<wisSongs.size();j++){



            songArray.add(new WISSongs(wisSongs.get(j).getId(),wisSongs.get(j).getName(),wisSongs.get(j).getArtists(),wisSongs.get(j).getDuration(),wisSongs.get(j).getPath(),wisSongs.get(j).getThumb(),wisSongs.get(j).getDateTime()));;

        }


        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int i) {


                        if(popupList.size()==4){


                            if(i == 0){
                                dialog.dismiss();
                                showGroupChatView(object_id,"single",position,songArray);


//                                FragChat fragment = new FragChat();
//                                FragmentManager fragmentManager = getFragmentManager();
//                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.frame_container, fragment).commit();
//
//                                //sendMsg(audio_path,receiverId,type,"Amis");
//
//                                new UserNotification().setObject(object_id);
//                                new UserNotification().setAudio(path);
//                                new UserNotification().setType(type);
//
//                                new UserNotification().setFROMMUSIC(true);
//                                new UserNotification().setChatSelected(true);
//                                new UserNotification().setDISCUSSIONSELECTED(false);

                            }

                            if(i ==1){
                                dialog.dismiss();

                                showGroupChatView(object_id,"group",position,songArray);

//
//                                FragChat fragment = new FragChat();
//                                FragmentManager fragmentManager = getFragmentManager();
//                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.frame_container, fragment).commit();
//
//
//                                new UserNotification().setObject(object_id);
//                                new UserNotification().setAudio(path);
//                                new UserNotification().setType(type);
//
//                                new UserNotification().setFROMMUSIC(true);
//                                new UserNotification().setDISCUSSIONSELECTED(true);
//                                new UserNotification().setChatSelected(false);

//                                Toast.makeText(context, data.get(position).getId()+data.get(position).getPath()+type, Toast.LENGTH_SHORT).show();

                            }
                            if(i ==2){
                                dialog.dismiss();

                                showGroupChatView(object_id,"shareActuality",position,songArray);
//                                shareActuality(object_id,type);
                            }
                            if(i==3)
                            {
                                dialog.dismiss();

                                showGroupChatView(object_id,"pub",position,songArray);
//                                startActivity(new Intent(getActivity(), NewAds.class));
//                                new UserNotification().setFROMMUSIC(true);
//                                new UserNotification().setAudio(path);
//                                Log.i("selectedPath",path);
//                                new UserNotification().setObject(object_id);

                            }

                        }
                        else if(popupList.size()==4){
                            dialog.dismiss();

                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }

    private void showsharePopUPView(final int object_id, final int position, final String type, final String path) {

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.copy_chat,getActivity().getResources().getString(R.string.chat_indivisul)));
        popupList.add(new Popupelements(R.drawable.copy_chat,getActivity().getResources().getString(R.string.chat_by_group)));
        popupList.add(new Popupelements(R.drawable.write_status,getActivity().getResources().getString(R.string.field_actuality)));
        popupList.add(new Popupelements(R.drawable.write_status,getActivity().getResources().getString(R.string.pub)));
        Log.i("popsize", String.valueOf(popupList));
        simpleAdapter = new Popupadapter(getActivity(),false,popupList);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int i) {


                        if(popupList.size()==4){


                            if(i == 0){



                                dialog.dismiss();
                                FragChat fragment = new FragChat();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                                //sendMsg(audio_path,receiverId,type,"Amis");

                                new UserNotification().setObject(object_id);
                                new UserNotification().setAudio(path);
                                new UserNotification().setType(type);

                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setChatSelected(true);
                                new UserNotification().setDISCUSSIONSELECTED(false);

                            }

                            if(i ==1){
                                dialog.dismiss();
                                FragChat fragment = new FragChat();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment).commit();


                                new UserNotification().setObject(object_id);
                                new UserNotification().setAudio(path);
                                new UserNotification().setType(type);

                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setDISCUSSIONSELECTED(true);
                                new UserNotification().setChatSelected(false);

//                                Toast.makeText(context, data.get(position).getId()+data.get(position).getPath()+type, Toast.LENGTH_SHORT).show();

                            }
                            if(i ==2){
                                dialog.dismiss();
                                shareActuality(object_id,type);
                            }
                            if(i==3)
                            {
                                dialog.dismiss();
                                startActivity(new Intent(getActivity(), NewAds.class));
                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setAudio(path);
                                Log.i("selectedPath",path);
                                new UserNotification().setObject(object_id);

                            }

                        }
                        else if(popupList.size()==4){
                            dialog.dismiss();

                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L menu_share_play dialog)
                .create();
        dialog.show();


    }

    private void displayFrag(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new FragChat();
                UserNotification userNotification = new UserNotification();
                userNotification.setChatSelected(Boolean.FALSE);
                break;
            case 1:
                fragment = new FragNotif();
                break;
            case 2:
                fragment = new FragChat();
                UserNotification userNotification1 = new UserNotification();
                userNotification1.setDISCUSSIONSELECTED(Boolean.TRUE);
                break;
            case 3:

//                fragment = new FragDirectory();
                break;
            case 4:
                fragment = new FragMusic();
                break;
            case 5:
                //fragment = new FragMyLocation();

                break;
            case 6:

                fragment = new FragWeather();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
            //((Dashboard) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }


    private void shareActuality(int song_id,String type) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("object_id", String.valueOf(song_id));
            jsonBody.put("type",type);
            Log.i("user_id", Tools.getData(getActivity(), "idprofile"));
            Log.i("object_id", String.valueOf(song_id));
            Log.i("type",type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.shareAudioToActuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("succuss", String.valueOf(response));
                                Toast.makeText(getActivity(),"Audio Succussfully Shared", Toast.LENGTH_SHORT).show();
                                //Toast.makeText(getActivity(),String.valueOf(response), Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    @Override
    public void showPopUP(int id, int position, String type, String path) {

        showsharePopUPView(id,position,type, UserNotification.getMusicChatMessage());

    }



    //    public class PlaylistAlbumAdapter extends BaseAdapter {
//        Context mContext;
//        LayoutInflater inflater;
//        private List<WISPlaylistAlbum> data = null;
//        private ArrayList<WISPlaylistAlbum> arraylist;
//
//        private SimpleDateFormat dateFormatter;
//        String playlist_date;
//
//        private DatePickerDialog fromDatePickerDialog;
//
//        public  ChatIndivisualListener listener;
//
//        DatabaseHandler db;
//        List<WISAlbumPlaylist> playlistalb;
//        List<WISPlaylist1> playlistalbum;
//
//
//        public PlaylistAlbumAdapter(Context context,
//                                    List<WISPlaylistAlbum> worldpopulationlist,ChatIndivisualListener listener) {
//            mContext = context;
//            this.data = worldpopulationlist;
//            inflater = LayoutInflater.from(mContext);
//            this.arraylist = new ArrayList<WISPlaylistAlbum>();
//            this.arraylist.addAll(worldpopulationlist);
//            this.listener=listener;
//        }
//
//
//
//        @Override
//        public int getCount() {
//            return data.size();
//        }
//
//        @Override
//        public WISPlaylistAlbum getItem(int position) {
//            return data.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        public class ViewHolder {
//            ImageView ivMusic;
//            TextView tvDate,name,duration;
//            Button bDelete, bShare;
//            LinearLayout shareLayout;
//            CheckBox checkBox;
//
//        }
//
//        public View getView(final int position, View view, ViewGroup parent) {
//            final ViewHolder holder;
//            db = new DatabaseHandler(mContext);
//            if (view == null) {
//                holder = new ViewHolder();
//                view = inflater.inflate(R.layout.listview_item1, null);
//                holder.ivMusic = (ImageView) view.findViewById(R.id.ivMusic);
//                holder.bShare = (Button) view.findViewById(R.id.bShare);
//                holder.shareLayout=(LinearLayout) view.findViewById(R.id.shareLayout);
//                holder.tvDate = (TextView) view.findViewById(R.id.tvDate);
//                holder.name = (TextView) view.findViewById(R.id.name);
//                holder.duration =(TextView) view.findViewById(R.id.duration);
//                holder.bDelete = (Button) view.findViewById(R.id.bDelete);
//                holder.checkBox = (CheckBox) view.findViewById(R.id.selected);
//                view.setTag(holder);
//            } else {
//                holder = (ViewHolder) view.getTag();
//            }
//            holder.ivMusic.setImageResource(0);
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date newDate = null;
//            String date = "";
//            try {
//                newDate = format.parse(data.get(position).getCreated_at());
//                format = new SimpleDateFormat("dd/MM/yyyy");
//                date = format.format(newDate);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            System.out.println("Formatted date====>"+ date);
//            holder.tvDate.setText("Date : "+date);
//            holder.name.setText("Titre : "+data.get(position).getName());
//
//            Log.d("playlistalb",String.valueOf(playlistalb));
//
//            Log.d("data",String.valueOf(data));
//
//            Log.d("PlaylistId",String.valueOf(data.get(position).getId()));
//
//            Log.e("P:Album Values::", String.valueOf(data.get(position).getListdata()));
//
//            try{
//
//                try{
//
//                    List<WISAlbumPlaylist> album = data.get(position).getListdata();
//
//                    Log.e("AlbumData",String.valueOf(album));
//
//                    String firstSongDuration = album.get(0).getListdata().get(0).getDuration();
//
//                    Log.e("DUree",firstSongDuration);
//                }
//                catch (IndexOutOfBoundsException ex){
//
//                }
//
//            }
//            catch (NullPointerException ex){
//
//            }
//
//
//
//
//
//
//
//
////            playlistalb = db.getAllPlaylistAddAlbums(data.get(position).getId());
//
//
//
//
//
////            if(playlistalb.size() > 0) {
//
//
//
////                playlistalbum = db.getPlaylistAlbums(playlistalb.get(0).getAlbum_id());
//
//
//
//
//
//            //Log.d("AlbumIds",String.valueOf(playlistalb.get(0).getAlbum_id()));
//
////            holder.duration.setText("Durée :"+playlistalbum.get(0).getDuration());
//
//            //holder.duration.setText("Durée :"+data.get(position).getListdata().get(0).getListdata().get(0).getDuration());
//
//            playlist_date = date;
//
//            holder.name.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    LayoutInflater factory = LayoutInflater.from(mContext);
//                    final View textEntryView = factory.inflate(R.layout.update_playlist, null);
//
//
//                    dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
//
//                    final EditText input1 = (EditText) textEntryView.findViewById(R.id.dateText);
//                    final EditText input2 = (EditText) textEntryView.findViewById(R.id.titleText);
//
//                    input1.setText(playlist_date, TextView.BufferType.EDITABLE);
//                    input2.setText(data.get(position).getName(), TextView.BufferType.EDITABLE);
//
//                    input1.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            fromDatePickerDialog.show();
//                        }
//                    });
//
//                    Calendar newCalendar = Calendar.getInstance();
//                    fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
//
//                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                            Calendar newDate = Calendar.getInstance();
//                            newDate.set(year, monthOfYear, dayOfMonth);
//                            input1.setText(dateFormatter.format(newDate.getTime()));
//                        }
//
//                    },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//
//
//                    final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
//                    alert.setIcon(R.drawable.wismusic).setTitle("Update Playlist:").setView(textEntryView).setPositiveButton("Save",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog,
//                                                    int whichButton) {
//
//                                    if(input1.getText().toString().isEmpty() || input2.getText().toString().isEmpty())
//                                    {
//                                        Toast.makeText(mContext,"please fill the fields", Toast.LENGTH_SHORT).show();
//                                    }
//                                    else {
//                                        updatePlaylist(data.get(position).getId(),input1.getText().toString(),input2.getText().toString());
//                                        db.updateAlbumPlaylist(new WISPlaylistAlbum(data.get(position).getId(),input1.getText().toString(),input2.getText().toString()));
//                                    }
//
//                                    Log.i("AlertDialog","TextEntry 1 Entered "+input1.getText().toString());
//                                    Log.i("AlertDialog","TextEntry 2 Entered "+input2.getText().toString());
//    /* User clicked OK so do some stuff */
//                                }
//                            }).setNegativeButton("Cancel",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog,
//                                                    int whichButton) {
//     /*
//     * User clicked cancel so do some stuff
//     */
//                                }
//                            });
//                    alert.show();
//                }
//            });
//            //holder.ivMusic.setImageResource(R.drawable.empty);
//            //data.get(position).getListdata().get(0).getListdata().get(0).getThumbnail()
////            ApplicationController.getInstance().getImageLoader().get(mContext.getString(R.string.server_url3) +playlistalbum.get(0).getThumbnail() , new ImageLoader.ImageListener() {
////                @Override
////                public void onErrorResponse(VolleyError error) {
////                    holder.ivMusic.setImageResource(R.drawable.empty);
////                }
////
////                @Override
////                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
////                    if (response.getBitmap() != null) {
////                        int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
////                        Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
////                        holder.ivMusic.setImageBitmap(scaled);
////                    } else {
////                        holder.ivMusic.setImageResource(R.drawable.empty);
////                    }
////                }
////            });
//            holder.bDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    deleteDialog(position);
//                }
//            });
//            holder.bShare.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(data.get(position).getListdata().get(0).getListdata().get(0).getArtists()).concat("\n");
////
////                    UserNotification.setMusicChatMessage(msg.concat(data.get(position).getListdata().get(0).getListdata().get(0).getAudio_path()));
////                    UserNotification.setSongThumb(data.get(position).getListdata().get(0).getListdata().get(0).getThumbnail());
////                    listener.showPopUP(data.get(position).getListdata().get(0).getListdata().get(0).getId(), position,"album",data.get(position).getListdata().get(0).getListdata().get(0).getAudio_path());
//
//                    String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(playlistalbum.get(0).getArtists()).concat("\n");
//
//                    UserNotification.setMusicChatMessage(msg.concat(playlistalbum.get(0).getAudio_path()));
//                    UserNotification.setSongThumb(playlistalbum.get(0).getThumbnail());
//
//                    listener.showPopUP(playlistalbum.get(0).getId(), position,"album",playlistalbum.get(0).getAudio_path());
//
//
//                }
//            });
////            }
//            return view;
//        }
//
//        private void updatePlaylist(int playlist_id, final String date,final String name) {
//            JSONObject jsonBody = new JSONObject();
//
//            try {
//                jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
//                jsonBody.put("playlist_id", String.valueOf(playlist_id));
//                jsonBody.put("title",name);
//                jsonBody.put("created_at",date);
//                Log.i("playlist_id",String.valueOf(playlist_id));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            Log.i("playlist",String.valueOf(jsonBody));
//
//            JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.update_playlist), jsonBody,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                if (response.getBoolean("result")) {
//                                    Log.i("updatePlaylist",String.valueOf(response));
//                                    //refreshEvents();
//                                    notifyDataSetChanged();
//                                    new UserNotification().setUpdateplaylist(true);
//                                    FragPlaylist fragment = new FragPlaylist();
//                                    FragmentManager fragmentManager = getFragmentManager();
//                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();
//
//
//
//
//
//
//                                }
//
//                            } catch (JSONException e) {
//
//                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    //if (context != null)
//                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<>();
//                    headers.put("lang", Tools.getData(mContext, "lang_pr"));
//                    headers.put("token", Tools.getData(mContext, "token"));
//                    return headers;
//                }
//            };
//
//            ApplicationController.getInstance().addToRequestQueue(reqDelete);
//        }
//
//        private void deleteDialog(final int position) {
//            new AlertDialog.Builder(mContext)
//                    .setMessage(mContext.getString(R.string.msg_confirm_delete_music))
//                    .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            deleteMusic(data.get(position).getId(), position,"playlist");
//                            db.deletePlaylistAlbumId(data.get(position).getId());
//                        }
//
//                    })
//                    .setNegativeButton(mContext.getString(R.string.no), null)
//                    .show();
//        }
//
//        private void deleteMusic(int playlist_id, final int position,String type) {
//            JSONObject jsonBody = new JSONObject();
//
//            try {
//                jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
//                jsonBody.put("object_id", String.valueOf(playlist_id));
//                jsonBody.put("type",type);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.delmusic_meth), jsonBody,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                if (response.getBoolean("result")) {
//                                    Log.i("deleteResponse",String.valueOf(response));
//                                    data.remove(position);
//                                    notifyDataSetChanged();
//                                }
//
//                            } catch (JSONException e) {
//
//                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    //if (context != null)
//                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<>();
//                    headers.put("lang", Tools.getData(mContext, "lang_pr"));
//                    headers.put("token", Tools.getData(mContext, "token"));
//                    return headers;
//                }
//            };
//
//            ApplicationController.getInstance().addToRequestQueue(reqDelete);
//        }
//
//        // Filter Class
//        public void filter(String charText) {
//            charText = charText.toLowerCase(Locale.getDefault());
//            data.clear();
//            if (charText.length() == 0) {
//                data.addAll(arraylist);
//            } else {
//                for (WISPlaylistAlbum wp : arraylist) {
//                    if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)||wp.getCreated_at().toLowerCase(Locale.getDefault()).contains(charText)) {
//                        data.add(wp);
//                    }
//                }
//            }
//            notifyDataSetChanged();
//        }
//    }
    public class PlaylistAlbumAdapter extends BaseAdapter {
        Context mContext;
        LayoutInflater inflater;
        private List<WISPlaylistAlbum> data = null;
        private ArrayList<WISPlaylistAlbum> arraylist;

        private SimpleDateFormat dateFormatter;
        String playlist_date;

        private DatePickerDialog fromDatePickerDialog;

        public  ChatIndivisualListener listener;

        List<WISAlbumPlaylist> playlistalb;

        List<WISSongs> wisSongs;

        RefreshListener refreshListener;

        Animation  scaleUp;




        public PlaylistAlbumAdapter(Context context,
                                    List<WISPlaylistAlbum> worldpopulationlist, ChatIndivisualListener listener, RefreshListener refereshlisener) {
            mContext = context;
            this.data = worldpopulationlist;
            inflater = LayoutInflater.from(mContext);
            this.arraylist = new ArrayList<WISPlaylistAlbum>();
            this.arraylist.addAll(worldpopulationlist);
            this.listener=listener;
            this.refreshListener =refereshlisener;

            scaleUp = AnimationUtils.loadAnimation(context, R.anim.down_from_top);
        }



        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public WISPlaylistAlbum getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            ImageView ivMusic;
            TextView tvDate,name,duration;
            Button bDelete, bShare;
            LinearLayout shareLayout;
            CheckBox checkBox;

        }

        public View getView(final int position, View view, ViewGroup parent) {
            final ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.listview_item1, null);
                holder.ivMusic = (ImageView) view.findViewById(R.id.ivMusic);
                holder.bShare = (Button) view.findViewById(R.id.bShare);
                holder.shareLayout=(LinearLayout) view.findViewById(R.id.shareLayout);
                holder.tvDate = (TextView) view.findViewById(R.id.tvDate);
                holder.name = (TextView) view.findViewById(R.id.name);
                holder.duration =(TextView) view.findViewById(R.id.duration);
                holder.bDelete = (Button) view.findViewById(R.id.bDelete);
                holder.checkBox = (CheckBox) view.findViewById(R.id.selected);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.ivMusic.setImageResource(0);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            String date = "";
            try {
                newDate = format.parse(data.get(position).getCreated_at());
                format = new SimpleDateFormat("dd.MMM.yyyy");
                date = format.format(newDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("Formatted date====>"+ date);
            holder.tvDate.setText("Date : "+date);

//         set typeface

            try{
                font= Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
                holder.tvDate.setTypeface(font);
                holder.name.setTypeface(font);
                holder.duration.setTypeface(font);

            }catch (NullPointerException exce){
                System.out.println("Null pointer Exception" + exce.getMessage());
            }



            try {


                holder.name.setText("Titre : "+data.get(position).getName());



                try {
                    List<WISAlbum> wisalbums = playlistalbums.get(position).getAlbumList();

//                for(int i=0;i<wisalbums.size();i++)
////                {
////                    Log.e("PlaylistAlbumSize", String.valueOf(albumData.size()));
////
////                }
                    wisSongs = wisalbums.get(0).getListdata();

                    holder.duration.setText("Durée :"+wisSongs.get(0).getDuration());
                }
                catch (IndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                }

//
                playlist_date = date;


                holder.name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        LayoutInflater factory = LayoutInflater.from(mContext);
                        final View textEntryView = factory.inflate(R.layout.update_playlist, null);


                        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                        final EditText input1 = (EditText) textEntryView.findViewById(R.id.dateText);
                        final EditText input2 = (EditText) textEntryView.findViewById(R.id.titleText);

                        input1.setText(playlist_date, TextView.BufferType.EDITABLE);
                        input2.setText(data.get(position).getName(), TextView.BufferType.EDITABLE);

                        input1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                fromDatePickerDialog.show();
                            }
                        });

                        Calendar newCalendar = Calendar.getInstance();
                        fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);
                                input1.setText(dateFormatter.format(newDate.getTime()));
                            }

                        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


                        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                        alert.setIcon(R.drawable.wismusic).setTitle("Update Playlist:").setView(textEntryView).setPositiveButton("Save",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        if(input1.getText().toString().isEmpty() || input2.getText().toString().isEmpty())
                                        {
                                            Toast.makeText(mContext,"please fill the fields", Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            updatePlaylist(data.get(position).getId(),input1.getText().toString(),input2.getText().toString());
                                            //listener.playlistUpdate(data.get(position).getId(),input.getText().toString());
                                        }

                                        Log.i("AlertDialog","TextEntry 1 Entered "+input1.getText().toString());
                                        Log.i("AlertDialog","TextEntry 2 Entered "+input2.getText().toString());
    /* User clicked OK so do some stuff */
                                    }
                                }).setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
     /*
     * User clicked cancel so do some stuff
     */
                                    }
                                });
                        alert.show();
                    }
                });
                //holder.ivMusic.setImageResource(R.drawable.empty);
                try {
                    ApplicationController.getInstance().getImageLoader().get(mContext.getString(R.string.server_url3) + wisSongs.get(0).getThumb(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            holder.ivMusic.setImageResource(R.drawable.empty);
                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null) {
                                int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                                Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                                holder.ivMusic.setImageBitmap(scaled);
                            } else {
                                holder.ivMusic.setImageResource(R.drawable.empty);
                            }
                        }
                    });
                }
                catch (IndexOutOfBoundsException e)
                {
                    e.printStackTrace();
                }

                holder.bDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteDialog(position);
                    }
                });
                holder.bShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(wisSongs.get(0).getA()).concat("\n");
//
//                UserNotification.setMusicChatMessage(msg.concat(wisSongs.get(0).getPath()));
//                UserNotification.setSongThumb(wisSongs.get(0).getThumb());
                        listener.showPopUP(data.get(position).getId(), position,"album",null);
                    }
                });
//        view.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                List<WISPlaylist1> array = worldpopulationlist.get(position).getListdata().get(0).getListdata();
//                Log.i("songsData",String.valueOf(array));
//                new UserNotification().setPlayAlbum(array);
//
//
//                Intent intent = new Intent(mContext,Player.class);
//                intent.putExtra("type","playlist1");
//                intent.putExtra("songname",worldpopulationlist.get(position).getName());
//                intent.putExtra("songIcon",worldpopulationlist.get(position).getListdata().get(0).getListdata().get(0).getThumbnail());
//                intent.putExtra("artist",worldpopulationlist.get(position).getListdata().get(0).getAlbum_artists());
//                intent.putExtra("duration",worldpopulationlist.get(position).getListdata().get(0).getListdata().get(0).getDuration());
//                startActivity(intent);
//
////                    List<WISPlaylist1> array = worldpopulationlist.get(position).getListdata().get(0).getListdata();
////                    Log.i("songsData",String.valueOf(array));
////                    Uri uri = Uri.parse(worldpopulationlist.get(position).getListdata().get(0).getListdata().get(0).getAudio_path());
////                    new UserNotification().setPlayAlbum(array);
////                    new UserNotification().setSelected(position);
////                    Intent intent = new Intent(getActivity(),Player.class);
////                    intent .putExtra("uri_Str", uri);
////                    intent.putExtra("type","playlist1");
////                    intent.putExtra("songIndex",worldpopulationlist.get(position).getListdata().get(0).getListdata().get(0).toString());
////                    intent.putExtra("songname",worldpopulationlist.get(position).getName());
////                    //intent.putExtra("artist",worldpopulationlist.get(position).getListdata().get(0).getAlbum_artists());
////                    intent.putExtra("duration",worldpopulationlist.get(position).getListdata().get(0).getListdata().get(0).getDuration());
////                    startActivity(intent);
//            }
//        });

            }
            catch (ArrayIndexOutOfBoundsException ex){

                Log.e("index",ex.getLocalizedMessage());
            }

//            view.setAnimation(scaleUp);
//            view.startAnimation(scaleUp);

            return view;
        }

        private void updatePlaylist(int playlist_id, final String date, final String name) {
            JSONObject jsonBody = new JSONObject();

            try {
                jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
                jsonBody.put("playlist_id", String.valueOf(playlist_id));
                jsonBody.put("title",name);
                jsonBody.put("created_at",date);
                Log.i("playlist_id", String.valueOf(playlist_id));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i("playlist", String.valueOf(jsonBody));

            JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.update_playlist), jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("result")) {
                                    Log.i("updatePlaylist", String.valueOf(response));
                                    //refreshEvents();
                                    notifyDataSetChanged();
                                    new UserNotification().setUpdateplaylist(true);
                                    FragPlaylist fragment = new FragPlaylist();
                                    FragmentManager fragmentManager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                                }

                            } catch (JSONException e) {

                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //if (context != null)
                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("lang", Tools.getData(mContext, "lang_pr"));
                    headers.put("token", Tools.getData(mContext, "token"));
                    return headers;
                }
            };

            ApplicationController.getInstance().addToRequestQueue(reqDelete);
        }

        private void deleteDialog(final int position) {
            new AlertDialog.Builder(mContext)
                    .setMessage(mContext.getString(R.string.msg_confirm_delete_music))
                    .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteMusic(data.get(position).getId(), position,"playlist");
                        }

                    })
                    .setNegativeButton(mContext.getString(R.string.no), null)
                    .show();
            try{
                TextView textView = (TextView) dialog.findViewById(android.R.id.message);
                Typeface face=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
                Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                button1.setTypeface(face);
                Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                textView.setTypeface(face);
                button2.setTypeface(face);
                textView.setTextSize(20);//to change font size
                button1.setTextSize(20);
                button2.setTextSize(20);
            }catch (NullPointerException ex){
                Log.e("Exception",ex.getMessage());
            }

        }

        private void deleteMusic(int playlist_id, final int position,String type) {
            JSONObject jsonBody = new JSONObject();
            try {
                jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
                jsonBody.put("object_id", String.valueOf(playlist_id));
                jsonBody.put("type",type);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.delmusic_meth), jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("result")) {
                                    Log.i("deleteResponse", String.valueOf(response));

                                    try {
                                        notifyDataSetChanged();
                                        db.deletePlaylistAlbumId(data.get(position).getId());
                                        data.remove(position);

                                        refreshListener.refreshData();

                                        refereshPalylist();
                                    }
                                    catch (IndexOutOfBoundsException e)
                                    {

                                    }


                                }

                            } catch (JSONException e) {

                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //if (context != null)
                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("lang", Tools.getData(mContext, "lang_pr"));
                    headers.put("token", Tools.getData(mContext, "token"));
                    return headers;
                }
            };

            ApplicationController.getInstance().addToRequestQueue(reqDelete);
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            data.clear();
            if (charText.length() == 0) {
                data.addAll(arraylist);
            } else {
                for (WISPlaylistAlbum wp : arraylist) {
                    if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)||wp.getCreated_at().toLowerCase(Locale.getDefault()).contains(charText)) {
                        data.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }
    public class PlaylistAudioAdapter extends BaseAdapter {
        Context mContext;
        LayoutInflater inflater;
        private List<WISPlaylistSongs> data = null;
        private ArrayList<WISPlaylistSongs> arraylist;

        private SimpleDateFormat dateFormatter;
        String playlist_date;

        private DatePickerDialog fromDatePickerDialog;
        public  ChatIndivisualListener listener;

        DatabaseHandler db;
        List<WISMusic> songs;

        List<WISPlaylist> songarray;

        String duration;
        String thumbnail;

        RefreshListener refreshListener;

        Animation scaleUp;

        public PlaylistAudioAdapter(Context context,
                                    List<WISPlaylistSongs> data, ChatIndivisualListener listener, RefreshListener refereshlisener) {
            mContext = context;
            this.data = data;
            inflater = LayoutInflater.from(mContext);
            this.arraylist = new ArrayList<WISPlaylistSongs>();
            this.arraylist.addAll(data);
            this.listener=listener;
            this.refreshListener =refereshlisener;

            scaleUp = AnimationUtils.loadAnimation(context, R.anim.down_from_top);
        }



        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public WISPlaylistSongs getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {
            ImageView ivMusic;
            TextView tvDate,name,duration;
            Button bDelete, bShare;
            LinearLayout shareLayout;
            CheckBox checkBox;

        }

        public View getView(final int position, View view, ViewGroup parent) {
            final ViewHolder holder;
            db = new DatabaseHandler(mContext);
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.listview_item1, null);
                holder.ivMusic = (ImageView) view.findViewById(R.id.ivMusic);
                holder.bShare = (Button) view.findViewById(R.id.bShare);
                holder.shareLayout=(LinearLayout) view.findViewById(R.id.shareLayout);
                holder.tvDate = (TextView) view.findViewById(R.id.tvDate);
                holder.name = (TextView) view.findViewById(R.id.name);
                holder.duration =(TextView) view.findViewById(R.id.duration);
                holder.bDelete = (Button) view.findViewById(R.id.bDelete);
                holder.checkBox = (CheckBox) view.findViewById(R.id.selected);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.ivMusic.setImageResource(0);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            String date = "";
            try {
                newDate = format.parse(data.get(position).getCreated_at());
                format = new SimpleDateFormat("dd.MMM.yyyy");
                date = format.format(newDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("Formatted date====>"+ date);
            holder.tvDate.setText("Date : "+date);



            //         set typeface

            try{
                font= Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
                holder.tvDate.setTypeface(font);
                holder.name.setTypeface(font);
                holder.duration.setTypeface(font);

            }catch (NullPointerException exce){
                System.out.println("Null pointer Exception" + exce.getMessage());
            }


            playlist_date = date;

            try {
                holder.name.setText("Titre : " + data.get(position).getName());

                songs = data.get(position).getSongsList();

                Log.e("Song Data::=>", String.valueOf(songs) + " " + position);

                Log.e("Songs duration::", songs.get(0).getDuration());

                Log.e("Song thumb::", songs.get(0).getThumb());

                holder.duration.setText("Durée :" + songs.get(0).getDuration());
                //holder.duration.setText("Durée :"+data.get(position).getListdata().get(0).getDuration());

                holder.name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        LayoutInflater factory = LayoutInflater.from(mContext);
                        final View textEntryView = factory.inflate(R.layout.update_playlist, null);


                        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                        final EditText input1 = (EditText) textEntryView.findViewById(R.id.dateText);
                        final EditText input2 = (EditText) textEntryView.findViewById(R.id.titleText);

                        input1.setText(playlist_date, TextView.BufferType.EDITABLE);
                        input2.setText(data.get(position).getName(), TextView.BufferType.EDITABLE);

                        input1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                fromDatePickerDialog.show();
                            }
                        });

                        Calendar newCalendar = Calendar.getInstance();
                        fromDatePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);
                                input1.setText(dateFormatter.format(newDate.getTime()));
                            }

                        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


                        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                        alert.setIcon(R.drawable.wismusic).setTitle("Update Playlist:").setView(textEntryView).setPositiveButton("Save",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        if (input1.getText().toString().isEmpty() || input2.getText().toString().isEmpty()) {
                                            Toast.makeText(mContext, "please fill the fields", Toast.LENGTH_SHORT).show();
                                        } else {
                                            updatePlaylist(data.get(position).getId(), input1.getText().toString(), input2.getText().toString());
                                            //listener.playlistUpdate(data.get(position).getId(),input.getText().toString());
                                            db.updateSongsPlaylist(new WISPlaylistSongs(data.get(position).getId(), input1.getText().toString(), input2.getText().toString()));
                                        }

                                        Log.i("AlertDialog", "TextEntry 1 Entered " + input1.getText().toString());
                                        Log.i("AlertDialog", "TextEntry 2 Entered " + input2.getText().toString());
    /* User clicked OK so do some stuff */
                                    }
                                }).setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
     /*
     * User clicked cancel so do some stuff
     */
                                    }
                                });
                        alert.show();
                    }
                });
                //holder.ivMusic.setImageResource(R.drawable.empty);
//            data.get(position).getListdata().get(0).getThumbnail()
                ApplicationController.getInstance().getImageLoader().get(mContext.getString(R.string.server_url3) + songs.get(0).getThumb(), new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.ivMusic.setImageResource(R.drawable.empty);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                            holder.ivMusic.setImageBitmap(scaled);
                        } else {
                            holder.ivMusic.setImageResource(R.drawable.empty);
                        }
                    }
                });
                holder.bDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteDialog(position);
                    }
                });
                holder.bShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //showPopUP(worldpopulationlist.get(position).getListdata().get(0).getId(), position,"songs",worldpopulationlist.get(position).getListdata().get(0).getAudio_path());
//                    String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(data.get(position).getListdata().get(0).getArtists()).concat("\n");
//
//                    UserNotification.setMusicChatMessage(msg.concat(data.get(position).getListdata().get(0).getAudio_path()));
//                    UserNotification.setSongThumb(data.get(position).getListdata().get(0).getThumbnail());
//
//                    listener.showPopUP(data.get(position).getListdata().get(0).getId(), position,"songs",data.get(position).getListdata().get(0).getAudio_path());

                        String msg = "Name :".concat(data.get(position).getName()).concat("\n").concat("Artists :").concat(songs.get(0).getArtists()).concat("\n");

                        UserNotification.setMusicChatMessage(msg.concat(songs.get(0).getPath()));
                        UserNotification.setSongThumb(songs.get(0).getThumb());

                        listener.showPopUP(songs.get(0).getId(), position, "songs", songs.get(0).getPath());

                    }
                });
//        view.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//                List<WISPlaylist> array = worldpopulationlist.get(position).getListdata();
//                Log.i("songsData",String.valueOf(array));
//
//                new UserNotification().setPlaySongs(array);
//
//                //new UserNotification().setPlaySongs(songs);
//
//                Intent intent = new Intent(mContext,Player.class);
//                intent.putExtra("type","playlist");
//                intent.putExtra("songIndex",position);
//                intent.putExtra("songname",worldpopulationlist.get(position).getName());
//                //intent.putExtra("artist",songs.get(position).getArtists());
//                intent.putExtra("songIcon",worldpopulationlist.get(position).getListdata().get(0).getThumbnail());
//                intent.putExtra("duration",worldpopulationlist.get(position).getListdata().get(0).getDuration());
//                startActivity(intent);
//
////                    List<WISPlaylist> array = worldpopulationlist.get(position).getListdata();
////                    Log.i("songsData",String.valueOf(array));
////                    Uri uri = Uri.parse(worldpopulationlist.get(position).getListdata().get(0).getAudio_path());
////                    new UserNotification().setPlaySongs(array);
////                    new UserNotification().setSelected(position);
////                    //new UserNotification().setPlaySongs(songs);
////
////                    Intent intent = new Intent(getActivity(),Player.class);
////                    intent .putExtra("uri_Str", uri);
////                    intent.putExtra("type","playlist");
////                    intent.putExtra("songIndex",worldpopulationlist.get(position).getListdata().get(0).toString());
////                    intent.putExtra("songname",worldpopulationlist.get(position).getName());
////                    //intent.putExtra("artist",songs.get(position).getArtists());
////                    intent.putExtra("duration",worldpopulationlist.get(position).getListdata().get(0).getDuration());
////                    startActivity(intent);
//            }
//        });
//            }

            }
            catch(ArrayIndexOutOfBoundsException ex){

                Log.e("ex",ex.getLocalizedMessage());
            }

//            view.setAnimation(scaleUp);
//            view.startAnimation(scaleUp);

            return view;
        }

        private void updatePlaylist(int playlist_id, final String date, final String name) {
            JSONObject jsonBody = new JSONObject();

            try {
                jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
                jsonBody.put("playlist_id", String.valueOf(playlist_id));
                jsonBody.put("title",name);
                jsonBody.put("created_at",date);
                Log.i("playlist_id", String.valueOf(playlist_id));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i("playlist", String.valueOf(jsonBody));

            JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.update_playlist), jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("result")) {
                                    Log.i("updatePlaylist", String.valueOf(response));
                                    //refreshEvents();
                                    notifyDataSetChanged();
                                    new UserNotification().setUpdateplaylist(true);

                                    FragPlaylist fragment = new FragPlaylist();
                                    FragmentManager fragmentManager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();




                                }

                            } catch (JSONException e) {

                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //if (context != null)
                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("lang", Tools.getData(mContext, "lang_pr"));
                    headers.put("token", Tools.getData(mContext, "token"));
                    return headers;
                }
            };

            ApplicationController.getInstance().addToRequestQueue(reqDelete);
        }

        private void deleteDialog(final int position) {
            new AlertDialog.Builder(mContext)
                    .setMessage(mContext.getString(R.string.msg_confirm_delete_music))
                    .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteMusic(data.get(position).getId(), position,"playlist");


                        }

                    })
                    .setNegativeButton(mContext.getString(R.string.no), null)
                    .show();



            try{
                TextView textView = (TextView) dialog.findViewById(android.R.id.message);
                Typeface face=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
                Button button1 = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                button1.setTypeface(face);
                Button button2 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                textView.setTypeface(face);
                button2.setTypeface(face);
                textView.setTextSize(20);//to change font size
                button1.setTextSize(20);
                button2.setTextSize(20);
            }catch (NullPointerException ex){
                Log.e("Exception",ex.getMessage());
            }
        }

        private void deleteMusic(int playlist_id, final int position,String type) {
            JSONObject jsonBody = new JSONObject();

            try {
                jsonBody.put("user_id", Tools.getData(mContext, "idprofile"));
                jsonBody.put("object_id", String.valueOf(playlist_id));
                jsonBody.put("type",type);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest reqDelete = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.delmusic_meth), jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("result")) {
                                    Log.i("deleteResponse", String.valueOf(response));
                                    try {
                                        db.deletePlaylistSongId(data.get(position).getId());
                                        data.remove(position);
                                        notifyDataSetChanged();
                                        refreshListener.refreshData();
                                        refereshPalylist();
                                    }
                                    catch (IndexOutOfBoundsException e)
                                    {

                                    }

                                }

                            } catch (JSONException e) {

                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //if (context != null)
                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("lang", Tools.getData(mContext, "lang_pr"));
                    headers.put("token", Tools.getData(mContext, "token"));
                    return headers;
                }
            };

            ApplicationController.getInstance().addToRequestQueue(reqDelete);
        }

        // Filter Class
        public void filter1(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            data.clear();
            if (charText.length() == 0) {
                data.addAll(arraylist);
            } else {
                for (WISPlaylistSongs wp : arraylist) {
                    if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)||wp.getCreated_at().toLowerCase(Locale.getDefault()).contains(charText)) {
                        data.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        Log.i("calling","onResume");
//        if(songs.size()>0 || album.size()>0){
//            songs.clear();
//            album.clear();
//            Log.i("calling","clearData");
//            loadPlaylist();
//        }
//
//    }
}
