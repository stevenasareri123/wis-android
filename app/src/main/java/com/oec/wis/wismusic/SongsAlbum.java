package com.oec.wis.wismusic;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.oec.wis.R;

public class SongsAlbum extends Activity {
    /** Called when the activity is first created. */

    ImageView album_art;
    TextView album, artist, genre;

    MediaMetadataRetriever metaRetriver;
    byte[] art;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs_album);

        getInit();

//        String[] columns = { android.provider.MediaStore.Audio.Albums._ID,
//                android.provider.MediaStore.Audio.Albums.ALBUM };
//
//        Cursor cursor = managedQuery(
//                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, columns, null,
//                null, null);
//
//        if (cursor.moveToFirst()) {
//            do {
//                Log.v("Vipul",
//                        cursor.getString(cursor
//                                .getColumnIndex(android.provider.MediaStore.Audio.Albums.ALBUM)));
//            } while (cursor.moveToNext());
//        }
//
//        // I want to list down song in album Rolling Papers (Deluxe Version)
//
//        String[] column = { MediaStore.Audio.Media.DATA,
//                MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
//                MediaStore.Audio.Media.DISPLAY_NAME,
//                MediaStore.Audio.Media.MIME_TYPE, };
//
//        String where = android.provider.MediaStore.Audio.Media.ALBUM + "=?";
//
//        String whereVal[] = { "Rolling Papers (Deluxe Version)" };
//
//        String orderBy = android.provider.MediaStore.Audio.Media.TITLE;
//
//        cursor = managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
//                column, where, whereVal, orderBy);
//
//        if (cursor.moveToFirst()) {
//            do {
//                Log.v("Vipul",
//                        cursor.getString(cursor
//                                .getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)));
//            } while (cursor.moveToNext());
//        }

        metaRetriver = new MediaMetadataRetriever(); metaRetriver.setDataSource("/storage/sdcard0/SHAREit/audios/13. Emako [www.AtoZmp3.in].mp3"); try { art = metaRetriver.getEmbeddedPicture();
            Bitmap songImage = BitmapFactory.decodeByteArray(art, 0, art.length);
            album_art.setImageBitmap(songImage);
            album.setText(metaRetriver .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
            artist.setText(metaRetriver .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            genre.setText(metaRetriver .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE)); }
        catch (Exception e)
        { album_art.setBackgroundColor(Color.GRAY);
            album.setText("Unknown Album");
            artist.setText("Unknown Artist");
            genre.setText("Unknown Genre"); }



    }

    // Fetch Id's form xml
    public void getInit() { album_art = (ImageView) findViewById(R.id.album_art); album = (TextView) findViewById(R.id.Album); artist = (TextView) findViewById(R.id.artist_name); genre = (TextView) findViewById(R.id.genre); }

}
