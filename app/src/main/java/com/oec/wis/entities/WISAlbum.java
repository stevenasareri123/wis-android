package com.oec.wis.entities;

import java.util.List;

/**
 * Created by asareri12 on 08/11/16.
 */

public class WISAlbum {



    //album

    private String album_name;
    private int album_id;
    private String album_artists;
    private String dateTime;
    private List<WISSongs> listdata;
    private boolean box;

    //album
    public WISAlbum(String album_name, int album_id, String album_artists, String dateTime, List<WISSongs> listdata) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_artists=album_artists;
        this.dateTime = dateTime;
        this.listdata=listdata;
//        this.jsonArray=jsonArray;
    }
    public WISAlbum(String album_name, int album_id, String album_artists, String dateTime, List<WISSongs> listdata, boolean box) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_artists=album_artists;
        this.dateTime = dateTime;
        this.listdata=listdata;
        this.box = box;
//        this.jsonArray=jsonArray;
    }

    //album
    public WISAlbum(int album_id, String album_name, String album_artists, String dateTime) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_artists=album_artists;
        this.dateTime = dateTime;
    }

    public WISAlbum(){}





    public int getId() {
        return album_id;
    }

    public void setId(int album_id) {
        this.album_id = album_id;
    }

    public String getName() {
        return album_name;
    }

    public void setName(String album_name) {
        this.album_name = album_name;
    }


    public String getArtists()
    {
        return album_artists;

    }
    public void setArtists(String album_artists)
    {
        this.album_artists = album_artists;

    }

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public List<WISSongs> getListdata()
    {
        return listdata;
    }
    public void setListdata(List<WISSongs> listdata)
    {
        this.listdata=listdata;
    }


}
