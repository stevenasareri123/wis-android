package com.oec.wis.entities;

/**
 * Created by asareri12 on 14/11/16.
 */

public class WISSongs {

    //songs
    private int id;
    private String name;
    private String artists;
    private String duration;
    private String audio_path;
    private String audio_thumbnail;
    private String created_at;



    private int album_id;



    public WISSongs(int id, String name, String artists, String duration, String audio_path, String audio_thumbnail, String created_at) {
        this.id = id;
        this.name = name;
        this.artists=artists;
        this.duration = duration;
        this.audio_path=audio_path;
        this.audio_thumbnail=audio_thumbnail;
        this.created_at = created_at;
    }

    public WISSongs(int id, String name, String artists, String duration, String audio_path, String audio_thumbnail, String created_at, int album_id) {
        this.id = id;
        this.name = name;
        this.artists=artists;
        this.duration = duration;
        this.audio_path=audio_path;
        this.audio_thumbnail=audio_thumbnail;
        this.created_at = created_at;
        this.album_id=album_id;
    }

    public WISSongs(){}


    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id=id;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getA()
    {
        return artists;

    }
    public void setA(String artists)
    {
        this.artists = artists;

    }
    public int getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(int album_id) {
        this.album_id = album_id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


    public String getPath() {
        return audio_path;
    }

    public void setPath(String audio_path) {
        this.audio_path = audio_path;
    }

    public String getThumb() {
        return audio_thumbnail;
    }

    public void setThumb(String audio_thumbnail) {
        this.audio_thumbnail = audio_thumbnail;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

}
