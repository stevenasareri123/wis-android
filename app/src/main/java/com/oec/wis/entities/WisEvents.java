package com.oec.wis.entities;

/**
 * Created by asareri08 on 29/09/16.
 */
public class WisEvents {

//    $allEvents[$i]['eventDate']=$events->event_date;
//    $allEvents[$i]['eventTime']=$events->event_time;
//    $allEvents[$i]['comments'] = $events->comments;
//    $allEvents[$i]['created_at'] = $events->created_at;
//    $allEvents[$i]['status']

    private  int id;

    private String eventDate;

    private String eventTime;

    private String comments;

    private String  status;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WisEvents(int id, String eventDate, String eventTime, String comments, String status){

        this.id = id;

        this.eventDate= eventDate;

        this.eventTime = eventTime;

        this.comments = comments;

        this.status = status;

    }

}
