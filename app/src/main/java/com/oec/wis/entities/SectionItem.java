package com.oec.wis.entities;

/**
 * Created by asareri08 on 21/07/16.
 */
public class SectionItem implements Item{
    public String title;
    public SectionItem(String title) {
        this.title = title;
    }
    @Override
    public boolean isSection() {
        return true;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getfullName() {
        return null;
    }
}
