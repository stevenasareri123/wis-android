package com.oec.wis.entities;

/**
 * Created by asareri08 on 21/07/16.
 */
public interface Item {
    public boolean isSection();
    public String getTitle();
    public String getfullName();
}