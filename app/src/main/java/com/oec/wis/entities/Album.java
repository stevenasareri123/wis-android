package com.oec.wis.entities;

/**
 * Created by asareri12 on 14/11/16.
 */

public class Album {

    //songs
    private int album_id;



    private int song_id;


    public Album(){

    }
    public Album(int album_id, int song_id){
        this.album_id = album_id;
        this.song_id = song_id;
    }

    public int getSong_id() {
        return song_id;
    }

    public void setSong_id(int song_id) {
        this.song_id = song_id;
    }

    public int getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(int album_id) {
        this.album_id = album_id;
    }



}
