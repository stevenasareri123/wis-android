package com.oec.wis.entities;

/**
 * Created by asareri12 on 14/11/16.
 */

public class WISPlaylist {

    //songs
    private int id;
    private String title;
    private String artists;

    private String duration;
    private String audio_path;
    private String created_at;
    private String thumbnail;



    private String type;
    private int album_id;



    private int album;

    public WISPlaylist(){}

    //playlist songs
    public WISPlaylist(int id, String thumbnail, String title, String artists, String created_at, String audio_path, String duration, String type, int album) {
        this.id = id;
        this.title = title;
        this.artists=artists;
        this.duration = duration;
        this.audio_path=audio_path;
        this.thumbnail=thumbnail;
        this.created_at = created_at;
        this.type=type;
//        this.album_id=album_id;
        this.album=album;

    }
    public int getAlbum() {
        return album;
    }

    public void setAlbum(int album) {
        this.album = album;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return title;
    }

    public void setName(String name) {
        this.title = name;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAudio_path() {
        return audio_path;
    }

    public void setAudio_path(String audio_path) {
        this.audio_path = audio_path;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(int album_id) {
        this.album_id = album_id;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
