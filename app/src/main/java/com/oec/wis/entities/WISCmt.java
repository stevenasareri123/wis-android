package com.oec.wis.entities;

public class WISCmt {
    private int id;
    private WISUser user;
    private String cmt;
    private String date;
    private String commentview_type;



    public WISCmt(int id, WISUser user, String cmt, String date, String commentview_type) {
        this.id = id;
        this.user = user;
        this.cmt = cmt;

        this.date = date;
        this.commentview_type = commentview_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WISUser getUser() {
        return user;
    }

    public void setUser(WISUser user) {
        this.user = user;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCommentview_type() {
        return commentview_type;
    }

    public void setCommentview_type(String commentview_type) {
        this.commentview_type = commentview_type;
    }
}
