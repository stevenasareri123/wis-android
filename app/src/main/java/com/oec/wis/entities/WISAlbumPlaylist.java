package com.oec.wis.entities;

import java.util.List;

/**
 * Created by asareri12 on 22/11/16.
 */


public class WISAlbumPlaylist {



    private String album_name;
    private int album_id;
    private String album_artists;




    private String created_at;

    private List<WISPlaylist1> listdata;

    public List<WISSongs> songsdata;



    private int playlist_id;

    public WISAlbumPlaylist(){}

    public WISAlbumPlaylist(int playlist_id, String album_name, int album_id, String album_artists, String created_at, List<WISPlaylist1> listdata) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_artists=album_artists;
        this.created_at = created_at;
        this.listdata = listdata;
        this.playlist_id = playlist_id;
    }
    public WISAlbumPlaylist(int album_id, String album_name, String album_artists, String created_at, int playlist_id) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_artists=album_artists;
        this.created_at = created_at;
        this.playlist_id = playlist_id;
    }

    public List<WISSongs> getSongsdata() {
        return songsdata;
    }

    public void setSongsdata(List<WISSongs> songsdata) {
        this.songsdata = songsdata;
    }

    public WISAlbumPlaylist(List<WISSongs> songsdata, String album_name, int album_id, String album_artists, String created_at) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_artists=album_artists;

        this.created_at = created_at;
        this.songsdata = songsdata;
    }

    public int getPlaylist_id() {
        return playlist_id;
    }

    public void setPlaylist_id(int playlist_id) {
        this.playlist_id = playlist_id;
    }


    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public int getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(int album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_artists() {
        return album_artists;
    }

    public void setAlbum_artists(String album_artists) {
        this.album_artists = album_artists;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<WISPlaylist1> getListdata() {
        return listdata;
    }

    public void setListdata(List<WISPlaylist1> listdata) {
        this.listdata = listdata;
    }


}

