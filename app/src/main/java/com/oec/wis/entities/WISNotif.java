package com.oec.wis.entities;

public class WISNotif {
    private int id;
    private String title;
    private String desc;
    private String dateTime;
    private String photo;

    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public WISNotif(int id, String title, String desc, String dateTime, String photo, String type) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.dateTime = dateTime;
        this.photo = photo;
        this.type = type;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
