package com.oec.wis.entities;

public class WISVideo {
    private int id;
    private String title;
    private String path;
    private String dateTime;
    private boolean selected = false;
    private String thumb;
    private String chat_type;

    public WISVideo(int id, String title, String path, String dateTime, String thumb) {
        this.id = id;
        this.title = title;
        this.dateTime = dateTime;
        this.path = path;
        this.thumb = thumb;
    }

    public String getChat_type() {
        return chat_type;
    }

    public void setChat_type(String chat_type) {
        this.chat_type = chat_type;
    }

    public WISVideo(int id, String title, String path, String dateTime, String thumb, String chat_type) {
        this.id = id;
        this.title = title;
        this.dateTime = dateTime;
        this.path = path;

        this.thumb = thumb;
        this.chat_type = chat_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
