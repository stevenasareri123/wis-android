package com.oec.wis.entities;

import java.util.List;

public class WISActuality {
    private int id;
    private String title;
    private String desc;
    private String img;
    private int nLike;
    private int nView;
    private boolean iLike;
    private String pType;
    private List<WISCmt> cmts;
    private String video;
    private String cmtActu;
    private String date;
    private String thumb;
    private WISUser creator;
    private int comment_count;
    private int dislike_count;
    private int share_count;
    private boolean isdislike;
    private int isHide;
    private List<WISUser> likedUser;
    private List<WISUser> dislikedUser;
    private List<WISUser> commentsUser;
    private List<WISUser> viewedUser;

    private String audio;



    private String thumbnail;
    public int isHide() {
        return isHide;
    }

    public void setHide(int hide) {
        isHide = hide;
    }


    public List<WISUser> getLikedUser() {
        return likedUser;
    }

    public void setLikedUser(List<WISUser> likedUser) {
        this.likedUser = likedUser;
    }

    public List<WISUser> getDislikedUser() {
        return dislikedUser;
    }

    public void setDislikedUser(List<WISUser> dislikedUser) {
        this.dislikedUser = dislikedUser;
    }

    public List<WISUser> getCommentsUser() {
        return commentsUser;
    }

    public void setCommentsUser(List<WISUser> commentsUser) {
        this.commentsUser = commentsUser;
    }

    public List<WISUser> getViewedUser() {
        return viewedUser;
    }

    public void setViewedUser(List<WISUser> viewedUser) {
        this.viewedUser = viewedUser;
    }



    public WISActuality(int isHide, int id, String title, String desc, String img,String audio,String thumbnail,String video, String thumb, int nLike, int nView, boolean iLike, String pType,List<WISCmt> cmts, String cmtActu, String date, WISUser creator, int dislike_count, boolean isdislike, int sharecount, int comment_count, List<WISUser> likedUser, List<WISUser> dislikedUser, List<WISUser>commentsUser, List<WISUser>viewedUser) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.img = img;
        this.nLike = nLike;
        this.nView = nView;
        this.iLike = iLike;
        this.pType = pType;
        this.audio=audio;
        this.thumbnail=thumbnail;
        this.cmts = cmts;
        this.video = video;
        this.cmtActu = cmtActu;
        this.date = date;
        this.thumb = thumb;
        this.creator = creator;
        this.comment_count=comment_count;
        this.isdislike=isdislike;
        this.share_count = sharecount;
        this.dislike_count=dislike_count;
        this.isHide = isHide;

        this.likedUser = likedUser;
        this.dislikedUser = dislikedUser;
        this.commentsUser = commentsUser;
        this.viewedUser = viewedUser;

    }

    public WISActuality(int isHide, int id, String title, String desc, String img, String video, String thumb, int nLike, int nView, boolean iLike, String pType, List<WISCmt> cmts, String cmtActu, String date, WISUser creator, int dislike_count, boolean isdislike, int sharecount, int comment_count, List<WISUser> likedUser, List<WISUser> dislikedUser) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.img = img;
        this.nLike = nLike;
        this.nView = nView;
        this.iLike = iLike;
        this.pType = pType;



        this.cmts = cmts;
        this.video = video;
        this.cmtActu = cmtActu;
        this.date = date;
        this.thumb = thumb;
        this.creator = creator;
        this.comment_count=comment_count;
        this.isdislike=isdislike;
        this.share_count = sharecount;
        this.dislike_count=dislike_count;
        this.isHide = isHide;
        this.likedUser = likedUser;
        this.dislikedUser = dislikedUser;

    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public int getDislike_count() {
        return dislike_count;
    }

    public void setDislike_count(int dislike_count) {
        this.dislike_count = dislike_count;
    }

    public int getShare_count() {
        return share_count;
    }

    public void setShare_count(int share_count) {
        this.share_count = share_count;
    }

    public boolean isdislike() {
        return isdislike;
    }

    public void setIsdislike(boolean isdislike) {
        this.isdislike = isdislike;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getnLike() {
        return nLike;
    }

    public void setnLike(int nLike) {
        this.nLike = nLike;
    }

    public int getnView() {
        return nView;
    }

    public void setnView(int nView) {
        this.nView = nView;
    }

    public boolean isiLike() {
        return iLike;
    }

    public void setiLike(boolean iLike) {
        this.iLike = iLike;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public List<WISCmt> getCmts() {
        return cmts;
    }

    public void setCmts(List<WISCmt> cmts) {
        this.cmts = cmts;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getCmtActu() {
        return cmtActu;
    }

    public void setCmtActu(String cmtActu) {
        this.cmtActu = cmtActu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public WISUser getCreator() {
        return creator;
    }

    public void setCreator(WISUser creator) {
        this.creator = creator;
    }

    public String getAudio()
    {
        return audio;
    }
    public void setAudio(String audio)
    {
        this.audio=audio;
    }
}
