package com.oec.wis.entities;

/**
 * Created by asareri12 on 08/11/16.
 */

public class WISSearchs {


    private int id;
    private String name;
    private String artists;
    private String duration;
    private String audio_path;
    private String audio_thumbnail;
    private String created_at;
    private boolean box;


    public WISSearchs(int id, String name, String artists, String duration, String audio_path, String audio_thumbnail, String created_at) {
        this.id=id;
        this.name = name;
        this.artists=artists;
        this.duration=duration;
        this.audio_path=audio_path;
        this.audio_thumbnail = audio_thumbnail;
        this.created_at = created_at;
    }

    public WISSearchs(int id, String name, String artists, String duration, String audio_path, String audio_thumbnail, String created_at, boolean box) {
        this.id=id;
        this.name = name;
        this.artists=artists;
        this.duration=duration;
        this.audio_path=audio_path;
        this.audio_thumbnail = audio_thumbnail;
        this.created_at = created_at;
        this.box = box;
    }

    public WISSearchs(int id, String name, String audio_path, String audio_thumbnail, String created_at) {
        this.id=id;
        this.name = name;
        this.audio_path=audio_path;
        this.audio_thumbnail = audio_thumbnail;
        this.created_at = created_at;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getArtists()
    {
        return artists;

    }
    public void setArtists(String artists)
    {
        this.artists = artists;

    }

    public String getDuration()
    {
        return duration;

    }
    public void setDuration(String duration)
    {
        this.duration = duration;

    }

    public boolean isBox() {
        return box;
    }

    public void setBox(boolean box) {
        this.box = box;
    }

    public String getPath() {
        return audio_path;
    }

    public void setPath(String audio_path) {
        this.audio_path = audio_path;
    }

    public String getThumb() {
        return audio_thumbnail;
    }

    public void setThumb(String audio_thumbnail) {
        this.audio_thumbnail = audio_thumbnail;
    }



    public String getDateTime() {
        return created_at;
    }

    public void setDateTime(String created_at) {
        this.created_at = created_at;
    }

}
