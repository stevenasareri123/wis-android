package com.oec.wis;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {
    EditText etEmail, etPwd;
    Button bLogin,subscribe;
    TextView headerTitleTV,tvForgot;
    Typeface font;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loadControls();
        loadListener();
        setTypeFace();
    }

    private void loadControls() {
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPwd = (EditText) findViewById(R.id.etPwd);
        bLogin = (Button) findViewById(R.id.b_login);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
        tvForgot= (TextView)findViewById(R.id.tvForgot);
        subscribe=(Button)findViewById(R.id.subscribe);

    }


    private void setTypeFace() {
        Typeface font = Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
        etEmail.setTypeface(font);
        etPwd.setTypeface(font);
        bLogin.setTypeface(font);
        subscribe.setTypeface(font);
        tvForgot.setTypeface(font);
    }


    private void loadListener() {
        findViewById(R.id.subscribe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Subscribe.class));
            }
        });
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validation
                if (checkData()) {
                    doLogin();
                }
                //startActivity(new Intent(getApplicationContext(), Dashboard.class));
                //finish();
            }
        });

        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RecoverPwd.class));
            }
        });
    }
    //Validations
    private boolean checkData() {
        boolean check = true;
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            check = false;
            etEmail.setError(getString(R.string.msg_empty_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            check = false;
            etEmail.setError(getString(R.string.msg_invalid_email));
        }
        if (TextUtils.isEmpty(etPwd.getText().toString())) {
            check = false;
            etPwd.setError(getString(R.string.msg_empty_pwd));
        }
        return check;
    }

    // Login check
    private void doLogin() {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);

//        Tools.showLoader(Login.this,getResources().getString(R.string.progress_loading));
        disableControls();
        JSONObject jsonBody = null;

        try {
            String json = "{\"email\":\"" + etEmail.getText().toString() + "\",\"password\":\"" + etPwd.getText().toString() + "\",\"id_gcm\":\"" + Tools.getData(Login.this, "regid") + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
       // Log.i("url",getString(R.string.server_url)+getString(R.string.auth_meth));

        JsonObjectRequest reqLogin = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.auth_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                JSONObject data = response.getJSONObject("data");
                                saveUData(data);

                                startActivity(new Intent(getApplicationContext(), Dashboard.class));
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.msg_invalid_email_pwd), Toast.LENGTH_SHORT).show();
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);
//                            Tools.dismissLoader();
                            enableControls();
                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
//                            Tools.dismissLoader();
                            enableControls();
                            //Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Tools.dismissLoader();
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                enableControls();
            }
        });
        ApplicationController.getInstance().addToRequestQueue(reqLogin);
    }

    private void saveUData(JSONObject data) throws JSONException {
        Tools.saveData(getApplicationContext(), "firstname_prt", data.getString("firstname_prt"));
        Tools.saveData(getApplicationContext(), "lastname_prt", data.getString("lastname_prt"));
        Tools.saveData(getApplicationContext(), "name", data.getString("name"));
        Tools.saveData(getApplicationContext(), "token", data.getString("token"));
        Tools.saveData(getApplicationContext(), "photo", data.getString("photo"));
        Tools.saveData(getApplicationContext(), "last_connected", data.getString("last_connected"));
        Tools.saveData(getApplicationContext(), "idprofile", data.getString("idprofile"));
        Tools.saveData(getApplicationContext(), "sexe_prt", data.getString("sexe_prt"));
        try {
            String lang = data.getString("lang_pr").substring(0, data.getString("lang_pr").indexOf("_"));
            Tools.saveData(getApplicationContext(), "lang_pr", lang);
        } catch (Exception e) {
        }

        Tools.saveData(getApplicationContext(), "tel_pr", data.getString("tel_pr"));
        Tools.saveData(getApplicationContext(), "profiletype", data.getString("profiletype"));
        Tools.saveData(getApplicationContext(), "code_ape_etr", data.getString("code_ape_etr"));
        Tools.saveData(getApplicationContext(), "email_pr", data.getString("email_pr"));
        Tools.saveData(getApplicationContext(), "special_other", data.getString("special_other"));
        Tools.saveData(getApplicationContext(), "place_addre", data.getString("place_addre"));
        Tools.saveData(getApplicationContext(), "activite", data.getString("activite"));
        Tools.saveData(getApplicationContext(), "registered", data.getString("registered"));
        Tools.saveData(getApplicationContext(), "date_birth", data.getString("date_birth"));
        Tools.saveData(getApplicationContext(), "country", data.getString("country"));
        Tools.saveData(getApplicationContext(), "state", data.getString("state"));
        Tools.saveData(getApplicationContext(), "pwd", etPwd.getText().toString());
        Tools.saveData(getApplicationContext(), "touriste", data.getString("touriste"));
        Tools.saveData(getApplicationContext(), "name_representant_etr", data.getString("name_representant_etr"));

//        String CurrentString = spTimezone.getSelectedItem().toString();
//        String[] timezone_values = CurrentString.split(" ");

        Log.i("timezone",data.getString("time_zone"));
        Log.i("format",data.getString("time_zone_format"));

        Tools.saveData(getApplicationContext(),"timezone",data.getString("time_zone"));
        Tools.saveData(getApplicationContext(),"timezone_format",data.getString("time_zone_format"));

    }

    private void disableControls() {
        etEmail.setEnabled(false);
        etPwd.setEnabled(false);
        bLogin.setEnabled(false);
    }

    private void enableControls() {
        etEmail.setEnabled(true);
        etPwd.setEnabled(true);
        bLogin.setEnabled(true);
    }

@Override
    public  void onBackPressed(){
    moveTaskToBack(true);
}
}
